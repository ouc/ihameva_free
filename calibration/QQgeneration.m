function [QHsR,QHsI,QDir,maxHsR] = QQgeneration(HsR,HsI,DirR,inctheta,nq)
%   La funcion QQGENERACION calcula los quantiles que hay que calibrar teniendo en
%   cuenta un sector direccional movil de ventana inctheta y que se generan
%   nq quantiles equiespaciados
%
%   Datos:
%       HsR -> Alturas de ola de reanalisis a calibrar
%       HsI -> Valores de satelite/boya (Instrumental) de referencia para la
%       calibracion
%       DirR -> Direcciones de reanalisis empleadas para calibrar (en radianes)
%       nq -> Numero de cuantiles por sector direccional que se van a utilizar
%       increthet -> Anchura del sector direccional considerado en grados

%   Calculo de dimension de los datos
n = length(HsR);

%   Ordeno todos los datos de menor a mayor angulo direccional
[DirR,IX] = sort(DirR);
HsR = HsR(IX);
HsI = HsI(IX);

%   Paso todo a grados
DirR = DirR*360/(2*pi);

%   El numero de puntos generados es
nd = nq*(360);

%   Reservo espacio en memoria para los quantiles almacenados
QHsR = zeros(nd,1);
QHsI = zeros(nd,1);
QDir = zeros(nd,1);

nmin = min(4*nq,ceil(10*n/100)); %numero de datos minimos
% nmin = 2*nq;

%   Maximo valor de HS de reanalisis en cada direccion
maxHsR = zeros(360,1);

%   Recorro cada sector direccional grado a grado y calculo los quantiles
aviso = 0;
acum = 0;
iniin = []; %
finin = [];
for i = 0:360-1,
    % Selecct point in each directional sector
    [lista,np] = selectDirSector(DirR,i,inctheta);
    
    %   El numero de datos ha de ser mayor que el numero de cuantiles para
    %   ese sector direccional
    if np > nmin && np > 5,
        %   Calculo los quantiles que quiero calibrar
        a = -log(-log(1/np));
        b = -log(-log(1 - 5/np));
        x = linspace(a,b,nq);
        q = exp(-exp(-x));
        q(end) = 1;
        
        if acum>0,
            if QHsI(acum) == 0 && QHsR(acum) == 0,
                finin = [finin; acum];
            end
        end
        QHsR (acum+1:acum+nq) = quantile(HsR(lista),q);
        QHsI (acum+1:acum+nq) = quantile(HsI(lista),q);
        QDir (acum+1:acum+nq) = ones(nq,1)*(inctheta+2*i)/2;
        maxHsR(i+1)=max(HsR(lista));
        %         asd=length(find(QHsI (acum+1:acum+nq)==QHsI (acum+nq)));
        %         if asd>1,
        %             disp('Numero de cuantiles repetidos')
        %             asd
        %         end
    else
        %         QHsI (acum+1:acum+nq) = ones(nq,1);
        %         QHsR (acum+1:acum+nq) = ones(nq,1);
        if acum ==0,
            aviso = 1;
        end
        QDir (acum+1:acum+nq) = ones(nq,1)*(inctheta+2*i)/2;
        if acum>0,
            if QHsI (acum)~=0 && QHsR (acum)~=0,
                iniin = [iniin; acum+1];
            end
        end
    end
    acum = acum + nq;
end

if length(finin)+1 == length(iniin),
    finin = [finin; nd];
end
%   Relleno todos los sectores que no tiene ningun dato interpolandolos
%   linealmente con los de los extremos
if aviso,
    if ~isempty(iniin),
        iniin = [iniin(end); iniin(1:end-1)] ;
    else
        iniin = nd-nq+1;
    end
end
if ~isempty(iniin),
    for k = 1:length(iniin),
        if finin(k)>iniin(k),
            num = (finin(k)-iniin(k)+1)/nq;
            for j = 1:num,
                limup = finin(k)+1:finin(k)+nq;
                idup = find(limup>nd);
                if ~isempty(idup),
                    limup(idup) = limup(idup)-nd;
                end
                QHsI (iniin(k)+(j-1)*nq:iniin(k)+j*nq-1) = QHsI (iniin(k)-nq:iniin(k)-1)+j*(QHsI (limup)-QHsI (iniin(k)-nq:iniin(k)-1))/num;
                QHsR (iniin(k)+(j-1)*nq:iniin(k)+j*nq-1) = QHsR (iniin(k)-nq:iniin(k)-1)+j*(QHsR (limup)-QHsR (iniin(k)-nq:iniin(k)-1))/num;
            end
        else
            %   Se intercambian finin e iniin
            num = (finin(k)+nd-iniin(k)+1)/nq;
            for j = 1:num,
                if iniin(k)+j*nq-1==nd,
                    disp ('f')
                end
                ind = mod(iniin(k)+(j-1)*nq:iniin(k)+j*nq-1,nd);
                if iniin(k)+j*nq-1==nd,
                    ind(end)=nd;
                end
                QHsI (ind) = QHsI (iniin(k)-nq:iniin(k)-1)+j*(QHsI (finin(k)+1:finin(k)+nq)-QHsI (iniin(k)-nq:iniin(k)-1))/num;
                QHsR (ind) = QHsR (iniin(k)-nq:iniin(k)-1)+j*(QHsR (finin(k)+1:finin(k)+nq)-QHsR (iniin(k)-nq:iniin(k)-1))/num;
            end
        end
    end
end

lista = find(QDir>360);
QDir(lista) = QDir(lista)-360;

%    Selecciono los puntos que estan en este sector direccional
    function [lista,np] = selectDirSector(Dir,iD,incTheta)
        limUpper = incTheta+iD;
        if limUpper > 360,
            limUpper = limUpper-360;
            lista = find(Dir>=iD | Dir<limUpper);
        else
            lista = find(Dir>=iD & Dir<limUpper);
        end
        np = length(lista);
    end

end