function Pinta_felpudo(aj,bj,thetaj,np,HNC,Hlim,var_unit)

thetaplot = (0:1:360)*pi/180;
deriv = ((aj(2)-aj(1))/(thetaj(2)-thetaj(1))+(aj(np)-aj(np-1))/(thetaj(np)-thetaj(np-1)))/2;
B = spline(thetaj,[deriv; aj; deriv],thetaplot);
deriv = ((bj(2)-bj(1))/(thetaj(2)-thetaj(1))+(bj(np)-bj(np-1))/(thetaj(np)-thetaj(np-1)))/2;
C = spline(thetaj,[deriv; bj; deriv],thetaplot);


Theta=(0:1:360);
Theta=90-Theta;
nnn=length(0:0.25:ceil(2*max(max(HNC)))/2);
h_nc=ones(length(Theta),nnn)*NaN;
h_c=ones(length(Theta),nnn)*NaN;
alfa=ones(length(Theta),nnn);
for J=1:length(Theta)
    hhh=0:0.25:ceil(2*max(HNC(J,find(not(isnan(HNC(J,:)))))))/2;
    h_nc(J,1:length(hhh))=hhh;
    h_c(J,1:length(hhh))=B(J).*h_nc(J,1:length(hhh)).^C(J);
    alfa(J,1:length(hhh))=h_c(J,1:length(hhh))./h_nc(J,1:length(hhh));
end
alfa(:,1)=1;
R=(0:0.25:ceil(2*max(max(HNC)))/2);
X=zeros(length(Theta),length(R));
Y=zeros(length(Theta),length(R));
for I=1:length(R)
    for J=1:length(Theta)
        X(J,I)=R(I)*cos(Theta(J)*2*pi/360);
        Y(J,I)=R(I)*sin(Theta(J)*2*pi/360);
    end
end

nAngles=12;
usarlog=0;
roseaxislabel;

q=pcolor(X,-Y,alfa);
shading interp

indiceblanco=double(int8(128/2));
for i=1:indiceblanco
    r(i)=(i-1)/indiceblanco;
    g(i)=(i-1)/indiceblanco;
    b(i)=1;
    map(i,:)=[r(i) g(i) b(i)];
end
for i=1+indiceblanco:128
    r(i)=1;
    g(i)=(128-i)/(128-indiceblanco-1);
    b(i)=(128-i)/(128-indiceblanco-1);
    map(i,:)=[r(i) g(i) b(i)];
end

caxis([0.6 1.4]);
colormap(map);
colorbar('location','southoutside','position',[0.58 0.06 0.14 0.02],'fontsize',7,'XTick',[0.6:0.2:1.4])
set(gca,'dataaspectratio',[1 1 1]), axis off;
set(cax,'NextPlot',next);
set(get(gca,'xlabel'),'visible','on');
set(get(gca,'ylabel'),'visible','on');
axis ij;

function roseaxislabel
    % get hold state
    cax = newplot;
    next = lower(get(cax,'NextPlot'));
    % get x-axis text color so grid is in same color
    tc = get(cax,'xcolor');
    ls = get(cax,'gridlinestyle');
    % Hold on to current Text defaults, reset them to the
    % Axes' font attributes so tick marks use them.
    set(cax, 'DefaultTextFontAngle',  get(cax, 'FontAngle'), ...
    'DefaultTextFontName',   get(cax, 'FontName'), ...
    'DefaultTextFontSize',   get(cax, 'FontSize'), ...
    'DefaultTextFontWeight', get(cax, 'FontWeight'), ...
    'DefaultTextUnits','data')

    hold on;                                                            
    hkk=plot(R);                                                     
    set(gca,'dataaspectratio',[1 1 1],'plotboxaspectratiomode','auto')  
    vkk = get(gca,'ylim');                                              
    rlabshift=vkk(1);                                                   
    delete(hkk);  
    hhh=plot(R+rlabshift);
    set(cax,'dataaspectratio',[1 1 1],'plotboxaspectratiomode','auto')
    set(cax,'ylim',Hlim)
    v = [get(cax,'xlim') get(cax,'ylim')];
    ticks = length(get(cax,'ytick'));%     ticks = sum(get(cax,'ytick')>=0);
    delete(hhh);
    rmax = min(max(v(4)-rlabshift,vkk(2)),Hlim(2)); rticks = max(ticks-1,4);
    rmin = max(0,min(v(3)-rlabshift,vkk(1)));
    if rticks > 6   % see if we can reduce the number
        if rem(rticks,2) == 0
            rticks = rticks/2;
        elseif rem(rticks,3) == 0
            rticks = rticks/3;
        end
    end
    
    % define a circle
    th = 0:pi/50:2*pi;
    xunit = cos(th);
    yunit = sin(th);
    % now really force points on x/y axes to lie on them exactly
    inds = 1:(length(th)-1)/4:length(th);
    xunit(inds(2:2:4)) = zeros(2,1);
    yunit(inds(1:2:5)) = zeros(3,1);
    % draw radial circles
    c82 = cos(82*pi/180);
    s82 = sin(82*pi/180);
    rinc = (rmax-rmin)/rticks;
    cnt = 1;
    if usarlog, 
        for i=(rmin+rinc):rinc:rmax
            hhh = plot(xunit*log(i),yunit*log(i),ls,'color',tc,'linewidth',1,'handlevisibility','off');
            text(xunit(24)*log(i),-yunit(24)*log(i),['' num2str(floor(i+rlabshift)) var_unit], ... % Yo
                'verticalalignment','bottom','handlevisibility','off','FontSize',7);                                      % Yo
            %if (i+rlabshift-1)==0, set(hhh,'linestyle','-'); end
            cnt = cnt+1;
        end
    else
        for i=(rmin+rinc):rinc:rmax
            hhh = plot(xunit*i,yunit*i,ls,'color',tc,'linewidth',1,'handlevisibility','off');
            text((i+rinc/20)*c82,-(i+rinc/20)*s82,['' num2str(i+rlabshift) var_unit], ... % Yo
                'verticalalignment','bottom','handlevisibility','off','FontSize',7);                                      % Yo
            %if (i+rlabshift-1)==0, set(hhh,'linestyle','-'); end
            cnt = cnt+1;
        end
    end
    set(hhh,'linestyle','-','color',tc); % Make outer circle solid

%plot spokes. oc he anadido las lines de 45/2 en 45/2 como  
    if usarlog, rmax=log(rmax); end
    labelrotate=90;
    dnA=360/nAngles;
    nAp=round(nAngles/2);
    th = (1:nAp)*2*pi/nAngles;
    cst = cos(th); snt = sin(th);
    plot(repmat([rmin rmax],nAp,1)'.*repmat(cst,2,1),repmat([rmin rmax],nAp,1)'.*repmat(snt,2,1),':','color',tc,'linewidth',1,'handlevisibility','off');
    plot(repmat([rmin rmax],nAp,1)'.*repmat(-cst,2,1),repmat([rmin rmax],nAp,1)'.*repmat(-snt,2,1),':','color',tc,'linewidth',1,'handlevisibility','off');

    rt = 1.15*rmax;% annotate spokes in degrees
    for i = 1:length(th)
        angTextNum = (i*dnA)+labelrotate    ;
        if angTextNum<0, angTextNum=angTextNum+360; end
        text(rt*cst(i),rt*snt(i),[int2str(angTextNum) char(176)],...
             'horizontalalignment','center',...
             'handlevisibility','off','fontsize',8);%,'fontweight','bold')
        if i == length(th)
            loc = int2str(0+labelrotate);
        else
            angTextNum = 180+(i*dnA)+labelrotate    ;
            if angTextNum > 180, angTextNum = angTextNum - 360; end
            loc = int2str(angTextNum);
        end
        if str2num(loc)<0, loc=num2str(str2num(loc)+360); end
        text(-rt*cst(i),-rt*snt(i),[loc char(176)],'horizontalalignment','center',...
             'handlevisibility','off','fontsize',8);%,'fontweight','bold')
    end
    view(2);% set view to 2-D
    axis(rmax*[-1.1 1.1 -1.2 1.2]);% set axis limits
end


end
