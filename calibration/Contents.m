% CALIBRATION
%
% Files
%   arc2                           - arc2(r,az1, az2)
%   arcq                           - arc(r,az1, az2)
%   bias                           - 

%   CalibraSplineNLP               - La funcion CalibraSplineNLP 
%   CalibraSplineParam             - 
%   CalibraSplineQuantile          - La funcion CALIBRASPLINEQUEANTILE permite el calibrado direccional
%   calibration                    - tool.

%   cbhandle                       - Handle of current colorbar axes.
%   centax2                        - CENTAXES can be used to change the origin of a 2-D axes
%   coefCORR                       - 
%   correlaciones                  - 
%   DetClas2N                      - P1=0.1;
%   FigICParams                    - 
%   fobjNLP                        - 

%   GraficosAdicionales            - Los angulos deben estar en grados

%   InsReaCalPlot                  - 
%   IntepLineal                    - *********************************************************************************
%   lineaABC                       - 
%   niveles                        - 
%   normaliza                      - 
%   optiparamhessiancal            - Initial bounds and possible initial values for the constant parameters
%   outlierfilterRNoCte            - Funtion outlierfilterRNoCte automatically detects the presence of
%   ParamTable                     - 
%   ParamTableL                    - 
%   Pinta_felpudo                  - 
%   pinta_rosa_prob                - Se usa por medio de PintaRosasProbabilisticas.m

%   PintaRosasProbabilisticas      - Examples
%   PlotEmpiricalDist              - 
%   polargeo                       - Polar geographic coordinate plot.
%   polargeo_markercolor           - POLARGEO  Polar geographic coordinate plot.
%   polarLabels_IC_modOrigenSombra - 
%   QQgeneration                   - La funcion QQGENERACION calcula los quantiles que hay que calibrar teniendo en



%   rms                            - 
%   rsi                            - 

%   ScatterQQ                      - Scater Reanalisis y Calibrado green and red para la ficha de calibraiton




%   TransCarte                     - 
%   uplownewcal                    - 
%   worldfilter                    - WORLDFILTER. Outlier filter

%   QuanDir                        - tipo: R-reanalisis, I-instrumental, C-calibrada    
%   ScatterReaCal                  - 
%   calibrationplots               - Prepro. label (var_name and var_unit)
%   wfmedias                       - 
%   wfplots                        - 
%   wfresiduos                     - 
%   wfscatter                      - 
%   wftabla                        - 
