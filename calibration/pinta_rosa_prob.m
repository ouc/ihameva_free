function pinta_rosa_prob(cant,cant_log,VH,Hlim,IncD,usarlog,nAngles,etiqueta,var_name,var_unit)
%Se usa por medio de PintaRosasProbabilisticas.m

if ~exist('usarlog','var') || isempty(usarlog), usarlog=false;end
if ~exist('nAngles','var') || isempty(nAngles), nAngles=12;end
if ~exist('IncD','var') || isempty(IncD), IncD=5;end
if ~exist('idioma','var'), idioma='eng'; end
if ~exist('var_name','var'), var_name='Hs'; end
if ~exist('var_unit','var'), var_unit='m'; end

Theta=0:IncD:360;
Theta=90-Theta;
if usarlog, 
    R=(0:0.25:log(max(VH)));
else
    R=(0:0.25:ceil(2*max(VH))/2);
end

[cax,next]=roseaxislabel(R,Hlim,usarlog,nAngles,var_unit);

[ang,ro]=meshgrid(pi/180*(Theta),VH);
if usarlog, ro=log(ro); end
[x,y,z]=pol2cart(ang,ro,cant_log);

col2=[1,1,1;0.749019622802734,0,0.749019622802734;
    0.680759787559509,0.368750005960465,0.743259787559509;
    0.612500011920929,0.737500011920929,0.737500011920929;
    0.524999976158142,0.618749976158142,0.649999976158142;
    0.437500000000000,0.500000000000000,0.562500000000000;
    0.349999994039536,0.381249994039536,0.474999994039536;
    0.262499988079071,0.262499988079071,0.387499988079071;
    0.174999997019768,0.174999997019768,0.268750011920929;
    0.0874999985098839,0.0874999985098839,0.150000005960464;
    0,0,0.0312500000000000;];

z2=niveles(z,etiqueta(2:end));
z2=z2+1;% OJO OJO por que sumo +1 OJO OJO
if isvector(z2), z2=reshape(z2,size(y)); end
probn=find(cant==0);
z2(probn)=1;
pcolor(x,-y,z2); 
set(gcf,'Renderer','painters'); % OJO OJO sin esto no funciona el print para guardar en png OJO OJO
shading flat;
colormap(col2)

set(gca,'dataaspectratio',[1 1 1]); 
axis off;
set(cax,'NextPlot',next);
set(get(gca,'xlabel'),'visible','on')
set(get(gca,'ylabel'),'visible','on')
axis ij;

function [cax,next]=roseaxislabel(R,Hlim,usarlog,nAngles,var_unit)
    % get hold state
    cax = newplot;
    next = lower(get(cax,'NextPlot'));
    % get x-axis text color so grid is in same color
    tc = get(cax,'xcolor');
    ls = get(cax,'gridlinestyle');
    % Hold on to current Text defaults, reset them to the
    % Axes' font attributes so tick marks use them.
    set(cax, 'DefaultTextFontAngle',  get(cax, 'FontAngle'), ...
    'DefaultTextFontName',   get(cax, 'FontName'), ...
    'DefaultTextFontSize',   get(cax, 'FontSize'), ...
    'DefaultTextFontWeight', get(cax, 'FontWeight'), ...
    'DefaultTextUnits','data')

    hold on;
    hkk=plot(R);
    set(gca,'dataaspectratio',[1 1 1],'plotboxaspectratiomode','auto');
    vkk = get(gca,'ylim');                                              
    rlabshift=vkk(1);                                                   
    delete(hkk);  
    hhh=plot(R+rlabshift);
    set(cax,'dataaspectratio',[1 1 1],'plotboxaspectratiomode','auto');
    set(cax,'ylim',Hlim)
    v = [get(cax,'xlim') get(cax,'ylim')];
    ticks = length(get(cax,'ytick'));%     ticks = sum(get(cax,'ytick')>=0);
    delete(hhh);
    rmax = min(max(v(4)-rlabshift,vkk(2)),Hlim(2)); rticks = max(ticks-1,4);
    rmin = max(0,min(v(3)-rlabshift,vkk(1)));
    if rticks > 6   % see if we can reduce the number
        if rem(rticks,2) == 0
            rticks = rticks/2;
        elseif rem(rticks,3) == 0
            rticks = rticks/3;
        end
    end
    
    % define a circle
    th = 0:pi/50:2*pi;
    xunit = cos(th);
    yunit = sin(th);
    % now really force points on x/y axes to lie on them exactly
    inds = 1:(length(th)-1)/4:length(th);
    xunit(inds(2:2:4)) = zeros(2,1);
    yunit(inds(1:2:5)) = zeros(3,1);
    % draw radial circles
    c82 = cos(82*pi/180);
    s82 = sin(82*pi/180);
    rinc = (rmax-rmin)/rticks;
    cnt = 1;
    if usarlog, 
        for i=(rmin+rinc):rinc:rmax
            hhh = plot(xunit*log(i),yunit*log(i),ls,'color',tc,'linewidth',1,'handlevisibility','off');
            text(xunit(24)*log(i),-yunit(24)*log(i),['' num2str(floor(i+rlabshift),2) var_unit], ... % Yo
                'verticalalignment','bottom','handlevisibility','off','FontSize',7);                                      % Yo
            %if (i+rlabshift-1)==0, set(hhh,'linestyle','-'); end
            cnt = cnt+1;
        end
    else
        for i=(rmin+rinc):rinc:rmax
            hhh = plot(xunit*i,yunit*i,ls,'color',tc,'linewidth',1,'handlevisibility','off');
            text((i+rinc/20)*c82,-(i+rinc/20)*s82,['' num2str(i+rlabshift,2) var_unit], ... % Yo
                'verticalalignment','bottom','handlevisibility','off','FontSize',7);                                      % Yo
            %if (i+rlabshift-1)==0, set(hhh,'linestyle','-'); end
            cnt = cnt+1;
        end
    end
    set(hhh,'linestyle','-','color',tc); % Make outer circle solid

%plot spokes. oc he anadido las lines de 45/2 en 45/2 como  
    if usarlog, rmax=log(rmax); end
    labelrotate=90;
    dnA=360/nAngles;
    nAp=round(nAngles/2);
    th = (1:nAp)*2*pi/nAngles;
    cst = cos(th); snt = sin(th);
    plot(repmat([rmin rmax],nAp,1)'.*repmat(cst,2,1),repmat([rmin rmax],nAp,1)'.*repmat(snt,2,1),':','color',tc,'linewidth',1,'handlevisibility','off');
    plot(repmat([rmin rmax],nAp,1)'.*repmat(-cst,2,1),repmat([rmin rmax],nAp,1)'.*repmat(-snt,2,1),':','color',tc,'linewidth',1,'handlevisibility','off');

    rt = 1.15*rmax;% annotate spokes in degrees
    for i = 1:length(th)
        angTextNum = (i*dnA)+labelrotate    ;
        if angTextNum<0, angTextNum=angTextNum+360; end
        text(rt*cst(i),rt*snt(i),[num2str(angTextNum) char(176)],...
             'horizontalalignment','center',...
             'handlevisibility','off','fontsize',8);%,'fontweight','bold')
        if i == length(th)
            loc = num2str(0+labelrotate);
        else
            angTextNum = 180+(i*dnA)+labelrotate    ;
            if angTextNum > 180, angTextNum = angTextNum - 360; end
            loc = num2str(angTextNum);
        end
        if str2num(loc)<0, loc=num2str(str2num(loc)+360); end
        text(-rt*cst(i),-rt*snt(i),[loc char(176)],'horizontalalignment','center',...
             'handlevisibility','off','fontsize',8);%,'fontweight','bold')
    end
    view(2);% set view to 2-D
    axis(rmax*[-1.1 1.1 -1.2 1.2]);% set axis limits
end

end
