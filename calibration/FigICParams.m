function FigICParams(aj,ajlo,ajup,bj,bjlo,bjup,dj,djlo,djup,DirNoQ,np,thetaj,var_name,qlim,gcax) 

	if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
	if ~ishandle(gcax), disp('WARNING! FigICParams with axes'); end
	set(gcf,'CurrentAxes',gcax)

    thetaplot = linspace(0,2*pi);
    %   Para el parametro a
    deriv = ((aj(2)-aj(1))/(thetaj(2)-thetaj(1))+(aj(np)-aj(np-1))/(thetaj(np)-thetaj(np-1)))/2;
    ap = spline(thetaj,[deriv; aj; deriv],thetaplot);
    deriv = ((ajup(2)-ajup(1))/(thetaj(2)-thetaj(1))+(ajup(np)-ajup(np-1))/(thetaj(np)-thetaj(np-1)))/2;
    apup = spline(thetaj,[deriv; ajup; deriv],thetaplot);
    deriv = ((ajlo(2)-ajlo(1))/(thetaj(2)-thetaj(1))+(ajlo(np)-ajlo(np-1))/(thetaj(np)-thetaj(np-1)))/2;
    aplo = spline(thetaj,[deriv; ajlo; deriv],thetaplot);
    %   Para el parametro b
    deriv = ((bj(2)-bj(1))/(thetaj(2)-thetaj(1))+(bj(np)-bj(np-1))/(thetaj(np)-thetaj(np-1)))/2;
    bp = spline(thetaj,[deriv; bj; deriv],thetaplot);
    deriv = ((bjup(2)-bjup(1))/(thetaj(2)-thetaj(1))+(bjup(np)-bjup(np-1))/(thetaj(np)-thetaj(np-1)))/2;
    bpup = spline(thetaj,[deriv; bjup; deriv],thetaplot);
    deriv = ((bjlo(2)-bjlo(1))/(thetaj(2)-thetaj(1))+(bjlo(np)-bjlo(np-1))/(thetaj(np)-thetaj(np-1)))/2;
    bplo = spline(thetaj,[deriv; bjlo; deriv],thetaplot);
    
    if qlim ~=0 && qlim > 0 && qlim < 0.99,
        %   Para el parametro d
        deriv = ((dj(2)-dj(1))/(thetaj(2)-thetaj(1))+(dj(np)-dj(np-1))/(thetaj(np)-thetaj(np-1)))/2;
        dp = spline(thetaj,[deriv; dj; deriv],thetaplot);
        deriv = ((djup(2)-djup(1))/(thetaj(2)-thetaj(1))+(djup(np)-djup(np-1))/(thetaj(np)-thetaj(np-1)))/2;
        dpup = spline(thetaj,[deriv; djup; deriv],thetaplot);
        deriv = ((djlo(2)-djlo(1))/(thetaj(2)-thetaj(1))+(djlo(np)-djlo(np-1))/(thetaj(np)-thetaj(np-1)))/2;
        dplo = spline(thetaj,[deriv; djlo; deriv],thetaplot);
        polarLabels_IC_modOrigenSombra([thetaplot;thetaplot],[dp;zeros(size(dp))],[dpup; dplo; zeros(size(dp)); zeros(size(dp))],0,DirNoQ,qlim,gcax)
        title(gcax,['{',var_name,'}^C = c(\theta) {',var_name,'}^R'],'fontsize',11,'fontweight','bold');
    else
        polarLabels_IC_modOrigenSombra([thetaplot;thetaplot],[ap;bp],[apup; aplo; bpup; bplo],0,DirNoQ,0,gcax)
        title(['{',var_name,'}^C = a(\theta) {',var_name,'}^R ^{b(\theta)}'],'fontsize',11,'fontweight','bold');
    end

end
