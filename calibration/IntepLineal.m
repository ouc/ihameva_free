function theta = IntepLineal (theta1,theta2,theta3,x,y,x1,y1,x2,y2,x3,y3)

%*********************************************************************************
%*  function theta = IntepLineal (theta1,theta2,theta3,x,y,x1,y1,x2,y2,x3,y3)    *
%*  INTERPOLA linealmente en el triangulo                                        *
%*                                                                               *
%*  ARGUMENTOS DE ENTRADA:                                                       *
%*                                                                               *
%*            Coordenadas de los nodos que componen el elemento.                 *
%*                       x1  x2  x3                                              *
%*                       y1  y2  y3                                              *
%*  numeracion de nodos: 3                                                       *
%*                       | \                                                     *
%*                       |   \                                                   *
%*                       |     \                                                 *
%*                       1-------2                                               *
%*  x,y     : Posicion en la que interpolar dentro del elemento                  *
%*  theta1,theta2,theta3     : Angulos en los nodos en grados sexagesimales      *
%*                                                                               *
%*                                                                               *
%*  ARGUMENTOS DE SALIDA:                                                        *
%*                                                                               *
%*  theta       : Angulo sexagesimal interpolado                                 *
%*********************************************************************************

A = 0.5*(x1*(y2-y3)-x2*(y1-y3)+x3*(y1-y2));

N1 = (x*(y2-y3)+y*(x3-x2)+x2*y3-x3*y2)/(2*A);
N2 = (x*(y3-y1)+y*(x1-x3)+x3*y1-x1*y3)/(2*A);
N3 = (x*(y1-y2)+y*(x2-x1)+x1*y2-x2*y1)/(2*A);

theta = N1*theta1+N2*theta2+N3*theta3;