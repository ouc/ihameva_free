function [Yord,Prob,Yp,P]=DetClas2N(Y,Clas,P1,Pend,N)
% P1=0.1;
% Pend=0.999;
% Clas=20;

Yord=sort(Y);
Prob=(cumsum(ones(1,length(Y)))-0.5)./length(Y); % Hazen
% Prob=(cumsum(ones(1,length(Y)))-0.44)./(length(Y)+0.12); % Gringorten

X1=-log(log(1./P1));
Xend=-log(log(1./Pend));
Delta=(Xend-X1)/(Clas-1);
X=(X1:Delta:Xend);
P=1./(exp(exp(-X)));

i=1;
while i<=length(P)
    if length(Y)*(1-P(i))>=N%5
        pos=find(  Prob>P(i)-1/(2*length(Y)) & Prob<=P(i)+1/(2*length(Y))  );
        Yp(i)=mean(Yord(pos));
        i=i+1;
    else
        i=length(P)+1;
    end
end
P=P(1:length(Yp));
