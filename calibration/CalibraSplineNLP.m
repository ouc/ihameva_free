function [Hscaln] = CalibraSplineNLP(H_G,theta_G,thetaj,x,maxHsR,deltamaxHsR,Hs_Rea,Dir_Rea,Hs_Scal_old,svopt,carpeta)
%   La funcion CalibraSplineNLP 
% Examples 1
%   Hscaln=CalibraSplineNLP(HsGow,thetaGOW,thetaj,pdef,maxHsR,deltam,axHsR);%NO se hacen graficos
% Examples 2
%   carpeta='sample_dir_cal';if ~exist([pwd filesep carpeta],'dir'), mkdir(carpeta); end
%   dirscript='C:\IHCantabria\amevadatos\calibration\antoniot\';%directorio de los datos cambiar
%   load([dirscript,'DATA.mat'])
%   HsI = DATA(:,4);HsR = DATA(:,5);Dir = DATA(:,6);
%   GOW = load([dirscript,'GOW_DATA.dat']);%GOW_Valencia II Ext
%   HsGow = GOW(:,5);thetaGOW = GOW(:,8);
%   [Hscal,thetaj,pdef,maxHsR,deltamaxHsR]=CalibraSplineQuantile(HsI,HsR,Dir,nq,increthet,qlim,nivconf,carpeta);
%   Hscaln=CalibraSplineNLP(HsGow,thetaGOW,thetaj,pdef,maxHsR,deltamaxHsR,HsR,Dir,Hscal,carpeta);

if nargin==6,
    extrapola = 1;
else
    extrapola = 0;
end
% % % if length(find(theta_G>2*pi))>0.8*length(theta_G),
% % %     %   Los datos estan entre 0 y 360
% % %     if any(find(theta_G<0 | theta_G>360)),
% % %         error('Los datos han de estar entre 0 y 360 grados.')
% % %     end
% % %     theta_G = theta_G*2*pi/360;
% % % else
% % %     if any(find(theta_G<0 | theta_G>2*pi)),
% % %         error('Los datos han de estar entre 0 y 2pi radianes.')
% % %     end
% % % end
if nanmax(thetaj)>2*pi,
    thetaj = thetaj*pi/180;
end
if nanmax(theta_G)>2*pi,
    theta_G = theta_G*pi/180;
end
    if any(find(theta_G<0 | theta_G>2*pi)),
        error('Los datos han de estar entre 0 y 2pi radianes.')
    end

np = length(thetaj);
n = length(H_G);

if extrapola,
    maxHsR_G = interp1((1:360)'*pi/180,maxHsR,theta_G,'nearest');
    deltamaxHsR_G = interp1((1:360)'*pi/180,deltamaxHsR,theta_G,'nearest');
end

a = x(1:np);
b = x(np+1:2*np);
d = x(2*np+1:3*np);

%   Calculo los incrementos angulares y los almaceno en h
h = (thetaj(2:np)-thetaj(1:np-1));

C = sparse(zeros(np));
TIA = sparse(zeros(np,1));
TIB = sparse(zeros(np,1));
TID = sparse(zeros(np,1));

C(1,1)=2*h(1);
C(1,2)=h(1);
for i=1:np-2,
    C(i+1,i)=h(i);
    C(i+1,i+1)=2*(h(i)+h(i+1));
    C(i+1,i+2)=h(i+1);
end
C(np,np)=2*h(np-1);
C(np,np-1)=h(np-1);

TIA(1)=3*((a(2)-a(1))/h(1)-(a(1)-a(np-1))/h(np-1))/2;
for i=2:np-1,
    TIA(i)=3*((a(i+1)-a(i))/h(i)-(a(i)-a(i-1))/h(i-1));
end
% TIA(np)=3*((a(2)-a(1))/h(1)-(a(1)-a(np-1))/h(np-1))/2;
TIA(np)=TIA(1);
%   Solucion del sistema
yaj = full(C\TIA);
xaj = (a(2:np)-a(1:np-1))./h(1:np-1)-h(1:np-1).*(2*yaj(1:np-1)+yaj(2:np))/3;
zaj = (yaj(2:np)-yaj(1:np-1))./(3*h);


TIB(1)=3*((b(2)-b(1))/h(1)-(b(1)-b(np-1))/h(np-1))/2;
for i=2:np-1,
    TIB(i)=3*((b(i+1)-b(i))/h(i)-(b(i)-b(i-1))/h(i-1));
end
% TIB(np)=3*((b(2)-b(1))/h(1)-(b(1)-b(np-1))/h(np-1))/2;
TIB(np)=TIB(1);
%   Solucion del sistema
ybj = full(C\TIB);
xbj = (b(2:np)-b(1:np-1))./h(1:np-1)-h(1:np-1).*(2*ybj(1:np-1)+ybj(2:np))/3;
zbj = (ybj(2:np)-ybj(1:np-1))./(3*h);


TID(1)=3*((d(2)-d(1))/h(1)-(d(1)-d(np-1))/h(np-1))/2;
for i=2:np-1,
    TID(i)=3*((d(i+1)-d(i))/h(i)-(d(i)-d(i-1))/h(i-1));
end
TID(np)=TID(1);
%   Solucion del sistema
ydj = (C\TID);
xdj = (d(2:np)-d(1:np-1))./h(1:np-1)-h(1:np-1).*(2*ydj(1:np-1)+ydj(2:np))/3;
zdj = (ydj(2:np)-ydj(1:np-1))./(3*h);


A = [a [xaj;0] yaj [zaj;0]];
B = [b [xbj;0] ybj [zbj;0]];
D = [d [xdj;0] ydj [zdj;0]];
%   Calculo el resto de los parametros

Pos = zeros(n,1);
%   Esto vale si los datos estan ordenados o desordenados
jref = 1;
for i = 1:n  
    jref=1;
    for j = jref:np-1, 
        if theta_G(i)>=thetaj(j) && theta_G(i)<=thetaj(j+1),
            Pos(i) = j;
            break;
%             jref = j;
        end
    end
end

[f c] = size(A);
[f1 c1] = size(B);
if f~=f1 || c~=c1,
    error('Dimensiones de A y B no concordantes')
end

Hscaln = zeros(n,1);
if extrapola,
    for i = 1:n,
        try
            ai = A(Pos(i),1);
            for j = 2:c,
                ai = ai + A(Pos(i),j)*(theta_G(i)-thetaj(Pos(i)))^(j-1);
            end
            bi = B(Pos(i),1);
            for j = 2:c,
                bi = bi + B(Pos(i),j)*(theta_G(i)-thetaj(Pos(i)))^(j-1);
            end
            di = D(Pos(i),1);
            for j = 2:c,
                di = di + D(Pos(i),j)*(theta_G(i)-thetaj(Pos(i)))^(j-1);
            end

            if H_G(i)>maxHsR_G(i),
                Hscaln(i)=H_G(i)+deltamaxHsR_G(i);
            elseif H_G(i)>=di,
                Hscaln(i) = ai*H_G(i)^bi; 
            else
                aip = ai*di^(bi-1);
                Hscaln(i) = aip*H_G(i); 
            end
        catch
            Hscaln(i)=nan; 
        end

    end
else
    for i = 1:n,
        try
            ai = A(Pos(i),1);
            for j = 2:c,
                ai = ai + A(Pos(i),j)*(theta_G(i)-thetaj(Pos(i)))^(j-1);
            end
            bi = B(Pos(i),1);
            for j = 2:c,
                bi = bi + B(Pos(i),j)*(theta_G(i)-thetaj(Pos(i)))^(j-1);
            end
            di = D(Pos(i),1);
            for j = 2:c,
                di = di + D(Pos(i),j)*(theta_G(i)-thetaj(Pos(i)))^(j-1);
            end

            if H_G(i)>=di,
                Hscaln(i) = ai*H_G(i)^bi; 
            else
                aip = ai*di^(bi-1);
                Hscaln(i) = aip*H_G(i); 
            end
        catch
            Hscaln(i)=nan;
        end
    end
end

%graficos
if nargin>=9
    GraficosAdicionales(H_G,theta_G*180/pi,Hs_Rea,Dir_Rea,maxHsR,Hs_Scal_old,Hscaln,svopt,carpeta);
end
