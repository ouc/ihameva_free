function ParamTableL(np,aj,bj,ajlo,bjlo,dj,djlo,thetaj,idioma,gcax)

    if ~exist('idioma','var'), idioma='eng'; end

    box(gcax,'on'); %   Eliminacion de los label
    set(gcax,'XTick',[])
    set(gcax,'XTickLabel',[])
    set(gcax,'YTick',[])
    set(gcax,'YTickLabel',[])
   

    text(0.075,0.95,'\theta','FontSize',11,'FontWeight','bold');
    % text(0.075,0.95,'\theta','FontSize',11,'FontWeight','bold','FontName','Arial Narrow')
    hold(gcax,'on');
    if strcmp(idioma,'esp') %En Esp
    %     text(0.24,0.95,'a(\theta) \pm IC','FontSize',11,'FontWeight','bold');
    %     text(0.64,0.95,'b(\theta) \pm IC','FontSize',11,'FontWeight','bold');
    %     text(0.24,0.95,'a(\theta) \pm IC^{95%}','FontSize',11,'FontWeight','bold','FontName','Arial Narrow')
    %     text(0.64,0.95,'b(\theta) \pm IC^{95%}','FontSize',11,'FontWeight','bold','FontName','Arial Narrow')
        text(0.24,0.95,'a \pm IC^{95%}','FontSize',11,'FontWeight','bold')
        text(0.64,0.95,'b \pm IC^{95%}','FontSize',11,'FontWeight','bold')
    elseif strcmp(idioma,'eng') %En Ingles
    %     text(0.24,0.95,'a(\theta) \pm CI','FontSize',11,'FontWeight','bold');
    %     text(0.64,0.95,'b(\theta) \pm CI','FontSize',11,'FontWeight','bold');
    %     text(0.24,0.95,'a(\theta) \pm CI^{95%}','FontSize',11,'FontWeight','bold','FontName','Arial Narrow')
    %     text(0.64,0.95,'b(\theta) \pm CI^{95%}','FontSize',11,'FontWeight','bold','FontName','Arial Narrow')
%         text(0.24,0.95,'a \pm CI^{95%}','FontSize',11,'FontWeight','bold')
%         text(0.64,0.95,'b \pm CI^{95%}','FontSize',11,'FontWeight','bold')
            text(0.20,0.95,'a \pm CI^{95%}','FontSize',8.5,'FontWeight','bold')
            text(0.47,0.95,'b \pm CI^{95%}','FontSize',8.5,'FontWeight','bold')
            text(0.75,0.95,'c \pm CI^{95%}','FontSize',8.5,'FontWeight','bold')

    end

    plot(gcax,[0 1],[1 1],'k','linewidth',0.4);
    plot(gcax,[0 1],0.905*[1 1],'k','linewidth',0.4);
    % plot([0 0],[0 1],'k','linewidth',0.4);
    plot(gcax,[0.16 0.16],[0 1],'k','linewidth',0.4);
    plot(gcax,[0.44 0.44],[0 1],'k','linewidth',0.4);
    plot(gcax,[0.72 0.72],[0 1],'k','linewidth',0.4);
    % plot([1 1],[0 1],'k','linewidth',0.4);

    for I=1:np
    %     LocY=0.93-I/(np+3);
        LocY=0.93-I/(np+1.8);
        text(0.025,LocY,[num2str(thetaj(I)*180/pi,'%3.1f') char(176)],'FontSize',7);
    %     text(0.225,LocY,[num2str(aj(I),'%1.4f') ' \pm ' num2str(aj(I)-ajlo(I),'%1.4f')],'FontSize',8);
    %     text(0.625,LocY,[num2str(bj(I),'%1.4f') ' \pm ' num2str(bj(I)-bjlo(I),'%1.4f')],'FontSize',8);
        text(0.19,LocY,[num2str(aj(I),'%1.2f') ' \pm ' num2str(aj(I)-ajlo(I),'%1.3f')],'FontSize',6.5);
        text(0.47,LocY,[num2str(bj(I),'%1.2f') ' \pm ' num2str(bj(I)-bjlo(I),'%1.3f')],'FontSize',6.5);
        text(0.75,LocY,[num2str(dj(I),'%1.2f') ' \pm ' num2str(dj(I)-djlo(I),'%1.3f')],'FontSize',6.5);
        if I<np
            plot(gcax,[0 1],LocY*[1 1]-0.025,'k','linewidth',0.4);
        end
    end
end
    
