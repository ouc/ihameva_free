function calibration(varargin)
%   CALIBRATION tool.
%   CALIBRATION (calibration.m) opens a graphical user interface for displaying the main program posibility.
%   Tested on Ubuntu trusty v.14.04.1(developed), Windows 7 and Mac Os X v10.9.4 (MATLAB:R2013a)
%   To load test data, please click on: -Help, Load ameva test data-, to send
%   data to matlab worksapce and work with this.
%   
% Examples 1
%   load ameva;
%   Calibration->Calibration
% Examples 2 %Calibrar y re-calibrar por linea de comandos
%   load ameva;
%   nq=20;increthet=22.5;qlim=0;nivconf=0.05;carpeta='sample_dir_cal';
%   [Hscal,thetaj,pdef,maxHsR,deltamaxHsR]=CalibraSplineQuantile(excal_HsI,excal_HsR,excal_Dire,nq,increthet,qlim,nivconf,svopt,carpeta);
% Examples 3 %Calibrar y re-calibrar por linea de comandos
%   load ameva;
%   [Hscal,thetaj,pdef,maxHsR,deltamaxHsR]=CalibraSplineQuantile(excal_HsI,excal_HsR,excal_Dire);
%   Hscal_n=CalibraSplineNLP(excal_HsI,excal_Dire,thetaj,pdef,maxHsR,deltamaxHsR,excal_HsR,excal_Dire,Hscal);
%
% See also:
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   calibration(main program tool v1.3.5 (150320)).
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows 7
%   04-07-2011 - The first distribution version
%   07-05-2013 - Old distribution version
%   27-05-2013 - Old distribution version
%   20-08-2014 - Last distribution version

versionumber='v1.3.5 (150320)';
nmfctn='calibration';
Ax1Pos=[0.39 0.2 0.55 0.7];
figPos=[];

idioma='eng'; usertype=1;
if length(varargin)==1, idioma=varargin{1}.idioma; usertype=varargin{1}.usertype; end

% Texto idioma:
texto=amevaclass.amevatextoidioma(idioma);

namesoftware=[texto('nameCalibration') ' ',versionumber];


if ~isempty(findobj('Tag',['Tag_' nmfctn]))
   figPos=get(findobj('Tag',['Tag_' nmfctn]),'Position');
   close(findobj('Tag',['Tag_' nmfctn]));
end

%Lista de las figuras que se van a usar en este programa
hf=[];%ventana principal
hi=[];
hh=[];
hg=[];

% Information for all buttons and Spacing between the button
colorG=[0.94 0.94 0.94];	%Color general
btnWid=0.135;			
btnHt=0.05;
spacing=0.020;			
top=0.95;
xPos=[0.01 0.15 0.28 0.42];

%GLOBAL VARIALBES solo usar las necesarias!!
awinc=amevaclass;%ameva windows commons methods
awinc.nmfctn=nmfctn;
Hscal=[];
Hs_Ins=[];
Hs_Rea=[];
Dir_Rea=[];
date_time=[];
date_time_n=[];
stdvHs=[];A=[];
thetaj=[];residuo=[];HsI=[];HsR=[];calidadRHO=[];DirQ=[];statmoments=[];
x_G=[];
y_G=[];
stdvHs_G=[];
Hs_NCQ=[];Hs_CQ=[];Hs_SATQ=[];
aj=[];ajlo=[];ajup=[];
bj=[];bjlo=[];bjup=[];
dj=[];djlo=[];djup=[];
DirNoQ=[];Hsup=[];Hslo=[];pdef=[];
std=0;n=0;nq=0;np=0;Hlim=0;qlim=0;
maxHsR=[];deltamaxHsR=[];
carpeta=[];
var_name='';
tvr_name='';
new_var_name='';
var_unit='';
nivconf=0.0;
%newcal
Hs_n=[];
Dir_n=[];
Hscal_n=[];
Hsup_n=[];
Hslo_n=[];
ExistDir=false;
ExistOutLierVar='No';
outliervarselect='No';
validchildren=[];
svopt=[1 1 0];%save option 1-fig 2-png 3-eps (1/0)
%---LA FIGURA-VENTANA PRINCIPAL-
if isempty(figPos), figPos=awinc.amevaconst('mw'); end
hf=figure('Name',namesoftware,'Color',colorG,'CloseRequestFcn',@AmevaClose,'UserData',ExistOutLierVar, ...
    'NumberTitle','Off','DockControls','Off','Position',figPos,'Tag',['Tag_' nmfctn],'NextPlot','new');
ax1=axes('Units','normalized','Parent',hf,'Position',Ax1Pos,'Visible','Off');

if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('buttonPlot_1a'), ...
    'Position',[xPos(2) 0.94 btnWid btnHt*0.66]);

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_data=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'String','Data', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataOO('on'));

ui_tp=uicontrol('Style','popup','Units','normalized','Parent',hf,'String',texto('buttonPlot_1'), ...
    'Position',[xPos(2) yPos-0.005 btnWid btnHt],'Callback',@AmevaPlot);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_otl=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'Enable','Off','String',texto('outlierFilter'), ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback','');

uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('buttonPlot_2'), ...
    'Position',[xPos(2) yPos+0.036 btnWid btnHt*0.66]);
ui_pp=uicontrol('Style','popup','Units','normalized','Parent',hf,'Enable','Off','String',texto('buttonPlot_3'), ...
    'Position',[xPos(2) yPos-0.005 btnWid btnHt],'Callback',@AmevaPlotToPrint);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_avm=uicontrol('Style','push','Units','normalized','Parent',hf,'Enable','Off','String','Settings', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataO2('on'));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_start=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'Enable','Off','String','Start', ...
    'Interruptible','on','Position',[xPos(1) yPos btnWid btnHt],'Callback',@CalibrationRun);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_nf=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'Enable','Off','String',texto('newDataCalibrate'), ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataO1('on'));

%
ui_ttd=uicontrol('Style','listbox','Units','normalized','String','','FontSize',7.5, ...
    'Position',[xPos(1) 0.055 0.312 0.53],'FontName', 'FixedWidth','Visible','on');

uicontrol('Style','frame','Units','normalized','Parent',hf,'Position',[0 0 1 0.04]);
uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.59 0.004 0.18 0.03],'String',texto('saveFigure'));
ui_ckeps=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.77 0.004 btnWid/2 0.03],'String','*.eps','Value',svopt(3));
ui_ckfig=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.85 0.004 btnWid/2 0.03],'String','*.fig','Value',svopt(1));
ui_ckpng=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.92 0.004 btnWid/2 0.03],'String','*.png','Value',svopt(2),'Callback',@(source,event)EnableButton(ui_data,'Enable','on'));
SetSaveOpt;
ui_tb   =uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.01 0.004 0.58 0.03],...
    'String',[nmfctn,texto('dataCalibrate')],'HorizontalAlignment','left');
%% Calibration Data
btnWid=0.35;
btnHt=0.075;
spacing=0.02;
top=0.98;
xPos=[0.03 0.45 0.82];
hg=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Data'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataOO('off'),'WindowButtonMotionFcn',@ListBoxCallback1,...
    'NumberTitle','Off','MenuBar','none','Position',awinc.amevaconst('ds'),'Resize','Off','Visible','Off','NextPlot','new');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
amvcnf.idioma=idioma;amvcnf.usertype=usertype;

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid*1.15 btnHt],...
    'String','Data & WorkSpace','Callback',@(source,event)amevaworkspace(amvcnf));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_td(1)=uicontrol('Style','text','Units','normalized','Parent',hg,'String','* Instumental X:', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_all(1)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',1);
ui_cm(1)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid/2 btnHt],'String','(none)','visible','off','Callback',@(source,event)ltsplot(1));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_td(2)=uicontrol('Style','text','Units','normalized','Parent',hg,'String','* Reanalysis X:', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_all(2)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',2);
ui_cm(2)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid/2 btnHt],'String','(none)','visible','off','Callback',@(source,event)ltsplot(2));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_td(3)=uicontrol('Style','text','Units','normalized','Parent',hg,'String','Reanalysis dir:', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_all(3)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',3);
ui_cm(3)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid/2 btnHt],'String','(none)','visible','off','Callback',@(source,event)ltsplot(3));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_td(4)=uicontrol('Style','text','Units','normalized','Parent',hg,'String','Time:', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_all(4)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',4);
ui_cm(4)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid/2 btnHt],'String','(none)','visible','off');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hg,'String','X name & unit:', ...
    'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_fs=uicontrol('Style','edit','Units','normalized','Parent',hg,'String','Hs', ...
    'Position',[xPos(2) yPos btnWid/2 btnHt]);
ui_fu=uicontrol('Style','edit','Units','normalized','Parent',hg,'String','m', ...
    'Position',[xPos(2)+btnWid/2 yPos btnWid/2 btnHt]);

btnN=btnN+2.5;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Apply', ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@AmevaSetData);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Close', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataOO('off'));

uicontrol('Style','frame','Units','normalized','Parent',hg,'Position',[0 0 1 0.04]);
ui_tbd=uicontrol('Style','Text','Units','normalized','Parent',hg,'String',texto('dataWorkspace'), ...
    'Position',[0.01 0.004 0.99 0.03],'HorizontalAlignment','left');


%% Ameva New Fit
hh=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),texto('newDataCalibrate')],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataO1('off'),'WindowButtonMotionFcn',@ListBoxCallback1,...
    'NumberTitle','Off','MenuBar','none','Position',awinc.amevaconst('ds'),'Resize','Off','Visible','Off','NextPlot','new');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_td(5)=uicontrol('Style','text','Units','normalized','Parent',hh,'String','* new X to cal:', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_all(5)=uicontrol('Style','popup','Units','normalized','Parent',hh,'Position',[xPos(2) yPos btnWid btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',5);
ui_cm(5)=uicontrol('Style','popup','Units','normalized','Parent',hh,'Position',[xPos(3) yPos btnWid/2 btnHt],'String','(none)','visible','off');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_td(6)=uicontrol('Style','text','Units','normalized','Parent',hh,'String','new dir to cal:', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_all(6)=uicontrol('Style','popup','Units','normalized','Parent',hh,'Position',[xPos(2) yPos btnWid btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',6);
ui_cm(6)=uicontrol('Style','popup','Units','normalized','Parent',hh,'Position',[xPos(3) yPos btnWid/2 btnHt],'String','(none)','visible','off');


btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_td(7)=uicontrol('Style','text','Units','normalized','Parent',hh,'String','new  Time:', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_all(7)=uicontrol('Style','popup','Units','normalized','Parent',hh,'Position',[xPos(2) yPos btnWid btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',7);
ui_cm(7)=uicontrol('Style','popup','Units','normalized','Parent',hh,'Position',[xPos(3) yPos btnWid/2 btnHt],'String','(none)','visible','off');


btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hh,'String','new X Name:', ...
    'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_nfs=uicontrol('Style','edit','Units','normalized','Parent',hh,'String','newH', ...
    'Position',[xPos(2) yPos btnWid btnHt]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_cb=uicontrol('Style','check','Units','normalized','Parent',hh,'Position',[xPos(2) yPos btnWid btnHt], ...
    'String','Conf. Band','Value',0);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hh,'String',texto('applyCalib'), ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@AmevaSetNewFit);

btnN=btnN+4;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hh,'String','Close', ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@(source,event)FigDataO1('off'));

uicontrol('Style','frame','Units','normalized','Parent',hh,'Position',[0 0 1 0.04]);
ui_tbn=uicontrol('Style','Text','Units','normalized','Parent',hh,'String',texto('clickApCalibra'), ...
    'Position',[0.01 0.004 0.99 0.03],'HorizontalAlignment','left');

%% Ameva Settings
hi=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Settings'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataO2('off'),...
    'NumberTitle','Off','MenuBar','none','Position',awinc.amevaconst('ds'),'Resize','Off','Visible','Off','NextPlot','new');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

btnN=1.5;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hi,'String',{'Quantile','lineal-exp [0 1):'}, ...
    'Position',[xPos(1) yPos+spacing/2 btnWid btnHt+spacing/2],'HorizontalAlignment','right');
uicontrol('Style','text','Units','normalized','Parent',hi,'String','0','BackgroundColor',colorG, ...
    'Position',[xPos(2) yPos+btnHt btnWid/6 btnHt/2],'HorizontalAlignment','left');
uicontrol('Style','text','Units','normalized','Parent',hi,'String','1','BackgroundColor',colorG, ...
    'Position',[xPos(2)+btnWid-spacing yPos+btnHt btnWid/6 btnHt/2],'HorizontalAlignment','left');
ui_lt=uicontrol('Style','text','Units','normalized','Parent',hi,'String','0','BackgroundColor',colorG, ...
    'Position',[xPos(2)+btnWid/3 yPos+btnHt btnWid/3 btnHt/2],'HorizontalAlignment','center');
ui_le=uicontrol('Style','slider','Units','normalized','Parent',hi,'Callback',@ActCorre, ...
    'Position',[xPos(2) yPos btnWid btnHt],'Min',0,'Max',0.99,'Value',0,'SliderStep',[0.1 0.1]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hi,'String','N Quantiles:', ...
    'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_nq=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2) yPos btnWid btnHt],...
    'String','20');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hi,'String','N Direction:', ...
    'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_nd=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2) yPos btnWid btnHt], ...
    'String','16');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hi,'String','Significance level:', ...
    'Position',[xPos(1) yPos-btnHt*0.5 btnWid btnHt*1.5],'HorizontalAlignment','right');
ui_nc=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2) yPos btnWid btnHt], ...
    'String','0.05');

btnN=btnN+4;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hi,'String','Close', ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@(source,event)FigDataO2('off'));

%% C0 Open,Close figure data
    function FigDataOO(state,varargin)
        set(hg,'Visible',state);
    end
%% C0 Open,Close figure set
    function FigDataO1(state,varargin)
        set(hh,'Visible',state);
    end
%% C0 Open,Close figure adv
    function FigDataO2(state,varargin)
        set(hi,'Visible',state);
    end
%% C0 Actulizo los botones Enable on/off & The figure open/close visible on/off
    function EnableButton(listbutton,statetyp,statebutton)
        set(listbutton,statetyp,statebutton);
    end
%% C0
    function ListBoxCallback1(varargin) % Load workspace vars into list box
        awinc.ListBoxCallback(ui_all,true);
    end
%% C0
    function SetSaveOpt
        if isdeployed,
            set(ui_ckfig,'Value',1,'Enable','On','Visible','On');
            set(ui_ckeps,'Value',0,'Enable','On','Visible','On');
        end
    end
%% C0
    function AmevaClose(varargin) % Close all Ameva windows
        awinc.AmevaClose([hi,hg,hf,hh], ...
            {[upper(nmfctn(1)),nmfctn(2:end),'Data'] [upper(nmfctn(1)),nmfctn(2:end),'Settings'] [upper(nmfctn(1)),nmfctn(2:end),'NewFit']});
    end
%% C1 Update cell and matrix name
    function UpdateNameAll(source,event)
        nval=get(source,'UserData');
        awinc.AmevaUpdateName(nval,ui_cm,ui_all,ui_td)
        if ismember(nval,[1 2 3]), ltsplot(nval); end% Refresh plot 
    end
%% C1 Refresh plot
    function ltsplot(nlv,varargin)
        if get(ui_all(nlv),'Value')>1,
            nlvn=get(ui_td(nlv),'String');
            nlvn=char(nlvn(1));
            try
                awinc.AmevaEvalData(nlv,ui_cm,ui_all);
                X_=awinc.data;
                if not(isfloat(X_)), disp(texto('notDoubleVector')); return; end
                nmsize=size(X_);
                set(ui_td(nlv),'string',{nlvn,sprintf('(%dx%d) %s',nmsize(1),nmsize(2),class(X_))});
                set(0,'CurrentFigure',hf); set(hf,'CurrentAxes',ax1); cla(ax1,'reset');
                if min(nmsize)==1
                    plot(ax1,X_,'k');
                else
                    plot(ax1,X_);
                end
                grid on;
                title(ax1,'Serie','FontWeight','bold');
            catch ME
                disp(texto('variableErase'));
                rethrow(ME);
            end
        end
    end
%% C0
function AmevaPlotToPrint(varargin) % List of fig
    awinc.AmevaPlotToPrint(ui_pp,carpeta);
end
%% actualizar qlim
function ActCorre(varargin)
	set(ui_lt,'String',round(100*get(ui_le,'Value'))/100);
end
%% Apply: comprobamos los datos y creo el directorio de trabajo
function AmevaSetData(varargin)
    ione=get(ui_all(1),'Value');%ins
    sone=get(ui_all(1),'String');%ins
    itwo=get(ui_all(2),'Value');%rea
    ithr=get(ui_all(3),'Value');%theta
    ifou=get(ui_all(4),'Value');%time
    Hs_Ins=[];Hs_Rea=[];Dir_Rea=[];date_time=[];Hscal=[];
    if ione>1,
        awinc.AmevaEvalData(1,ui_cm,ui_all);
        Hs_Ins=awinc.data;
    else
        error(texto('xData'));
    end
    if itwo>1,
        awinc.AmevaEvalData(2,ui_cm,ui_all);
        Hs_Rea=awinc.data;
    else
        error(texto('xData'));
    end
    if ithr>1,
        awinc.AmevaEvalData(3,ui_cm,ui_all);
        Dir_Rea=awinc.data;
        EnableButton([ui_cm(6),ui_all(6),ui_nd],'Enable','On');
        ExistDir=true;
    else
        Dir_Rea=sort(unifrnd(179.125,180.125,size(Hs_Rea,1),size(Hs_Rea,2)));
        EnableButton([ui_cm(6),ui_all(6),ui_nd],'Enable','Off');
        ExistDir=false;
    end
    set(hg,'Visible','off');set(ui_tbd,'String',texto('applyData'));drawnow;
    if ifou>1,
        awinc.AmevaEvalData(4,ui_cm,ui_all);
        date_time=awinc.data;
        awinc.AmevaTimeCheck(ui_tbd,date_time);
        if awinc.state==true,
            disp(texto('dataFormat_1'));
            set(hg,'Visible','on'); return;
        end
        tvr_name='Time';
        EnableButton([ui_cm(7),ui_all(7)],'Enable','On');
    else
        date_time=(1:length(Hs_Ins))';
        tvr_name='';
        EnableButton([ui_cm(7),ui_all(7)],'Enable','Off');
    end
    %compruebo que las variables sean diferentes
    awinc.AmevaVarCheck(ui_tbd,Hs_Rea,Hs_Ins,'HsR-Data','HsI-Data');          if awinc.state, set(hg,'Visible','on'); return; end,
    awinc.AmevaVarCheck(ui_tbd,Dir_Rea,Hs_Ins,'Dir','HsI-Data');              if awinc.state, set(hg,'Visible','on'); return; end,
    awinc.AmevaVarCheck(ui_tbd,Dir_Rea,Hs_Rea,'Dir','HsR-Data');              if awinc.state, set(hg,'Visible','on'); return; end,
    %compruebo si las variables seleccionadas son las filtradas
    if (strncmp(sone{ione},'outlierf',8))
        outliervarselect='Yes';
    end
    var_name=get(ui_fs,'String');
    var_unit=get(ui_fu,'String');
    set(ui_tp,'String',texto('buttonPlot_1'),'Value',1);
    set(ui_pp,'String',texto('buttonPlot_2'),'Value',1);

    %Elimino los NaNs de Hs_Ins
    Hs_Rea(isnan(Hs_Ins)) = [];
    Dir_Rea(isnan(Hs_Ins)) = [];
    date_time(isnan(Hs_Ins)) = [];
    Hs_Ins(isnan(Hs_Ins)) = [];
    %Elimino los NaNs de Hs_Rea
    Hs_Ins(isnan(Hs_Rea)) = [];
    Dir_Rea(isnan(Hs_Rea)) = [];
    date_time(isnan(Hs_Rea)) = [];
    Hs_Rea(isnan(Hs_Rea)) = [];
    %Elimino los cero de Hs_Ins
    Hs_Rea(Hs_Ins==0) = [];
    Dir_Rea(Hs_Ins==0) = [];
    date_time(Hs_Ins==0) = [];
    Hs_Ins(Hs_Ins==0) = [];
    %Elimino los cero de Hs_Rea
    Hs_Ins(Hs_Rea==0) = [];
    Dir_Rea(Hs_Rea==0) = [];
    date_time(Hs_Rea==0) = [];
    Hs_Rea(Hs_Rea==0) = [];
    disp(texto('seriesNaN'));
    
    if ExistDir
        if all(sign(Dir_Rea-180))==-1, %a=0 radian
            disp(texto('dataRadian'));
            if (any(Dir_Rea<0) || any(Dir_Rea>2*pi))
                set(ui_tbd,'String',texto('notAngle'));drawnow; return; 
            end;
        else%a=0 grados
            disp(texto('dataDirec'));
            if (any(Dir_Rea<0) || any(Dir_Rea>360))
                set(ui_tbd,'String',texto('notAngle'));drawnow; return; 
            end
        end
    end
    
    if(length(Hs_Rea)<1000), set(ui_tbd,'String',texto('notEnoughData'));end
    
    %Creo el directorio de trabajo para almacenamiento
    awinc.NewFolderCheck(carpeta,nmfctn);carpeta=awinc.carpeta;% Methods for "amevaclass"
    %set OutLier WorldFilter callback
    if ~isempty(Hs_Ins) && ~isempty(Hs_Rea) && ~isempty(Dir_Rea),
        set(ui_otl,'Enable','on','Callback',@(source,event)worldfilter(Hs_Ins,Hs_Rea,Dir_Rea,date_time,[],0.95,carpeta,idioma,var_name,var_unit));
    else
        set(ui_otl,'Enable','off');
        disp(texto('enableClassification'));
    end
    
    %re-inicializo los ejes, variables y la tabla
    set(ui_tbd,'String',texto('timeOptions'));
    set(ui_start,'Enable','on');
    set(ui_nf,'Enable','off');
    set(ui_avm,'Enable','on');
    %set(hf,'Toolbar','figure');
    set(ui_ttd,'String',' ','Value',1,'Visible','Off','Max',1);drawnow;
    %
    set(0,'CurrentFigure',hf);
	set(hf,'CurrentAxes',ax1);cla(ax1,'reset');
    InsReaCalPlot(date_time,Hs_Ins,Hs_Rea,Hscal,date_time_n,Hs_n,Hscal_n,Hsup,Hslo,Hsup_n,Hslo_n,0,idioma,var_name,new_var_name,var_unit,tvr_name,ax1);
    set(ui_tb,'String',[nmfctn,texto('startCalib')]);drawnow ;
    if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
end
%% Run data
function CalibrationRun(varargin)
    EnableButton([ui_tp,ui_pp,ui_nf,ui_avm,ui_start,ui_data,ui_otl],'Enable','off');
    %this have a valid children for figure
    if isempty(validchildren)
        validchildren=get(gcf,'children');
    else
        newchildren=get(gcf,'children');%borro los nuevos children de la figura
        if length(newchildren)>length(validchildren) || not(isequal(newchildren,validchildren)),
            [CI,II]=setdiff(newchildren,validchildren);
            delete(newchildren(II));
        end
    end
    set(hg,'Visible','off');
    set(hh,'Visible','off');
    set(hi,'Visible','off');
    set(ui_ttd,'String',' ','Value',1,'Visible','Off','Max',1);
    set(ui_tb,'String',[nmfctn,texto('run')]);drawnow ;

    nquan=str2double(get(ui_nq,'String'));
    increthet=360/str2double(get(ui_nd,'String'));
    qlim=str2double(get(ui_lt,'String'));
    nivconf=str2double(get(ui_nc,'String'));
    
    %pregunto con que datos va a realizar la calibracion si existen datos filtrados
    ExistOutLierVar=get(hf,'UserData');
    if strcmp(ExistOutLierVar,texto('yes'));
        if strcmp(outliervarselect,texto('no')),
            StrVarQuestion=texto('calibrateOriginal');
        elseif strcmp(outliervarselect,texto('yes')),
            StrVarQuestion=texto('calibrateFilter');
        end
        selection = questdlg(StrVarQuestion,texto('calibrateOriFil'),texto('yes'),texto('no'),texto('yes'));
        if strcmp(selection,texto('no')),
            set(hg,'Visible','on');
            return;
        end
    end
    if exist('statusbar.m','file')==2
        statusbar(hf);  % delete status bar from current figure
        statusbar(0, texto('desktop'));
        statusbar(hf, texto('wait'));
        statusbar('Processing %d of %d (%.1f%%)...',1,100,1);
    end
    cla(ax1,'reset'); drawnow;
    %Run function
    try
        %save options
        svopt(1)=get(ui_ckfig,'Value');%fig
        svopt(2)=get(ui_ckpng,'Value');%png
        svopt(3)=get(ui_ckeps,'Value');%eps
        [Hscal,thetaj,pdef,maxHsR,deltamaxHsR,stdvHs,A,std,residuo,HsI,HsR,Hlim,calidadRHO,statmoments,DirQ,...
            nq,Hs_NCQ,Hs_CQ,Hs_SATQ,aj,ajlo,ajup,bj,bjlo,bjup,dj,djlo,djup,DirNoQ,np,Hsup,Hslo,n,x_G,y_G,stdvHs_G] =...
            CalibraSplineQuantile (Hs_Ins,Hs_Rea,Dir_Rea,nquan,increthet,qlim,nivconf,svopt,carpeta,idioma,var_name,var_unit);
        if exist('statusbar.m','file')==2, statusbar('Processing %d of %d (%.1f%%)...',75,100,75); end
        %Sting name for the plots
        i=1;%se cambia en calibration y CalibraSplineQuantile
        if true,  tpstring{1,i}='CDF'; i=i+1; end
        if length(HsR)>1,  tpstring{1,i}='Scatter IR'; i=i+1; end
        if length(Hscal)>1,  tpstring{1,i}='Scatter CI'; i=i+1; end
        if length(Hs_NCQ)>1,  tpstring{1,i}='Quantiles R'; i=i+1; end
        if length(Hs_CQ)>1,  tpstring{1,i}='Quantiles C'; i=i+1; end
        if length(Hs_SATQ)>1,  tpstring{1,i}='Quantiles I'; i=i+1; end
        if qlim ~=0 && qlim > 0 && qlim < 0.99, tpstring{1,i}='Parameters CIc';  i=i+1;
        else tpstring{1,i}='Parameters CI'; i=i+1; end
        %if true,  tpstring{1,i}='Parameters Table'; i=i+1; end %NO esta comentado en CalibraSplineQuantile
        if true,  tpstring{1,i}='Rose Cal/Rea'; i=i+1; end
        if true,  tpstring{1,i}='Rose Rea'; i=i+1; end;
        if true,  tpstring{1,i}='Rose Ins'; i=i+1; end;
        if length(Hscal)>1,  tpstring{1,i}='X rea, X ins'; end;%se cambia en calibration y CalibraSplineQuantile
        if exist('statusbar.m','file')==2, statusbar(hf); end; % delete status bar from current figure
        set(ui_tp,'String', tpstring);%Set Sting Plots. set(ui_pp,'String','Plot to print');
        %listar los plot para imprimir
        awinc.ListBoxCallbackP(ui_pp,carpeta);% Methods for "amevaclass"
        set(ui_tp,'Value',1);AmevaPlot;
    catch ME
        EnableButton([ui_tp,ui_pp,ui_nf,ui_avm,ui_start,ui_data,ui_otl],'Enable','on');
        statusbar(hf);  % delete status bar from current figure
        rethrow(ME);
    end
    
	%Salida de la tabla con los resultados
    numabc=length(thetaj);
    if qlim ~=0 && qlim > 0 && qlim < 0.99,
        faGAP{1,1}=[' Angle    a ',char(177),' CI95%    b ',char(177),' CI95%    c ',char(177),' CI95%'];% OJO OJO usar texto
        for i=1:numabc
            [linea]=lineaABC(num2str(thetaj(i)*180/pi,'%3.1f'),num2str(aj(i),'%1.2f'),num2str(aj(i)-ajlo(i),'%1.3f'),num2str(bj(i),'%1.2f'),num2str(bj(i)-bjlo(i),'%1.3f'),num2str(dj(i),'%1.2f'),num2str(dj(i)-djlo(i),'%1.3f'));
            faGAP{1,i+1}=linea;
        end
    else
        faGAP{1,1}=[' Angle    a ',char(177),' CI95%    b ',char(177),' CI95%'];% OJO OJO usar texto
        for i=1:numabc
            [linea]=lineaABC(num2str(thetaj(i)*180/pi,'%3.1f'),num2str(aj(i),'%1.2f'),num2str(aj(i)-ajlo(i),'%1.3f'),num2str(bj(i),'%1.2f'),num2str(bj(i)-bjlo(i),'%1.3f'));
            faGAP{1,i+1}=linea;
        end
    end
    set(ui_ttd,'String',faGAP,'Value',numabc,'Visible','on','Max',numabc,'FontSize',8,'FontWeight', 'light');
    refresh(hf);
	save([pwd,filesep,carpeta,filesep,'calibration[0][0].mat'],'date_time','-append');
    set(ui_tb,'String',[nmfctn,texto('newFitCalib')]);drawnow ;
	EnableButton([ui_tp,ui_pp,ui_nf,ui_avm,ui_start,ui_data,ui_otl],'Enable','on');
    if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
end

%% Ameva Set & Run New Fit
function AmevaSetNewFit(varargin)
    %re-inicializo los ejes y variables
    
    disp([nmfctn,texto('run')]);
    try
        new_var_name=get(ui_nfs,'String');%New var name
        %save options
        svopt(1)=get(ui_ckfig,'Value');%fig
        svopt(2)=get(ui_ckpng,'Value');%png
        svopt(3)=get(ui_ckeps,'Value');%eps
        %var        
        ione=get(ui_all(5),'Value');%ins new
        itwo=get(ui_all(6),'Value');%dir new
        ithr=get(ui_all(7),'Value');%time new
        if ione>1,
            awinc.AmevaEvalData(5,ui_cm,ui_all);
            Hs_n=awinc.data;
        else
            error(texto('xData'));
        end
        if itwo>1,
            awinc.AmevaEvalData(6,ui_cm,ui_all);
            Dir_n=awinc.data;
        else
            Dir_n=sort(unifrnd(179.125,180.125,size(Hs_n,1),size(Hs_n,2)));
        end
    	EnableButton([ui_tp,ui_pp,ui_nf,ui_avm,ui_start,ui_data,ui_otl],'Enable','off');
        set(ui_tbn,'String',texto('applyData'));drawnow;
        if ithr>1,
            awinc.AmevaEvalData(7,ui_cm,ui_all);
            date_time_n=awinc.data;
            awinc.AmevaTimeCheck(ui_tbn,date_time_n);	
            if awinc.state==true,
                disp(texto('dataFormat_1'));
                set(hh,'Visible','on'); return;
            end
        else
            date_time_n=(1:length(Hs_n))';
        end
        %compruebo que las variables sean diferentes
        awinc.AmevaVarCheck(ui_tbn,Dir_n,Hs_n,'Dir','Hsn-Data');                  if awinc.state, set(hh,'Visible','on'); return; end,
        if(length(Hs_n)<1000), set(ui_tbn,'String',texto('notEnoughData'));end

        set(hh,'Visible','off');
        set(ui_tb,'String',[nmfctn,texto('run')]);drawnow ;
        Hscal_n=CalibraSplineNLP(Hs_n,Dir_n,thetaj,pdef,maxHsR,deltamaxHsR);
        GraficosAdicionales(Hs_n,Dir_n,HsR,Dir_Rea,maxHsR,Hscal,Hscal_n,svopt,carpeta,idioma,var_name,var_unit);

        %listar los plot para imprimir
        tpstring=get(ui_tp,'String');
        if length(Hs_n)>1 && sum(cell2mat(strfind(tpstring,'Xrnew, Xsne')))<1,
            tpstring{length(tpstring)+1}='Xrnew, Xsnew';
        end
        set(ui_tp,'String', tpstring);clear tpstring;

        if get(ui_cb,'Value')==1, %para saber si se calcula la banda de confianza
            [Hsup_n,Hslo_n]=uplownewcal(Dir_n,Hs_n,Hscal_n,x_G,y_G,nivconf,stdvHs_G);
        else
            Hsup_n=[];Hslo_n=[];
        end
        save(fullfile(pwd,carpeta,['calibrated_data',new_var_name,'.mat']),'date_time_n','Hs_n','Dir_n','Hscal_n','Hsup_n','Hslo_n');
        %save new plot
        h=figure('Visible','off');
        InsReaCalPlot(date_time,HsI,HsR,Hscal,date_time_n,Hs_n,Hscal_n,Hsup,Hslo,Hsup_n,Hslo_n,1,idioma,var_name,new_var_name,var_unit,tvr_name);
        set(h, 'color', 'w');
        amevasavefigure(h,carpeta,svopt,'Xrnew,Xsnew');
        %listar los plot para imprimir
        awinc.ListBoxCallbackP(ui_pp,carpeta);% Methods for "amevaclass"
    catch ME
        EnableButton([ui_tp,ui_pp,ui_nf,ui_avm,ui_start,ui_data,ui_otl],'Enable','on');
        rethrow(ME);
    end
	EnableButton([ui_tp,ui_pp,ui_nf,ui_avm,ui_start,ui_data,ui_otl],'Enable','on');
    set(ui_tb,'String',[nmfctn,texto('variableSave_1'),new_var_name,texto('variableSave_2')]);drawnow ;
    disp(texto('finishCalib'));
    if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
end

%%Ameva plot
function AmevaPlot(varargin)
    [mins,nins]=size(get(ui_tp,'String'));
	iins=get(ui_tp,'String');
    [ni mi]=size(iins);
    if ni==1, return; end;
    plotname=iins(get(ui_tp,'Value'));%plot name

    if ~ishandle(ax1), disp([texto('plot') ' ' nmfctn]); return; end
    
    newchildren=get(gcf,'children');%borro los nuevos children de la figura
    if length(newchildren)>length(validchildren) || not(isequal(newchildren,validchildren)),
        [CI,II]=setdiff(newchildren,validchildren);
        delete(newchildren(II));
    end
    newchildren=get(gcf,'children');%reset all axis
    for i=1:length(newchildren)%reset only axes !!
        if strcmp(get(newchildren(i),'type'),'axes')
            newchildren(i);
            cla(newchildren(i),'reset');
            axis normal;%selects ax1 as the current axes
        end
    end
    
    if ~strcmp(get(hf,'NextPlot'),'add'), set(hf,'NextPlot','add'); end;
    
    try
        if strcmp(plotname,'Xrnew, Xsnew'),%   X_Ins, X_Rea new
            InsReaCalPlot(date_time,Hs_Ins,Hs_Rea,Hscal,date_time_n,Hs_n,Hscal_n,Hsup,Hslo,Hsup_n,Hslo_n,1,idioma,var_name,new_var_name,var_unit,tvr_name,ax1);
        else
            calibrationplots(HsI,HsR,Dir_Rea,nq,Hscal,Hlim,Hsup,Hslo,n,nivconf,Hs_NCQ,Hs_CQ,Hs_SATQ,DirQ,qlim,aj,ajlo,ajup,bj,bjlo,bjup,dj,djlo,djup,DirNoQ,np,thetaj,calidadRHO,statmoments,plotname,svopt,carpeta,idioma,var_name,var_unit,ax1);
        end
    catch ME
        rethrow(ME);
    end
	%Salida de la tabla con los resultados
    numabc=length(thetaj);
    if qlim ~=0 && qlim > 0 && qlim < 0.99,
        faGAP{1,1}=[' Angle    a ',char(177),' CI95%    b ',char(177),' CI95%    c ',char(177),' CI95%'];
        for i=1:numabc
            [linea]=lineaABC(num2str(thetaj(i)*180/pi,'%3.1f'),num2str(aj(i),'%1.2f'),num2str(aj(i)-ajlo(i),'%1.3f'),num2str(bj(i),'%1.2f'),num2str(bj(i)-bjlo(i),'%1.3f'),num2str(dj(i),'%1.2f'),num2str(dj(i)-djlo(i),'%1.3f'));
            faGAP{1,i+1}=linea;
        end
    else
        faGAP{1,1}=[' Angle    a ',char(177),' CI95%    b ',char(177),' CI95%'];
        for i=1:numabc
            [linea]=lineaABC(num2str(thetaj(i)*180/pi,'%3.1f'),num2str(aj(i),'%1.2f'),num2str(aj(i)-ajlo(i),'%1.3f'),num2str(bj(i),'%1.2f'),num2str(bj(i)-bjlo(i),'%1.3f'));
            faGAP{1,i+1}=linea;
        end
    end
    set(ui_tb,'String',[nmfctn,texto('startCalib')]);drawnow ;
    set(ui_ttd,'String',faGAP,'Value',numabc,'Visible','on','Max',numabc,'FontSize',8,'FontWeight', 'light');
    if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
end


end
