function worldfilter(varargin)
%   WORLDFILTER. Outlier filter
%   WORLDFILTER (worldfilter.m) opens a graphical user interface for displaying the main program posibility.
%   Tested on Ubuntu trusty v.14.04.1(developed), Windows 7 and Mac Os X v10.9.4 (MATLAB:R2013a)
%   To load test data, please click on: -Help, Load ameva test data-, to send
%   data to matlab worksapce and work with this.
% 
% Examples 1
%   ameva-->Data Preprocesing-->Oulier Filter Detection
% Examples 2
%   load ameva;
%   worldfilter(excal_HsI,excal_HsR,excal_Dire);
% Examples 3
%   load ameva;
%   conf = 0.9998;          %Confidence level
%   alfa = 0.95;           %Filter
%   carpeta='sample_dir_wf';	%Directory name-relative to work directory
%   idioma='eng';           %idioma: 'eng' o 'esp' falta terminar
%   var_name='Hs';          %Variable name
%   var_unit='m';           %Variable unit
%   worldfilter(excal_HsI,excal_HsR,excal_Dire,excal_datime,conf,alfa,carpeta,idioma,var_name,var_unit);
%
% See also: 
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   worldfilter(main program tool v1.1.0 (150420)).
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows 7
%   04-07-2011 - The first distribution version
%   07-05-2013 - Old distribution version
%   27-05-2013 - Old distribution version
%   20-08-2014 - Old distribution version
%   20-04-2015 - Last distribution version

versionumber='v1.1.0 (150420)';
nmfctn='worldfilter';
Ax1Pos=[0.39 0.2 0.55 0.7];
FigPos=[50 100 1250 800];
screenSize=get(0,'ScreenSize');
figPos=[(screenSize(3)-FigPos(3)-FigPos(1)) (screenSize(4)-FigPos(4)-FigPos(2)) FigPos(3) FigPos(4)];

if ~isempty(findobj('Tag',['Tag_' nmfctn]));
    figPos=get(findobj('Tag',['Tag_' nmfctn]),'Position');
    close(findobj('Tag',['Tag_' nmfctn]));
end

%Lista de las figuras que se van a usar en este programa
hf=[];%ventana principal
hg=[];%ventana auxiliar
%global hsp;
% Information for all buttons and Spacing between the button
colorG='w';	%Color general
btnWid=0.120;			
btnHt=0.05;
spacing=0.015;			
top=0.95;
xPos=[1-btnWid-spacing 1-btnWid-spacing];
var_lab={'Time:' '* X-Data i:' '* Y-Data r:' 'Dir'};
%GLOBAL VARIALBES solo usar las necesarias!!
awinc=amevaclass;%ameva windows commons methods
awinc.nmfctn=nmfctn;
carpeta=[];
X=[]; Y=[]; Time=[]; Dir=[]; idx=[];
latcn=0;loncn=0;
ID=0;
idioma='eng'; usertype=1;
if nargin==12,
    X=varargin{1};
    Y=varargin{2};
    Dir=varargin{3};
    Time=varargin{4};
    conf=varargin{5};
    alfa=varargin{6};
    carpeta=varargin{7};
    idioma=varargin{8};
    var_name=varargin{9};
    var_unit=varargin{10};
    latcn=varargin{11};
    loncn=varargin{12};
    texto=amevaclass.amevatextoidioma(idioma);
elseif nargin==10,
    X=varargin{1};
    Y=varargin{2};
    Dir=varargin{3};
    Time=varargin{4};
    conf=varargin{5};
    alfa=varargin{6};
    carpeta=varargin{7};
    idioma=varargin{8};
    var_name=varargin{9};
    var_unit=varargin{10};
    texto=amevaclass.amevatextoidioma(idioma);
elseif nargin==3, 
    X=varargin{1};
    Y=varargin{2};
    Dir=varargin{3};
    texto=amevaclass.amevatextoidioma(idioma);
elseif nargin==1
    idioma=varargin{1}.idioma;
    usertype=varargin{1}.usertype;
    texto=amevaclass.amevatextoidioma(idioma);
elseif nargin==0, %Los datos se seleccionan desde el espacio de trabajo
    texto=amevaclass.amevatextoidioma(idioma);
    disp(texto('dataClasif'));
else
    texto=amevaclass.amevatextoidioma(idioma);
    error(texto('xyData'));
end
namesoftware=[texto('nameWorldfilter'),' ',versionumber];
%compruebo
if ~exist('var_name','var') || isempty(var_name), var_name='Hs'; end %Nombre de 3 variables por defecto sino existen
if ~exist('var_unit','var') || isempty(var_unit), var_unit='m'; end %Unidades por defecto sino existen
if ~exist('svopt','var') || isempty(svopt), svopt=[0 0 0]; end %save option 1-fig 2-png 3-eps (1/0)
if ~exist('conf','var') || isempty(conf), conf=[0.9 0.95 0.99 0.999 0.9999]; end; %Confidence level
if ~exist('alfa','var') || isempty(alfa), alfa=0.95; end; %Confidence level
if ~isempty(find(isnan(X),1)), disp(texto('nanX')); end
if ~isempty(find(isnan(Y),1)), disp(texto('nanY')); end
if ~isempty(find(isnan(Dir),1)), disp(texto('nanDir')); end

%---LA FIGURA-VENTANA PRINCIPAL-
hf=figure('Name',namesoftware,'Color',colorG,'CloseRequestFcn',@AmevaClose, ...
    'NumberTitle','Off','DockControls','Off','Tag',['Tag_' nmfctn],'NextPlot','new','Resize','off');
if ~isempty(figPos), set(hf,'Position',figPos); end

if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

%   Primer plot: Medias
hsp(1)=subplot('position',[0.55 0.7 0.18 0.25],'Parent',hf);title(texto('mean_1'));
%   Segundo plot: Residuo
hsp(2)=subplot('position',[0.035 0.7 0.47 0.25],'Parent',hf);title(texto('residue'));
%   Tercer plot: Scatter
hsp(3)=subplot('position',[0.035 0.05 0.47 0.6],'Parent',hf);title(texto('scatter'));
%   Cuarto plot: Tablita de informacion
hsp(4)=subplot('position',[0.55 0.35 0.18 0.30],'Parent',hf);title(texto('table'));
hsp(5)=subplot('position',[0.52 0.05 0.23 0.23],'Parent',hf);title(texto('legend'));
hsp(6)=subplot('position',[0.85 0.90 0.15 0.05],'Parent',hf);
[fmus,fsis]=amevaclass.textmodel(6);
htext(1)=text(0,1,texto('typeModel'),'FontSize',10,'Parent',hsp(6));
htext(2)=text(0,0.5,fmus,'Interpreter','latex','FontSize',11,'Parent',hsp(6));
htext(3)=text(0,0,fsis,'Interpreter','latex','FontSize',11,'Parent',hsp(6));axis off;

btnN=2;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_kindv=uicontrol('Style','popup','Units','normalized','Parent',hf,'String',{'1' '2' '3' '4' '5' '6'}, ...
    'Position',[xPos(1) yPos btnWid btnHt],'value',6,'Callback',@OutlierText);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_data=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'String','Data', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataOO('on'));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_confs=uicontrol('Style','text','Units','normalized','Parent',hf,'String','Confidence Level (0-1)', ...
    'Position',[xPos(1) yPos-btnHt/2 btnWid btnHt],'BackgroundColor','w');
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_confv=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(conf), ...
    'Position',[xPos(1) yPos btnWid btnHt]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_start=uicontrol('Style','push','Units','normalized','Parent',hf,'String','Start', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@Outlier,'Enable','Off');
if not(isempty(X)), set(ui_start,'Enable','On'); end

ui_tb   =uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.01 0.004 0.23 0.016],...
    'String',[nmfctn,texto('applyFilter')],'HorizontalAlignment','left');

set(hf,'NextPlot','new');

%% Data
[figPos,xPos,btnWid,btnHt,top,spacing]=awinc.amevaconst('setting');
hg=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Data'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataOO('off'),'WindowButtonMotionFcn',@ListBoxCallback1,...
    'NumberTitle','Off','MenuBar','none','Position',figPos,'Resize','Off','Visible','Off','NextPlot','new');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
amvcnf.idioma=idioma;amvcnf.usertype=usertype;

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(end) btnHt],...
    'String','Data & WorkSpace','Callback',@(source,event)amevaworkspace(amvcnf));
for ii=1:length(var_lab)
    btnN=btnN+1;
    yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
    ui_td(ii)=uicontrol('Style','text','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(6) btnHt],'String',var_lab(ii),'BackgroundColor',colorG,'HorizontalAlignment','right');
    ui_all(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid(1) btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',ii);
    ui_cm(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid(5) btnHt],'String','(none)','visible','off');
end; clear ii;

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hg,'String','X name & unit:','Position',[xPos(1) yPos btnWid(1) btnHt],'BackgroundColor',colorG,'HorizontalAlignment','right');
ui_fs=uicontrol('Style','edit','Units','normalized','Parent',hg,'String','Hs','Position',[xPos(2) yPos btnWid(1)/2 btnHt]);
ui_fu=uicontrol('Style','edit','Units','normalized','Parent',hg,'String','m','Position',[xPos(2)+btnWid(1)/2 yPos btnWid(1)/2 btnHt]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Set Data','Position',[xPos(2) yPos btnWid(1) btnHt],'Callback',@AmevaSetData);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Close','Position',[xPos(1) yPos btnWid(1) btnHt],'Callback',@(source,event)FigDataOO('off'));

uicontrol('Style','frame','Units','normalized','Parent',hg,'Position',[0 0 1 0.05]);
ui_tbd=uicontrol('Style','Text','Units','normalized','Parent',hg,'String',texto('dataWorkspace'),'Position',[0.01 0.004 0.99 0.04],'HorizontalAlignment','left');

if ~strcmp(get(hg,'NextPlot'),'new'), set(hg,'NextPlot','new'); end;

%% C0 Open,Close figure data
    function FigDataOO(state,source,event)
        set(hg,'Visible',state);
    end
%% C0 Actulizo los botones Enable on/off & The figure open/close visible on/off
    function EnableButton(listbutton,statetyp,statebutton)
        set(listbutton,statetyp,statebutton);
    end
%% C0
    function ListBoxCallback1(source,event) % Load workspace vars into list box
        awinc.ListBoxCallback(ui_all,true);
    end
%% C0
    function AmevaClose(source,event) % Close all Ameva windows
        awinc.AmevaClose([hf,hg], ...
            {[upper(nmfctn(1)),nmfctn(2:end),'Data']});
    end
%% C1 Update cell and matrix name
    function UpdateNameAll(source,event)
        nval=get(source,'UserData');
        awinc.AmevaUpdateName(nval,ui_cm,ui_all,ui_td);
    end

%% update text model
function OutlierText(source,event)
    [fmus,fsis]=amevaclass.textmodel(get(ui_kindv,'Value'));
    set(htext(1),'String',texto('typeModel'));
    set(htext(2),'String',fmus);
    set(htext(3),'String',fsis);
end
%% P0 Set data comprobamos que los Data para calcular
    function AmevaSetData(source,event)
        var_name=get(ui_fs,'String');
        var_unit=get(ui_fu,'String');
        ione=get(ui_all(1),'Value');
        itwo=get(ui_all(2),'Value');
        ithr=get(ui_all(3),'Value');
        ifou=get(ui_all(4),'Value');
        X=[]; Y=[]; Time=[]; Dir=[]; idx=[];
        if itwo>1,
            awinc.AmevaEvalData(2,ui_cm,ui_all);
            X=awinc.data; %instrumental
        else
            error(texto('xInsNeces'));
        end
        if ithr>1,
            awinc.AmevaEvalData(3,ui_cm,ui_all);
            Y=awinc.data; %reanalisis
        else
            error(texto('xReaNeces'));
        end
        set(hg,'Visible','off');set(ui_tbd,'String',texto('applyData'));drawnow;
        if ione>1,
            awinc.AmevaEvalData(1,ui_cm,ui_all);
            Time=awinc.data;
            awinc.AmevaTimeCheck(ui_tbd,Time);	
            if awinc.state==true,
                disp(texto('dataFormat_1'));
                set(hg,'Visible','on'); return;
            end
        else
            Time=[];
        end
        if ifou>1,
            awinc.AmevaEvalData(4,ui_cm,ui_all);
            Dir=awinc.data;
        else
            Dir=[];
        end
        %compruebo que las tres variables sean diferentes
        awinc.AmevaVarCheck(ui_tbd,Y,X,'X-Data r','X-Data i');       if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,Dir,X,'Dir','X-Data i');        if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,Dir,Y,'Dir','X-Data r');        if awinc.state, set(hg,'Visible','on'); return; end,
        %set new data
        set(ui_tbd,'String',texto('timeOptions'));drawnow ;
        %ActButton;
        %ActDefOption;
        EnableButton(ui_start,'Enable','On');
    end
%% elimino nans y ceros
function DeleteNansCeros
    %Elimino los NaNs de X
    ceropos=isnan(X);
    X(ceropos) = [];
    Y(ceropos) = [];
    if ~isempty(Dir), Dir(ceropos) = []; end
    if ~isempty(Time), Time(ceropos) = []; end
    if ~isempty(ceropos), disp(texto('existNaN_1')); end
    %Elimino los NaNs de Y
    ceropos=isnan(Y);
    X(ceropos) = [];
    Y(ceropos) = [];
    if ~isempty(Dir), Dir(ceropos) = []; end
    if ~isempty(Time), Time(ceropos) = []; end
    if ~isempty(ceropos), disp(texto('existNaN_2')); end
    %elimino los menores e iguales a cero, se suponen alturas de olas
    %mayore que cero
    ceropos = (X>0 & Y>0);
    X = X(ceropos);
    Y = Y(ceropos);
    if ~isempty(Dir), Dir = Dir(ceropos); end
	if ~isempty(Time), Time = Time(ceropos); end
end

%% Model
function Outlier(source,event)
	% Creo el directorio de trabajo para almacenamiento
    awinc.NewFolderCheck(carpeta,nmfctn);carpeta=awinc.carpeta;% Methods for "amevaclass"
    
    % elimino los nans y los menores que cero, alturas positivas
    DeleteNansCeros;
    
    disp('as')
    
    set(hf,'NextPlot','Add');

    % calcula los outlier filter
    if ID~=get(ui_kindv,'Value') || ~exist([pwd,filesep,carpeta,filesep,'WOF.mat'],'file')
        conf=str2num(get(ui_confv,'String'));ConfLevelCheck;
        ID=get(ui_kindv,'Value');
        [idx,p,rN,fmu,upperx,lowerx,Derivatives,fval,exitflag,output,lambda,grad,hessian,residuo,fsigma,Omega,wJ]=outlierfilterRNoCte(Y,X,alfa,ID,conf);        
    elseif sum(conf)~=sum(str2num(get(ui_confv,'String'))) && ID==get(ui_kindv,'Value') && exist([pwd,filesep,carpeta,filesep,'WOF.mat'],'file')
        conf=str2num(get(ui_confv,'String'));ConfLevelCheck;
            wof=load([pwd,filesep,carpeta,filesep,'WOF.mat']);
            p=wof.p;
            rN=wof.rN;
            upperx=wof.upperx;
            lowerx=wof.lowerx;
            Derivatives=wof.Derivatives;
            auxmean=wof.auxmean;
            auxstd=wof.auxstd;
            idx=wof.idx;
            fmu=wof.fmu;
            Y=wof.Y;
            clear wof;
    else
        msgbox(texto('sameParame'),'Outlier');
        return;
    end
   
    %   Analisis discreto de los datos
    n = length(Y);
    ra = range(Y);
    incre = 0.25;
    interv = linspace(min(Y),max(Y),ceil(ra/incre));
    while length(interv)<=5,
        incre = incre/2;
        interv = linspace(min(Y),max(Y),ceil(ra/incre));
    end
    auxmean = zeros(length(interv)-1,1);
    auxstd = zeros(length(interv)-1,1);
    absrea = zeros(length(interv)-1,1);
    numdat = zeros(length(interv)-1,1);
    for j=1:length(interv)-1,
        aux = find(Y>interv(j) & Y<=interv(j+1));
        absrea(j)= (interv(j+1)+interv(j))/2;
        auxmean(j)= mean(X(aux));
        auxstd(j)= std(X(aux));
        if j==1,
            numdat(j)=length(aux)/n;
        else
            numdat(j)=numdat(j-1)+length(aux)/n;
        end
    end

    pos = find(numdat<=0.9);  
    if length(pos)<=1,
        pos = [pos(1); pos(1)+1];
    end
    
    if amevaTheseToolboxesInstalled('Curve Fitting Toolbox'),
        ft_ = fittype('poly1');
        % Fit this model using new data
        pos2 = find(~isnan(auxmean(1:pos(end))) & ~isnan(auxstd(1:pos(end))) & auxmean(1:pos(end))~=0 & auxstd(1:pos(end))~=0);
        cf_ = fit(log(auxmean(pos2)),log(auxstd(pos2)),ft_);
        intervins = linspace(min(X),max(X),ceil(ra/incre));
    end

    wfplots(X,Y,p,rN,upperx,fmu,absrea,auxmean,auxstd,conf,idx,ID,svopt,carpeta,idioma,var_name,var_unit,hf)
    
    annotation('rectangle',[0 0 1 1],'linewidth',2);%RECUADRO

    namewof = (['WOF[',num2str(latcn),'][',num2str(loncn),']']);
    % save duration vector and plots
    save([pwd,filesep,carpeta,filesep,namewof,'.mat'],'rN','auxmean','absrea','auxstd','fmu','conf','Y','X','ID','idx','p','upperx','lowerx','Derivatives');
    % save plot
    set(htext(1),'String','');
    set(htext(2),'String','');
    set(htext(3),'String','');
    EnableButton([ui_tb,ui_kindv,ui_confs,ui_confv,ui_data,ui_start],'Visible','off');
    set(hf,'PaperPositionMode','auto')
    saveas(hf,[pwd,filesep,carpeta,filesep,namewof,'.png']);%print(hf,'-dpng',[pwd,filesep,carpeta,filesep,'WOF.png']);
    EnableButton([ui_tb,ui_kindv,ui_confs,ui_confv,ui_data,ui_start],'Visible','on');
    OutlierText(ID);

    X_ = X; X_(idx{end}) = [];
    Y_ = Y; Y_(idx{end}) = [];
    if ~isempty(Dir), Dir_ = Dir; Dir_(idx{end}) = []; end
    if ~isempty(Time), Time_ = Time; Time_(idx{end}) = []; end
    
    assignin('base',['outlierf_','idx'],idx);
    assignin('base',['outlierf_','X'],X_);
    assignin('base',['outlierf_','Y'],Y_);
    if ~isempty(Dir), assignin('base',['outlierf_','Dir'],Dir_); end
    if ~isempty(Time), assignin('base',['outlierf_','Time'],Time_); end
    disp([datestr(now,'yyyymmddHHMMSS'), texto('outlierData')]);
    % To now that exist outlier data only if open Calibration Tool
    if ~isempty(findobj('Tag','Tag_calibration')),
    	set(findobj('Tag','Tag_calibration'),'UserData','Yes');
    end
    amevaws_update_listbox_ext;

    set(hf,'NextPlot','new');    
end
%% Comprobar la longitud maxima de conf
function ConfLevelCheck
    if length(conf)>5
        conf=conf(1:5);
        set(ui_confv,'String',num2str(conf));
        disp(texto('vectorConf'));
    end
end

end

