function wfplots(HsI,HsR,p,rN,upper,fmu,absrea,auxmean,auxstd,conf,idx,ID,svopt,carpeta,idioma,var_name,var_unit,hf,visb)
if ~exist('hf','var') || isempty(hf) || ishandle(hf)==0, 
    %---LA FIGURA-VENTANA PRINCIPAL-
    FigPos=[50 100 1250 800];
    screenSize=get(0,'ScreenSize');
    figPos=[(screenSize(3)-FigPos(3)-FigPos(1)) (screenSize(4)-FigPos(4)-FigPos(2)) FigPos(3) FigPos(4)];
    hf=figure('Name','Outlier Worldfilter Poster','Color','w','NumberTitle','off','Position',figPos,...
        'Resize','off','Visible','off');
end
if ~exist('idioma','var'), idioma='eng'; end
if ~exist('var_name','var'), var_name = ''; end
if ~exist('var_unit','var'), var_unit = ''; end
if exist('visb','var'), set(hf,'Visible',visb); end

%   Primer plot: Medias
hsp(1)=subplot('position',[0.55 0.7 0.18 0.25],'Parent',hf);title('1.Means vs. Standard deviation');
    set(hf,'CurrentAxes',hsp(1));cla(gca,'reset');
    wfmedias(p,auxmean,auxstd,ID)
%   Segundo plot: Residuos
hsp(2)=subplot('position',[0.035 0.7 0.47 0.25],'Parent',hf);title('2.Standarized Residuals');
    set(hf,'CurrentAxes',hsp(2));cla(gca,'reset');
    wfresiduos(rN,conf)
%   Tercer plot: Scatter
hsp(3)=subplot('position',[0.035 0.05 0.47 0.6],'Parent',hf);title('3.Scatter plot');
    set(hf,'CurrentAxes',hsp(3));cla(gca,'reset');
    wfscatter(HsI,HsR,fmu,absrea,auxmean,conf,idx,idioma,var_name,var_unit);
%   Cuarto plot: Tablita de informacion
hsp(4)=subplot('position',[0.55 0.35 0.18 0.30],'Parent',hf);title('4.');
    set(hf,'CurrentAxes',hsp(4));cla(gca,'reset');
    wftabla(p,upper,conf,idx,length(HsI))
    %5.Figura para explicar del scatter plot si existe la fiugura
if exist('amlegendegow.tif','file')==2,
hsp(5)=subplot('position',[0.52 0.05 0.18 0.23],'Parent',hf);
    set(hf,'CurrentAxes',hsp(5));
    apos=get(gca,'Position');
    set(gca,'Position',[apos(1) apos(2) apos(4) apos(4)],'YDir','Reverse');
    him=image(imread(which('amlegendegow.tif')));
    axis(gca,'image');
    axis(gca,'off');
	title(gca,'Figure legend','FontSize',9,'fontweight','bold');
    freezeColors(him);% para fijar el colormap
end
hsp(6)=subplot('position',[0.56 0.01 0.18 0.01],'Parent',hf);
    set(hf,'CurrentAxes',hsp(6));
    title(gca,'This figure allows interpreting the scatter plot on the left','FontSize',6.3,'fontweight','bold');
    set(gca,'XTick',[])
    set(gca,'XTickLabel',[])
    set(gca,'YTick',[])
    set(gca,'YTickLabel',[])
    axis(gca,'off')
    
    %guardo los plots de forma individual de la ficha
    if sum(svopt)>0 && exist(carpeta,'dir')
        savename='';savepath=[pwd,filesep,carpeta,filesep];
        tpstring={'medias','residuos','scatter'};
        for i=1:3,
            plotname=tpstring{i};
            h=figure('Visible','off');
            switch plotname
                case 'medias'
                wfmedias(p,auxmean,auxstd,ID)
                savename=[savepath,'wfmedias'];
                case 'residuos'
                wfresiduos(rN,conf)
                savename=[savepath,'wfresiduos'];
                case 'scatter'
                wfscatter(HsI,HsR,fmu,absrea,auxmean,conf,idx,idioma,var_name,var_unit);
                savename=[savepath,'wfscatter'];
                otherwise
                disp(['Warnig!.Not exist this plot case ',plotname]);
            end
            if svopt(3), saveas(h,[savename,'.eps'],'psc2'); end
            if svopt(2), saveas(h,[savename,'.png']); end
            set(h,'Visible','on');
            if svopt(1), saveas(h,[savename,'.fig']); end
            close(h);
        end
    end
end
