function wfscatter(HsI,HsR,fmu,absrea,auxmean,conf,idx,idioma,var_name,var_unit)
if ~exist('idioma','var'), idioma='eng'; end
if ~exist('var_name','var'), var_name = ''; end
if ~exist('var_unit','var'), var_unit = ''; end

%   Tercer plot: Scatter
    maximo = ceil(max(max(HsI),max(HsR)));
    [fmuord odf] = sort(fmu);
    plot(auxmean,absrea,'-.k','LineWidth',2);
    hold on;
    plot(fmuord,HsR(odf),'-b','LineWidth',2);
    
    legend('Empirical model','Fitted model','Location','NorthWest');
    set(gca,'FontSize',7);
    colors = [220 20 60;100 149 237;220 20 60;100 149 237;220 20 60]/255;
    for j = 1:length(conf),
            plot(HsI(idx{j}),HsR(idx{j}),'o','Color',colors(j,:),'MarkerFaceColor',colors(j,:),'MarkerSize',1.8)
            hold on
    end
    plot(HsI,HsR,'o','Color',[100/255 149/255 237/255],'MarkerFaceColor',[100/255 149/255 237/255],'MarkerSize',1.8)
    for j = 1:length(conf),
        plot(HsI(idx{j}),HsR(idx{j}),'o','Color',colors(j,:),'MarkerFaceColor',colors(j,:),'MarkerSize',1.8)
        hold on
    end
    plot(HsI(idx{j}),HsR(idx{j}),'o','Color',[0 0 0],'MarkerFaceColor',[0 0 0],'MarkerSize',3)
    plot([0 maximo],[0 maximo],'k')
    plot(fmuord,HsR(odf),'-b','LineWidth',2)
    plot(auxmean,absrea,'-.k','LineWidth',2)

    title('Scatter plot','fontsize',11,'fontweight','bold');
    xlim([0 maximo])
    ylim([0 maximo])
    grid on
    xlabel(['{',var_name,'}^I[',var_unit,']'],'fontsize',7,'fontweight','bold','Position',[maximo/2 -maximo*0.2/5]);
    ylabel(['{',var_name,'}^R[',var_unit,']'],'fontsize',7,'fontweight','bold','Position',[-maximo*0.2/5 maximo/2]);
end
