function ScatterQQ(HsSat,HsReaCal,Hlim,rho,statmoments,var_name,var_unit,IR,fs,scattipo,gcax)
% Scater Reanalisis y Calibrado green and red para la ficha de calibraiton
% y para los plots de calibracion
if nargin<9, % IF NO FONT SIZE, INITIALIZE IT
    fs = 11;
end
if nargin<10, % IF NO SCATTER TIPO, INITIALIZE IT
    scattipo = 'OC';
end
if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
if ~ishandle(gcax), disp('WARNING! ScatterCal with axes'); end
set(gcf,'CurrentAxes',gcax)

ScatterReaCal(HsSat,HsReaCal,[],[],Hlim,[],{var_name var_name},{var_unit var_unit},IR,fs,scattipo);
[R2, RMS, BIAS,CORR,SI] = correlaciones (HsSat,HsReaCal);
vv=axis(gcax);
A=[vv(1)+diff(vv(1:2))*0.63 vv(1)+diff(vv(1:2))*0.63 vv(1)+diff(vv(1:2))*0.98 vv(1)+diff(vv(1:2))*0.98 vv(1)+diff(vv(1:2))*0.63];
B=[vv(4)-diff(vv(3:4))*0.98 vv(4)-diff(vv(3:4))*0.76 vv(4)-diff(vv(3:4))*0.76 vv(4)-diff(vv(3:4))*0.98 vv(4)-diff(vv(3:4))*0.98];
fill(A,B,[1 1 1],'linestyle','none');
text(vv(1)+diff(vv(1:2))*0.56,vv(4)-diff(vv(3:4))*0.725,['BIAS=' num2str(sprintf('%1.3f',BIAS))],'FontSize',8);
text(vv(1)+diff(vv(1:2))*0.56,vv(4)-diff(vv(3:4))*0.8,['RMS=' num2str(sprintf('%1.3f',RMS))],'FontSize',8);
text(vv(1)+diff(vv(1:2))*0.56,vv(4)-diff(vv(3:4))*0.875,['\rho=' num2str(sprintf('%1.3f',rho))],'FontSize',8);
text(vv(1)+diff(vv(1:2))*0.56,vv(4)-diff(vv(3:4))*0.95,['RSI=' num2str(sprintf('%1.3f',SI))],'FontSize',8);
text(vv(1)+diff(vv(1:2))*0.03,vv(4)-diff(vv(3:4))*0.07,['\Delta\mu/\mu=' num2str(sprintf('%1.2f',statmoments(5,1)*100)),'%'],'FontSize',8);
text(vv(1)+diff(vv(1:2))*0.03,vv(4)-diff(vv(3:4))*0.145,['\Delta\sigma/\sigma=' num2str(sprintf('%1.2f',statmoments(6,1)*100)),'%'],'FontSize',8);
text(vv(1)+diff(vv(1:2))*0.03,vv(4)-diff(vv(3:4))*0.22,['\Delta\gamma/\gamma=' num2str(sprintf('%1.2f',statmoments(7,1)*100)),'%'],'FontSize',8);
text(vv(1)+diff(vv(1:2))*0.03,vv(4)-diff(vv(3:4))*0.295,['\Delta\xi/\xi=' num2str(sprintf('%1.2f',statmoments(8,1)*100)),'%'],'FontSize',8);
YT=get(gca, 'ytick');
set(gca, 'xtick',YT);
for I=1:length(YT)
    plot(gcax,[YT(1)-2 YT(end)+2],YT(I)*[1 1],':k','linewidth',1);
    plot(gcax,YT(I)*[1 1],[YT(1)-2 YT(end)+2],':k','linewidth',1);
end
set(gcf,'Renderer','zbuffer');
freezeColors;

end