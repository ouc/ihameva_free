function [Hscal,thetaj,pdef,maxHsR,deltamaxHsR,stdvHs,A,stdev,residuo,HsI,HsR,Hlim,calidadRHO,statmoments,DirQ,...
    nq,Hs_NCQ,Hs_CQ,Hs_SATQ,aj,ajlo,ajup,bj,bjlo,bjup,dj,djlo,djup,DirNoQ,np,Hsup,Hslo,n,x_G,y_G,stdvHs_G] ...
    = CalibraSplineQuantile (HsI,HsR,Dir,nq,increthet,qlim,nivconf,svopt,carpeta,idioma,var_name,var_unit,latcn,loncn)
%   La funcion CALIBRASPLINEQUEANTILE permite el calibrado direccional
%   mediante
%   la expresion a*H^b, y permite incorporar un tramo inicial lineal del 
%   tipo a*H para valores inferiores a un determinado quantil. Todo ellos 
%   empleando los quantiles para calibrar y suponiendo una variacion 
%   direccional suave con SPLINES
%
%   DATA:
%       HsI -> Valores de satelite o boya de referencia para la
%       calibracion
%       HsR -> Alturas de ola de reanalisis a calibrar
%       Dir -> Direcciones de reanalisis empleadas para calibrar (en radianes)
%       nq -> Numero de cuantiles por sector direccional que se van a
%       utilizar (default: 20)
%       increthet -> Anchura del sector direccional considerado en grados
%       (defaul: 22.5)
%       qlim -> Quantil limite que marca el limite superior del quiebro
%       entre el tramo lineal y el potencial (default 0)
%       nivconf -> Nivel de confianza para el calcuo de los intervalos
%       (default: 0.05)
%       carpeta -> String con el nombre de la carpeta en la que se van a
%       almacenar los graficos de diagnostico, si no existe no se
%       realizaran los graficos
%       A partir de residuo, devuelve lo necesario para hacer los graficos,
%       en orden
% Examples 1
%   load ameva;
%   [Hscal,thetaj,pdef,maxHsR,deltamaxHsR]=CalibraSplineQuantile(excal_HsI,excal_HsR,excal_Dire);
% Examples 2
%   load ameva;
%   nq=20;increthet=22.5;qlim=0;nivconf=0.05;
%   [Hscal,thetaj,pdef,maxHsR,deltamaxHsR]=CalibraSplineQuantile(excal_HsI,excal_HsR,excal_Dire,nq,increthet,qlim,nivconf,svopt);
% Examples 3
%   load calibration_data;%From a valid path result
%   CalibraSplineQuantile(HsI,HsR,Dir,nq,22.5,0,0.05,[0 1 0],'borrar','eng',Hs','m');

if nargin<3, error('Faltan datos, se necesitan tres series para poder calibrar'); end
if ~exist('nq','var'), nq=20; end
if ~exist('increthet','var'), increthet=22.5; end
if ~exist('qlim','var'), qlim=0; end
if ~exist('nivconf','var'), nivconf=0.05; end
if ~exist('idioma','var'), idioma='eng'; end
if ~exist('var_name','var'), var_name='Hs'; end
if ~exist('var_unit','var'), var_unit='m'; end
if ~exist('latcn','var'), latcn=0; end
if ~exist('loncn','var'), loncn=0; end
if ~exist('svopt','var') || isempty(svopt), svopt=[0 1 0]; end %save option 1-fig 2-png 3-eps (1/0)
if ~exist('carpeta','var') || isempty(carpeta), carpeta='calibration_sample_dir'; end;
if sum(svopt)>0, if ~exist([pwd filesep carpeta],'dir'),  mkdir(carpeta); end; end

%   Comprobacion de que los datos estan en radianes
%   Si no lo estan se transforman
if max(Dir)>2*pi,
    Dir = Dir*2*pi/360;
    disp('CalibraSplineQuantile, las direcciones se han transformado a radianes.');
end
%   Con la midificacion en la funcion fobjNLP no hace falta
% %   Ordeno todos los datos de menor a mayor angulo para el ajuste
% [Dir,IX] = sort(Dir);
% HsI = HsI(IX);
% HsR = HsR(IX);

%   Procedimiento de eliminacion de datos posiblemente erroneoa
%   Numero de datos disponibles para la calibracion
n=length(Dir);

%   Dado que se van a emplear cuantiles para la calibracion es necesario
%   calcularlos
if isempty(nq) || exist('nq','var')~=1,
    %   Numero de cuantiles por defecto
    nq = 20;
end
if isempty(increthet) || exist('increthet','var')~=1,
    %   Anchura de los sectores direccionales por defecto
    increthet = 22.5;
end
%   Calculo de los cuantiles que se desean calibrar equiespaciados en papel
%   de Gumbel
a = -log(-log(1/n));
b = -log(-log(1 - 5/n));
x = linspace(a,b,nq);
q = exp(-exp(-x));

%   Obtencion de los quantiles por sector direccional a partir de los datos
%   disponibles
%   Llamada a la funcion QQgeneration
[Hs_SQ,Hs_SATQ,DirQ,maxHsR] = QQgeneration (HsR,HsI,Dir,increthet,nq);

%   Transformo los datos direccionales de los cuantiles de grados a
%   radianes
DirQ = DirQ*(2*pi)/360;

%   Reordenacion de los datos de menor a mayor para la calibracion
[DirQ,IX] = sort(DirQ);
Hs_SATQ = Hs_SATQ(IX);
Hs_SQ = Hs_SQ(IX);

%   Calculo la posicion en radianes en la que se encuentran cada uno de los
%   puntos del spline distribuidos de forma uniforme a lo largo de la
%   circunferencia
np = 360/increthet+1;
%   Obtengo los angunlos en los que situar los puntos del spline
thetaj = linspace(0,2*pi,np)';

%   PROCEDIMIENTO DE OPTIMIZACION PARA EL AJUSTE
%   Selecciono las opciones del algoritmo de optimizacion
options = optimset('GradObj','off','Hessian','off','TolFun',1e-12,'Algorithm','Active-set','MaxIter',10000,'MaxFunEvals',10000);

%   Dado que voy a permitir un quiebro en el ajuste entre el tramo lineal
%   de 0 a Hslim y el tramo potencial obligo a que el tramo lineal llegue
%   como maximo a un quantil limite
if isempty(qlim) || exist('qlim','var')~=1,
    %   Anchura de los sectores direccionales por defecto
    qlim = 0;
    %   Si vale cero no introduzco el tramo lineal
end
uplim = zeros(np-1,1);
incre = 2*pi/(np-1);
for i = 1:np-1,
    indi = find(Dir>=incre*(i-1) & Dir<incre*i);
    if ~isempty(indi)
        uplim(i)=quantile(HsR(indi),qlim);
    else
        uplim(i) = NaN;
    end
end
uplim(find(isnan(uplim)))=max(uplim);
disp('Inicio de la optimizacion')

%   Llamada a la rutina de optimizacion
%   Los
%statusbar('Processing. Optimization was started %d of %d (%.1f%%)...',2,100,2);
if qlim ~=0 && qlim > 0 && qlim < 0.99,
    [p,fval,exitflag,output,lambda,grad,hessian] = fmincon(@(x) fobjNLP(Hs_SATQ,Hs_SQ,DirQ,thetaj,x),[ones(2*(np-1),1);0.5*uplim],[],[],[],[],[0.1*ones((np-1),1); -Inf*ones((np-1),1); zeros((np-1),1)],[5*ones(2*(np-1),1); uplim],[],options);
else
    exitflag = Inf;
end
if qlim ==0 || exitflag==0 || exitflag==-1 || exitflag==-2,
%   Para hacerlo mas robusto solo considero el aH^b, sin el tramo lineal.
    [p,fval,exitflag,output,lambda,grad,hessian] = fmincon(@(x) fobjNLP(Hs_SATQ,Hs_SQ,DirQ,thetaj,x),[ones(2*(np-1),1);zeros((np-1),1)],[],[],[],[],[0.1*ones((np-1),1); -Inf*ones((np-1),1); zeros((np-1),1)],[5*ones(2*(np-1),1); zeros((np-1),1)],[],options);
end
if exitflag==0 || exitflag==-1 || exitflag==-2,
    error('El procedimiento de optimizacion no funciona')
end
%statusbar('Processing. Optimization is complete  %d of %d (%.1f%%)...',85,100,85);

% hess = hessianapprox(Hs_SATQ,Hs_SQ,DirQ,thetaj,p);
%   En este momento los datos estan calibrados pero desordenados con
%   respecto a los datos iniciales
%   Calculo la varianza de los residuos y la funcion logaritmica de maxima verosimiliud
[fv flog var residuo HscalQ] = fobjNLP(Hs_SATQ,Hs_SQ,DirQ,thetaj,p);

a_deltamaxHsR=spline(thetaj,[p(1:np-1);p(1)],(1:360)'*pi/180);
b_deltamaxHsR=spline(thetaj,[p(np:2*(np-1)); p(np)],(1:360)'*pi/180);
deltamaxHsR=a_deltamaxHsR.*maxHsR.^b_deltamaxHsR-maxHsR;

% % %     bbb=spline(thetaj,[p(17:32);p(17)],DirQ((kk-1)*20+1));

% % % for kk = 1:360,
% % %     figure
% % %     auuuu=(kk-1)*20+1:kk*20;
% % %     plot(Hs_SQ(auuuu),Hs_SATQ(auuuu))
% % %     grid on
% % %     hold on
% % %     plot(Hs_SQ(auuuu),HscalQ(auuuu),'r')
% % %     aaa=spline(thetaj,[p(1:16);p(1)],DirQ((kk-1)*20+1));
% % %     bbb=spline(thetaj,[p(17:32);p(17)],DirQ((kk-1)*20+1));
% % %     plot(linspace(0,6),aaa*linspace(0,6).^bbb,'r--')
% % %     plot(Hs_SQ(auuuu),Hs_SQ(auuuu),'g')
% % %     plot(linspace(0,6),linspace(0,6),'g--')
% % %     legend('Satelite','Calibrado','Reanalisis','Location','Best')
% % %     title(['ANGULO ' num2str(180*DirQ((kk-1)*20+1)/(2*pi))])
% % %     % print('-djpeg ',[ejemplo filename]);
% % %     % print('-depsc' ,'-tiff',[ejemplo filename]);
% % %     saveas(gcf,['ANGULO ' num2str(180*DirQ((kk-1)*20+1)/(2*pi)) '.png']);   
% % %     close
% % % end

%   CALCULO DE LOS INTERVALOS DE CONFIANZA DE LOS PARAMETROS
%   Almaceno en una lista los parametros duplicando el primer valor al
%   final para cerrar el spline
pdef = [p(1:np-1); p(1); p(np:2*np-2); p(np); p(2*np-1:end); p(2*np-1)];
%   Calculo de la matriz de informacion, que es la matriz hessiana de la
%   funcion loglikelihood cambiada de signo
I0 = hessian/(2*var);
stdev = sqrt(var);
%   Descomposicion LU de la matriz de Informacion
[LX,UX,PX] = lu(I0);
%   Inversa mediante la descomposicion LU
invI0 = (UX)\(LX\(PX*eye(size(I0))));
if isempty(nivconf) || exist('nivconf','var')~=1,
    %   Nivel de confianza de los parametros
    nivconf = 0.05;
end
quan = norminv(1-nivconf/2,0,1);
varipara = diag(invI0);
stdvpara = sqrt(varipara);
%   Intervalos de confianza para el parametro a
aj = pdef(1:np);
ajup = zeros(np,1);
ajlo = zeros(np,1);
for i=1:np-1,
    ajup(i) = aj(i) + quan*sqrt(varipara(i));
    ajlo(i) = aj(i) - quan*sqrt(varipara(i));

end
ajup(np)=ajup(1);
ajlo(np)=ajlo(1);
%   Intervalos de confianza para el parametro b
bj = pdef(np+1:2*np);
bjup = zeros(np,1);
bjlo = zeros(np,1);
for i=1:np-1,
    bjup(i) = bj(i) + quan*sqrt(varipara(np-1+i));
    bjlo(i) = bj(i) - quan*sqrt(varipara(np-1+i));

end
bjup(np)=bjup(1);
bjlo(np)=bjlo(1);
%   Intervalos de confianza para el parametro d
dj = pdef(2*np+1:3*np);
djup = zeros(np,1);
djlo = zeros(np,1);
for i=1:np-1,
    djup(i) = dj(i) + quan*sqrt(varipara(2*np-2+i));
    djlo(i) = dj(i) - quan*sqrt(varipara(2*np-2+i));
end
djup(np)=djup(1);
djlo(np)=djlo(1);

outpar = [aj ajlo ajup bj bjlo bjup];
% save(['outpar' carpeta '.txt'],'outpar','-ASCII')

%   Una vez obtenidos los parametros optimos de los splines procedo a
%   calibrar y calcular los intervalos de confianza de cada dato calibrado
%   Calculo las alturas de ola calibradas
[Hscal] = CalibraSplineNLP(HsR,Dir,thetaj,[p(1:np-1); p(1); p(np:2*np-2); p(np); p(2*np-1:end); p(2*np-1)]);
%   Generacion de una malla regular en las localizaciones de los puntos
%   del spline y tomo el mismo numero de puntos que para los cuantiles
increthetrad = increthet*2*pi/360;
ntheta = 360/increthet;
increH = 1.1*max(HsR)/nq;
x_G = zeros(nq*ntheta,1);
y_G = zeros(nq*ntheta,1);
H_G = zeros(nq*ntheta,1);
theta_G = zeros(nq*ntheta,1);
%   Recorro primero los angulos
for i = 1:ntheta,
    %   Recorro despues las alturas de ola
    for j = 1:nq,
        x_G((i-1)*nq+j) = increH*j*cos(increthetrad*(i-1));
        y_G((i-1)*nq+j) = increH*j*sin(increthetrad*(i-1));
        H_G((i-1)*nq+j) = increH*j;
        theta_G((i-1)*nq+j) = increthetrad*(i-1);
    end
end
%   A continuacion para la discretizacion seleccionada calculo la varianza
%   asociada a cada posicion angular y para cada altura de ola
%   Empleo la matriz de varianzas-covarianzas: invI0
%   inicializo el vector de varianzas para cada punto
stdvHs_G = zeros(nq*ntheta,1);
for i = 1:ntheta,
    %   Recorro despues las alturas de ola
    for j = 1:nq,
        aux = zeros(3*ntheta,1);
        if H_G((i-1)*nq+j)<=dj(i),
            aux (i) = dj(i)^(-1+bj(i))*H_G((i-1)*nq+j);
            aux (ntheta+i) = aj(i)*aux (i)*log(dj(i));
            aux (2*ntheta+i) = aj(i)*(-1+bj(i))*dj(i)^(-2+bj(i))*H_G((i-1)*nq+j);
        else
            aux (i) = H_G((i-1)*nq+j)^bj(i);
            aux (ntheta+i) = aj(i)*aux (i)*log(H_G((i-1)*nq+j));
            aux (2*ntheta+i) = 0;
        end
            stdvHs_G((i-1)*nq+j) = sqrt(var+aux'*invI0*aux);
    end
end

%   Interpolacion de la desviacion standard para cada dato mediante radial
%   basis functions
%   Transformo las posiciones de los datos reales disponibles
[x_S,y_S] = TransCarte(HsR,Dir);

% [x_S,y_S] = TransCarte(Hs_SQ,DirQ);

%   Quiero interpolar stdvHs_G conociendo stdvHs_G en las coordenadas x_G e
%   y_G en los puntos de coordenadas x_S e y_S
%   Genero una triangulacion para interpolar
% if version matlab < 2011a 
% TRI = delaunay(x_G,y_G);
% T = tsearch(x_G,y_G,TRI,x_S,y_S);
TRI = DelaunayTri(x_G,y_G);
T = pointLocation(TRI,[x_S y_S]);
% % % figure
% % % [delta,margen]=tri_grid(TRI,[x_G,y_G],'k',1,1,0,0);
% % % hold on
% % % plot(x_S,y_S,'.r')
%   Calculo para cada punto en el que quiero interpolar a que triangulo
%   pertence
%   Procedimiento de interpolacion punto a punto
stdvHs = zeros (n,1);
for i = 1:n,
    stdvHs(i) = IntepLineal(stdvHs_G(TRI(T(i),1)),stdvHs_G(TRI(T(i),2)),stdvHs_G(TRI(T(i),3)),...
        x_S(i),y_S(i),x_G(TRI(T(i),1)),y_G(TRI(T(i),1)),x_G(TRI(T(i),2)),y_G(TRI(T(i),2)),x_G(TRI(T(i),3)),y_G(TRI(T(i),3)));
end
% % % length(Hs_SATQ)
% % % aaa=find(Hs_SATQ<Hs_SQ+stdvHs*norminv(1-0.05/2) & Hs_SATQ>Hs_SQ-stdvHs*norminv(1-0.05/2))
% % % length(aaa)/length(Hs_SATQ)*100
Hs_SQcal = CalibraSplineNLP(Hs_SQ,DirQ,thetaj,pdef);
A = [pdef [stdvpara(1:np-1); stdvpara(1); stdvpara(np:2*np-2); stdvpara(np); stdvpara(2*np-1:end); stdvpara(2*np-1)]];
%   Correlacion entre los datos de reanalisis calibrados y satelite
[Rcal,Pcal,RLOcal,RUPcal]=corrcoef(HsI,Hscal);
%   Bias
% disp('Bias sin calibrar y calibrado')
bi = bias(HsI,HsR);
bical = bias(HsI,Hscal);
%   Residuo medio cuadratico
% disp('Residuo medio cuadratico sin calibrar y calibrado')
rm = rms(HsI,HsR);
rmcal = rms(HsI,Hscal);
%   Residual index
% disp('Residual index cuadratico sin calibrar y calibrado')
rs = rsi(HsI,HsR);
rscal = rsi(HsI,Hscal);

% disp('Coeficiente de correlacion sin calibrar y calibrado (Quantiles)')
[RQ]=corrcoef(Hs_SATQ,Hs_SQ);
%   Correlacion entre los datos de reanalisis calibrados y satelite
[RcalQ]=corrcoef(Hs_SATQ,Hs_SQcal);
%   Bias
% disp('Bias sin calibrar y calibrado (Quantiles)')
biQ = bias(Hs_SATQ,Hs_SQ);
bicalQ = bias(Hs_SATQ,Hs_SQcal);
%   Residuo medio cuadratico
% disp('Residuo medio cuadratico sin calibrar y calibrado (Quantiles)')
rmQ = rms(Hs_SATQ,Hs_SQ);
rmcalQ = rms(Hs_SATQ,Hs_SQcal);
%   Residual index
% disp('Residual index cuadratico sin calibrar y calibrado (Quantiles)')
rsQ = rsi(Hs_SATQ,Hs_SQ);
rscalQ = rsi(Hs_SATQ,Hs_SQcal);
% disp('Coeficiente de correlacion sin calibrar y calibrado')
[R,P,RLO,RUP]=corrcoef(HsI,HsR);

Hlim=[0 ceil(max([max(HsI),max(HsR),max(Hscal)]))];
calidadRHO = [R(1,2) Rcal(1,2) 100*(Rcal(1,2)-R(1,2))/R(1,2)];
statmoments = [mean(HsR) mean(Hscal) mean(HsI);
               std(HsR) std(Hscal) std(HsI);
               skewness(HsR) skewness(Hscal) skewness(HsI);
               kurtosis(HsR) kurtosis(Hscal) kurtosis(HsI); 
               (mean(HsR)-mean(HsI))/mean(HsI) (mean(Hscal)-mean(HsI))/mean(HsI) 0;
               (std(HsR)-std(HsI))/std(HsI) (std(Hscal)-std(HsI))/std(HsI) 0;
               (skewness(HsR)-skewness(HsI))/skewness(HsI) (skewness(Hscal)-skewness(HsI))/skewness(HsI) 0;
               (kurtosis(HsR)-kurtosis(HsI))/kurtosis(HsI) (kurtosis(Hscal)-kurtosis(HsI))/kurtosis(HsI) 0];
           %   Calculo para los cuantiles direccionales
SerieHs=HsI;
SerieDir=Dir*180/pi;
%   Metodo de Roberto
n=length(SerieDir);
nq=20;

X1=-log(-log(1./n));
Xend=-log(-log(1-5./n));
Delta=(Xend-X1)/(nq-1);
X=[X1:Delta:Xend];
P100=100./(exp(exp(-X)));

NPMQ=min(5*nq,ceil(0.1*n));

DirQ=[1:360];

Hs_SATQ=NaN*ones(nq,length(DirQ));
Hs_NCQ=NaN*ones(nq,length(DirQ));
Hs_CQ=NaN*ones(nq,length(DirQ));

DeltaT=22.5;

cont=0;
for DIR=DirQ
    cont=cont+1;
    dum=find(SerieDir>=DIR-DeltaT/2 & SerieDir<DIR+DeltaT/2);
    if DIR-DeltaT/2<0
        dum=find(SerieDir>=DIR-DeltaT/2+360 | SerieDir<DIR+DeltaT/2);
    end
    if DIR+DeltaT/2>360
        dum=find(SerieDir>=DIR-DeltaT/2 | SerieDir<DIR+DeltaT/2-360);
    end
    if length(dum)>NPMQ   
        YpB = prctile(HsI(dum),P100);
        YpNC = prctile(HsR(dum),P100);
        YpC = prctile(Hscal(dum),P100);
        Hs_SATQ(:,cont)=YpB';
        Hs_NCQ(:,cont)=YpNC';
        Hs_CQ(:,cont)=YpC';
    end
    clear dum
end
DirNoQ=find(isnan(Hs_SATQ(nq,:)));
Hsup = Hscal+stdvHs*norminv(1-nivconf/2);
Hslo = Hscal-stdvHs*norminv(1-nivconf/2);

%Sting name for the plots
i=1;%se cambia en calibration y CalibraSplineQuantile
if length(HsR)>1,  tpstring{1,i}='Scatter IR'; i=i+1; end
if length(Hscal)>1,  tpstring{1,i}='Scatter CI'; i=i+1; end
if length(Hs_NCQ)>1,  tpstring{1,i}='Quantiles R'; i=i+1; end
if length(Hs_CQ)>1,  tpstring{1,i}='Quantiles C'; i=i+1; end
if length(Hs_SATQ)>1,  tpstring{1,i}='Quantiles I'; i=i+1; end
if qlim ~=0 && qlim > 0 && qlim < 0.99, tpstring{1,i}='Parameters CIc';  i=i+1; 
else tpstring{1,i}='Parameters CI'; i=i+1; end
if true,  tpstring{1,i}='Parameters Table'; i=i+1; end %esta comentado en CalibraSplineQuantile
if true,  tpstring{1,i}='CDF'; i=i+1; end
if true,  tpstring{1,i}='Rose Cal/Rea'; i=i+1; end
if true,  tpstring{1,i}='RoseP'; i=i+1; end;
if length(Hscal)>1,  tpstring{1,i}='X rea, X ins'; end;clear i;%se cambia en calibration y CalibraSplineQuantile
%plot dir en grados Dir*180/pi para usar calibrationplots
%statusbar('Processing. Drawing and keeping the figures  %d of %d (%.1f%%)...',90,100,90);
calibrationplots(HsI,HsR,Dir*180/pi,nq,Hscal,Hlim,Hsup,Hslo,n,nivconf,Hs_NCQ,Hs_CQ,Hs_SATQ,DirQ,qlim,aj,ajlo,ajup,bj,bjlo,bjup,dj,djlo,djup,DirNoQ,np,thetaj,calidadRHO,statmoments,tpstring,svopt,carpeta,idioma,var_name,var_unit);

%save
%statusbar('Processing. Save the all data  %d of %d (%.1f%%)...',90,100,90);
save([pwd,filesep,carpeta,filesep,'calibration[',num2str(latcn),'][',num2str(loncn),'].mat'],'Hscal','stdvHs','A','stdev','thetaj','residuo','HsI','HsR','Dir','Hlim','calidadRHO','statmoments','DirQ',...
    'nq','Hs_NCQ','Hs_CQ','Hs_SATQ','aj','ajlo','ajup','bj','bjlo','bjup','DirNoQ','np','Hsup','Hslo','n','pdef','maxHsR','deltamaxHsR');
end
