function [hpol,radlab_h] = polarLabels_IC_modOrigenSombra(theta,rho,rhoIC,rlabshift_value,DirNoQ,qlim,gcax)

if ~exist('qlim','var'), qlim=0; end
if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
if ~ishandle(gcax), disp('WARNING! polarLabels_IC_modOrigenSombra with axes'); end
set(gcf,'CurrentAxes',gcax)
    
% No poner line_style ni clear, solo las tres matrices theta (angulo respecto al norte, + reloj), rho y rhoIC

% function [hpol,radlab_h] =
% polarLabels_Yo(theta,rho,line_style,labelrotate,rlabshift,clear)
% [hpol,radlab_h] = polarLabels(theta,rho,line_style,labelrotate,rlabshift,clear)
%
% polarLabels   Polar coordinate plot. (with added options)
%   polarLabels(THETA, RHO) makes a plot using polar coordinates of
%   the angle THETA, in radians, versus the radius RHO.
%   polarLabels(THETA,RHO,S) uses the linestyle specified in string S.
%   See PLOT for a description of legal linestyles.
%
%   See also PLOT, LOGLOG, SEMILOGX, SEMILOGY.
%
% Additions
%   Plot is labelled +/-180 instead of 0-360
%   Additional input options
%       labelrotate     value (in degrees) to add to angle labels to facilitate rotation of LABEL
%                       def = 0
%       rlabshift       value to add to radial labels to facilitate impression of negative values
%                       def = 0
%       clear           make polar background clear
%                       def = false (0)
%
% Ex. polarlabels([0:360]*pi/180,abs(sin([0:360]*pi/180))*10,'b',-90,-10)
%
%   Additional output option
%       [hpol,radlab_h] = polarLabels(theta,rho,line_style,labelrotate,rlabshift,clear)
%       radlab_h           vector of handles to radial text labels
%
%       Backward compatible with standard POLAR
%       For multiple plots (hold on) should only need to be used with the first one

%   Revised by BFGK 26-mars-2002
%    For better access to labels
%   Revised by BFGK 27-mai-2002
%    Fix to work with full 360 plot
%   Revised by BFGK 22-mai-2003
%    Added "clear" option for special background application
%    Tidied up nargin section

% %     hold on;                                                            % Yo
%     hkk=plot(rho);                                                      % Yo  
%     set(gca,'dataaspectratio',[1 1 1],'plotboxaspectratiomode','auto')  % Yo
%     vkk = get(gca,'ylim');                                              % Yo
%     rlabshift=vkk(1);                                                   % Yo
%     delete(hkk);                                                        % Yo
% close(hkk)
% 
% 
% labelrotate=90;                     % Yo
% % rlabshift=floor(min(rho));        % Yo
% % if rlabshift>0                    % Yo
% %     rlabshift=0;                  % Yo
% % end                               % Yo
% rho=rho-rlabshift;                  % Yo
% theta=theta-labelrotate*2*pi/360;   % Yo

% colordef black
% colordef white

% figure('color','w','visible',visibility)

labelrotate=90; 
rlabshift=0; % Inicializo 

    rho1=rho(1,:);
    rho2=rho(2,:);
    
    rho1Sup=rhoIC(1,:);
    rho1Inf=rhoIC(2,:);
    
    rho2Sup=rhoIC(3,:);
    rho2Inf=rhoIC(4,:);
        
    theta1=theta(1,:);
    theta2=theta(2,:);

if nargin < 1
    error('Requires at least 2 input arguments.')
elseif nargin == 2 
    if isstr(rho)
        line_style = rho;
        rho = theta;
        [mr,nr] = size(rho);
        if mr == 1
            theta = 1:nr;
        else
            th = (1:mr)';
            theta = th(:,ones(1,nr));
        end
    else
        line_style = 'auto';
    end
elseif nargin == 1
    line_style = 'auto';
    rho = theta;
    [mr,nr] = size(rho);
    if mr == 1
        theta = 1:nr;
    else
        th = (1:mr)';
        theta = th(:,ones(1,nr));
    end
end

if ~isequal(size(theta),size(rho))
    error('THETA and RHO must be the same size.');
end

% Dealing with additional arguments
% if nargin < 4,  labelrotate = 0 ;    end
% if nargin < 5,  rlabshift = 0   ;    end
% if nargin < 6,  clear = 0       ;    end
if nargin < 5,  clear = 0       ;    end
clear =0; 

if isstr(theta) | isstr(rho) | isstr(labelrotate) | isstr(rlabshift)
    error('Input arguments must be numeric.');
end


% get hold state
cax = newplot;
next = lower(get(cax,'NextPlot'));
hold_state = ishold;

% get x-axis text color so grid is in same color
tc = get(cax,'xcolor');
ls = get(cax,'gridlinestyle');

% Hold on to current Text defaults, reset them to the
% Axes' font attributes so tick marks use them.
fAngle  = get(cax, 'DefaultTextFontAngle');
fName   = get(cax, 'DefaultTextFontName');
fSize   = get(cax, 'DefaultTextFontSize');
fWeight = get(cax, 'DefaultTextFontWeight');
fUnits  = get(cax, 'DefaultTextUnits');
set(cax, 'DefaultTextFontAngle',  get(cax, 'FontAngle'), ...
    'DefaultTextFontName',   get(cax, 'FontName'), ...
    'DefaultTextFontSize',   get(cax, 'FontSize'), ...
    'DefaultTextFontWeight', get(cax, 'FontWeight'), ...
    'DefaultTextUnits','data')

% only do grids if hold is off
if ~hold_state

   
    hold on;                                                            % Yo
%     hkk=plot([rho1 rho2]);                                                      % Yo  
    hkk=plot(gcax,[rho1Sup rho1Inf rho2Sup rho2Inf]);
    set(gcax,'dataaspectratio',[1 1 1],'plotboxaspectratiomode','auto')  % Yo
    vkk = rlabshift_value;      % //// modificacion ////
    rlabshift=vkk;  % //// modificacion ////    
%     rlabshift=0;    % BORJA -> empieza en 0 en vez de en el auto!!! 
    delete(hkk);  
    
    rho1=rho1-rlabshift;                  % Yo
    theta1=theta1-labelrotate*2*pi/360;   % Yo
    rho1Sup=rho1Sup-rlabshift;                  % Yo
    rho1Inf=rho1Inf-rlabshift;                  % Yo
        
    rho2=rho2-rlabshift;                  % Yo
    theta2=theta2-labelrotate*2*pi/360;   % Yo
    rho2Sup=rho2Sup-rlabshift;                  % Yo
    rho2Inf=rho2Inf-rlabshift;                  % Yo
%     rho=rho-rlabshift;                  % Yo
%     theta=theta-labelrotate*2*pi/360;   % Yo
% make a radial grid
%     hold on;
%     maxrho = max(abs(rho(:)));
%     hhh=plot([-maxrho -maxrho maxrho maxrho],[-maxrho maxrho maxrho -maxrho]);

%     hhh=plot(rho+rlabshift);
%     hhh=plot([rho1 rho2]+rlabshift);

%     hhh=plot([rho1Sup rho2Sup rho1Inf rho2Inf]+rlabshift);
    hhh=plot(gcax,[rho1Sup; rho2Sup; rho1Inf; rho2Inf]'+rlabshift);
    set(gcax,'dataaspectratio',[1 1 1],'plotboxaspectratiomode','auto')
%     set(gca,'dataaspectratio',[1000 1 1],'plotboxaspectratiomode','auto')
    v = [get(cax,'xlim') get(cax,'ylim')];
%     if v(4)>ceil(max([rho1Sup rho2Sup]))
%         set(cax,'ylim',[v(3) ceil(max([rho1Sup rho2Sup]))]);
%         v = [get(cax,'xlim') get(cax,'ylim')];
%     end
%     ticks = sum(get(cax,'ytick')>=0);
    ticks = length(get(cax,'ytick'));
    delete(hhh);
    
%     v(4)=min([v(4) ceil(max([rho1Sup rho2Sup]))]);
    
% check radial limits and ticks
    rmin = 0; rmax = v(4)-rlabshift; rticks = max(ticks-1,4);
    rmin = v(3)-rlabshift;
    if rticks > 6   % see if we can reduce the number
        if rem(rticks,2) == 0
            rticks = rticks/2;
        elseif rem(rticks,3) == 0
            rticks = rticks/3;
        end
    end
   
    if rmax>=3 & max([rho1Sup rho2Sup])<=3
        rmax=3;
        rticks=3;
    end
    
% define a circle
    th = 0:pi/50:2*pi;
    xunit = cos(th);
    yunit = sin(th);
% now really force points on x/y axes to lie on them exactly
    inds = 1:(length(th)-1)/4:length(th);
    xunit(inds(2:2:4)) = zeros(2,1);
    yunit(inds(1:2:5)) = zeros(3,1);
% % plot background if necessary
%     if (~isstr(get(cax,'color')) & ~clear),
%        patch('xdata',xunit*rmax,'ydata',yunit*rmax, ...
%              'edgecolor',tc,'facecolor',get(gcax,'color'),...
%              'handlevisibility','off');
%     end
    
xx1 = rho1.*cos(theta1);
yy1 = rho1.*sin(theta1);

xx2 = rho2.*cos(theta2);
yy2 = rho2.*sin(theta2);

if qlim ~=0 && qlim > 0 && qlim < 0.99,
%     plot(gcax,xx1,yy1,'-','Color',[0 250 154]/255,xx2,yy2,'k','linewidth',1.5);
    plot(gcax,xx1,yy1,'-','Color',[0 100 0]/255,'linewidth',1.5);
    plot(gcax,xx2,yy2,'-k','linewidth',1.5);
    
else
    plot(gcax,xx1,yy1,'b',xx2,yy2,'r','linewidth',1.5);
end

% plot background if necessary
    if (~isstr(get(cax,'color')) & ~clear),
       patch('xdata',xunit*rmax,'ydata',yunit*rmax, ...
             'edgecolor',tc,'facecolor',get(gcax,'color'),...
             'handlevisibility','off');
    end

AAAXXX=gcax;
xx1IC = [rho1Sup rho1Inf].*cos([theta1 theta1]);
yy1IC = [rho1Sup rho1Inf].*sin([theta1 theta1]);

xx2IC = [rho2Sup rho2Inf].*cos([theta2 theta2]);
yy2IC = [rho2Sup rho2Inf].*sin([theta2 theta2]);


hold on;

for SS=DirNoQ
    ss=SS-labelrotate;
    fill([0 rmax.*cos((ss+[0.5 -0.5]).*pi/180) 0],[0 rmax.*sin((ss+[0.5 -0.5]).*pi/180) 0],[0.8 0.8 0.8],'LineStyle','none')%,'handlevisibility','off')
end

if qlim ~=0 && qlim > 0 && qlim < 0.99,
    fill(xx1IC,yy1IC,[154 205 50]/255,'LineStyle','none')%,'handlevisibility','off')
    fill(xx2IC,yy2IC,[0 0 0],'LineStyle','none')%,'handlevisibility','off')
else
    fill(xx1IC,yy1IC,[0.6 0.6 1],'LineStyle','none')%,'handlevisibility','off')
    fill(xx2IC,yy2IC,[1 0.6 0.6],'LineStyle','none')%,'handlevisibility','off')
end

auxax=get(gca,'Position');
VectorLocLegend=[auxax(1)+auxax(3)/2 auxax(2)-1.5*0.03 0.01 0.03];
if qlim ~=0 && qlim > 0 && qlim < 0.99,
    h=legend(gcax,'c(\theta)','Orientation','horizontal','Location','SouthOutside','Location',VectorLocLegend);
else
    h=legend(gcax,'a(\theta)','b(\theta)','Orientation','horizontal','Location','SouthOutside','Location',VectorLocLegend);
end
set(h,'box','off');

% hold on;
if qlim ~=0 && qlim > 0 && qlim < 0.99,
     plot(gcax,xx1,yy1,'-','Color',[0 100 0]/255,'linewidth',1.5);
    plot(gcax,xx2,yy2,'-k','linewidth',1.5);
else
    plot(gcax,xx1,yy1,'b',xx2,yy2,'r','linewidth',1.5);
end
% plot(xx1IC,yy1IC,'b--',xx2IC,yy2IC,'r--','linewidth',1.0);
    
% draw radial circles
    c82 = cos(82*pi/180);
    s82 = sin(82*pi/180);
    rinc = (rmax-rmin)/rticks;
    cnt = 1;
    for i=rmin:rinc:rmax % (rmin+rinc):rinc:rmax
        hhh = plot(gcax,xunit*i,yunit*i,ls,'color',tc,'linewidth',1,...
                   'handlevisibility','off');
        radlab_h(cnt) = text((i+rinc/20)*c82,-(i+rinc/20)*s82, ...          % Yo
            ['' num2str(i+rlabshift)],'verticalalignment','bottom',...    % Yo
            'handlevisibility','off','fontsize',7);                                      % Yo
%         radlab_h(cnt) = text((i+rinc/20)*c82,(i+rinc/20)*s82, ...         % Yo
%             ['  ' num2str(i+rlabshift)],'verticalalignment','bottom',...  % Yo
%             'handlevisibility','off');                                    % Yo
        if (i+rlabshift)==1, set(hhh,'linestyle','-'); end
        cnt = cnt+1;
    end
    set(hhh,'linestyle','-') % Make outer circle solid
%     set(hhh,'linewidth',1.5) % Make outer circle solid
    
% plot spokes
    th = (1:6)*2*pi/12;
    cst = cos(th); snt = sin(th);
    cs = [-cst; cst];
    sn = [-snt; snt];
    plot(rmax*cs,rmax*sn,ls,'color',tc,'linewidth',1,...
         'handlevisibility','off')

% annotate spokes in degrees
%     rt = 1.1*rmax;
    rt = 1.15*rmax;
    for i = 1:length(th)
        angTextNum = (i*30)+labelrotate    ;
        if angTextNum<0, angTextNum=angTextNum+360; end
        text(rt*cst(i),rt*snt(i),[int2str(angTextNum) char(176)],...
             'horizontalalignment','center',...
             'handlevisibility','off','fontsize',8);%,'fontweight','bold')
        if i == length(th)
            loc = int2str(0+labelrotate);
        else
            angTextNum = 180+(i*30)+labelrotate    ;
            if angTextNum > 180, angTextNum = angTextNum - 360; end
            loc = int2str(angTextNum);
        end
        if str2num(loc)<0, loc=num2str(str2num(loc)+360); end
        text(-rt*cst(i),-rt*snt(i),[loc char(176)],'horizontalalignment','center',...
             'handlevisibility','off','fontsize',8);%,'fontweight','bold')
    end

% set view to 2-D
    view(2);
% set axis limits
%     axis(rmax*[-1 1 -1.15 1.15]);
    axis(rmax*[-1.1 1.1 -1.2 1.2]);
end

% Reset defaults.
set(cax, 'DefaultTextFontAngle', fAngle , ...
    'DefaultTextFontName',   fName , ...
    'DefaultTextFontSize',   fSize, ...
    'DefaultTextFontWeight', fWeight, ...
    'DefaultTextUnits',fUnits );

% transform data to Cartesian coordinates.
% xx1 = rho1.*cos(theta1);
% yy1 = rho1.*sin(theta1);
% 
% xx2 = rho2.*cos(theta2);
% yy2 = rho2.*sin(theta2);
% 
% plot(xx1,yy1,'b',xx2,yy2,'r','linewidth',1.5);
% 
% 
% xx1IC = [rho1Sup rho1Inf].*cos([theta1 theta1]);
% yy1IC = [rho1Sup rho1Inf].*sin([theta1 theta1]);
% 
% xx2IC = [rho2Sup rho2Inf].*cos([theta2 theta2]);
% yy2IC = [rho2Sup rho2Inf].*sin([theta2 theta2]);
% 
% 
% hold on;
% fill(xx1IC,yy1IC,[0.8 0.8 0.8],'LineStyle','none')
% fill(xx2IC,yy2IC,[0.8 0.8 0.8],'LineStyle','none')
% 
% 
% legend('b(\theta)','c(\theta)','IC \alpha=0.95',1)
% 
% plot(xx1,yy1,'b',xx2,yy2,'r','linewidth',1.5);

% set(get(gca,'xlabel'),'visible','on')
% set(get(gca,'ylabel'),'visible','on')

% hold off;

if nargout > 0
    hpol = q;
end
if ~hold_state
    set(gcax,'dataaspectratio',[1 1 1])
    axis(gcax,'off');
% axis('visibility','off')
    set(cax,'NextPlot',next);
%     set(gca,'XColor','w')
%     set(gca,'YColor','w')
end
set(get(gcax,'xlabel'),'visible','on')
set(get(gcax,'ylabel'),'visible','on')
% set(get(gca,'xlabel'),'visible','off')
% set(get(gca,'ylabel'),'visible','off')

% hYo = findobj(gcf,'Type','axes');
% set(hYo,'ydir','reverse')

axis(gcax,'ij'); % Yo

colordef white

