function GraficosAdicionales(HsGow,thetaGOW,HsR,thetaR,maxHsR,Hscal,Hscal_n,svopt,carpeta,idioma,var_name,var_unit)
%Los angulos deben estar en grados
if ~exist('idioma','var'), idioma='eng'; end
if ~exist('var_name','var'), var_name='Hs'; end
if ~exist('var_unit','var'), var_unit='m'; end
if ~exist('svopt','var') || isempty(svopt), svopt=[1 1 0]; end %save option 1-fig 2-png 3-eps (1/0)

nmlbl={'New Data','Base Data','Threshold','Scatter Plot Reanalysis vs Calibration'};
if strcmp(idioma,'esp'),
    nmlbl={'Datos nuevos','Datos de base','Umbral','Scatter Plot Reanalisis versus Calibracion'};
end

tpstring{1}='limites';
tpstring{2}='Ext2';
for i=1:length(tpstring),
    plotname=tpstring{i};
    h=figure('Visible','off');
    switch plotname     %   Grafico verde
        case 'limites'
        polar(gca,thetaGOW*pi/180,HsGow,'.r');
        hold(gca,'on');
        polar(gca,thetaR*pi/180,HsR,'b.');
        polar(gca,(1:360)'*pi/180,maxHsR,'k-');
        legend(nmlbl{1},nmlbl{2},nmlbl{3},'Location','SouthOutside');
        set(gca,'LineWidth',2)
        hold(gca,'off');
        case 'Ext2'
        plot(gca,HsGow,Hscal_n,'.r')
        hold(gca,'on');
        plot(gca,HsR,Hscal,'.b');
        xlabel(gca,[var_name,'^R (',var_unit,')']);
        ylabel(gca,[var_name,'^C (',var_unit,')']);
        legend(nmlbl{1},nmlbl{2},'Location','Best');
        grid(gca,'on');
        title(nmlbl{4});
        hold(gca,'off');
        otherwise
        disp(['Warnig!.Not exist this plot case ',plotname]);
    end
    savename=[pwd,filesep,carpeta,filesep,plotname];
    if svopt(3), saveas(h,[savename,'.eps'],'psc2'); end
    if svopt(2), saveas(h,[savename,'.png']); end
    set(h,'Visible','on');
    if svopt(1), saveas(h,[savename,'.fig']); end
    close(h);
end

end
    


