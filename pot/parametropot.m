function y = parametropot (x,alpha0,alpha,betaT,indices,varphi)
%   Funcion para pintar la evolucion temporal anual de los distintos
%   parametros de la distribucion GEV 
%
%   Input:
%        x-> Abscisas en las que calcular el valor del parametro (Entre 0 y 1)
%        alpha0-> Parametro constante
%        alpha-> Parametros variables, los valores impares van asociados al
%        seno y los pares al coseno
%        betaT-> parametro de tendencia
%        indices-> Datos de los indices
%        varphi-> Parametros asociados a los indices o covariables

%   Output:
%        y-> Valores del parametro en x
%
%   OJO OJO NO  SE USA
[m,n] = size(x);

if nargin<4, betaT = [];end
if nargin<5, indices = [];end
if nargin<6, varphi = [];end

tend = length(betaT);
[na nind] = size(indices);

if na ~= m && nind>0,
    error('Las dimensiones de los datos son incorrectas');
end

np = length(alpha);

if mod(np,2)~=0,
    error('El numero de parametros ha de ser par');
end

y = alpha0*ones(m,n);

for i = 1:np/2,
    y = y + alpha(2*i-1)*cos(i*2*pi*x);
end

for i = 1:np/2,
    y = y + alpha(2*i)*sin(i*2*pi*x);
end

if tend>0,
    y = y + betaT*x;
end

if nind>0,
    for i = 1:nind,
        y = y + varphi(i)*indices(:,i);
    end
end