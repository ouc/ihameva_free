function [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,grad,hessian,p] = ...
    OptiParamHessianPOT(nla,nsig,neps,x,t,up0,up1,Ts,dias,betaT,indices,indicesT,varphi,betaT2,indices2,varphi2,pini)

if nargin<10, betaT = [];end
if nargin<11, indices = [];end
if nargin<12, indicesT = [];end
if nargin<13, varphi = [];end
if nargin<14, betaT2 = [];end
if nargin<15, indices2 = [];end
if nargin<16, varphi2 = [];end
if nargin<17, pini = [];end

[na nind2] = size(indices);
nind = length(varphi);
if nind2~=nind,
    error('Hay algo raro relativo a las covariables')
end
if ~isempty(indices)
if na~=length(x) || na~=length(t) || length(t)~=length(x),
    error('Hay algo raro relativo a las dimensiones de los datos')
end
end
[na2 nind2] = size(indices2);
if ~isempty(indices2),
if na2~=length(x) || na2~=length(t) || length(t)~=length(x),
    error('Hay algo raro relativo a las dimensiones de los datos: funcion loglikelihood')
end
end
%
%   OPTIPARAM es una funcion que calcula los parametros optimos obtenidos
%   de maximizar la funcion logaritmica de verosimilitud
%

nla = 2*nla;
nsig = 2*nsig;
neps = 2*neps;
ntend = length(betaT);
ntend2 = length(betaT2);

%   Porcentaje de incremento permitido en las variables con respecto al
%   valor anterior
incre = 0.25;

valmax = 100;

%   Ajuste inicial de la distribucion de extremos estacionaria
p = zeros(3+nla+nsig+neps+ntend+nind+ntend2+nind2,1);
p(1) = (length(x)/Ts);
p(2+nla+ntend+nind) = log(std(x));
p(3+nla+nsig+ntend+nind+ntend2+nind2) = -0.1;

%   Bounds on variables

lb = -valmax*ones(3+nla+nsig+neps+ntend+nind+ntend2+nind2,1);
% lb(1) = (1-incre)*p_(3);
% lb(2+nla+ntend+nind) = (1-incre)*log(p_(2));
lb(3+nla+nsig+ntend+nind+ntend2+nind2) = -0.2;
if neps>0,
    lb(3+nla+nsig+ntend+nind+ntend2+nind2+1:3+nla+nsig+ntend+nind+ntend2+nind2+neps) = -valmax;    
end
up = valmax*ones(3+nla+nsig+neps+ntend+nind+ntend2+nind2,1);
% up(1) = (1+incre)*p_(3);
% up(2+nla+ntend+nind) = (1+incre)*log(p_(2));
up(3+nla+nsig+ntend+nind+ntend2+nind2) = 0.2;
if neps>0,
    up(3+nla+nsig+ntend+nind+ntend2+nind2+1:3+nla+nsig+ntend+nind+ntend2+nind2+neps) = valmax;    
end

if ~isempty(pini),
     p = pini;
end

% options =
% optimset('GradObj','on','Hessian','user-supplied','Display','iter','TolFun',1e-12);
% options = optimset('GradObj','on','Hessian','user-supplied','TolFun',1e-12,'Display','iter');
options = optimset('GradObj','on','Hessian','off','TolFun',1e-12,'Algorithm','trust-region-reflective');
[p,fval,exitflag,output,lambda,grad,hessian] = fmincon(@(p) loglikelihood (p,x,t,up0,up1,Ts,dias,nla,nsig,neps,ntend,indices,indicesT,ntend2,indices2),p,[],[],[],[],lb,up,[],options);


auxlo = find(abs(lambda.lower)>1e-8);
lb(auxlo)=lb(auxlo)-incre*abs(lb(auxlo));
auxup = find(abs(lambda.upper)>1e-8);
up(auxup)=up(auxup)+incre*abs(up(auxup));
it = 1;
while (~isempty(auxlo) | ~isempty(auxup)) & it<=10,
    it = it+1;
    [p,fval,exitflag,output,lambda,grad,hessian] = fmincon(@(p) loglikelihood (p,x,t,up0,up1,Ts,dias,nla,nsig,neps,ntend,indices,indicesT,ntend2,indices2),p,[],[],[],[],lb,up,[],options);
    auxlo = find(abs(lambda.lower)>1e-8);
    lb(auxlo)=lb(auxlo)-incre*abs(lb(auxlo));
    auxup = find(abs(lambda.upper)>1e-8);
    up(auxup)=up(auxup)+incre*abs(up(auxup));
end

beta0 = p(1);
if nla>0,
    beta = p(2:1+nla);  
else
    beta = [];
end
if ntend>0,
    betaT = p(2+nla);
else
    betaT = [];
end
if nind>0,
    varphi = p(1+nla+ntend+1:1+nla+ntend+nind);
else
    varphi = [];
end

alpha0 = p(2+ntend+nind+nla);
if nsig>0,
    alpha = p(2+nla+ntend+nind+1:2+nla+ntend+nind+nsig);
else
    alpha = [];
end
if ntend2>0,
    betaT2 = p(2+nla+ntend+nind+nsig+1);
else
    betaT2 = [];
end
if nind2>0,
    varphi2 = p(2+nla+ntend+nind+nsig+ntend2+1:2+nla+ntend+nind+nsig+ntend2+nind2);
else
    varphi2 = [];
end

gamma0 = p(3+nla+ntend+nind+nsig+ntend2+nind2);
if neps>0,
    gamma = p(3+nla+ntend+nind+nsig+ntend2+nind2+1:3+nla+ntend+nind+nsig+ntend2+nind2+neps);
else
    gamma = [];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function [f Jx Hxx] = loglikelihood (p,x,t,up0,up1,Ts,dias,nla,nsig,neps,ntend,indices,indicesT,ntend2,indices2)
        
        if nargin<11, ntend =0; end
        if nargin<12, indices = [];end
        if nargin<13, indicesT = [];end
        if nargin<14, ntend2 =0; end
        if nargin<15, indices2 = [];end
        
        [na nind] = size(indices);
        if ~isempty(indices),
        if na~=length(x) || na~=length(t) || length(t)~=length(x),
            error('Hay algo raro relativo a las dimensiones de los datos: funcion loglikelihood')
        end
        end
        [na2 nind2] = size(indices2);
        if ~isempty(indices2),
        if na2~=length(x) || na2~=length(t) || length(t)~=length(x),
            error('Hay algo raro relativo a las dimensiones de los datos: funcion loglikelihood')
        end
        end
        
        %   Localizacion
        if ntend == 0 && nind == 0,
            lambt = (parametro (t,p(1),p(2:1+nla)));
        elseif ntend == 0 && nind ~= 0,
            lambt = (parametro (t,p(1),p(2:1+nla),[],indices,p(1+nla+ntend+1:1+nla+ntend+nind)));
        elseif ntend ~= 0 && nind == 0,
            lambt = (parametro (t,p(1),p(2:1+nla),p(2+nla),[],[]));
        else
            lambt = (parametro (t,p(1),p(2:1+nla),p(2+nla),indices,p(1+nla+ntend+1:1+nla+ntend+nind)));
        end
        %   Escala
        if ntend2 == 0 && nind2 == 0,
            sigt = exp(parametro (t,p(2+nla+ntend+nind),p(2+nla+ntend+nind+1:2+nla+ntend+nind+nsig)));
        elseif ntend2 == 0 && nind2 ~= 0,
            sigt = exp(parametro (t,p(2+nla+ntend+nind),p(2+nla+ntend+nind+1:2+nla+ntend+nind+nsig),[],indices2,p(2+nla+ntend+nind+nsig+ntend2+1:2+nla+ntend+nind+nsig+ntend2+nind2)));
        elseif ntend2 ~= 0 && nind2 == 0,
            sigt = exp(parametro (t,p(2+nla+ntend+nind),p(2+nla+ntend+nind+1:2+nla+ntend+nind+nsig),p(2+nla+ntend+nind+nsig+ntend2),[],[]));
        else
            sigt = exp(parametro (t,p(2+nla+ntend+nind),p(2+nla+ntend+nind+1:2+nla+ntend+nind+nsig),p(2+nla+ntend+nind+nsig+ntend2),indices2,p(2+nla+ntend+nind+nsig+ntend2+1:2+nla+ntend+nind+nsig+ntend2+nind2)));
        end
        
%         if ~isempty(find(sigt<0)),
%             disp('Valores de escala negativos')
%         end
        %   Forma
        if neps == 0,
            epst = parametro (t,p(3+nla+nsig+ntend+nind+ntend2+nind2),[]);
        else
            epst = parametro (t,p(3+nla+nsig+ntend+nind+ntend2+nind2),p(3+nla+nsig+ntend+nind+ntend2+nind2+1:3+nla+nsig+ntend+nind+ntend2+nind2+neps));
        end
        
        %   Umbral
        u = parametro (t,up0,up1);
        
        %   Calculo los puntos en los que la distribucion es de GUMBEL
        posG = find(abs(epst)<=1e-8);

        %   Calculo los puntos en los que la distribucion es de WEIBULL o FRECHET
        pos  = find(abs(epst)>1e-8);
        %   Modifico los valores problematicos de epst y los igualo a 1
        epst(posG)=1;

        %   Calculo las derivadas primeras de la funcion log-likelihood con
        %   respecto a los parametros de localizacion, escala y forma,
        %   respectivamente
        Yt = x-u;
        xn = Yt./sigt; 
        z = 1 + epst.*xn; 
        z = max(1e-4,z);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   Esto para todos los dias del periodo de estudio
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   Parametros de Poisson para el resto de los dias
        % dias = (1:Numdias)'/Ts;
        if ntend == 0 && nind == 0,
            lambtT = (parametro(dias,p(1),p(2:1+nla)));
        elseif ntend == 0 && nind ~= 0,
            lambtT = (parametro(dias,p(1),p(2:1+nla),[],indicesT,p(1+nla+ntend+1:1+nla+ntend+nind)));
        elseif ntend ~= 0 && nind == 0,
            lambtT = (parametro(dias,p(1),p(2:1+nla),p(2+nla),[],[]));
        else
            lambtT = (parametro(dias,p(1),p(2:1+nla),p(2+nla),indicesT,p(1+nla+ntend+1:1+nla+ntend+nind)));
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        f = -sum(lambtT/Ts)+sum(log(lambt(pos))-log(sigt(pos))-(1+1./epst(pos)).*log(z(pos)))...
              +sum(log(lambt(posG))-log(sigt(posG))-xn(posG));
          
        f = -f;
        
        if nargout>1,
            %   Derivadas primeras de la funcion loglikelihood con respecto a los
            %   parametros de la distribucion Pareto -Poisson
            Dlambt = 1./lambt;
            Dsigt = -(1-xn)./(sigt.*z);
            Depst = (-1-epst+(1+epst)./z+log(z))./(epst.^2);

            %   GUMBEL DERIVATIVES
            % Dlambt(posG) = 1./lambt(posG);
            Dsigt(posG) = -(1-xn(posG))./(sigt(posG));
            Depst(posG)=0;

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %   Esto para todos los dias del periodo de estudio
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %   Parametros de Poisson para el resto de los dias
            % dias = (1:Numdias)'/Ts;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            DlambtT = -1/Ts*ones(size(dias));

            neps0 = 1;
            if length(posG)==length(epst),
                neps0 = 0;
            end

            %   Dimensiones del Jacobiano
            Jx = sparse(zeros(2+neps0+nla+nsig+neps+ntend+nind+ntend2+nind2,1));

            %   Elementos del Jacobiano asociados al parametro constante
            %   beta0,alpha0,gamma0

            Jx(1,1) = sum(Dlambt)+sum(DlambtT);

            %   Elementos asociados a la parte inferior de cada bloque
            if nla>0,
                for i = 1:nla,
                    %   Primer elemento asociado al valor constante (primera columna)
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dlambt(k)*Dparam (t(k),i);
                    end
                    for k = 1:length(dias),
                        aux = aux + DlambtT(k)*Dparam (dias(k),i);
                    end
                    Jx(1+i,1) = aux;
                end
            end

            if ntend>0,
                Jx(2+nla,1) =  sum(Dlambt.*t)+sum(DlambtT.*dias);
            end

            if nind>0,
                for i = 1:nind,
                    Jx(1+nla+ntend+i,1) =  sum(Dlambt.*indices(:,i)) + sum(DlambtT.*indicesT(:,i));
                end
            end

            Jx(2+ntend+nind+nla,1) = sum(Dsigt.*sigt);

            if nsig>0,
                for i = 1:nsig,
                    %   Primer elemento asociado al valor constante (primera columna)
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dsigt(k)*Dparam (t(k),i)*sigt(k);
                    end
                    Jx(2+nla+ntend+nind+i,1) = aux;
                end
            end

            if ntend2>0,
                Jx(3+nla+ntend+nind+nsig,1) =  sum(Dsigt.*t.*sigt);
            end

            if nind2>0,
                for i = 1:nind2,
                    Jx(2+nla+ntend+nind+nsig+ntend2+i,1) =  sum(Dsigt.*indices2(:,i).*sigt);
                end
            end


            if neps0 == 1,
                Jx(2+neps0+ntend+nind+nla+nsig+ntend2+nind2,1) = sum(Depst);
            end

            if neps>0,
                for i = 1:neps,
                    %   Primer elemento asociado al valor constante (primera columna)
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Depst(k)*Dparam (t(k),i);
                    end
                    Jx(neps0+2+nla+nsig+ntend+nind+ntend2+nind2+i,1) = aux;
                end
            end
            Jx = -Jx;
        end     
        %   HESSIAN MATRIX
        if nargout>2,
            %   Calculo las derivadas segundas de la funcion log-likelihood con
            %   respecto a los parametros de Pareto-Posson, escala y forma,
            %   respectivamente
            D2lambt = -1./(lambt.^2);
            D2sigt = (-1+(1+epst)./(z.^2))./(sigt.*sigt.*epst);
            D2epst = xn.*(2+epst.*(3+epst).*xn)./((epst.*z).^2)-2*log(z)./(epst.^3);
            Dlambtsigt = zeros(size(D2sigt));
            Dlambtepst = zeros(size(D2sigt));
            Dsigtepst = xn.*(1-xn)./(sigt.*z.^2);

            %   GUMBEL DERIVATIVES
            % D2lambt(posG) = -1./(lambt(posG).^2);
            D2sigt(posG) = (1-2*xn(posG))./(sigt(posG).*sigt(posG));
            D2epst(posG) = 0;
            Dlambtsigt(posG) = 0;
            Dlambtepst(posG) = 0;
            Dsigtepst(posG) = 0;

            %   Dimensiones del Hessiano


            Hxx = sparse(zeros(2+neps0+nla+nsig+neps+ntend+nind+ntend2+nind2));

            %   Elementos del Hessiano asociados al parametro constante
            %   beta0,alpha0,gamma0
            %   Elemento 1 (CAMBIO EXPONENCIAL)
            Hxx(1,1) = sum((D2lambt.*lambt+Dlambt).*lambt)+sum(DlambtT.*lambtT);
            %   Elemento 2 (CAMBIO EXPONENCIAL)
            if ntend>0
                Hxx(2+nla,2+nla) =  sum((D2lambt.*lambt+Dlambt).*lambt.*(t.^2))+sum(DlambtT.*lambtT.*(dias.^2));
            end
            %   Elemento 3 (CAMBIO EXPONENCIAL)
            if nind>0
                for i = 1:nind,
                    for j = 1:i,
                        Hxx(1+nla+ntend+i,1+nla+ntend+j) =  sum((D2lambt.*lambt+Dlambt).*lambt.*indices(:,i).*indices(:,j))+...
                            sum(DlambtT.*lambtT.*indicesT(:,i).*indicesT(:,j));                              
                    end
                end
            end
            %   Elemento 4 (CAMBIO EXPONENCIAL)
            if ntend2>0
                Hxx(3+nla+nsig+ntend+nind,3+nla+nsig+ntend+nind) =  sum((D2sigt.*sigt+Dsigt).*sigt.*(t.^2));
            end
            %   Elemento 5 (CAMBIO EXPONENCIAL)
            if nind2>0
                for i = 1:nind2,
                    for j = 1:i,
                        Hxx(2+nla+nsig+ntend+nind+ntend2+i,2+nla+nsig+ntend+nind+ntend2+j) =  sum((D2sigt.*sigt+Dsigt).*sigt.*indices2(:,i).*indices2(:,j));
                    end
                end
            end
            %   Elemento 6 (CAMBIO EXPONENCIAL)
            Hxx(2+nla+ntend+nind,2+nla+ntend+nind) = sum((D2sigt.*sigt+Dsigt).*sigt);
            %   Elemento 7
            if neps0==1,
                Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,2+neps0+nla+nsig+ntend+nind+ntend2+nind2) = sum(D2epst);
            end
            %   Elementos del Hessiano asociados al parametro constante
            %   beta0,alpha0,gamma0 cruzados
            %   Elemento 8 (CAMBIO EXPONENCIAL)
            Hxx(2+nla+ntend+nind,1) = sum(Dlambtsigt.*sigt.*lambt);
            %   Elemento 9 (CAMBIO EXPONENCIAL)
            if neps0 == 1,
                Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,1) = sum(Dlambtepst.*lambt);
            %   Elemento 10 (CAMBIO EXPONENCIAL)
                Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,2+nla+ntend+nind) = sum(Dsigtepst.*sigt);
            end
            %   Elemento 11 (CAMBIO EXPONENCIAL)
            if ntend>0,
                Hxx(1+nla+ntend,1) = sum((D2lambt.*lambt+Dlambt).*lambt.*t)+sum(DlambtT.*lambtT.*dias);
            end
            %   Elemento 12 (CAMBIO EXPONENCIAL)
            if ntend2>0,
                Hxx(2+nla+ntend+nind+nsig+ntend2,1) = sum(Dlambtsigt.*t.*sigt.*lambt);
            end 
            %   Elemento 52 (CAMBIO EXPONENCIAL)
            if ntend2>0,
                Hxx(2+nla+ntend+nind+nsig+ntend2,2+nla+ntend+nind) = sum((D2sigt.*sigt+Dsigt).*t.*sigt);
            end 
            %   Elemento 48 (CAMBIO EXPONENCIAL)
            if ntend>0 & ntend2>0,
                Hxx(2+nla+ntend+nind+nsig+ntend2,1+nla+ntend) = sum(Dlambtsigt.*t.*t.*sigt.*lambt);
            end
            %   Elemento 13 (CAMBIO EXPONENCIAL)
            if nind>0,
                for i = 1:nind,
                    Hxx(1+nla+ntend+i,1) = sum((D2lambt.*lambt+Dlambt).*lambt.*indices(:,i))+sum(DlambtT.*lambtT.*indicesT(:,i));
                end
            end
            %   Elemento 14 (CAMBIO EXPONENCIAL)
            if nind2>0,
                for i = 1:nind2,
                    Hxx(2+nla+ntend+nind+nsig+ntend2+i,1) = sum(Dlambtsigt.*indices2(:,i).*sigt.*lambt);
                end
            end
            %   Elemento 53 (CAMBIO EXPONENCIAL)
            if nind2>0,
                for i = 1:nind2,
                    Hxx(2+nla+ntend+nind+nsig+ntend2+i,2+nla+ntend+nind) = sum((D2sigt.*sigt+Dsigt).*indices2(:,i).*sigt);
                end
            end
            %   Elemento 49 (CAMBIO EXPONENCIAL)
            if ntend>0 & nind2>0,
                for i = 1:nind2,
                    Hxx(2+nla+ntend+nind+nsig+ntend2+i,1+nla+ntend) = sum(Dlambtsigt.*t.*indices2(:,i).*sigt.*lambt);
                end
            end
            %   Elemento 15 (CAMBIO EXPONENCIAL)
            if nind>0 && ntend>0,
                for i = 1:nind,
                    Hxx(1+nla+ntend+i,1+nla+ntend) = sum((D2lambt.*lambt+Dlambt).*lambt.*t.*indices(:,i))+...
                        sum(DlambtT.*lambtT.*dias.*indicesT(:,i));                  
                end
            end
            %   Elemento 16 (CAMBIO EXPONENCIAL)
            if nind2>0 && ntend2>0,
                for i = 1:nind2,
                    Hxx(2+nla+ntend+nind+nsig+ntend2+i,2+nla+ntend+nind+nsig+ntend2) = sum((D2sigt.*sigt+Dsigt).*t.*indices2(:,i).*sigt);
                end
            end
            %   Elemento 17 (CAMBIO EXPONENCIAL)
            if ntend>0,
                Hxx(2+nla+ntend+nind,1+nla+ntend) = sum(Dlambtsigt.*t.*sigt.*lambt);
            end
            %   Elemento 18 (CAMBIO EXPONENCIAL)
            if ntend>0 && neps0 == 1,
                Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,1+nla+ntend) = sum(Dlambtepst.*t.*lambt);
            end
            %   Elemento 19 (CAMBIO EXPONENCIAL)
            if ntend2>0 && neps0 == 1,
                Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,2+nla+ntend+nind+nsig+ntend2) = sum(Dsigtepst.*t.*sigt);
            end
            %   Elemento 20 (CAMBIO EXPONENCIAL)
            if nind>0,
                for i = 1:nind,
                    Hxx(2+nla+ntend+nind,1+nla+ntend+i) = sum(Dlambtsigt.*indices(:,i).*sigt.*lambt);
                end
            end
            %   Elemento 21 (CAMBIO EXPONENCIAL)
            if nind>0 && neps0 == 1,
                for i = 1:nind,
                    Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,1+nla+ntend+i) = sum(Dlambtepst.*indices(:,i).*lambt);
                end
            end
            %   Elemento 22 (CAMBIO EXPONENCIAL)
            if nind2>0 && neps0 == 1,
                for i = 1:nind2,
                    Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,2+nla+ntend+nind+nsig+ntend2+i) = sum(Dsigtepst.*indices2(:,i).*sigt);
                end
            end

            %   Elementos asociados a la parte unicamente asociada a localizacion
            if nla>0,
                for i = 1:nla,
                    %   Primer elemento asociado al valor constante (primera columna)
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + (D2lambt(k)*lambt(k)+Dlambt(k))*lambt(k)*Dparam (t(k),i);      
                    end
                    for k = 1:length(dias),
                        aux = aux + DlambtT(k)*lambtT(k)*Dparam (dias(k),i);      
                    end
                    %   Elemento 23 (CAMBIO EXPONENCIAL)
                    Hxx(1+i,1) = aux;
                    %   Elementos asociados a los demas parametros
                    for j = 1:i,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + (D2lambt(k)*lambt(k)+Dlambt(k))*Dparam (t(k),i)*Dparam (t(k),j)*lambt(k);
                        end
                        for k = 1:length(dias),
                            aux = aux + (DlambtT(k))*Dparam (dias(k),i)*Dparam (dias(k),j)*lambtT(k);
                        end
                        %   Elemento 24 (CAMBIO EXPONENCIAL)
                        Hxx(1+i,1+j) = aux;
                    end
                end
                %   Elementos de localizacion con respecto alpha0
                for i = 1:nla,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dlambtsigt(k)*Dparam (t(k),i)*sigt(k)*lambt(k);
                    end
                    %   Elemento 25 (CAMBIO EXPONENCIAL)
                    Hxx(2+nla+ntend+nind,1+i) = aux; 
                end
                %   Elementos de localizacion con respecto gamma0
                if neps0 == 1,
                for i = 1:nla,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dlambtepst(k)*lambt(k)*Dparam (t(k),i);
                    end
                    %   Elemento 26 (CAMBIO EXPONENCIAL)
                    Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,1+i) = aux; 
                end  
                end
                %   Elementos de localizacion con respecto a la tendencia
                if ntend>0,
                    for i = 1:nla,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + (D2lambt(k)*lambt(k)+Dlambt(k))*lambt(k)*t(k)*Dparam (t(k),i);
                        end
                        for k = 1:length(dias),
                            aux = aux + DlambtT(k)*lambtT(k)*dias(k)*Dparam (dias(k),i);
                        end
                        %   Elemento 27  (CAMBIO EXPONENCIAL)
                        Hxx(2+nla,1+i) = aux; 
                    end
                end
                %   Elementos de localizacion con respecto a la tendencia 2
                if ntend2>0,
                    for i = 1:nla,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dlambtsigt(k)*t(k)*Dparam (t(k),i)*sigt(k)*lambt(k);
                        end
                        %   Elemento 46 (CAMBIO EXPONENCIAL)
                        Hxx(2+nla+ntend+nind+nsig+ntend2,1+i) = aux;
                    end
                end
                %   Elementos de localizacion con respecto a las covariables
                if nind>0,
                    for i = 1:nla,
                        for j = 1:nind,
                            aux = 0;
                            for k = 1:length(t),
                                aux = aux + (D2lambt(k)*lambt(k)+Dlambt(k))*lambt(k)*indices(k,j)*Dparam (t(k),i);
                            end
                            for k = 1:length(dias),
                                aux = aux + DlambtT(k)*lambtT(k)*indicesT(k,j)*Dparam (dias(k),i);
                            end
                            %   Elemento 28 (CAMBIO EXPONENCIAL)
                            Hxx(1+nla+ntend+j,1+i) = aux; 
                        end
                    end
                end 
                %   Elementos de localizacion con respecto a las covariables 2
                if nind2>0,
                    for i = 1:nla,
                        for j = 1:nind2,
                            aux = 0;
                            for k = 1:length(t),
                                aux = aux + Dlambtsigt(k)*indices2(k,j)*Dparam (t(k),i)*sigt(k)*lambt(k);
                            end
                            %   Elemento 47 (CAMBIO EXPONENCIAL)
                            Hxx(2+nla+ntend+nind+nsig+ntend2+j,1+i) = aux; 
                        end
                    end
                end 
            end

            %   Elementos asociados a la parte unicamente asociada a escala
            if nsig>0,
                for i = 1:nsig,
                %   Primer elemento asociado al valor constante (primera columna)
                aux = 0;
                for k = 1:length(t),
                    aux = aux + (D2sigt(k)*sigt(k)+Dsigt(k))*Dparam (t(k),i)*sigt(k);
                end
                %   Elemento 29 (CAMBIO EXPONENCIAL)
                Hxx(2+nla+ntend+nind+i,2+ntend+nind+nla) = aux;
                %   Elementos asociados a los demas parametros
                for j = 1:i,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + (D2sigt(k)*sigt(k)+Dsigt(k))*Dparam (t(k),i)*Dparam (t(k),j)*sigt(k);
                    end
                    %   Elemento 30 (CAMBIO EXPONENCIAL)
                    Hxx(2+nla+ntend+nind+i,2+nla+ntend+nind+j) = aux;
                end
                end

                %   Elementos de escala con respecto gamma0
                if neps0 == 1,
                for i = 1:nsig,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dsigtepst(k)*Dparam (t(k),i)*sigt(k);
                    end
                    %   Elemento 31 (CAMBIO EXPONENCIAL)
                    Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,2+nla+ntend+nind+i) = aux; 
                end 
                end
                %   Elementos de escala con respecto beta0
                for i = 1:nsig,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dlambtsigt(k)*Dparam (t(k),i)*sigt(k)*lambt(k);
                    end
                    %   Elemento 32 (CAMBIO EXPONENCIAL)
                    Hxx(2+nla+ntend+nind+i,1) = aux; 
                end 
                %   Elementos de localizacion con respecto a la tendencia
                if ntend>0,
                    for i = 1:nsig,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dlambtsigt(k)*t(k)*Dparam (t(k),i)*sigt(k)*lambt(k);
                        end
                        %   Elemento 33 (CAMBIO EXPONENCIAL)
                        Hxx(2+nla+ntend+nind+i,2+nla) = aux; 
                    end
                end
                %   Elementos de localizacion con respecto a las covariables
                if nind>0,
                    for i = 1:nsig,
                        for j = 1:nind,
                            aux = 0;
                            for k = 1:length(t),
                                aux = aux + Dlambtsigt(k)*indices(k,j)*Dparam (t(k),i)*sigt(k)*lambt(k);
                            end
                            %   Elemento 34 (CAMBIO EXPONENCIAL)
                            Hxx(2+nla+ntend+nind+i,1+nla+ntend+j) = aux; 
                        end
                    end
                end    
            end

            %   Elementos asociados a la parte unicamente asociada a forma
            if neps>0 && neps0 == 1,
                for i = 1:neps,
                %   Primer elemento asociado al valor constante (primera columna)
                aux = 0;
                for k = 1:length(t),
                    aux = aux + D2epst(k)*Dparam (t(k),i);
                end
                %   Elemento 35
                Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,2+neps0+nla+nsig+ntend+nind+ntend2+nind2) = aux;
                %   Elementos asociados a los demas parametros
                for j = 1:i,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + D2epst(k)*Dparam (t(k),i)*Dparam (t(k),j);
                    end
                    %   Elemento 36
                    Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,2+neps0+nla+nsig+ntend+nind+ntend2+nind2+j) = aux;
                end
                end
                %   Elementos de escala con respecto alpha0
                for i = 1:neps,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dsigtepst(k)*Dparam (t(k),i)*sigt(k);
                    end
                    %   Elemento 37 (CAMBIO EXPONENCIAL)
                    Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,2+nla+ntend+nind) = aux; 
                end 

                %   Elementos de escala con respecto beta0
                for i = 1:neps,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dlambtepst(k)*Dparam (t(k),i)*lambt(k);
                    end
                    %   Elemento 38 (CAMBIO EXPONENCIAL)
                    Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,1) = aux; 
                end
                %   Elementos de forma con respecto a la tendencia
                if ntend>0,
                    for i = 1:neps,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dlambtepst(k)*t(k)*Dparam (t(k),i)*lambt(k);
                        end
                        %   Elemento 39 (CAMBIO EXPONENCIAL)
                        Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,2+nla) = aux; 
                    end
                end
                %   Elementos de forma con respecto a la tendencia2
                if ntend2>0,
                    for i = 1:neps,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dsigtepst(k)*t(k)*Dparam (t(k),i)*sigt(k);
                        end
                        %   Elemento 44 (CAMBIO EXPONENCIAL)
                        Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,2+nla+nsig+ntend+nind+ntend2) = aux; 
                    end
                end
                %   Elementos de forma con respecto a las covariables
                if nind>0,
                    for i = 1:neps,
                        for j = 1:nind,
                            aux = 0;
                            for k = 1:length(t),
                                aux = aux + Dlambtepst(k)*indices(k,j)*Dparam (t(k),i)*lambt(k);  
                            end
                            %   Elemento 40 (CAMBIO EXPONENCIAL)
                            Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,1+nla+ntend+j) = aux; 
                        end
                    end
                end
                %   Elementos de forma con respecto a las covariables 2
                if nind2>0,
                    for i = 1:neps,
                        for j = 1:nind2,
                            aux = 0;
                            for k = 1:length(t),
                                aux = aux + Dsigtepst(k)*indices2(k,j)*Dparam (t(k),i)*sigt(k);
                            end
                            %   Elemento 45 (CAMBIO EXPONENCIAL)
                            Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,2+nla+nsig+ntend+nind+ntend2+j) = aux; 
                        end
                    end
                end
            end

            %   TENDENCIA COVARIABLES
            if nind>0 & ntend2>0,
                for i = 1:nind,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dlambtsigt(k)*t(k)*indices(k,i)*sigt(k)*lambt(k);
                    end
                    %   Elemento 50 (CAMBIO EXPONENCIAL)
                    Hxx(2+nla+ntend+nind+nsig+ntend2,1+nla+ntend+i) = aux;
                end
            end

            %   COVARIABLES
            if nind>0 & nind2>0,
                for i = 1:nind,
                    for j = 1:nind2,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dlambtsigt(k)*indices2(k,j)*indices(k,i)*sigt(k)*lambt(k);
                        end
                        %   Elemento 51 (CAMBIO EXPONENCIAL)
                        Hxx(2+nla+ntend+nind+nsig+ntend2+j,1+nla+ntend+i) = aux; 
                    end
                end
            end 

            % ESCALA CON TENDENCIA
            if ntend2>0,
                for i = 1:nsig,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + (D2sigt(k)*sigt(k)+Dsigt(k))*t(k)*Dparam (t(k),i)*sigt(k);
                    end
                    %   Elemento 54 (CAMBIO EXPONENCIAL)
                    Hxx(2+nla+ntend+nind+nsig+ntend2,2+nla+ntend+nind+i) = aux;
                end
            end

            %   COVARIABLES CON ESCALA
            if nind2>0,
                for i = 1:nsig,
                    for j = 1:nind2,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + (D2sigt(k)*sigt(k)+Dsigt(k))*indices2(k,j)*Dparam (t(k),i)*sigt(k);
                        end
                        %   Elemento 55 (CAMBIO EXPONENCIAL)
                        Hxx(2+nla+ntend+nind+nsig+ntend2+j,2+nla+ntend+nind+i) = aux; 
                    end
                end
            end 

            %   SUBBLOQUE 2-1
            if nla>0 && nsig>0,
                for j = 1:nla,    
                    %   betas y alphas
                    %   Elementos asociados a los demas parametros
                    for i = 1:nsig,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dlambtsigt(k)*Dparam (t(k),i)*Dparam (t(k),j)*sigt(k)*lambt(k);
                        end
                        %   Elemento 41 (CAMBIO EXPONENCIAL)
                        Hxx(2+nla+ntend+nind+i,1+j) = aux; 
                    end
                end
            end



            %   SUBBLOQUE 3-1
            if nla>0 && neps>0 && neps0==1,
                for j = 1:nla,    
                    %   betas y gamma
                    %   Elementos asociados a los demas parametros
                    for i = 1:neps,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dlambtepst(k)*lambt(k)*Dparam (t(k),i)*Dparam (t(k),j);
                        end
                        %   Elemento 42 (CAMBIO EXPONENCIAL)
                        Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,1+j) = aux; 
                    end
                end
            end

            %   SUBBLOQUE 3-2
            if nsig>0 && neps>0 && neps0==1,
                for j = 1:nsig,    
                    %   alphas y gamma
                    %   Elementos asociados a los demas parametros
                    for i = 1:neps,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dsigtepst(k)*Dparam (t(k),i)*Dparam (t(k),j)*sigt(k);
                        end
                        %   Elemento 43 (CAMBIO EXPONENCIAL)
                        Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,2+nla+ntend+nind+j) = aux; 
                    end
                end
            end             

            Hxx = Hxx + tril(Hxx,-1)';
            
            Hxx = -Hxx;  
        end
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function dp = Dparam (t,j)
    %
    %   Derivada con respecto a los parametros asociados a senos y cosenos
    %
    %   Input:
    %        t-> Abscisas o tiempo en la que calcular el valor de la 
    %            derivada (Entre 0 y 1)
    %        j-> Numero de armonico dentro del vector
    %
    %   Output:
    %        dp-> Valor de la derivada
    %
    
        if mod(j,2) == 0,
            dp = sin(j/2*2*pi*t);
        else
            dp = cos((j+1)/2*2*pi*t);
        end

    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
