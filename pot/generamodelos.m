function model = generamodelos(np,n)
%   np es el numero maximo de parametros a incluir
%   n es el numero de parametros
if nargin<2,
    n = 3;
end


ne = (np + 1)^n;
model = zeros(ne,n);
pos = zeros(n,1);

for i = 1:ne,
    div = i;
    for j=n-1:-1:1,
        coc = floor(div/(np + 1)^j);
        resto = mod(div, (np + 1)^j);
        if resto>0,
            if coc < np + 1,
                pos(n - j) = coc + 1;
                if j == 1, pos(n) = resto; end
                div = resto;
            else
                pos(n - j) = coc;
                if j == 1, pos(n) = resto; end
                div = resto;
            end
        else
            pos(n - j) = coc;
            for k = max(n - j + 1, n):n,
                pos(k) = np + 1;
            end
            break
        end
        
    end
    
    aux = pos(1);
    for k=2:n,
    	aux = (aux - 1)*(np + 1) + pos(k);
    end
    model(i,:) = pos'-ones(1,n);
end