function [Ts,quanaggr,stdlo,stdup,aqPOTLegend]=amevaAgregateQuantilesPOT(xamax,umbral,confidenceLevelAlpha,lambda0,sigma0,gamma0,invI0,ny,Opciones,gcax,upc)
if confidenceLevelAlpha>0.5, error(['confidenceLevelAlpha=',num2str(confidenceLevelAlpha)]); end
if ~exist('Opciones','var'), Opciones = ameva_options(); end
if ~exist('upc','var') || isempty(upc), upc = 99.5; disp('Se supone que el calculo del umbral se ha hecho con el percentil del 99.5'); end

%  Aggregate quantiles for different return periods
Ts = [2:1:9 10:10:90 100:100:500]; xlimtick = [2 10 50 100];
if exist('Opciones','var') && isprop(Opciones,'Ts') && ~isempty(Opciones.Ts),
    Ts=Opciones.Ts;
end
Ts = Ts(:);

if max(Ts)>100
    xlimtick=[2 10 50 100 Ts(end)];
end
nts = length(Ts);
quanaggr = zeros(nts,1);
stdup = zeros(nts,1);
stdlo = zeros(nts,1);
%   Sarting calculations
for j = 1:nts,
    %    Annual
    [quan,Dq] = QuantilePOT (1-1/Ts(j),umbral,lambda0,sigma0,gamma0);
    stdDq = sqrt(sum((Dq'*invI0).*Dq',2));
    quanaggr(j)=quan;
    %   Intervalos de confianza del cuantil anual
    stdup(j)= quan+stdDq*norminv(1-confidenceLevelAlpha/2,0,1);
    stdlo(j)= quan-stdDq*norminv(1-confidenceLevelAlpha/2,0,1);
end
%   Datos anuales
Hsmaxor = sort(xamax);
%Proxor = (((1:ceil(ny)))/(ceil(ny)+1))';
Proxor = (((1:length(Hsmaxor)))/(length(Hsmaxor)+1))';
Tapprox = 1./(1-Proxor);
id = find(Tapprox>Ts(1));

if Opciones.myPlots
    if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
    if ~ishandle(gcax), disp('WARNING! amevaAgregateQuantilesPOT with axes'); end
    set(gcf,'CurrentAxes',gcax)
    
    % Texto idiomas eng esp Cargo los titulos en los diferentes idiomas de todas las fichas
    texto = amevaclass.amevatextoidioma(Opciones.idioma);
    Lab = labelsameva(Opciones.var_name,Opciones.var_unit);%Labels
    confidenceLevel = 1-confidenceLevelAlpha;
    
    semilogx(gca,Tapprox(id),Hsmaxor(id),'.','Color',[0 0 0]);hold on;
    semilogx(gca,Ts,quanaggr,'r')
    semilogx(gca,Ts,stdlo,'--r')
    semilogx(gca,Ts,stdup,'--r'); hold off;
    set(gca,'XTick',xlimtick,'XTickLabel',num2cell(xlimtick),'Xlim',[xlimtick(1) xlimtick(end)])
    grid(gca,'on')
    xlabel(gca,texto('rmfigPeriodret'));
    ylabel(gca,Lab);
    
    aqPOTLegend = legend({texto('anuald'),texto('anualf'),[num2str(confidenceLevel,2) ' ',texto('ic')]},'Location','SouthEast');
    title({texto('rmfigTituloPOT'),['Pareto-Poisson (\sigma=',num2str(exp(sigma0),3),' \xi=', ...
        num2str(gamma0,3),' \lambda=',num2str(lambda0,3),'). ',Opciones.var_name{1}, ...
        '_{',num2str(upc),'}=',num2str(umbral)]},'fontsize',11,'fontweight','b');
    
    hold(gca,'off')
end

end