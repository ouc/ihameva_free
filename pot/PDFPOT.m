function f = PDFPOT (x,u,lambda0,sigma0,gamma0)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   PDFPOT function calculates the GEV distribution function corresponding
%    to the given parameters
%
%   Input:
%       x -> maximum data
%   Output:
%        F-> Values of the distribution function
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if nargin<5,
        gamma0 = [];
    end
    if isempty(gamma0),
        neps0 = 0;
    else
        neps0 = 1;
    end
    %   Evaluate the location parameter at each time t as a function
    %   of the actual values of the parameters given by p
    lambt = lambda0*ones(size(x));
    %   Evaluate the scale parameter at each time t as a function
    %   of the actual values of the parameters given by p
    sigt = exp(sigma0)*ones(size(x));
    %   Evaluate the shape parameter at each time t as a function
    %   of the actual values of the parameters given by p
    if neps0
        epst = gamma0*ones(size(x));
    else
        epst = zeros(size(x));
    end

    %   The values whose shape parameter is almost cero corresponds to
    %   the GUMBEL distribution, locate their positions if they exist
    posG = find(abs(epst)<=1e-8);
    %   The remaining values correspond to WEIBULL or FRECHET
    pos  = find(abs(epst)>1e-8);
    %   The corresponding GUMBEl values are set to 1 to avoid 
    %   numerical problems, note that for those cases the GUMBEL
    %   expressions are used
    epst(posG)=1;

% % %     pos2 = find(epst<0);
% % %     if ~isempty(pos2),
% % %         for ii = 1:length(pos2),
% % %             x(pos2(ii)) = min(x(pos2(ii)),-sigt(pos2(ii))/epst(pos2(ii)));
% % %         end
% % %     end
    
    xn = (x-u)./sigt;
    
    aux = (1+epst(pos).*xn);
    posS = find(aux<=0);
    aux(posS)=0;

    f = zeros (size(x));
    %   WEIBULL or FRECHET distribution function
    f(pos) = exp(-lambt(pos).*aux.^(-1./epst(pos))).*lambt(pos).*aux.^(-1-1./epst(pos))./sigt(pos);
    %   GUMBEL distribution function
    f(posG) = exp(-lambt(posG).*exp(-xn(posG))-xn(posG)).*lambt(posG)./sigt(posG);




