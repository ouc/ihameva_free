function  [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,hessian,popt,lista,lista2,datemaxanual,up0,up,Ts,solucion,rowLabels,columnLabels]...
        = FuncAutoAdjustPOT(Hsmax,datemax,kt,indices,dias,indicesT,quanval,criterio,cityname,carpeta,var_name,var_unit)

if nargin<2, error('Faltan datos, se necesitan dos series para poder continuar'); end
if ~exist('kt','var') || isempty(kt),  kt = ones(size(Hsmax)); end
if ~exist('indices','var') || isempty(indices), indices = []; end
if ~exist('quanval','var') || isempty(quanval), quanval = 0.95; end
if ~exist('criterio','var') || isempty(criterio), criterio = 'ProfLike'; end
if ~exist('cityname','var'), cityname = 'Tabevol'; end
if ~exist('var_name','var'), var_name = 'Hs'; end
if ~exist('var_unit','var'), var_unit = 'm'; end
if ~exist('svopt','var') || isempty(svopt), svopt=[0 0 0]; end %save option 1-fig 2-png 3-eps (1/0)
if ~exist('carpeta','var') || isempty(carpeta), carpeta='sample_pot_dir'; end;
if sum(svopt)>0, if ~exist([pwd filesep carpeta],'dir'),  mkdir(carpeta); end; end

%   AJUSTE AUTOMATICO EXTREMAL NO ESTACIONARIO MEDIANTE EL METODO DE
%   SENSIBILIDAD. INICIALMENTE SE AJUSTAN LOS ARMONICOS, DESPUES TENDENCIA
%   Y DESPUES COVARIABLES
time = cputime;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Maximos mensuales de Melisa y Cristina
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%tipo = 'Mensual';
%Hsmax = load('Y+umbral.txt');
Hsmax = Hsmax/100;
%u = load('umbrales.txt');
%datemax = load('T.txt');
datemaxanual = mod(datemax,1);
Ts = 365.25;
%dias = load('Tdias.txt')/Ts;


%   Introducimos los indices 
% indices =  load('Modo1.txt');
% indices =  [indices load('Modo2.txt')];
% indices =  [indices load('Modo3.txt')];
% indices =  [indices load('Modo4.txt')];
% indices =  [indices load('Modo5.txt')];
% indices =  [indices load('Modo6.txt')];
% indices =  [indices load('Modo7.txt')];
% indices =  [indices load('Modo8.txt')];
% indices =  [indices load('Modo9.txt')];
% indices =  [indices load('Modo10.txt')];
% indicestotal = indices;

%   Cargo los modos totales
%indicesT = load('ModosDias.txt')';
indicesT = indicesT(:,1:2:end);

%   Elimino de dias ndayex (3 dias) los dias posteriores a un evento de excedencia
ndayex = 3;
pos = 1;
elimlist = [];
numero = 0;
for i = 1: length(datemax),
    for j = pos:length(dias),
        if datemax(i)>=dias(j) && datemax(i)<dias(j+1),
            elimlist = [elimlist; (j+1:min(length(dias),j+ndayex))'];
            pos = j+4;
            numero = numero +1 ;
            break
        end
    end
end

%   Elimino esos dias tanto de los dias como de las covariables
select = setdiff((1:length(dias))',elimlist);
dias = dias(select);
indicesT = indicesT(select,:);

% % % % indices = [];
% % % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % %%%%%%%   COMPROBACION DE DERIVADAS
% % % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Parametros para la definicion de umbral
if ~exist('up','var') || isempty(up)
    up0 = 502.52342/100;
    up = [230.42668; 58.401071]/100;
else
    up=[0 0];
end
    
% % % auxU = parametro (datemax,up0,up);
% % % 
% % % beta0 = log(765.71813/100);
% % % % beta = [326.37649; 65.388902];
% % % beta = [0.05  0.02];
% % % alpha0 = 97.091091/100;
% % % % alpha = [33.379328;0.33528097];
% % % alpha = [0.05  0.02];
% % % gamma0 = -0.14068436;
% % % gamma = [0.05  0.02];
% % % betaT = [0.1];
% % % varphi = [0.8];
% % % betaT2 = [0.1];
% % % varphi2 = [0.7];
% % % 
% % % %   COMPROBACION DE DERIVADAS
% % % % dias = 0;
% % % beta0=log(length(Hsmax)/Ts);
% % % alpha0 = log(std(Hsmax));
% % % gamma0 = -0.1;
% % % 
% % % logli = loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,[],...
% % %     [],[],beta0,[],alpha0,[],gamma0,[],[],[],[],[]);
% % % 
% % % Jx = Jacobian (Hsmax,datemax,up0,up,Ts,dias,[],...
% % %     [],[],beta0,[],alpha0,[],gamma0,[],[],[],[],[]);
% % % 
% % % Hxx = Hessian (Hsmax,datemax,up0,up,Ts,dias,[],...
% % %     [],[],beta0,[],alpha0,[],gamma0,[],[],[],[],[]);
% % % 
% % % logli = loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,indicesT(:,1),...
% % %     indices(:,1),indices(:,1),beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
% % % 
% % % Jx = Jacobian (Hsmax,datemax,up0,up,Ts,dias,indicesT(:,1),...
% % %     indices(:,1),indices(:,1),beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
% % % 
% % % Hxx = Hessian (Hsmax,datemax,up0,up,Ts,dias,indicesT(:,1),...
% % %     indices(:,1),indices(:,1),beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
% % % 
% % % incre = 0.001;
% % % varphi(1)= varphi(1)*(1+incre);
% % % logli2 = loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,indicesT(:,1),...
% % %     indices(:,1),indices(:,1),beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
% % % varphi(1) = varphi(1)/(1+incre);
% % % varphi(1) = varphi(1)*(1-incre);
% % % logli1 = loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,indicesT(:,1),...
% % %     indices(:,1),indices(:,1),beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
% % % varphi(1) = varphi(1)/(1-incre);
% % % (logli2-logli1)/(2*varphi(1)*incre)
% % % full(Jx)
% % % (logli2-2*logli+logli1)/((varphi(1)*incre)^2)
% % % full(diag(Hxx))
% % % 
% % % incre = 0.001;
% % % varphi2(1) = varphi2(1)*(1+incre);
% % % betaT2(1) = betaT2(1)*(1+incre);
% % % logli22 = loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,indicesT(:,1),...
% % %     indices(:,1),indices(:,1),beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
% % % varphi2(1) = varphi2(1)/(1+incre);
% % % betaT2(1) = betaT2(1)/(1+incre);
% % % varphi2(1) = varphi2(1)*(1-incre);
% % % betaT2(1) = betaT2(1)*(1-incre);
% % % logli11 = loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,indicesT(:,1),...
% % %     indices(:,1),indices(:,1),beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
% % % varphi2(1) = varphi2(1)/(1-incre);
% % % betaT2(1) = betaT2(1)/(1-incre);
% % % varphi2(1) = varphi2(1)*(1+incre);
% % % betaT2(1) = betaT2(1)*(1-incre);
% % % logli21 = loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,indicesT(:,1),...
% % %     indices(:,1),indices(:,1),beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
% % % varphi2(1) = varphi2(1)/(1+incre);
% % % betaT2(1) = betaT2(1)/(1-incre);
% % % varphi2(1) = varphi2(1)*(1-incre);
% % % betaT2(1) = betaT2(1)*(1+incre);
% % % logli12 = loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,indicesT(:,1),...
% % %     indices(:,1),indices(:,1),beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
% % % varphi2(1) = varphi2(1)/(1-incre);
% % % betaT2(1) = betaT2(1)/(1+incre);
% % % der2b = (logli22-logli21-logli12+logli11)./(4*(betaT2(1)*incre).*varphi2(1)*incre)
% % % full((Hxx))


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   COMIENZO DEL PROCEDIMIENTO ITERATIVO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Numero maximo de iteraciones del proceso de ajuste
itermax = 20;

%   Paramtetros para almacenar la evolucion del proceso
beta0IT = zeros(itermax,1);
betaIT = zeros(2*itermax,itermax);
alpha0IT = zeros(itermax,1);
alphaIT = zeros(2*itermax,itermax);
gamma0IT = zeros(itermax,1);
gammaIT = zeros(2*itermax,itermax);

betaTIT  = zeros(itermax,1);
varphiIT = zeros(2*itermax,itermax);
betaT2IT  = zeros(itermax,1);
varphi2IT = zeros(2*itermax,itermax);

AIKIT = zeros(itermax,1);
loglikIT = zeros(itermax,1);
npIT = zeros(itermax,1);
model=zeros(1,3);

%   Initial values for iteration 1, sin tendencia no covariables
nla = 0;
nsig = 0;
neps = 0;
indicesaux = [];
nind = 0;
ntend = 0;
nind2 = 0;
ntend2 = 0;



% % % [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,grad,hessian] = OptiParamHessianPOT(0,0,0,Hsmax,datemax,up0,up,Ts,dias,[],[],[],[],[],[],[],[]);
% % % logli = loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,[],...
% % %     [],[],beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
% % % 
% % % Jx = Jacobian (Hsmax,datemax,up0,up,Ts,dias,[],...
% % %     [],[],beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
% % % 
% % % Hxx = Hessian (Hsmax,datemax,up0,up,Ts,dias,[],...
% % %     [],[],beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   COMIENZO DEL PROCEDIMIENTO DE ARMONICOS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i = 1:itermax,

    [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,grad,hessian,pold] = OptiParamHessianPOT(nla,nsig,neps,Hsmax,datemax,up0,up,Ts,dias,[],[],[],[],[],[],[],[]);
   
    [loglikeobj, Jx,Hxx]=loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,[],[],[],beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
    
    neps0 = 1;
    if (abs(gamma0)<=1e-8),
        neps0 = 0;
    end
    
    beta0IT(i) = beta0;
    betaIT (1:length(beta),i) = beta;
    alpha0IT(i) = alpha0;
    alphaIT (1:length(alpha),i) = alpha;
    if neps0 == 1,
        gamma0IT (i) = gamma0;
        gammaIT (1:length(gamma),i) = gamma;
    end

    loglikIT (i)= loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,[],[],[],beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
    AIKIT (i)= -2*loglikIT (i)+2*(2+neps0+2*nla+2*nsig+2*neps+ntend+nind); 
    npIT (i) = 2+neps0+2*nla+2*nsig+2*neps+ntend+nind+ntend2+nind2;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Calculo del Jacobiano
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [auxobj auxJx] = loglikeobjpot (Hsmax,datemax,up0,up,Ts,dias,[],[],[],beta0,[beta;0;0],alpha0,[alpha;0;0],gamma0,[gamma;0;0],[],[],[],[]);
    
    if i>1,   
        if AIKIT (i)<AIKIT (i-1),
            modelant = [nla nsig neps]';
            popt = pold;
        end
    end
    
    pos = 1;
    maximumval = abs(sum(auxJx(1+2*nla+1:1+2*nla+2)));
    auxmax = abs(sum(auxJx(2+2*nla+ntend+nind+2*nsig+2+1:2+2*nla+ntend+nind+2*nsig+2+2)));
    if auxmax>maximumval,
        maximumval=auxmax;
        pos = 2;
    end
    if neps0 ==1,
    auxmax = abs(sum(auxJx(2+neps0+2*nla+ntend+nind+ntend2+nind2+2*nsig+2*neps+4+1:2+neps0+2*nla+ntend+nind+ntend2+nind2+2*nsig+2*neps+4+2)));
    if auxmax>maximumval,
        maximumval=auxmax;
        pos = 3;
    end
    end
    
    if pos ==1,
%         pold = [pold(1:1+2*nla); 0; 0; pold(2+2*nla:end)];
        nla = nla+1;
    end
    if pos ==2,
%         pold = [pold(1:2+2*nla+ntend+nind+2*nsig); 0; 0; pold(3+2*nla+ntend+nind+2*nsig:end)];
        nsig = nsig+1;
    end
    if pos ==3,
%         pold = [pold; 0; 0];
        neps = neps+1;
    end    
    
    if i>1,   
        if AIKIT (i)>=AIKIT (i-1),
            model = modelant;
            AIKITini = AIKIT (i-1);
            break;
        else
            model = [nla nsig neps]';
        end
    end
    
end
%   End of the harmonic iterative process
%   Storage the iteration number
    nit = i;
%   Obtaining the maximum likelihood estimators for the best model 
    nla=model(1);nsig=model(2);neps=model(3);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   COMIENZO DEL PROCEDIMIENTO DE COVARIABLES  EN EL PARAMETRO DE LOCALIZACION
%   Y EN EL PARAMETRO DE ESCALA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[nd nind] = size(indices);
[nd nind2] = size(indices);
auxindices = [];
lista = [];
auxnind = nind;
auxindices2 = [];
lista2 = [];
auxnind2 = nind2;
nind = 0;
nind2 = 0;

if ~isempty(indices),

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Calculo del Jacobiano
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


for i = nit:nit-1+2*auxnind,
    auxvarphi = zeros(auxnind,1);
    if ~isempty(lista),
        auxvarphi(lista) = varphi;
    end
    auxvarphi2 = zeros(auxnind2,1);
    if ~isempty(lista2),
        auxvarphi2(lista2) = varphi2;
    end
    [auxobj auxJx] = loglikeobjpot (Hsmax,datemax,up0,up,Ts,dias,indicesT,indices,indices,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,auxvarphi,betaT2,auxvarphi2);
         
    [maximo pos]= max(abs(auxJx(2+2*nla+ntend:1+2*nla+ntend+auxnind)));
    [maximo2 pos2]= max(abs(auxJx(2+2*nla+ntend+auxnind+2*nsig+1:2+2*nla+ntend+auxnind+2*nsig+auxnind2)));
%      maximo2 = 0;
    if maximo>maximo2,
        lista = [lista; pos];
        nind = nind + 1;
    else
        lista2 = [lista2; pos2];
        nind2 = nind2 + 1;
    end
    
    auxindices = indices(:,lista);
    auxindicesT = indicesT(:,lista);
    auxindices2 = indices(:,lista2);
    
    
    [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,grad,hessian] = ...
        OptiParamHessianPOT(nla,nsig,neps,Hsmax,datemax,up0,up,Ts,dias,betaT,auxindices,auxindicesT,zeros(nind,1),betaT2,auxindices2,zeros(nind2,1),[popt(1:1+2*nla+ntend); zeros(nind,1); popt(2+2*nla+ntend:2+2*nla+ntend+2*nsig+ntend2); zeros(nind2,1); popt(3+2*nla+ntend+2*nsig+ntend2:end)]);
    if max(abs(grad))>0.1,
        [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,grad,hessian] = ...
            OptiParamHessianPOT(nla,nsig,neps,Hsmax,datemax,up0,up,Ts,dias,betaT,auxindices,auxindicesT,zeros(nind,1),betaT2,auxindices2,zeros(nind2,1));
    end
    [loglikeobj, Jx,Hxx]= loglikeobjpot (Hsmax,datemax,up0,up,Ts,dias,auxindicesT,auxindices,auxindices2,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);

    
    neps0 = 1;
    if (abs(gamma0)<=1e-8),
        neps0 = 0;
    end
    
    beta0IT(i) = beta0;
    betaIT (1:length(beta),i) = beta;
    alpha0IT(i) = alpha0;
    alphaIT (1:length(alpha),i) = alpha;
    if neps0 == 1, gamma0IT (i) = gamma0; end
    gammaIT (1:length(gamma),i) = gamma;
    if ~isempty(betaT),
        betaTIT(i) = betaT;
    end
    if ~isempty(varphiIT),
        varphiIT(lista,i) = varphi;
    end
    if ~isempty(betaT2),
        betaT2IT(i) = betaT2;
    end
    if ~isempty(varphi2IT),
        varphi2IT(lista2,i) = varphi2;
    end
 
    
    %     [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,grad,hessian] = ...
%     OptiParamHessianPOT(nla,nsig,neps,Hsmax,datemax,up0,up,Ts,dias,betaT,[],[],[],ntend2,[],[],[popt(1:1+2*nla); zeros(ntend,1); popt(2+2*nla:2+2*nla+ntend+nind+2*nsig); zeros(ntend2,1);  popt(2+2*nla+ntend+nind+2*nsig+1:end)]);
%     Jacobian (Hsmax,datemax,up0,up,Ts,dias,[],[],[],beta0,beta,alpha0,alpha,gamma0,gamma,betaT,[],betaT2,[]);
%     loglikIT (nit+2)= loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,[],[],[],beta0,beta,alpha0,alpha,gamma0,gamma,betaT,[],betaT2,[]);
%     AIKIT (nit+2) = -2*loglikIT (nit+1)+2*(2+neps0+2*nla+2*nsig+2*neps+ntend+nind+ntend2+nind2); 
%     npIT (nit+2) = 2+neps0+2*nla+2*nsig+2*neps+ntend+nind+ntend2+nind2;

    loglikIT (i)= loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,auxindicesT,auxindices,auxindices2,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
    AIKIT (i) = -2*loglikIT (i)+2*(2+neps0+2*nla+2*nsig+2*neps+ntend+nind+ntend2+nind2); 
    npIT (i) = 2+neps0+2*nla+2*nsig+2*neps+ntend+nind+ntend2+nind2;
      
    if AIKIT (i)<AIKITini,
        AIKITini = AIKIT (i);
    else
        if maximo>maximo2,
            lista = lista(1:end-1);
            nind = nind-1;
        else
            lista2 = lista2(1:end-1);
            nind2 = nind2-1;
        end
        nit = i-1;
        break; 
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   COMIENZO DEL PROCEDIMIENTO DE TENDENCIA 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%   Trato de introducir la pendiente
%   Tendencia en el parametro de localizacion
ntend = 1;
[beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,grad,hessian] = ...
    OptiParamHessianPOT(nla,nsig,neps,Hsmax,datemax,up0,up,Ts,dias,ntend,[],[],[],[],[],[],[popt(1:1+2*nla); 0 ;popt(2+2*nla:end)]);
% Jacobian (Hsmax,datemax,up0,up,Ts,dias,[],[],[],beta0,beta,alpha0,alpha,gamma0,gamma,betaT,[],[],[]);

[loglikeobj, Jx,Hxx]= loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,[],...
            [],[],beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);

loglikIT (nit+1)= loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,[],...
            [],[],beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);
AIKIT (nit+1) = -2*loglikIT (nit+1)+2*(2+neps0+2*nla+2*nsig+2*neps+ntend+nind+ntend2+nind2); 
npIT (nit+1) = 2+neps0+2*nla+2*nsig+2*neps+ntend+nind+ntend2+nind2;

beta0IT(nit+1) = beta0;
betaIT (1:length(beta),nit+1) = beta;
alpha0IT(nit+1) = alpha0;
alphaIT (1:length(alpha),nit+1) = alpha;
if neps0 == 1, gamma0IT (nit+1) = gamma0; end
gammaIT (1:length(gamma),nit+1) = gamma;

if ~isempty(betaT),
    betaTIT(nit+1) = betaT;
end

if AIKIT (nit+1)>AIKITini,
    ntend = 0;
    betaT = [];
    [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,grad,hessian,pold] = ...
        OptiParamHessianPOT(nla,nsig,neps,Hsmax,datemax,up0,up,Ts,dias,betaT,[],[],[],[],[],[],popt);
else
    AIKITini = AIKIT (nit+1);
    nit=nit+1;
end

%   Tendencia en el parametro de escala
ntend2 = 1;
[beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,grad,hessian] = ...
    OptiParamHessianPOT(nla,nsig,neps,Hsmax,datemax,up0,up,Ts,dias,betaT,[],[],[],ntend2,[],[],[popt(1:1+2*nla); zeros(ntend,1); popt(2+2*nla:2+2*nla+ntend+nind+2*nsig); zeros(ntend2,1);  popt(2+2*nla+ntend+nind+2*nsig+1:end)]);
% Jacobian (Hsmax,datemax,up0,up,Ts,dias,[],[],[],beta0,beta,alpha0,alpha,gamma0,gamma,betaT,[],betaT2,[]);

[loglikeobj, Jx,Hxx]= loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,[],[],[],beta0,beta,alpha0,alpha,gamma0,gamma,betaT,[],betaT2,[]);

loglikIT (nit+1)= loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,[],[],[],beta0,beta,alpha0,alpha,gamma0,gamma,betaT,[],betaT2,[]);
AIKIT (nit+1) = -2*loglikIT (nit+1)+2*(2+neps0+2*nla+2*nsig+2*neps+ntend+nind+ntend2+nind2); 
npIT (nit+1) = 2+neps0+2*nla+2*nsig+2*neps+ntend+nind+ntend2+nind2;

beta0IT(nit+1) = beta0;
betaIT (1:length(beta),nit+1) = beta;
alpha0IT(nit+1) = alpha0;
alphaIT (1:length(alpha),nit+1) = alpha;
if neps0 == 1, gamma0IT (nit+1) = gamma0; end
gammaIT (1:length(gamma),nit+1) = gamma;
if ~isempty(betaT),
    betaTIT(nit+1) = betaT;
end
if ~isempty(betaT2),
    betaT2IT(nit+1) = betaT2;
end

if AIKIT (nit+1)>AIKITini,
    ntend2 = 0;
    betaT2 = [];
    [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,grad,hessian,pold] = ...
        OptiParamHessianPOT(nla,nsig,neps,Hsmax,datemax,up0,up,Ts,dias,betaT,[],[],[],betaT2,[],[],[popt(1:1+2*nla); zeros(ntend,1); popt(2+2*nla:2+2*nla+ntend+nind+2*nsig); zeros(ntend2,1);  popt(2+2*nla+ntend+nind+2*nsig+1:end)]);
else
    AIKITini = AIKIT (nit+1);
    nit=nit+1;
end


%   Modelo definitivo con covariables en el parametro de localizacion
[beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,grad,hessian] = ...
        OptiParamHessianPOT(nla,nsig,neps,Hsmax,datemax,up0,up,Ts,dias,betaT,indices(:,lista),indicesT(:,lista),zeros(nind,1),betaT2,indices(:,lista2),zeros(nind2,1),[popt(1:1+2*nla+ntend); zeros(nind,1); popt(2+2*nla+ntend:2+2*nla+ntend+2*nsig+ntend2); zeros(nind2,1); popt(3+2*nla+ntend+2*nsig+ntend2:end)]);


auxvarphi = zeros(auxnind,1);
if ~isempty(lista),
    auxvarphi(lista) = varphi;
end
auxvarphi2 = zeros(auxnind2,1);
if ~isempty(lista2),
    auxvarphi2(lista2) = varphi2;
end
[loglikeobj Jx Hxx] = loglikeobjpot (Hsmax,datemax,up0,up,Ts,dias,indicesT,indices,indices,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,auxvarphi,betaT2,auxvarphi2);
% Hxx = Hessian (Hsmax,datemax,up0,up,Ts,dias,indicesT,indices,indices,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,auxvarphi,betaT2,auxvarphi2);
% loglikeobj= loglikeobjpot(Hsmax,datemax,up0,up,Ts,dias,indicesT(:,lista),indices(:,lista),indices(:,lista2),beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2);

AIK = -2*loglikeobj+2*(2+neps0+2*nla+2*nsig+2*neps+ntend+nind+ntend2+nind2); 
disp(['AKAIKE DEFINITIVO: aik=' num2str(AIK)])
disp(['Tiempo de ejecucion: time=' num2str(cputime-time)])

else
    auxnind = 0;
    i = i+1;
end

[solucion,rowLabels,columnLabels]=texsolutionpot(beta0IT,betaIT,alpha0IT,alphaIT,gamma0IT,gammaIT,betaTIT,varphiIT,betaT2IT,varphi2IT,...
npIT,loglikIT,AIKIT,nla,nsig,neps,neps0,ntend,nind,ntend2,nind2,nit,lista,lista2);
%   Plots
if ~isempty(cityname),
    %Por aqui Graficos POT OJO OJO
    %quanval=1-quanval;
    %[quan95,stdmut,stdpsit,stdepst,stdDq,mut,psit,epst,datemaxanual]=preplot(datemax,kt,indices,quanval,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,invI0);
    %GraficosGEV(Hsmax,datemax,kt,indices,quanval,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,quan95,stdmut,stdpsit,stdepst,stdDq,mut,psit,epst,lista,lista2,invI0,datemaxanual,cityname,carpeta);
end
%   Save file
outpres=4;%precision de salida de los resultado 3
if nargout==0,
    save([carpeta,filesep,'data_pot',cityname,'_ML.mat'],'Hsmax','datemax','kt','up0','up','Ts','dias','indicesT','indices','quanval','criterio',...
	'beta0','beta','alpha0','alpha','gamma0','gamma','betaT','varphi','betaT2','varphi2','loglikeobj','grad','hessian','popt', ...
	'lista','lista2','datemaxanual',...
    'beta0IT','betaIT','alpha0IT','alphaIT','gamma0IT','gammaIT','betaTIT','varphiIT','betaT2IT','varphi2IT','npIT','loglikIT','AIKIT');
    caption = {'Automatic parameter selection.' cityname};
    matrix2latex(solucion(1:length(rowLabels),1:length(columnLabels)),[carpeta '/' cityname '.tex' ], 'rowLabels', rowLabels,'columnLabels', columnLabels, 'alignment', 'c', 'format', '%-8.3f', 'size', 'scriptsize','tablelong', 'normal','caption',caption);
    faGAP=[char(rowLabels) num2str(solucion,outpres) char(rowLabels)];
    faGAP=strvcat(faGAP,'Code:MatLab');
    faGAP=strvcat(faGAP,['Stop criteria:',criterio]);
    save([carpeta,filesep,'data_OParam',cityname,'.mat'],'faGAP');
end
