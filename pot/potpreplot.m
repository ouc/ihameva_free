function [quan95,stdDq,mut,psit,epst,damax,xamax,invI0]=potpreplot(datemax,Hsmax,lambda0,sigma0,gamma0,quanval,hessian,loglikeobj,grad,umbral)
global neps0

%   Time (yearly scale) maximos anuales
xamax = zeros(ceil(datemax(end,1)),1);
damax = zeros(ceil(datemax(end,1)),1);
k=1;
for i=1:length(xamax),
    pos = find(datemax>=(i-1) & datemax(:,1)<i);
    [val pos2] = max(Hsmax(pos));
    if ~isempty(val)
        xamax(k) = val;
        damax(k) = datemax(pos(pos2));
        k=k+1;
    end
end
xamax=xamax(1:k-1);
damax=damax(1:k-1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Standard deviation for the parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[LX,UX,PX] = lu(hessian);%   LU decomposition of the Information matrix
invI0 = (UX)\(LX\(PX*eye(2+neps0)));%   Inverse
stdpara = sqrt(diag(invI0));%    Standard deviation
popt = [lambda0; sigma0; gamma0];%   Best parameter vector

pup = popt+norminv(1-quanval/2)*stdpara;
plo = popt-norminv(1-quanval/2)*stdpara;
%[plo popt pup]

%   Change the sign for the loglikelihood function, Jacobian and Hessian
loglikeobj = -loglikeobj;
grad = -grad;
hessian = -hessian;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Evaluation of the final parameters for plotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = linspace(0,1,100)';
t = mod(datemax,1);
%   Location
mut = lambda0;
%   Scale
psit = exp(sigma0);
%   Shape
epst = gamma0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Location and scale parameter plotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[quan95,Dq] = QuantilePOT (0.95,umbral,lambda0,sigma0,gamma0);
stdDq = sqrt(sum((Dq'*invI0).*Dq',2));


