function HsmaxPlot(Hsmax,datemax,umbral,mkrformat,cityname,var_name,var_unit,gcax)

    if ~exist('mkrformat','var') || isempty(mkrformat), mkrformat='b'; end
    if ~exist('cityname','var'), cityname=''; end
    if ~exist('var_name','var'), var_name=''; end
    if ~exist('var_unit','var'), var_unit=''; end
	if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
	if ~ishandle(gcax), disp('WARNING! HsmaxPlot with axes'); end
	set(gcf,'CurrentAxes',gcax)
    
    axis(gcax,'xy');
    plot(gcax,datemax,Hsmax,mkrformat);
    if exist('umbral','var') && not(isempty(umbral)),
        hold on;
        line(datemax,umbral,'Color','r','LineWidth',4);
        hold off;
    end
    grid(gcax,'on');
    ylabel(gcax,labelsameva(var_name,var_unit));
    xlabel(gcax,'Time (years)');
    legend(gcax,var_name);
    title(gcax,cityname);
end
