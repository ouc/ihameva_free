%   COMPROBACION
%
%   Lectura y del fichero de oleaje en Valencia
%fclose(fid)
% clear;
% clc
% close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Maximos mensuales de Melisa y Cristina
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tipo = 'Mensual';
Hsmax = load('Y_Valencia.txt');
Hsmax = Hsmax/100;
datemax = load('T_Valencia.txt');
datemaxanual = mod(datemax,1);

%%%%%   Eliminacion de los datos erroneos
% lista = [57];
lista = [];
good = [];
for i=1:length(Hsmax),
    if isempty(find(lista==i)),
        good = [good i];
    end
end

Hsmax = Hsmax(good);
datemax = datemax(good);
datemaxanual = datemaxanual(good);

nfig = 1;
%   Ajuste inicial de la distribucion de extremos estacionaria
[p_ nfig] = ajusteGEV(Hsmax,nfig);


indices =  load('Modo1.txt');
indices =  [indices load('Modo2.txt')];
indices =  [indices load('Modo3.txt')];
indices =  [indices load('Modo4.txt')];
indices =  [indices load('Modo5.txt')];
indices =  [indices load('Modo6.txt')];
indices =  [indices load('Modo7.txt')];
indices =  [indices load('Modo8.txt')];
indices =  [indices load('Modo9.txt')];
indices =  [indices load('Modo10.txt')];

%   Fijo el parametro para que sea Weibull 
nmu = 2;
npsi = 2;
neps = 0;
tendencia = 0;
nind = 0;
ntend = tendencia;
indices = [];



disp('Funcion Objetivo')
OBJopt = loglikeobj(Hsmax,datemax,indices,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi)

Jx = Jacobian (Hsmax,datemax,indices,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi);

variacion = 0.000001;
(loglikeobj(Hsmax,datemax,indices,beta0*(1+variacion),beta,alpha0,alpha,gamma0,gamma,betaT,varphi)-...
    loglikeobj(Hsmax,datemax,indices,beta0*(1-variacion),beta,alpha0,alpha,gamma0,gamma,betaT,varphi))/...
    (2*beta0*variacion)

(loglikeobj(Hsmax,datemax,indices,beta0,beta,alpha0*(1+variacion),alpha,gamma0,gamma,betaT,varphi)-...
    loglikeobj(Hsmax,datemax,indices,beta0,beta,alpha0*(1-variacion),alpha,gamma0,gamma,betaT,varphi))/...
    (2*alpha0*variacion)

(loglikeobj(Hsmax,datemax,indices,beta0,beta,alpha0,alpha,gamma0*(1+variacion),gamma,betaT,varphi)-...
    loglikeobj(Hsmax,datemax,indices,beta0,beta,alpha0,alpha,gamma0*(1-variacion),gamma,betaT,varphi))/...
    (2*gamma0*variacion)

Hxx = Hessian (Hsmax,datemax,indices,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi);

[beta0,beta,alpha0,alpha,gamma0,gamma,grad,hessian] = OptiParamGood (1,0,0,Hsmax,datemax)

[beta02,beta2,alpha02,alpha2,gamma02,gamma2,betaT2,varphi2,grad2,hessian2] = OptiParam (0,1,0,Hsmax,datemax,[],[],[]);
Jx2 = Jacobian (Hsmax,datemax,[],beta02,[beta2; 0; 0],alpha02,[alpha2;0; 0],gamma02,[gamma2;0; 0],[],[]);
Hxx = Hessian (Hsmax,datemax,[],beta02,beta2,alpha02,alpha2,gamma02,gamma2,[],[])

[beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,grad,hessian] = OptiParamHessian_bak (2,2,0,Hsmax,datemax,[],[],[]);
Jx = Jacobian (Hsmax,datemax,[],beta0,[beta;0;0],alpha0,[alpha;0;0],gamma0,[gamma;0;0],[],[]);
full([Jx Jx2])

Hxx = Hessian (Hsmax,datemax,[],beta0,beta,alpha0,alpha,gamma0,gamma,[],[])

Jx = Jacobian (Hsmax,datemax,indices,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi)
