function [quanGEVX,Ts,Tapprox,Xor,popt,stdDqX,stdloX,stdupX,quanX10]=potajuste(xumb,umbral,ny,xmax,Opciones)
%ATENCION unificar con pot-herramienta. OJO OJO usado por rmall_fig
%xmax  maximos anuales
%Peak over threshold selected data
% aux=25;ny=60;k=0;
% xumb=gprnd(-0.1512,0.6863,umbral,ny*aux,1);
% for i=1:ny, xmax(i)=max(xumb(k+1:k+aux)); k=k+aux; end

ejemplo='';
alfa = 0.05;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Ajuste extremal reanalisis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
paramEsts=mle((xumb - umbral), 'dist','generalized pareto', 'alpha',alfa);%gpfit(xumb - umbral);
neps0 = 1;
if neps0
[lambda0,sigma0,gamma0,loglikeobj,grad,hessianPOT,pold] = ...
    OptiParamHessianPOT_anual(xumb,umbral,ny,[length(xumb)/ny; log(paramEsts(2));paramEsts(1)]);
else
[lambda0,sigma0,gamma0,loglikeobj,grad,hessianPOT,pold] = ...
    OptiParamHessianPOT_anual(xumb,umbral,ny,[length(xumb)/ny; log(paramEsts(2))]);%Gumbel
end
if gamma0 ~= 0, neps0 = 1; end

%   LU decomposition of the Information matrix
[LX,UX,PX] = lu(hessianPOT);
invI0 = (UX)\(LX\(PX*eye(2+neps0)));%   Inverse
stdpara = sqrt(diag(invI0));%    Standard deviation
%   Best parameter vector and confidence value
popt = [lambda0; sigma0; gamma0];
pup = popt+norminv(1-alfa/2)*stdpara;
plo = popt-norminv(1-alfa/2)*stdpara;
%   Standarized empirical variable
[Ze ordenZe]= sort(xmax);
%   Standarized adjustment variable
[Zm,Dzm] = QuantilePOT (((1:length(xmax))'/(length(xmax)+1)),umbral,lambda0,sigma0,gamma0);
stdDwei = sqrt(sum((Dzm'*invI0).*Dzm',2));
%   Sorted standarized adjustment variable
[Zmsort ordenZm]= sort(Zm);
%   Empirical distribution function values
Fe = (1:length(xmax))'/(length(xmax)+1);
Fm = CDFPOT (xmax,umbral,lambda0,sigma0,gamma0);
%   Sort the distribution function values
[Fmsort ordenFm]= sort(Fm);

%  Aggregate quantiles for different return periods
Ts = [2:1:9 10:10:90 100:100:500];
if exist('Opciones','var') && isprop(Opciones,'Ts') && ~isempty(Opciones.Ts),
    Ts=Opciones.Ts;
end
% if min(Ts)<=1, error('El valor min de Ts tiene que ser mayor que 1'); end

%   Datos anuales OC OJO
Proxorny = (((1:ny))/(ny+1))';
Proxorxm = (((1:length(xmax)))/(length(xmax)+1))';
%
if length(Proxorxm)<length(Proxorny)
    Xor = sort(xmax);
    Proxor=Proxorny(end-length(Proxorxm)+1:end);
elseif length(Proxorxm)==length(Proxorny);
    Xor = sort(xmax);
    Proxor=Proxorny;
else
    error('length error of vector xmax and year number');
end
Tapprox = 1./(1-Proxor);
id = find(Tapprox>=Ts(1));
Tapprox=Tapprox(id);
Xor=Xor(id);
%   Aggregate quantiles for different return periods
nts = length(Ts);
quanGEVX = zeros(nts,1);
stdDqX = zeros(nts,1);
stdupX = zeros(nts,1);
stdloX = zeros(nts,1);
invI0X = inv(hessianPOT);
for j = 1:nts, %            Annual
    [quanX,DqX] = QuantilePOT (1-1/Ts(j),umbral,lambda0,sigma0,gamma0);
    if Ts(j)==10, quanX10=quanX; end;
    stdDqX(j) = sqrt(sum((DqX'*invI0X).*DqX',2));
    quanGEVX(j)=quanX;
    %           Intervalos de confianza del cuantil anual
    stdupX(j) = quanX+stdDqX(j)*norminv(1-alfa/2,0,1);
    stdloX(j) = quanX-stdDqX(j)*norminv(1-alfa/2,0,1);
end
quanGEVX=quanGEVX(:);

%si solo salen 3 el utimo son los parametros optimos
popt(2)=exp(popt(2));%paso a lineal el sigma del parametro optomo
if nargout==3,
    Tapprox=popt;
elseif nargout==4,
    Tapprox=popt;
    Xor=stdDqX;
end

if Opciones.myPlots
    % compruebo que existe la tool
    ecoinstall=amevaTheseToolboxesInstalled('Econometrics Toolbox');
    %   Tests de ajuste
    trxmax = norminv(CDFPOT (xmax,umbral,lambda0,sigma0,gamma0));
    [htrxmax,pValuertrxmax] = kstest(trxmax);
    [Htrxmax,pValue,Qstat,CriticalValue] = lbqtest(trxmax,[1 2 3 4 5],0.05);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   QQ plot
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Quantile-quantile plot
    fonsiz = 10;
    scrsz = get(0,'ScreenSize');
    figure('Position',[1 1 scrsz(3) scrsz(4)])
    ax_ = subplot(2,2,1);
    %   Plot of the aggregate quantiles
    legh_ = []; legt_ = {};% handles and text for legend
    h_=plot(linspace(min(Zmsort),max(Zmsort)),linspace(min(Zmsort),max(Zmsort)),'k');
    legh_(end+1) = h_;
    legt_{end+1} = 'Bisector';
    hold on
    h_=plot(Ze,Zmsort,'o','MarkerEdgeColor','k',...
        'MarkerFaceColor',[0.0 0.0 0.0],...
        'MarkerSize',3)
    legh_(end+1) = h_;
    legt_{end+1} = 'Data ($x^{\rm max}$)';
    h_=plot (Ze,Zmsort+norminv(1-alfa/2,0,1)*stdDwei(ordenZm),'--k');
    legh_(end+1) = h_;
    legt_{end+1} = 'Upper';
    h_=plot (Ze,Zmsort-norminv(1-alfa/2,0,1)*stdDwei(ordenZm),'--k')
    legh_(end+1) = h_;
    legt_{end+1} = 'Lower';
    grid on
    %
    hh=title('Best model QQ plot')
    set(hh,'Interpreter','latex','FontSize',fonsiz+2);
    leginfo_ = {'Orientation', 'vertical', 'Location', 'NorthWest'};
    hh=legend(ax_,legh_,legt_,leginfo_{:});  % create legend
    set(hh,'box','on');
    set(hh,'Interpreter','latex','FontSize',fonsiz);
    
    hh=xlabel('Empirical');  % create legend
    set(hh,'Interpreter','latex','FontSize',fonsiz);
    hh=ylabel('Fitted');  % create legend
    set(hh,'Interpreter','latex','FontSize',fonsiz);
    set(gca,'FontSize',fonsiz,'FontName','Times New Roman')
    axis equal
    axis([min(min(Ze),min(Zmsort)) max(max(Ze),max(Zmsort)) min(min(Ze),min(Zmsort)) max(max(Ze),max(Zmsort))])
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   PP plot
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Dibujo del grafico probabilidad-probabilidad
    ax_ = subplot(2,2,2);
    %   Plot of the aggregate quantiles
    legh_ = []; legt_ = {};% handles and text for legend
    
    h_=plot(linspace(0,1),linspace(0,1),'k');
    legh_(end+1) = h_;
    legt_{end+1} = 'Bisector';
    hold on
    %
    h_=plot(Fe,Fmsort,'o','MarkerEdgeColor','k',...
        'MarkerFaceColor',[0 0 0],...
        'MarkerSize',3);
    legh_(end+1) = h_;
    legt_{end+1} = 'Data ($x^{\rm max}$)';
    grid on
    
    %
    hh=title(['Best model PP plot, ' ejemplo ''])
    set(hh,'Interpreter','latex','FontSize',fonsiz+2);
    %
    leginfo_ = {'Orientation', 'vertical', 'Location', 'NorthWest'};
    hh=legend(ax_,legh_,legt_,leginfo_{:});  % create legend
    set(hh,'box','on');
    set(hh,'Interpreter','latex','FontSize',fonsiz);
    
    hh=xlabel('Empirical');  % create legend
    set(hh,'Interpreter','latex','FontSize',fonsiz);
    hh=ylabel('Fitted');  % create legend
    set(hh,'Interpreter','latex','FontSize',fonsiz);
    
    set(gca,'FontSize',fonsiz,'FontName','Times New Roman')
    
    axis equal
    axis([0 1 0 1])
    
    %   Partial autocorrelation
    ax_ = subplot(2,2,3);
    legh_ = []; legt_ = {};% handles and text for legend
    if ecoinstall, autocorr(trxmax,10); end
    %
    hh=title(['Autocorrelation function, ' ejemplo ''])
    set(hh,'Interpreter','latex','FontSize',fonsiz+2);
    %
    hh=xlabel('Lag (years)');  % create legend
    set(hh,'Interpreter','latex','FontSize',fonsiz);
    hh=ylabel('Sample autocorrelation');  % create legend
    set(hh,'Interpreter','latex','FontSize',fonsiz);
    set(gca,'FontSize',fonsiz,'FontName','Times New Roman')
    %   Partial autocorrelation
    ax_ = subplot(2,2,4);
    legh_ = []; legt_ = {};% handles and text for legend
    if ecoinstall, parcorr(trxmax,10); end
    %
    hh=title(['Partial Autocorr. function, ' ejemplo ''])
    set(hh,'Interpreter','latex','FontSize',fonsiz+2);
    %
    hh=xlabel('Lag (years)');  % create legend
    set(hh,'Interpreter','latex','FontSize',fonsiz);
    hh=ylabel('Sample partial autocorr.');  % create legend
    set(hh,'Interpreter','latex','FontSize',fonsiz);
    set(gca,'FontSize',fonsiz,'FontName','Times New Roman')
    filename = ['XmaxFitdiag' ejemplo];
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Periodos de retorno
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fonsiz = 14;
    scrsz = get(0,'ScreenSize');
    figure('Position',[1 1 scrsz(3) scrsz(4)])
    ax_ = newplot;
    legh_ = []; legt_ = {};% handles and text for legend
    h_=semilogx(Tapprox,Xor,'o','MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0.6 0.6 .6],'MarkerSize',6)
    legh_(end+1) = h_;
    legt_{end+1} = 'Reanalysis data ($x$)';
    grid on
    hold on
    h_=semilogx(Ts,quanGEVX,'k','Linewidth',2)
    legh_(end+1) = h_;
    legt_{end+1} = 'POT fit ($x$)';
    h_=semilogx(Ts,stdloX,'--k','Linewidth',2)
    legh_(end+1) = h_;
    legt_{end+1} = 'POT lo ($x$)';
    h_=semilogx(Ts,stdupX,'--k','Linewidth',2)
    legh_(end+1) = h_;
    legt_{end+1} = 'POT up ($x$)';
    hold on
    
    hh=xlabel('Return Period (years)');  % create legend
    set(hh,'Interpreter','latex','FontSize',fonsiz);
    hh=ylabel('Quantiles');  % create legend
    set(hh,'Interpreter','latex','FontSize',fonsiz);
    hh=title(['Annual return period diagnostics'])
    set(hh,'Interpreter','latex','FontSize',fonsiz+2);
    leginfo_ = {'Orientation', 'vertical', 'Location', 'NorthWest'};
    hh=legend(ax_,legh_,legt_,leginfo_{:});  % create legend
    set(hh,'box','on');
    set(hh,'Interpreter','latex','FontSize',fonsiz);
    
    set(gca,'FontSize',fonsiz,'FontName','Times New Roman')
end
