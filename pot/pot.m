function pot(varargin)
%   POT Peak Over Threshold tool. Pareto adjustment.
%   POT (pot.m) opens a graphical user interface for displaying the main program posibility.
%   Tested on Ubuntu trusty v.14.04.1(developed), Windows 7 and Mac Os X v10.9.4 (MATLAB:R2013a)
%   To load test data, please click on: -Help, Load ameva test data-, to send
%   data to matlab worksapce and work with this.
%   
% Examples 1
%   load ameva;
%   Tools-->Extreme statistic-->POT
%
% See also: 
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   pot(main program tool v1.0.3 (140820)).
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain. 
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows 7
%   04-07-2011 - The first distribution version
%   07-05-2013 - Old distribution version
%   20-08-2014 - Last distribution version

versionumber='v1.0.3 (140820)';
nmfctn='pot';
Ax1Pos=[0.39 0.2 0.55 0.7];
figPos=[];

idioma='eng'; usertype=1;
if length(varargin)==1, idioma=varargin{1}.idioma; usertype=varargin{1}.usertype; end

% Texto idioma:
texto=amevaclass.amevatextoidioma(idioma);

namesoftware=[texto('namePot') ' ',versionumber];

if ~isempty(findobj('Tag',['Tag_' nmfctn]));
   figPos=get(findobj('Tag',['Tag_' nmfctn]),'Position');
   close(findobj('Tag',['Tag_' nmfctn]));
end

%Lista de las figuras que se van a usar en este programa
hf=[];%ventana principal
hg=[];%ventana auxiliar

% Information for all buttons and Spacing between the button
colorG=[0.94 0.94 0.94];	%Color general
btnWid=0.120;			
btnHt=0.05;
spacing=0.015;			
top=0.95;
xPos=[0.01 0.15 0.28 0.42];

%GLOBAL VARIALBES solo usar las necesarias OJO!!
awinc=amevaclass;%ameva windows commons methods
awinc.nmfctn=nmfctn;
Hsmax=[];
datemax=[];
umbral=[];
xamax=[];damax=[];
lambda0=0;
sigma0=0;
gamma0=0;
loglikeobj=0;
grad=[];
hessian=[];
confidenceLevel = 0.95;
confidenceLevelAlpha = 1-confidenceLevel;

var_name='';
var_unit='';
carpeta=[];
quan95=[];stdDq=[];mut=[];psit=[];epst=[];invI0=[];
cityname = '';
global neps0
svopt=[1 1 0];%save option 1-fig 2-png 3-eps (1/0)
validchildren=[];
% compruebo que existe la tool
ecoinstall=amevaTheseToolboxesInstalled(texto('econoTool'));


%---LA FIGURA-VENTANA PRINCIPAL-
if isempty(figPos), figPos=awinc.amevaconst('mw'); end
hf=figure('Name',namesoftware,'Color',colorG,'CloseRequestFcn',@AmevaClose, ...
	'NumberTitle','Off','DockControls','Off','Position',figPos,'Tag',['Tag_' nmfctn],'NextPlot','new');
ax1=axes('Units','normalized','Parent',hf,'Position',Ax1Pos,'Visible','Off');
set(hf,'CurrentAxes',ax1);
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('buttonPlot_1a'), ...
    'Position',[xPos(2) 0.935 btnWid btnHt],'FontWeight','Bold');

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_data=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'String','Data', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataOO('On'));

ui_tp=uicontrol('Style','popup','Units','normalized','Parent',hf,'Enable','Off','String',texto('buttonPlot_1'), ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@AmevaPlot);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_run=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'Enable','Off','String','Start', ...
    'Interruptible','on','Position',[xPos(1) yPos btnWid btnHt],'Callback',@AmevaRun);

uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('buttonPlot_2'), ...
    'BackgroundColor',colorG,'Position',[xPos(2) yPos+0.036 btnWid btnHt*0.66]);
ui_pp=uicontrol('Style','popup','Units','normalized','Parent',hf,'Enable','Off','String',texto('buttonPlot_3'), ...
    'Position',[xPos(2) yPos-0.005 btnWid btnHt],'Callback',@AmevaPlotToPrint);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ttd=uicontrol('Style','listbox','Units','normalized','String','','FontSize',7.5, ...
    'Position',[xPos(1) 0.055 0.312 0.65],'FontName', 'FixedWidth','Visible','on');

uicontrol('Style','frame','Units','normalized','Parent',hf,'Position',[0 0 1 0.04]);
uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.59 0.004 0.18 0.03],'String',texto('saveFigure'));
ui_ckeps=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.77 0.004 btnWid/2 0.03],'String','*.eps','Value',svopt(3));
ui_ckfig=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.85 0.004 btnWid/2 0.03],'String','*.fig','Value',svopt(1));
ui_ckpng=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.92 0.004 btnWid/2 0.03],'String','*.png','Value',svopt(2),'Callback',@(source,event)EnableButton(ui_data,'Enable','on'));
SetSaveOpt;
ui_tb   =uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.01 0.004 0.58 0.03],...
    'String',[nmfctn,texto('dataButton')],'HorizontalAlignment','left');


%% Data
btnWid=0.385;
btnHt=0.075;
spacing=0.02;
top=0.98;
xPos=[0.02 0.42 0.83];
hg=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Data'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataOO('off'),'WindowButtonMotionFcn',@ListBoxCallback1,...
    'NumberTitle','Off','MenuBar','none','Position',awinc.amevaconst('ds'),'Resize','Off','Visible','Off','NextPlot','new');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
amvcnf.idioma=idioma;amvcnf.usertype=usertype;

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid btnHt],...
    'String','Data & WorkSpace','Callback',@(source,event)amevaworkspace(amvcnf));
uicontrol('Style','Push','Units','normalized','Parent',hg,'String','Max. Selection', ...
    'BackgroundColor',colorG,'Position',[xPos(2) yPos btnWid btnHt],'Callback',@(source,event)maximostemporales(amvcnf));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hg,'String','Name:', ...
    'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right','BackgroundColor',colorG);
ui_fn=uicontrol('Style','edit','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid btnHt],...
    'String','Tabevol');
%name of variables globales
var_lab={'* Time max.:' '* X max.:'};
for ii=1:length(var_lab)
    btnN=btnN+1;
    yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
    ui_td(ii)=uicontrol('Style','text','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid btnHt],'String',var_lab(ii),'BackgroundColor',colorG,'HorizontalAlignment','right');
    ui_all(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',ii);
    ui_cm(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid/2.7 btnHt],'String','(none)','visible','off','Callback',@(source,event)ltsplot(ii));
end; clear ii;

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hg,'String','* Threshold:', ...
    'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right','BackgroundColor',colorG);
ui_umbral=uicontrol('Style','edit','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid btnHt],'String',' ');
ui_cmm=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid/2.7 btnHt],'String','(none)','Callback',@UpdateUmbral);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hg,'String','Significance level:', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_nq=uicontrol('Style','edit','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid btnHt],...
    'String','0.05');


btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hg,'String','X name & unit:', ...
    'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_fs=uicontrol('Style','edit','Units','normalized','Parent',hg,'String','Hs', ...
    'Position',[xPos(2) yPos btnWid/2 btnHt]);
ui_fu=uicontrol('Style','edit','Units','normalized','Parent',hg,'String','m', ...
    'Position',[xPos(2)+btnWid/2 yPos btnWid/2 btnHt]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Apply', ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@AmevaSetData);
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Close', ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@(source,event)FigDataOO('off'));

uicontrol('Style','frame','Units','normalized','Parent',hg,'Position',[0 0 1 0.05]);
ui_tbd=uicontrol('Style','Text','Units','normalized','Parent',hg,...
    'String',texto('dataWorkspace'), ...
    'Position',[0.01 0.004 0.99 0.04],'HorizontalAlignment','left');

%% C0 Open,Close figure data
    function FigDataOO(state,source,event)
        set(hg,'Visible',state);
    end
%% C0 Actulizo los botones Enable on/off & The figure open/close visible on/off
    function EnableButton(listbutton,statetyp,statebutton)
        set(listbutton,statetyp,statebutton);
    end
%% C1
    function ListBoxCallback1(source,event) % Load workspace vars into list box
        awinc.ListBoxCallback(ui_all,true);%Lista tambien las structuras
        awinc.ListBoxCallback(ui_cmm,'double');
    end
%% C0
    function SetSaveOpt
        if isdeployed,
            set(ui_ckfig,'Value',1,'Enable','On','Visible','On');
            set(ui_ckeps,'Value',0,'Enable','On','Visible','On');
        end
    end
%% C0
    function AmevaClose(source,event) % Close all Ameva windows
        awinc.AmevaClose([hf,hg], ...
            {[upper(nmfctn(1)),nmfctn(2:end),'Data']});
    end
%% C0
    function AmevaPlotToPrint(source,event) % List of fig
        awinc.AmevaPlotToPrint(ui_pp,carpeta);
    end
%% C0 Update cell and matrix name
    function UpdateNameAll(source,event)
        nval=get(source,'UserData');
        awinc.AmevaUpdateName(nval,ui_cm,ui_all,ui_td);
        ltsplot(nval);% Refresh plot
    end
%% C1 Refresh plot
    function ltsplot(nlv,source,event)
        if get(ui_all(nlv),'Value')>1,
            nlvn=get(ui_td(nlv),'String');
            nlvn=char(nlvn(1));
            try
                awinc.AmevaEvalData(nlv,ui_cm,ui_all);
                X_=awinc.data;
                if not(isfloat(X_)), disp(texto('notDoubleVector')); return; end
                nmsize=size(X_);
                set(ui_td(nlv),'string',{nlvn,sprintf('(%dx%d) %s',nmsize(1),nmsize(2),class(X_))});
                set(0,'CurrentFigure',hf); set(hf,'CurrentAxes',ax1); cla(ax1,'reset');
                if min(nmsize)==1
                    plot(ax1,X_,'k');
                else
                    plot(ax1,X_);
                end
                grid on;
                title(ax1,'Serie','FontWeight','bold');
            catch ME
                disp(texto('variableErase'));
                rethrow(ME);
            end
        end
    end
%% Selecion de maximo
function UpdateUmbral(source,event)
    ipos=get(ui_cmm,'Value');
    spos=get(ui_cmm,'String');
    set(ui_umbral,'String',num2str(evalin('base',spos{ipos})))
end
%% Apply comprobamos que los Data para calcular pot son validdos
function AmevaSetData(source,event)
    var_name=get(ui_fs,'String');
    var_unit=get(ui_fu,'String');
    ione=get(ui_all(1),'Value');
    itwo=get(ui_all(2),'Value');
    if itwo>1, 
        awinc.AmevaEvalData(2,ui_cm,ui_all);
        Hsmax=awinc.data;
    else
        error(texto('dataHsmax'));
    end
    if ione>1,
        awinc.AmevaEvalData(1,ui_cm,ui_all);
        awinc.AmevaTimeCheck(ui_tbd,awinc.data);
        if awinc.timestate==true,
            datemax=yearlytimescale(awinc.data,'R');
        elseif awinc.timestate==false,
            datemax=awinc.data;
        else
            disp(texto('dataFormat'));
            set(hg,'Visible','on'); return;
        end %maximum annual time scale normalizado para usar GEV
    else
        %disp(texto('notTemporaryData'));
        %Time=(1:length(Hsmax))';
        error(texto('timeNecessary'));
    end
    %compruebo y elimino si existen los NaNs en Hsmax
    datemax(isnan(Hsmax)) = [];
    if sum(isnan(Hsmax))>0, disp(texto('deleteData')); end
    Hsmax(isnan(Hsmax)) = [];
    %compruebo que las variables sean diferentes
    awinc.AmevaVarCheck(ui_tbd,Hsmax,datemax,'Hsmax','datemax');	if awinc.state, set(hg,'Visible','on'); return; end,
    set(hg,'Visible','off');
    %umbral
    umbral=str2num(get(ui_umbral,'String'));
    if isempty(umbral),
        set(hg,'Visible','on');
        set(ui_tbd,'String',[nmfctn,texto('thresholdDefine')]);
        disp([nmfctn,texto('thresholdExist')]);
        return;
    end
    %las funciones de roberto utilizan los datos sumados con el umbral
    %compruebo que los datos tiene o no el umbral sumado
    if min(Hsmax)-umbral+3e-005<0,
        Hsmax=Hsmax+umbral;
        disp(texto('potFit'));
    else
        disp(texto('thresholdSum'));
    end
    if(length(Hsmax)<60), set(ui_tbd,'String',texto('notEnoughData'));end
    
    %cierro la ventana de datos simple o avanzada si todo ha ido bien!
    set(hg,'Visible','off');
    %re-inicializo los ejes y variables
    set(ui_tbd,'String',[nmfctn,texto('timeOptions')]);drawnow;
    set(ui_run,'Enable','on');
    set(ui_tp,'String',texto('buttonPlot_1'),'Enable','Off','Value',1);
    set(ui_pp,'String',texto('buttonPlot_3'),'Enable','Off','Value',1);
    %set(hf,'Toolbar','figure');
    %
    set(0,'CurrentFigure',hf)
	set(hf,'CurrentAxes',ax1);cla(ax1,'reset');
    HsmaxPlot(Hsmax,datemax,umbral,[],cityname,var_name,var_unit,ax1);% Plot Hsmax
    set(ui_tb,'String',[nmfctn,texto('start_1')]);drawnow ;
end % listBoxCallback

%% Run data
function AmevaRun(source,event)
    %this have a valid children for figure
    if isempty(validchildren)
        validchildren=get(gcf,'children');
    else
        newchildren=get(gcf,'children');%borro los nuevos children de la figura
        if length(newchildren)>length(validchildren) || not(isequal(newchildren,validchildren)),
            [CI,II]=setdiff(newchildren,validchildren);
            delete(newchildren(II));
        end
    end
    %Creo el directorio de trabajo para almacenamiento
    awinc.NewFolderCheck(carpeta,nmfctn);carpeta=awinc.carpeta;% Methods for "amevaclass"
    EnableButton([ui_tp,ui_pp,ui_run,ui_data],'Enable','off');
    EnableButton(hg,'Visible','off');
    set(ui_tb,'String',[nmfctn,texto('run')]);drawnow ;
    %Settings parameter
    cityname=get(ui_fn,'String');%region name
    confidenceLevelAlpha=str2double(get(ui_nq,'String'));%quantile chi2
    ny = datemax(end);%ny numero de anos
    %   Plots
    i=1;%Sting name for the plots
    if true,  tpstring{1,i}='LocalScaleParmPlot'; i=i+1; end;
    if true,  tpstring{1,i}='QQplot'; i=i+1; end;
    if true,  tpstring{1,i}='PPplot'; i=i+1; end;
    if ecoinstall,  tpstring{1,i}='Autocorr'; i=i+1; end;
    if ecoinstall,  tpstring{1,i}='Parcorr'; i=i+1; end;
    if true,  tpstring{1,i}='AggregateQuantiles'; i=i+1; end;
    if true,  tpstring{1,i}='QQplotPareto'; i=i+1; end;
    if true,  tpstring{1,i}='PPplotPareto'; i=i+1; end;
    if ecoinstall,  tpstring{1,i}='AutocorrPareto'; i=i+1; end;
    if ecoinstall,  tpstring{1,i}='ParcorrPareto'; i=i+1; end;
    if true,  tpstring{1,i}='Hsmax'; i=i+1; end; clear i;
    %Run function
    try
        %save options
        svopt(1)=get(ui_ckfig,'Value');%fig
        svopt(2)=get(ui_ckpng,'Value');%png
        svopt(3)=get(ui_ckeps,'Value');%eps
        neps0 = 1;
        [lambda0,sigma0,gamma0,loglikeobj,grad,hessian]=OptiParamHessianPOTsimple(Hsmax,umbral,ny);
        
        [quan95,stdDq,mut,psit,epst,damax,xamax,invI0]=potpreplot(datemax,Hsmax,lambda0,sigma0,gamma0,confidenceLevelAlpha,hessian,loglikeobj,grad,umbral);
        graficos_pot_pareto(quan95,stdDq,datemax,Hsmax,damax,xamax,psit,umbral,confidenceLevelAlpha,lambda0,sigma0,gamma0,invI0,tpstring,svopt,cityname,carpeta,var_name,var_unit);
        ny=length(xamax);
        Opciones = ameva_options(); Opciones.myPlots = false;
        [rtTs,rtQuanaggr,rtStdLo,rtStdUp] = amevaAgregateQuantilesPOT(xamax,umbral,confidenceLevelAlpha,lambda0,sigma0,gamma0,invI0,ny,Opciones);
        
        save([carpeta,filesep,'data_',nmfctn,cityname,'_ML.mat'],'Hsmax','datemax','ny','umbral','confidenceLevelAlpha', ...
            'lambda0','sigma0','gamma0','loglikeobj','grad','hessian','invI0', ...
            'quan95','rtTs','rtQuanaggr','rtStdLo','rtStdUp');
    catch ME
        EnableButton([ui_tp,ui_pp,ui_run,ui_data],'Enable','on');
        rethrow(ME);
    end
    
    set(ui_tp,'String', tpstring);%Set Sting Plots. set(ui_pp,'String','Plot to print');
    %listar los plot para imprimir
    awinc.ListBoxCallbackP(ui_pp,carpeta);% Methods for "amevaclass"
	set(ui_tp,'Value',1);AmevaPlot;

    EnableButton([ui_tp,ui_pp,ui_run,ui_data],'Enable','on');
    if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
end

%% Ameva plot
function AmevaPlot(source,event)
    EnableButton([ui_tp,ui_pp,ui_run],'Enable','off');
    set(ui_tb,'String',[nmfctn,texto('run')]);drawnow ;
    [mins,nins]=size(get(ui_tp,'String'));
    iin=get(ui_tp,'Value');
    iins=get(ui_tp,'String');
    [ni mi]=size(iins);
    if ~strcmp(get(hf,'NextPlot'),'add'), set(hf,'NextPlot','add'); end;
    if ~ishandle(ax1), disp([texto('plot') ,nmfctn]); return; end
    
%     newchildren=get(gcf,'children');%borro los nuevos children de la figura
%     if length(newchildren)>length(validchildren) || not(isequal(newchildren,validchildren)),
%         [CI,II]=setdiff(newchildren,validchildren);
%         delete(newchildren(II));
%     end
    
    set(hf,'CurrentAxes',ax1);cla(ax1,'reset');axis(ax1,'square');
    if ni==1, return; end;
    plotname=iins{get(ui_tp,'Value')};
    try
    switch plotname
        case 'LocalScaleParmPlot'   %Location parameter plotting
            LocationShapePOT(quan95,stdDq,damax,xamax,psit,confidenceLevelAlpha,cityname,umbral);
        case 'QQplot'
            qq_plot_pot_pareto(xamax,umbral,confidenceLevelAlpha,lambda0,sigma0,gamma0,invI0,'POT',cityname,var_name,ax1);
        case 'PPplot'
            pp_plot_pot_pareto(xamax,umbral,confidenceLevelAlpha,lambda0,sigma0,gamma0,invI0,'POT',cityname,var_name,ax1);
        case 'Autocorr'
            if ecoinstall, autocorr(norminv(CDFPOT(xamax,umbral,lambda0,sigma0,gamma0)),10); end
            title(texto('title_1'))
            xlabel(texto('xLabel'));  % create legend
            ylabel(texto('yLabel_1')');  % create legend
        case 'Parcorr'
            if ecoinstall, parcorr(norminv(CDFPOT(xamax,umbral,lambda0,sigma0,gamma0)),10); end
            title(texto('title_2'))
            xlabel(texto('xLabel'));  % create legend
            ylabel(texto('yLabel_2'));  % create legend
        case 'AggregateQuantiles'
        	ny=length(xamax);
            Opciones = ameva_options(); Opciones.cityname = cityname; Opciones.var_name = {var_name}; Opciones.var_unit = {var_unit};
            amevaAgregateQuantilesPOT(xamax,umbral,confidenceLevelAlpha,lambda0,sigma0,gamma0,invI0,ny,Opciones,ax1);
        case 'QQplotPareto'
            qq_plot_pot_pareto(Hsmax,umbral,confidenceLevelAlpha,lambda0,sigma0,gamma0,invI0,'Pareto',cityname,var_name,ax1);
        case 'PPplotPareto'
            pp_plot_pot_pareto(Hsmax,umbral,confidenceLevelAlpha,lambda0,sigma0,gamma0,invI0,'Pareto',cityname,var_name,ax1)
        case 'AutocorrPareto'
            if ecoinstall, autocorr(norminv(CDFParetoSimple(Hsmax,umbral,sigma0,gamma0)),10); end
            title(texto('title_3'))
            xlabel(texto('xLabel'));  % create legend
            ylabel(texto('yLabel_3'));  % create legend
        case 'ParcorrPareto'
            if ecoinstall, parcorr(norminv(CDFParetoSimple(Hsmax,umbral,sigma0,gamma0)),10); end
            title(texto('title_4'))
            xlabel(texto('xLabel'));  % create legend
            ylabel(texto('yLabel_4'));  % create legend
        case 'Hsmax'
            HsmaxPlot(Hsmax,datemax,umbral,[],cityname,var_name,var_unit,ax1);
        otherwise
            disp([texto('notPlotCase') ' ',plotname]);
            cla(ax1,'reset');legend(ax1,'hide');axis(ax1,'xy');
    end
    catch ME
        EnableButton([ui_tp,ui_pp,ui_run],'Enable','on');
        rethrow(ME);
    end
    set(ui_tb,'String',[nmfctn,texto('start')]);drawnow ;
    EnableButton([ui_tp,ui_pp,ui_run],'Enable','on');
    if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
end

end
