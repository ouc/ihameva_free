function pp_plot_pot_pareto(xamax,umbral,ConfidenceLevelAlpha,lambda0,sigma0,gamma0,invI0,tipo,cityname,var_name,gcax)

if ConfidenceLevelAlpha>0.5, error(['ConfidenceLevelAlpha=',num2str(ConfidenceLevelAlpha)]); end
if ~exist('cityname','var'), cityname=''; end
if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
if ~ishandle(gcax), disp('WARNING! pp_plot_pot_pareto with axes'); end
set(gcf,'CurrentAxes',gcax)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   PP plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Empirical distribution function values
Fe = (1:length(xamax))'/(length(xamax)+1);
%   Standarized adjustment variable
if strcmpi(tipo,'POT') %xamax
    %   Standarized adjustment variable
    [Zm,Dzm] = QuantilePOT (((1:length(xamax))'/(length(xamax)+1)),umbral,lambda0,sigma0,gamma0);
    stdDwei = sqrt(sum((Dzm'*invI0).*Dzm',2));
    Fm = CDFPOT(xamax,umbral,lambda0,sigma0,gamma0);
    titulo=['Best model PP plot (' cityname ')'];
    legdata='Data';
elseif strcmpi(tipo,'Pareto') %Hsmax
    %   Standarized adjustment variable
    [Zm,Dzm]= QuantileParetoSimple (((1:length(xamax))'/(length(xamax)+1)),umbral,sigma0,gamma0);
    auxDzm = Dzm(:,:,1);
    if ~isempty(gamma0),
        auxDzm = [auxDzm Dzm(:,:,2)];
    end
    stdDwei = sqrt(sum((auxDzm*invI0(2:end,2:end)).*auxDzm,2));
    Fm = CDFParetoSimple(xamax,umbral,sigma0,gamma0);
    titulo='Best model PP plot, Pareto (SURGE EXCEEDANCES)';
    legdata=['Data ($',var_name,'^{\rm max}$)'];
else
    error('Please select tipo POT or Pareto!');
end
%   Sort the distribution function values
Fmsort= sort(Fm);
%   Dibujo del grafico probabilidad-probabilidad
plot(linspace(0,1),linspace(0,1),'k');hold on;
plot(Fe,Fmsort,'o','MarkerEdgeColor','k','MarkerFaceColor',[0 0 0],'MarkerSize',3)
if strcmpi(tipo,'POT') %xamax
    plot(Fe,CDFPOT(Zm+norminv(1-ConfidenceLevelAlpha/2,0,1)*stdDwei,umbral,lambda0,sigma0,gamma0),'--k')
    plot(Fe,CDFPOT(Zm-norminv(1-ConfidenceLevelAlpha/2,0,1)*stdDwei,umbral,lambda0,sigma0,gamma0),'--k')
else
    plot (Fe,CDFParetoSimple(Zm+norminv(1-ConfidenceLevelAlpha/2,0,1)*stdDwei,umbral,sigma0,gamma0),'--k')
    plot (Fe,CDFParetoSimple(Zm-norminv(1-ConfidenceLevelAlpha/2,0,1)*stdDwei,umbral,sigma0,gamma0),'--k')
end

hh=legend({'Bisector',legdata,'Lower','Upper'},'Location','SouthEast');
set(hh,'Interpreter','latex');
grid(gca,'on')
title(gca,titulo)
xlabel(gca,'Empirical');
ylabel(gca,'Fitted');
hold(gca,'off');
axis(gca,'square');
axis(gca,[0 1 0 1]);

end
