function [p_ nfig] = ajusteGEV(x,nfig)

if nargin < 2,
    nfig = [];
end

% This function was automatically generated on 02-Feb-2009 11:01:36
 
% Data from dataset "Hs data":
%    Y = Hs
 
% Force all inputs to be column vectors
x = x(:);

if ~isempty(nfig),
    % Set up figure to receive datasets and fits
    figure(nfig);
    nfig = nfig+1;
    legh_ = []; legt_ = {};   % handles and text for legend
    ax_ = newplot;
    set(ax_,'Box','on');
    hold on;

    % --- Plot data originally in dataset "x data"
    t_ = ~isnan(x);
    Data_ = x(t_);
    [F_,X_] = ecdf(Data_,'Function','cdf'...
                  );  % compute empirical cdf
    Bin_.rule = 1;
    [C_,E_] = dfswitchyard('dfhistbins',Data_,[],[],Bin_,F_,X_);
    [N_,C_] = ecdfhist(F_,X_,'edges',E_); % empirical pdf from cdf
    h_ = bar(C_,N_,'hist');
    set(h_,'FaceColor','none','EdgeColor',[0.333333 0 0.666667],...
           'LineStyle','-', 'LineWidth',1);
    xlabel('Data');
    ylabel('Density')
    legh_(end+1) = h_;
    legt_{end+1} = 'x data';
    % --- Create fit "fit 1"
    
    % Nudge axis limits beyond data limits
    xlim_ = get(ax_,'XLim');
    if all(isfinite(xlim_))
       xlim_ = xlim_ + [-1 1] * 0.01 * diff(xlim_);
       set(ax_,'XLim',xlim_)
    end

    x_ = linspace(xlim_(1),xlim_(2),100);
end



% Fit this distribution to get parameter values
t_ = ~isnan(x);
Data_ = x(t_);
% To use parameter estimates from the original fit:
%     p_ = [-0.7210615700758, 0.6009055358833];
p_ = gevfit(Data_, 0.05);


if ~isempty(nfig),
    y_ = gevpdf(x_,p_(1), p_(2), p_(3));
    h_ = plot(x_,y_,'Color',[1 0 0],...
              'LineStyle','-', 'LineWidth',2,...
              'Marker','none', 'MarkerSize',6);
    legh_(end+1) = h_;
    legt_{end+1} = 'GEV inicial';

    hold off;
    h_ = legend(ax_,legh_,legt_,'Location','NorthEast');  
    set(h_,'Interpreter','none');
end
