function F = CDFPOT (x,u,lambda0,sigma0,gamma0)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   CDFPOT function calculates the GEV distribution function corresponding
%    to the given parameters
%
%   Input:
%       x -> maximum data
%   Output:
%        F-> Values of the distribution function
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if nargin<5,
        gamma0 = [];
        neps0 = 0;
    end
    if isempty(gamma0),
        neps0 = 0;
    else
        neps0 = 1;
    end
 
    %   Evaluate the location parameter at each time t as a function
    %   of the actual values of the parameters given by p
    lambt = lambda0*ones(size(x));
    %   Evaluate the scale parameter at each time t as a function
    %   of the actual values of the parameters given by p
    sigt = exp(sigma0)*ones(size(x));
    %   Evaluate the shape parameter at each time t as a function
    %   of the actual values of the parameters given by p
    if neps0,
        epst = gamma0*ones(size(x));
    else
        epst = zeros(size(x));
    end

    %   The values whose shape parameter is almost cero corresponds to
    %   the GUMBEL distribution, locate their positions if they exist
    posG = find(abs(epst)<=1e-8);
    %   The remaining values correspond to WEIBULL or FRECHET
    pos  = find(abs(epst)>1e-8);
    %   The corresponding GUMBEl values are set to 1 to avoid 
    %   numerical problems, note that for those cases the GUMBEL
    %   expressions are used
    epst(posG)=1;

    aux = (1+epst(pos).*((x(pos)-u)./sigt(pos)));
    posS = find(aux<=0);
    aux(posS)=0;

    F = zeros (size(x));
    
    %   WEIBULL or FRECHET distribution function
    F(pos) = exp(-lambt(pos).*aux.^(-1./epst(pos)));
    %   GUMBEL distribution function
    F(posG) = exp(-lambt(posG).*exp(-((x(posG)-u)./sigt(posG))));

end
