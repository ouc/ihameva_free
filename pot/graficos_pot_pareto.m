function graficos_pot_pareto(quan95,stdDq,datemax,Hsmax,damax,xamax,psit,umbral,quanval,lambda0,sigma0,gamma0,invI0,tpstring,svopt,cityname,carpeta,var_name,var_unit)
% compruebo que existe la tool
ecoinstall=amevaTheseToolboxesInstalled('Econometrics Toolbox');

%POT Extremos plot's
[ni mi]=size(tpstring);
if mi==1, return; end;
savename='';
savepath=[pwd,filesep,carpeta,filesep];

for i=1:mi,
    plotname=tpstring{i};
    h=figure('Visible','off');
    switch plotname
        case 'LocalScaleParmPlot'   %Location parameter plotting
            LocationShapePOT(quan95,stdDq,damax,xamax,psit,quanval,cityname,umbral);
        case 'QQplot'
            qq_plot_pot_pareto(xamax,umbral,quanval,lambda0,sigma0,gamma0,invI0,'POT',cityname,var_name);
        case 'PPplot'
            pp_plot_pot_pareto(xamax,umbral,quanval,lambda0,sigma0,gamma0,invI0,'POT',cityname,var_name);
        case 'Autocorr'
            if ecoinstall, autocorr(norminv(CDFPOT(xamax,umbral,lambda0,sigma0,gamma0)),10); end
            
            title('Autocorrelation function POT')
            xlabel('Lag (years)');  % create legend
            ylabel('Sample autocorrelation');  % create legend
        case 'Parcorr'
            if ecoinstall, parcorr(norminv(CDFPOT(xamax,umbral,lambda0,sigma0,gamma0)),10); end
            title('Partial Autocorr. function POT')
            xlabel('Lag (years)');  % create legend
            ylabel('Sample partial autocorr.');  % create legend
        case 'AggregateQuantiles'
        	ny=length(xamax);
            Opciones = ameva_options(); Opciones.cityname = cityname; Opciones.var_name = {var_name}; Opciones.var_unit = {var_unit};
            amevaAgregateQuantilesPOT(xamax,umbral,quanval,lambda0,sigma0,gamma0,invI0,ny,Opciones);
        case 'QQplotPareto'
            qq_plot_pot_pareto(Hsmax,umbral,quanval,lambda0,sigma0,gamma0,invI0,'Pareto',cityname,var_name);
        case 'PPplotPareto'
            pp_plot_pot_pareto(Hsmax,umbral,quanval,lambda0,sigma0,gamma0,invI0,'Pareto',cityname,var_name)
        case 'AutocorrPareto'
            if ecoinstall, autocorr(norminv(CDFParetoSimple(Hsmax,umbral,sigma0,gamma0)),10); end
            title('Autocorrelation function, Pareto (SURGE EXCEEDANCES)')
            xlabel('Lag (years)');  % create legend
            ylabel('Sample autocorrelation, Pareto (SURGE EXCEEDANCES)');  % create legend
        case 'ParcorrPareto'
            if ecoinstall, parcorr(norminv(CDFParetoSimple(Hsmax,umbral,sigma0,gamma0)),10); end
            title('Partial Autocorr. function Pareto')
            xlabel('Lag (years)');  % create legend
            ylabel('Sample partial autocorr. Pareto');  % create legend
        case 'Hsmax'
            HsmaxPlot(Hsmax,datemax,umbral,[],cityname,var_name,var_unit);
        otherwise
            disp(['Warnig!.Not exist this plot case ',plotname]);
    end
    savename=[savepath,plotname,var_name];
    if svopt(3), saveas(h,[savename,'.eps'],'psc2'); end
    if svopt(2), saveas(h,[savename,'.png']); end
    set(h,'Visible','on');
    if svopt(1), saveas(h,[savename,'.fig']); end
    close(h);
    
end
