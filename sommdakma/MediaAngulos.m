%Programa que calcula la media sobre el circulo y la desviacion tipica

%clear all

function [minimo,desviacion] = MediaAngulos(Dir)

%direcciones cada 1 grado para el primer barrido 
direcciones=linspace(0,2*pi,360);

dist2=[];
for i=1:length(direcciones)
    dir=direcciones(i);
    dis=min(abs(Dir-dir),2*pi-abs(Dir-dir));
    dist2(i)=sum(dis.^2);
end

% plot(dist2)

[mindis,posmin]=min(dist2);

minimo1=direcciones(posmin);

%direcciones para el segundo barrido (el valor del minimo del 1er barrido +/- 3)
%cada 0.01 grados
direcciones2=linspace((minimo1-3*pi/180),(minimo1+3*pi/180),600);

for i=1:length(direcciones2)
    dir2=direcciones2(i);
    dis2=min(abs(Dir-dir2),2*pi-abs(Dir-dir2));
    dist2_2(i)=sum(dis2.^2);
end

[mindis2,posmin2]=min(dist2_2);
min2=direcciones2(posmin2);
minimo=min2*180/pi;

distancia=min(abs(Dir-min2),2*pi-abs(Dir-min2));
temp1=distancia.^2;
desviacion=(sum(temp1)/(length(Dir))).^0.5;



