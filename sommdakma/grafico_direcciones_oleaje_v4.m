function [Co]=grafico_direcciones_oleaje_v4(t,dato1,dato2,dato3,varargin)
% OJO OJO OJO revisar esta funcion que valga tambien en el caso:
% grafico_direcciones_oleaje_v5 OJO OJO OJO

acierto_cien = varargin{1};
maximo = varargin{2};
% dato3 tienes que estar en grados (0 - 360)
conv2Rad1 = pi/180;
if min(dato3) >= 0 && max(dato3) <= pi,
    conv2Rad1 = 1;
    warning('Los datos ya estan en radianes y deberian estar en grados[0-360]!');
end
M1=normaliza(dato1,[0.3,1]);
Dir1=pi/2-dato3*conv2Rad1;
d1=find(Dir1(:,1)<-pi);
Dir1(d1)=Dir1(d1)+2*pi;
U1=-M1.*cos(Dir1);
V1=M1.*sin(Dir1);

if  length(varargin) == 4,
    dato4 = varargin{1}; %wind
    dato5 = varargin{2}; %dir wind
    acierto_cien = varargin{3};
    maximo = varargin{4};
    % dato3 tienes que estar en grados (0 - 360)
    conv2Rad2 = pi/180;
    if min(dato5) >= 0 && max(dato5) <= pi,
        conv2Rad2 = 1;
        warning('Los datos ya estan en radianes y deberian estar en grados[0-360]!');
    end
    M2=normaliza(dato4,[0.3,1]);
    Dir2=pi/2-dato5*conv2Rad2;
    d2=find(Dir2(:,1)<-pi);
    Dir2(d2)=Dir2(d2)+2*pi;
    U2=-M2.*cos(Dir2);
    V2=M2.*sin(Dir2);
end

%Escala de frecuencia de presentacion
cm=1-copper(15);    %Escala de azules (10 gamas)
cm1=[cm(1:9,:); cm(12,:)];  %Se eliminan las gamas mas oscuras
%Se buscan los centroides con frecuencia de presentacion nula
dum=find(acierto_cien==0);
if isempty(dum)==0
    acierto_cien(dum)=maximo;   %los valores nulos se igual al valor maximo para que no haya problemas al tomar logaritmos
end
acierto_log=log(acierto_cien);
%Normalizacion de los valores de la frecuencia de presentacion en la escala logaritmica
CM=round(normaliza(acierto_log,[2 10]));
%Los centroides con frecuencia de presentacion nula se igualan a 1 (para que tengan color blanco)
CM(dum)=1;
%Hexagonos con el fondo en la escala de grises, segun la frecuencia de presentacion
som_cplane('hexa',[t(1) t(2)],cm1(CM,:));

hold on
cm2=flipud(hot(320));cm2=cm2(1:256,:);
%cm2=hot(256);%cm2=autumn(256); % OJO OJO OJO cambio color
CM2=round(normaliza(dato1,[256 1]));
som_cplane('hexa',[t(1) t(2)],cm2(CM2,:),0.3);
hold on

Co = som_vis_coords('hexa',[t(1) t(2)]);

%flechas de direccion, con escala de periodos
c1=repmat([1 1 1],14,1)-gray(14);%c1=1-copper(256);
c2=[c1(4:12,:); c1(end,:)];
dum=find(dato2~=0);
per=dato2(dum);
mmax=max(per);
mmin=min(per);
%mmin=0;
%Flechas direccion del oleaje en la escala de azules segun el valor del periodo
C1=round(normaliza2(dato2,mmax,mmin,[1 10]));
h=miquiver(Co(:,1),Co(:,2),U1(:,1),V1(:,1),1.5,c2(C1,:));%1 o 1.5 o 1,[0 0 0]
%Flechas direccion del viento, en un unico azul
if  length(varargin) == 4
    C2=round(normaliza2(dato4,mmax,mmin,[1 256]));
    h=miquiver(Co(:,1),Co(:,2),U2,V2,0.7,'m');
end