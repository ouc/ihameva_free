function [qerr,bmu] = som_distancia_seq(M,D,mask,tipo, escalar, direccional)  %% la xi, yi no hace falta

    switch tipo
        case 'euclidiana'
            [qerr bmu] = min(((M-D).^2)*mask); % minimum distance(^2) and the BMU
                  
        case  'euclidiana2'
            [m,n]=size(M);   
            dif=zeros(m,n);
            for i=1:length(escalar)
                dif(:,escalar(i))=(D(:,escalar(i))-M(:,escalar(i))).*mask(escalar(i));
            end
            for i=1:length(direccional)
                dif(:,direccional(i))=min(abs(D(:,direccional(i))-M(:,direccional(i))),2*pi-abs(D(:,direccional(i))-M(:,direccional(i))))/pi.*mask(direccional(i));
            end
            Dist=sum(dif.^2,2);
            [qerr bmu] = min(Dist); % minimum distance(^2) and the BMU    
          
      otherwise error('Error');
          
  end    