function hs4 = desplazamiento (t, cmaxx, fmaxx,hs)

hs=reshape(hs,t(1),t(2));

fcentro=ceil(t(1)/2);
ccentro=ceil(t(2)/2);
fdiferencia=fcentro-fmaxx;
fresto=rem(fdiferencia,2);
cdiferencia=ccentro-cmaxx;
cresto=rem(cdiferencia,2);

if fresto~=0
    fcentro=fcentro-1;
end
if cresto~=0
    ccentro=ccentro-1;
end

%hs2=hs;
hs2=zeros(t(1),t(2));
if cmaxx<ccentro
    hs2(:,ccentro:t(2)) = hs(:,cmaxx:cmaxx+(t(2)-ccentro));
    hs2(:,1:ccentro-cmaxx) = hs(:,cmaxx+(t(2)-ccentro)+1:t(2));
    hs2(:,ccentro-cmaxx+1:ccentro-1) = hs(:,1:cmaxx-1);
else
    if cmaxx>ccentro
        hs2(:,ccentro:ccentro+(t(2)-cmaxx))=hs(:,cmaxx:t(2));
        hs2(:,ccentro+(t(2)-cmaxx)+1:t(2))=hs(:,1:cmaxx-ccentro);
        hs2(:,1:ccentro-1)=hs(:,cmaxx-ccentro+1:cmaxx-1);
    else
        %cmaxx==fcentro
        hs2=hs;
    end
end

hs3=zeros(t(1),t(2));
if fmaxx<fcentro
    hs3(fcentro:t(1),:) = hs2(fmaxx:fmaxx+(t(1)-fcentro),:);
    hs3(1:fcentro-fmaxx,:) = hs2(fmaxx+(t(1)-fcentro)+1:t(1),:);
    hs3(fcentro-fmaxx+1:fcentro-1,:) = hs2(1:fmaxx-1,:);
else
    if fmaxx>fcentro
        hs3(fcentro:fcentro+(t(1)-fmaxx),:)=hs2(fmaxx:t(1),:);
        hs3(fcentro+(t(1)-fmaxx)+1:t(1),:)=hs2(1:fmaxx-fcentro,:);
        hs3(1:fcentro-1,:)=hs2(fmaxx-fcentro+1:fmaxx-1,:);
    else
        %fmaxx==fcentro
        hs3=hs2;
    end
end

hs4=reshape(hs3,t(1)*t(2),1);
%hs4=reshape(hs2,t*t,1);


    