classdef amevaSOM < handle
    %   Methods for my SOM
    %   $Revision: 1.2.0.1 $  $Date: 07/Dec/2015 14:44:40 $
    properties(GetAccess='public', SetAccess='public')
    end
    methods
        function hf = amevaSOM()
        end
    end
    methods (Static=true)
        %
        function grafica(som, acierto_cien, t, LabX, LabY, titulo, pos_colorbars)
            if ~exist('pos_colorbars','var') || isempty(pos_colorbars), pos_colorbars = 'vertical'; end
            % direccional tienes que estar en grados (0 - 360)
            if size(som,2) == 3 && min(som(:,3)) >= 0 && max(som(:,3)) <= pi,
                som(:,3) = som(:,3)*180/pi;
                warning('La dir. esta en radianes y deberian estar en grados[0-360]!, Se transforman aqui');
            end
            if size(som,2) == 5 && min(som(:,5)) >= 0 && max(som(:,5)) <= pi,
                som(:,5) = som(:,5)*180/pi;
                warning('La dir. esta en radianes y deberian estar en grados[0-360]!, Se transforman aqui');
            end
            pfig=[360   502   560   420];
            posSom=[0.05 0.05 0.60 0.9];
            posEsc1=[0.70 0.1 0.05 0.80];
            posEsc2=[0.80 0.1 0.05 0.80];
            posEsc3=[0.90 0.1 0.05 0.80];
            if strcmp(pos_colorbars,'horizontal')
                pfig=[360   300   420 560];
                posSom=[0.1 0.27 0.8 0.7];
                posEsc1=[0.10 0.05 0.77 0.05];
                posEsc2=[0.10 0.14 0.77 0.05];
                posEsc3=[0.10 0.22 0.77 0.05];
            end
            set(gcf,'position',pfig)          % posicion de la fig
            subplot('position',posSom)
            if size(som,2) == 3
                [Co, cmEs23Frec, CMEs23Frec, cmEs1, CMEs1, cmEs2, CMEs2] = amevaSOM_cplane(t,som(:,1),som(:,2),som(:,3),acierto_cien);
            elseif size(som,2) == 5,
                [Co, cmEs23Frec, CMEs23Frec, cmEs1, CMEs1, cmEs2, CMEs2] = amevaSOM_cplane(t,som(:,1),som(:,2),som(:,3),som(:,4),som(:,5));
            end
            title(titulo,'Fontsize',12,'Fontweight','bold');
            
            % Color bar Frec
            subplot('position',posEsc1)   % frecuencia de presentacion
            amevaSOM.colorBarFrecuency(acierto_cien,[],[],[],0.01,pos_colorbars);
            
            subplot('position',posEsc2)   % escala de Hs
            xp = [1  32    64    96   128   160   192   224   size(cmEs1,1)];
            amevaSOM.colorBarScalar(som(:,1),LabX,xp,cmEs1,8,false,pos_colorbars);
            
            subplot('position',posEsc3)  % escala de Tp
            xp = [1  32    64    96   120   140   160   size(cmEs2,1)];
            amevaSOM.colorBarScalar(som(:,2),LabY,xp,cmEs2,8,false,pos_colorbars);
        end
        %
        function colorBarScalar(Hc,lab,xp,varColorMap,fntSize,moveTitleTF,pos_colorbars)
            if ~exist('fntSize','var') || isempty(fntSize), fntSize = 8; end
            if ~exist('moveTitleTF','var') || isempty(moveTitleTF), moveTitleTF = false; end
            if ~exist('pos_colorbars','var') || isempty(pos_colorbars), pos_colorbars = 'horizontal'; end
            etiqueta = [prctile(Hc,100/xp(end)*xp(end));prctile(Hc,100/xp(end)*xp(end-1)); ...
                prctile(Hc,100/xp(end)*xp(end-2)); prctile(Hc,100/xp(end)*xp(end-3)); ...
                prctile(Hc,100/xp(end)*xp(end-4)); prctile(Hc,100/xp(end)*xp(end-5)); ...
                prctile(Hc,100/xp(end)*xp(end-6)); prctile(Hc,100/xp(end)*xp(end-7));  ...
                prctile(Hc,100/xp(end)*0)];
            xp=linspace(etiqueta(1),etiqueta(end),100);
            [xx, yy]=meshgrid(xp,xp);   yy = flipud(yy);
            %pcolor
            if strcmp(pos_colorbars,'horizontal')
                pcolor(yy,xx,yy);
                shading interp; colormap(varColorMap); daspect([0.1 1 0.9]);
            elseif strcmp(pos_colorbars,'fichas')
                pcolor(yy,xx,yy);
                shading interp; colormap(varColorMap);
            else
                pcolor(xx,yy,yy); %Vertical
                shading interp; colormap(varColorMap); daspect([0.9 0.05 1]);
            end
            ttl = title(lab,'FontSize',fntSize);%,'fontsize',10,'fontweight','b')
            if moveTitleTF
                aux = get(ttl,'position');
                set(ttl, 'position', aux.*[1,0.85,1]);
            end
            
            if strcmp(pos_colorbars,'horizontal') || strcmp(pos_colorbars,'fichas')
                set(gca,'yticklabel','','ytick',[],'FontSize',fntSize);
            else
                set(gca,'xticklabel','','xtick',[],'FontSize',fntSize);
            end
            box on; freezeColors;
        end
        %
        function acierto_mes = aciertoestaciones(TimeVec,bmus,tamt)
            mes = [12,1,2;3,4,5;6,7,8;9,10,11];%estaciones
            acierto_mes = zeros(tamt,4);
            for k=1:4,
                aux_ = find(TimeVec(k,2)==mes(k,1) | TimeVec(:,2)==mes(k,2) | TimeVec(:,2)==mes(k,3));
                bmus_ = bmus(aux_);
                for j=1:tamt,
                    aux = find(bmus_==j);
                    if ~isempty(aux)
                        acierto_mes(j,k) = length(aux)/length(aux_)*100;
                    end
                end
            end
        end
        %
        function [acierto_log,minac,maxac,acierto_mes,acierto_log_mes,varColorMap,ncp]=colorBarFrecuency(acierto_cien,TimeVec,bmus2,tamt,minProb,pos_colorbars)
            if ~exist('minProb','var') || isempty(minProb), minProb = 0.04; end
            if ~exist('pos_colorbars','var') || isempty(pos_colorbars), pos_colorbars = 'horizontal'; end
            ncp=65; %colorbar probabilidades anuales y por estaciones
            cm=1-copper(ncp+10);%Escala de azules (65 gamas)%acierto mes/anual
            varColorMap=cm(1:ncp,:);%Se eliminan las gamas mas oscuras
            %varColorMap(1:32,:)=cm_(1:32,:);%OJO aqui mezcla de colores
            acierto_cien(acierto_cien==0) = min(acierto_cien(acierto_cien~=0))/10;%ponemos el acierto mes 10 veces mas pequeno OJO
            acierto_cien(acierto_cien<0.01) = minProb;
            acierto_log=log(acierto_cien);
            
            acierto_mes = nan; acierto_log_mes = nan;
            if exist('TimeVec','var') && ~isempty(TimeVec),
                acierto_mes = amevaSOM.aciertoestaciones(TimeVec,bmus2,tamt);
                acierto_mes(acierto_mes<0.01) = minProb;
                acierto_log_mes=log(acierto_mes);
            end
            
            minac=nanmin(min(acierto_log),min(min(acierto_log_mes)));
            maxac=nanmax(max(acierto_log),max(max(acierto_log_mes)));
            xp=linspace(exp(minac),exp(maxac),100);
            [xx,yy]=meshgrid(xp,xp); yy=flipud(yy);
            %pcolor
            if strcmp(pos_colorbars,'horizontal')
                pcolor(yy,xx,yy);
                shading interp; colormap(varColorMap); daspect([0.1 1 0.9]);
            else
                pcolor(xx,yy,yy); %Vertical
                shading interp; colormap(varColorMap); daspect([0.9 0.05 1]);
            end
            naux=13;
            etiqueta(1)=0;%ticks del colorbar
            etiqueta(2)=minac;
            etiqueta(13)=maxac;
            dx=(maxac-minac)/naux;
            for j=3:naux-1, etiqueta(j)=(etiqueta(2)+dx*(j-2)); end
            etiqueta=exp(etiqueta);
            xptick=linspace(exp(minac),exp(maxac),naux);
            if strcmp(pos_colorbars,'horizontal')
                title('Frecuencia de ocurrencia (%)','FontSize',8);%,'fontsize',10)
                set(gca,'XTick',xptick(1:3:end),'XTickLabel',{'0' num2str(etiqueta(4),'%1.3f') num2str(etiqueta(7),'%1.2f') num2str(etiqueta(10),'%1.2f') num2str(etiqueta(13),'%1.1f')},'yticklabel','','ytick',[],'FontSize',8);
            else
                title({'Frecuencia (%)'},'FontSize',8);%,'fontsize',10)
                set(gca,'YTick',xptick(1:3:end),'YTickLabel',{'0' num2str(etiqueta(4),'%1.3f') num2str(etiqueta(7),'%1.2f') num2str(etiqueta(10),'%1.2f') num2str(etiqueta(13),'%1.1f')},'xticklabel','','xtick',[],'FontSize',8);
            end
            box on; freezeColors;
        end
        %
        function colorBarDir(finalDir,lab,fntSize,moveTitleTF)
            if ~exist('fntSize','var') || isempty(fntSize), fntSize = 8; end
            if ~exist('moveTitleTF','var') || isempty(moveTitleTF), moveTitleTF = false; end
            
            [varColorMap,nDirColor,xp,ttdir] = amevaSOM.colorBarDirDiv(finalDir);
            [xx, yy] = meshgrid(xp,xp);
            yy = flipud(yy);
            pcolor(yy,xx,yy);
            shading interp;
            colormap(varColorMap);
            daspect([0.1 1 0.9]);
            ttl = title(lab,'FontSize',fntSize);%,'fontsize',10,'fontweight','b')
            if moveTitleTF
                aux = get(ttl,'position');
                set(ttl, 'position', aux.*[1,0.85,1]);
            end
            
            set(gca,'XTick',ttdir,'XTickLabel',num2str(ttdir'),'yticklabel','','ytick',[],'FontSize',8); box on; freezeColors;
        end
        %%
        function [varColorMap,nDirColor,xp,ttdir] = colorBarDirDiv(finalDir)
            auxndir = 0;
            minFinalDir = min(min(finalDir));
            maxFinalDir = max(max(finalDir));
            deltaDir = maxFinalDir - minFinalDir;
            if deltaDir < 210,
                auxndir = 120;
            elseif deltaDir < 270,
                auxndir = 60;
            elseif deltaDir < 330,
                auxndir = 30;
            end
            if auxndir > 0 && min(size(finalDir)) == 1, %Colores fijos si son 2 variables
                varColorMap = colormap(hsv(720)); %colorbar dir
                varColorMap = varColorMap(auxndir:end-auxndir,:);
                nDirColor = length(varColorMap);
                xp = linspace(minFinalDir,maxFinalDir,100);
                hf_ = figure;plot(xp,xp);axis equal;
                ttdir = get(gca,'xtick'); delete(hf_);
            else
                [varColorMap,nDirColor,xp,ttdir] = amevaSOM.colorBarDirDivFijos;
            end
        end
        %
        function [varColorMap,nDirColor,xp,ttdir] = colorBarDirDivFijos
            varColorMap = colormap(hsv(360)); %colorbar dir
            nDirColor = length(varColorMap);
            xp = linspace(0,360,100);
            ttdir = [0 45 90 135 180 225 270 315 360];
        end
        %
    end
end