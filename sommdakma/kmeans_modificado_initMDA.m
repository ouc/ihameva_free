function [io,c,nx] = kmeans_modificado_initMDA(x,nc,escalar,direccional,mascara)
% KMEANS : k-means clustering
% c = kmeans(x,nc)
%	x       - d*n samples
%	nc      - number of clusters wanted
%	c       - calculated membership vector
% algorithm taken from Sing-Tze Bow, 'Pattern Recognition'

% Copyright (c) 1995 Frank Dellaert
% All rights Reserved

[d,n] = size(x);

%------------------------------------------------------------------------
% step 1: Arbitrarily choose nc samples as the initial cluster centers
%------------------------------------------------------------------------
%La inicializacion se hace mediante el algoritmo MDA
semilla=find(max(x(:,1))==x(:,1));
c = algoritmo_MaxDiss_MaxMinSimplificado_SinUmbral(semilla, nc, x, escalar, direccional);
%[indAng,distAng]=knn(VCP(:,1:NCP),ERACP(:,1:NCP),ParamPrdc.NumA*2,'Norm-2');
io=NaN*ones([d,1]);
moved=d;
while(moved~=0)
%   [in]=MLknn(x,c,1,'Norm-2');
   %Busqueda del centroide mas proximo a cada dato, se utiliza distancia en el circulo para las variables circulares
   m=ones(nc,1);
   for i=1:d
       xx=x(i,:);
       xx2=xx(m,:);
       [qerr,bmu]=som_distancia_seq(c,xx2,mascara,'euclidiana2',escalar,direccional);
       in(i,1)=bmu;
   end
   for i=1:nc
      ix=find(in==i);
      nx(i,1)=size(ix,1);
      c(i,escalar)=nanmean(x(ix,escalar),1); %nanmean(x(ix,noang),1);
      %para las variables circulares se emplea otra expresion para determinar la media
      if isempty(ix)
          c(i,direccional)=NaN;
      else   
         for j=1:length(direccional)
            c(i,direccional(j))=MediaAngulos(x(ix,direccional(j)))*pi/180;
         end
      end   
   end
   moved=sum(io~=in,1);
   io=in;
   disp(['moved = ' num2str(moved)]);
end



