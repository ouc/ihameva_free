function M=normaliza2(M,mmax,mmin,rM)

%M(:)=(rM(2)-rM(1))*(M(:)-min(M(:)))./(max(M(:))-min(M(:)))+rM(1);
M(:)=(rM(2)-rM(1))*(M(:)-mmin)./(mmax-mmin)+rM(1);
