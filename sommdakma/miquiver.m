function hh = miquiver(X,Y,U,V,S,C)

% Arrow head parameters
alpha = 0.33; % Size of arrow head relative to the length of the vector
beta = 0.33;  % Width of the base of the arrow head relative to the length
hh=zeros(size(X))+NaN;

if nargin<5
    S=1;
end
if nargin<6
    C=[0 0 1];
end


N=length(X);
if(length(S)==1 & N>1)
    S=repmat(S,size(X));
end
if(size(C,1)==1 & N>1)
    C=repmat(C,size(X));
end

for i=1:N
    % Make velocity vectors
    x = X(i).'; y = Y(i).';
    u = U(i).'; v = V(i).';
    uu = [x;x+u;repmat(NaN,size(u))];
    vv = [y;y+v;repmat(NaN,size(u))];
    
    %h1 = plot(uu(:),vv(:),[col ls]);
    
    hu = [x+u-alpha*(u+beta*(v+eps));x+u; ...
            x+u-alpha*(u-beta*(v+eps));repmat(NaN,size(u))];
    hv = [y+v-alpha*(v-beta*(u+eps));y+v; ...
            y+v-alpha*(v+beta*(u+eps));repmat(NaN,size(v))];
    hold on
    hh(i) = plot([uu;hu(:)],[vv;hv(:)],'Color',C(i,:),'LineWidth',S(i,:));
    
end