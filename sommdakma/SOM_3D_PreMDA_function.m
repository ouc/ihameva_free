function [final2,bmus2,acierto_cien2]=SOM_3D_PreMDA_function(datos,escalar,direccional,t,classTipo,Opciones)
if ~exist('Opciones','var') || isempty(Opciones), Opciones=ameva_options; end

[N,dim]=size(datos);

%datos a utilizar en el entrenamiento de la SOM
cantidad=10000;
if cantidad>N-2, error('N-longitud del vector tiene que ser mayor que 10002'); end

%Normalizacion de los datos
minimos=zeros(length(escalar),1);
maximos=zeros(length(escalar),1);
for i=1:length(escalar)
    minimos(i)=min(datos(:,escalar(i)));
    maximos(i)=max(datos(:,escalar(i)));
end

datos_n=zeros(N,(length(escalar)+length(direccional)));
for i=1:length(escalar)
    datos_n(:,escalar(i))=(datos(:,escalar(i))-minimos(i))./(maximos(i)-minimos(i));
end
for i=1:length(direccional)
    datos_n(:,direccional(i))=datos(:,direccional(i))*pi/180; % grados a radianes
end

%aplicamos MaxDiss
semilla=find(max(datos(:,1))==datos(:,1));
[subset, ncenters] = algoritmo_MaxDiss_MaxMinSimplificado_SinUmbral(semilla(1), cantidad, datos_n, escalar, direccional);


%Datos en la estructura que necesita la SOM (los seleccionados con MaxDiss y normalizados)
sD=som_data_struct(subset);

%creo los label para la som
lab = labelsameva(Opciones.var_name,Opciones.var_unit,[1:size(datos,2)]);
sD.comp_names=lab;

%Size
tam=t(1)*t(2);

%Calculo de la SOM
mascara=ones(dim,1);
sM = som_make_modificado_InitEOFs(sD,'msize',[t(1) t(2)],'algorithm','seq','init','lininit','shape','toroid','training','long','mask',mascara,...
    'tipo','euclidiana2','escalares',escalar,'direccionales',direccional);


%calculo de los bmus de todos los datos
bmus=zeros(N,1);
m=ones(length(sM.codebook),1);
for i=1:N
    retro=datos_n(i,:);
    retro2=retro(m,:);
    [qerr,bmu]=som_distancia_seq(sM.codebook,retro2,mascara,'euclidiana2',escalar,direccional);
    bmus(i)=bmu;
end

SM=sM.codebook;

%Desnormalizar los resultados
final=zeros(tam,dim);
for i=1:length(escalar)
    final(:,escalar(i))=sM.codebook(:,escalar(i))*(maximos(i)-minimos(i))+minimos(i);
end
for i=1:length(direccional)
    final(:,direccional(i))=sM.codebook(:,direccional(i))*180/pi; % radianes a grados
end

%frecuencia de presentacion de los centroides segun todos los datos de retroanalisis
acierto=zeros(tam,1);
for i=1:length(sM.codebook)
    acierto(i)=sum(bmus==i);
end
%frecuencia de presentacion de los centroides segun todos los datos de retroanalisis
acierto_cien=acierto/length(bmus)*100;
% maximo=max(acierto_cien);


%Desplamiento de la SOM para que la celda con la mayor altura de ola quede en el centro
xx=reshape(final(:,1),t(1),t(2));
temp=max(xx);
cmaxx=find(max(temp)==temp);    %columna con la maxima altura de ola
temp2=xx(:,cmaxx);
fmaxx=find(max(temp2)==temp2);  %fila con la maxima altura de ola

final2(:,1) = desplazamiento (t, cmaxx, fmaxx, xx);
for i=2:dim
    final2(:,i) = desplazamiento (t, cmaxx, fmaxx, final(:,i));
end
acierto_cien2 = desplazamiento (t, cmaxx, fmaxx, acierto_cien);

for i=1:dim
    SM2(:,i) = desplazamiento (t, cmaxx, fmaxx, SM(:,i));
end

%calculo de los bmus de todos los datos
bmus2=zeros(N,1);
m=ones(length(SM2),1);
for i=1:N
    retro=datos_n(i,:);
    retro2=retro(m,:);
    [qerr,bmu]=som_distancia_seq(SM2,retro2,mascara,'euclidiana2',escalar,direccional);
    bmus2(i)=bmu;
end

%PLOTS
if sum(Opciones.svopt)>0
    classification_plots(datos,escalar,direccional,t,classTipo,Opciones,final2,sM,SM2,acierto_cien2);
end