function [Co, cmEs23Frec, CMEs23Frec, cmEs1, CMEs1, cmEs2, CMEs2] = amevaSOM_cplane(t,escalar1,escalar2,direccion12,varargin)
escalar3 = []; %wind
direccion3 = []; %dir wind
CMEs2 = [0 0 0]; cmEs2 = []; % Flechas color negro
colorFechas1  = CMEs2; % negro para todas de igual color
colorFechas2  = [1 0 1]; % margenta para wind dir
lngFechas1 = 1;

if nargin == 4,
    %%%%%SOM3 GENERAL azules-Frecuency, rojos-Hs, fechas(grises-Tp)<->Dir(Tam-Hs)
    acierto_cien = varargin{1};
    maximo = max(acierto_cien);
    %Se buscan los centroides con frecuencia de presentacion nula
    dum=find(acierto_cien==0);
    if isempty(dum)==0
        acierto_cien(dum)=maximo;   %los valores nulos se igual al valor maximo para que no haya problemas al tomar logaritmos
    end
    acierto_log=log(acierto_cien);
    %Normalizacion de los valores de la frecuencia de presentacion en la escala logaritmica
    %Escala de frecuencia de presentacion
    cm = 1-copper(15);    %Escala de azules (10 gamas)
    cmEs23Frec = [cm(1:9,:); cm(12,:)];  %Se eliminan las gamas mas oscuras
    CMEs23Frec=round(normaliza(acierto_log,[2 10]));
    %Los centroides con frecuencia de presentacion nula se igualan a 1 (para que tengan color blanco)
    CMEs23Frec(dum)=1;
    
    %Hs <->escalar1<-> autumn(256); hot(256); flipud(hot(320))
    [cmEs1,CMEs1,colorHezaInterno] = varColor(escalar1,'hot',320,1,64);
    %Hs <->escalar2<-> Color del periodo Tp Verdes
    [cmEs2,CMEs2,colorHezaExterno] = varColor(escalar2,'copper',256,2);
elseif nargin == 5 && length(varargin{1}) == length(escalar1),
    %%%%%SOM3 FICHAS verdes-Tp, rojos-Hs, fechas(negras)<->Dir(Tam-Hs)
    %%%%%SOM3 GENERAL azules-Frecuency, rojos-Hs, fechas(grises-Tp)<->Dir(Tam-Hs)
    acierto_cien = varargin{1};
    maximo = max(acierto_cien);
    %Se buscan los centroides con frecuencia de presentacion nula
    dum=find(acierto_cien==0);
    if isempty(dum)==0
        acierto_cien(dum)=maximo;   %los valores nulos se igual al valor maximo para que no haya problemas al tomar logaritmos
    end
    acierto_log=log(acierto_cien);
    %Normalizacion de los valores de la frecuencia de presentacion en la escala logaritmica
    %Escala de frecuencia de presentacion
    cm = 1-copper(15);    %Escala de azules (10 gamas)
    cmEs23Frec = [cm(1:9,:); cm(12,:)];  %Se eliminan las gamas mas oscuras
    CMEs23Frec=round(normaliza(acierto_log,[2 10]));
    %Los centroides con frecuencia de presentacion nula se igualan a 1 (para que tengan color blanco)
    CMEs23Frec(dum)=1;
    %Hs <->escalar1<-> autumn(256); hot(256); flipud(hot(320))
    [cmEs1,CMEs1,colorHezaInterno] = varColor(escalar1,'hot',320,1,64);
    %Tp <->escalar2<-> copper-verdes
    [cmEs2,CMEs2,colorHezaExterno] = varColor(escalar2,'copper',256,2);
elseif nargin == 6 && length(varargin{2}) == 3
    %%%%%SOM3 FICHAS/WindWave verdes-Wind, rojos-Hs, fechas(margenta)<->Dir(Wind)
    acierto_cien = varargin{1};
    maximo = max(acierto_cien);
    %Se buscan los centroides con frecuencia de presentacion nula
    dum=find(acierto_cien==0);
    if isempty(dum)==0
        acierto_cien(dum)=maximo;   %los valores nulos se igual al valor maximo para que no haya problemas al tomar logaritmos
    end
    acierto_log=log(acierto_cien);
    %Normalizacion de los valores de la frecuencia de presentacion en la escala logaritmica
    %Escala de frecuencia de presentacion
    cm = 1-copper(15);    %Escala de azules (10 gamas)
    cmEs23Frec = [cm(1:9,:); cm(12,:)];  %Se eliminan las gamas mas oscuras
    CMEs23Frec=round(normaliza(acierto_log,[2 10]));
    %Los centroides con frecuencia de presentacion nula se igualan a 1 (para que tengan color blanco)
    CMEs23Frec(dum)=1;

    %Wind <->escalar2<-> autumn(256); hot(256); flipud(hot(320))
    [cmEs2,CMEs2,colorHezaInterno] = varColor(escalar2,'hot',320);
    %Hs <->escalar1<-> copper-verdes
    [cmEs1,CMEs1,colorHezaExterno] = varColor(escalar1,'copper',256,2);
    colorFechas1 = varargin{2};
elseif nargin == 6 && length(varargin{2}) == length(direccion12),
    %%%%%SOM5 FICHAS verdes-W10, rojos-Hs, fechas(grise-Tp)<->Dir(Tam-Hs),fechas(margenta)<->DirWTam-W10)
    escalar3 = varargin{1}; % wind
    direccion3 = varargin{2}; % dir wind
    lngFechas1 = 1.5;
    lngFechas2 = 0.7;
    
    %Hs <->escalar1<-> autumn(256); hot(256); flipud(hot(320))
    [cmEs1,CMEs1,colorHezaInterno] = varColor(escalar1,'hot',320,1,64);
    %Hs <->escalar2<-> Color del W10 Verdes
    [cmEs23Frec,CMEs23Frec,colorHezaExterno] = varColor(escalar3,'copper',256,2);
    % Flechas direccion del oleaje en la escala de grises segun el valor del periodo
    [cmEs2,CMEs2,colorFechas1] = varColor(escalar2,'gray',320,70,70);
else
    error('formato incorrecto de los datos de entrada amevaSOM_cplane');
end

% direccion12 tienes que estar en grados (0 - 360)
conv2Rad1 = pi/180;
if min(direccion12) >= 0 && max(direccion12) <= pi,
    conv2Rad1 = 1;
    warning('Los datos ya estan en radianes y deberian estar en grados[0-360]!');
end
M1 = normaliza(escalar1,[0.3,1]);
Dir12 = pi/2-direccion12*conv2Rad1;
d1 = find(Dir12(:,1)<-pi);
Dir12(d1) = Dir12(d1)+2*pi;
U1 = -M1.*cos(Dir12);
V1 = M1.*sin(Dir12);
if not(isempty(escalar3)) && not(isempty(direccion3)),
    % direccion12 tienes que estar en grados (0 - 360)
    conv2Rad2 = pi/180;
    if min(direccion3) >= 0 && max(direccion3) <= pi,
        conv2Rad2 = 1;
        warning('Los datos ya estan en radianes y deberian estar en grados[0-360]!');
    end
    M2=normaliza(escalar3,[0.3,1]);
    Dir3 = pi/2-direccion3*conv2Rad2;
    d2 = find(Dir3(:,1)<-pi);
    Dir3(d2) = Dir3(d2)+2*pi;
    U2 = -M2.*cos(Dir3);
    V2 = M2.*sin(Dir3);
end

%Unit coordinates used in visualizations
Co = som_vis_coords('hexa',[t(1) t(2)]);
%Hexagonos Externos
som_cplane('hexa',[t(1) t(2)],colorHezaExterno); hold on
%Hexagonos Internos
som_cplane('hexa',[t(1) t(2)],colorHezaInterno,0.5); hold on
%Flechas de direccion en el color de escalar2
miquiver(Co(:,1),Co(:,2),U1(:,1),V1(:,1),lngFechas1,colorFechas1);%1 o 1.5 o 1,[0 0 0]
%Flechas direccion del viento, en un unico margenta
if not(isempty(escalar3)) && not(isempty(direccion3)),
    miquiver(Co(:,1),Co(:,2),U2,V2,lngFechas2,colorFechas2);
end

end

function [matrixColor,norMatrizColor,colorHezagono] = varColor(escalar,myColor,N,deltaIni,deltaFin)
if ~exist('myColor','var') || isempty(myColor), myColor='copper'; end % Color
if ~exist('N','var') || isempty(N), N=11; end % Número de colores
if ~exist('deltaIni','var') || isempty(deltaIni), deltaIni=1; end % Eliminar blando o no
if ~exist('deltaFin','var') || isempty(deltaFin), deltaFin=0; end % Final igual no cambia
cc = [];
if strcmpi(myColor,'hot')
    matrixColor = flipud(hot(N));
    matrixColor = matrixColor(deltaIni:N-deltaFin,:); %Color Hezagon Ingerno, comun en todos los casos
    norMatrizColor = round(normaliza(escalar,[1 length(matrixColor)]));
else
    eval(['cc=1-',myColor,'(',num2str(N),');']);
    c1 = cc(deltaIni:N-deltaFin,:);
    matrixColor(:,1) = c1(:,1);
    matrixColor(:,2) = c1(:,3);
    matrixColor(:,3) = c1(:,2);
    per = escalar(escalar~=0);
    mmax2 = max(per);
    mmin2 = min(per);
    norMatrizColor = round(normaliza2(escalar,mmax2,mmin2,[1 length(c1)]));
end
colorHezagono = matrixColor(norMatrizColor,:);
end