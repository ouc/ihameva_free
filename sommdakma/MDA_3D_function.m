function [final ,bmus,acierto_cien,datos]=MDA_3D_function(datos,escalar,direccional,tam,classTipo,Opciones)

[N,dim]=size(datos);

% %Size KMedias
% tam=t^2;
% tam=round(tam);%%%% OC to intro the squared 
%Size del subconjunto a seleccionar
cantidad=tam;


%Normalizacion de los datos
minimos=zeros(length(escalar),1);
maximos=zeros(length(escalar),1);
for i=1:length(escalar)
    minimos(i)=min(datos(:,escalar(i)));
    maximos(i)=max(datos(:,escalar(i)));
end

datos_n=zeros(N,3);
for i=1:length(escalar)
    datos_n(:,escalar(i))=(datos(:,escalar(i))-minimos(i))./(maximos(i)-minimos(i));
end
for i=1:length(direccional)
    datos_n(:,direccional(i))=datos(:,direccional(i))*pi/180;
end

    % OJO OJO aqui modificar el máximo o semilla OJO OJO
    semilla=find(max(datos(:,1))==datos(:,1));
    
    [subset, ncenters] = algoritmo_MaxDiss_MaxMinSimplificado_SinUmbral(semilla(1), cantidad, datos_n, escalar, direccional);

    %calculo de los bmus de todos los datos
    bmus=zeros(N,1);
    m=ones(cantidad,1);
    for i=1:N
        retro=datos_n(i,:);
        retro2=retro(m,:);
        [qerr,bmu]=dist_normalizada(subset,retro2,escalar, direccional);
        bmus(i)=bmu;
    end
   
    
    %Desnormalizar los resultados
    final=zeros(cantidad,dim);
    for i=1:length(escalar)
        final(:,escalar(i))=subset(:,escalar(i))*(maximos(i)-minimos(i))+minimos(i);
    end
    for i=1:length(direccional)
        final(:,direccional(i))=subset(:,direccional(i))*180/pi;
    end

   
    %frecuencia de presentacion de los centroides segun todos los datos de retroanalisis
    acierto=zeros(cantidad,1);
    for i=1:cantidad
        acierto(i)=sum(bmus==i);
    end

    %frecuencia de presentacion de los centroides segun todos los datos de retroanalisis
    acierto_cien(1:cantidad)=acierto/length(bmus)*100;
    
    
%     %Se busca el subconjunto seleccionado en la base completa de los datos para
%     %indentificar las fechas y tomar los casos reales
%     pos=zeros(cantidad(cc),1);
%     fecha=zeros(cantidad(cc),4);
%     for i=1:length(subset)
%         mm=ones(length(datos_n),1);
%         temp1=subset(i,:);
%         temp2=temp1(mm,:);
%         temp3=datos_n-temp2;
%         temp4=sum(temp3,2);
%         temp5=find(temp4==0);  %Posiciones dentro del fichero de datos de reanalisis
%         pos(i)=temp5(1);
%         fecha(i,:)=A(pos(i),1:4);
%     end
%     save([pathr 'Fechas' num2str(cantidad(cc)) 'MDA' titulo '.dat'],'fecha','-ascii');
%     save([pathr 'Posiciones' num2str(cantidad(cc)) 'MDA' titulo '.dat'],'pos','-ascii');
%set axis dir lim

%PLOTS
if sum(Opciones.svopt)>0
    classification_plots(datos,escalar,direccional,tam,classTipo,Opciones,final);
end