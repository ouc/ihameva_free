function [final ,bmus,acierto_cien,datos]=KMA_3D_function(datos,escalar,direccional,tam,classTipo,Opciones)

[N,dim]=size(datos);

%Normalizacion de los datos
minimos=zeros(length(escalar),1);
maximos=zeros(length(escalar),1);
for i=1:length(escalar)
    minimos(i)=min(datos(:,escalar(i)));
    maximos(i)=max(datos(:,escalar(i)));
end

datos_n=zeros(N,3);
for i=1:length(escalar)
    datos_n(:,escalar(i))=(datos(:,escalar(i))-minimos(i))./(maximos(i)-minimos(i));
end
for i=1:length(direccional)
    datos_n(:,direccional(i))=datos(:,direccional(i))*pi/180;
end

% %Size
% tam=t^2;
% tam=round(tam);%%%% OC to intro the squared 

%Obtener agrupamiento, con K-medias
mascara=ones(dim,1);
[bmus,centers]=kmeans_modificado_initMDA(datos_n,tam,escalar,direccional,mascara);


%Desnormalizar los resultados
final=zeros(tam,dim);
for i=1:length(escalar)
    final(:,escalar(i))=centers(:,escalar(i))*(maximos(i)-minimos(i))+minimos(i);
end
for i=1:length(direccional)
    final(:,direccional(i))=centers(:,direccional(i))*180/pi;
end

%frecuencia de presentacion de los centroides segun todos los datos de retroanalisis
acierto=zeros(tam,1);
for i=1:tam
    acierto(i)=sum(bmus==i);
end

%frecuencia de presentacion de los centroides segun todos los datos de retroanalisis
acierto_cien=acierto/length(bmus)*100;


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %Busqueda del estado de mar real mas cercano al centroide
% real=zeros(tam,4+dim);  
% for i=1:tam
%     d=find(bmus==i);
%     elegidos=datos_n(d,:);    %en el espacio K-Medias   
%     eleg=datos(d,:);
%     date=A(d,1:4);            %fecha  correspondiente a esos datos
%     %calculo de los bmus de todos los datos de retroanalisis
%     [mm,nn]=size(elegidos);
%     m=ones(mm,1);
%     centro=centers(i,:);
%     centro2=centro(m,:);
%     [qerr,bmu] = som_distancia_seq(centro2,elegidos,mascara,'euclidiana2',escalar,direccional); 
%     if isempty(bmu)==0
%        real(i,:)=[date(bmu,1:4) eleg(bmu,:)];  %se guarda la fecha y los parametros del oleaje de ese estado de mar
%     end
% end
% 
% save(strcat(pathr,'EstadosMarReales_KMA',num2str(tam),'.dat'),'real','-ascii');

%PLOTS
if sum(Opciones.svopt)>0
    classification_plots(datos,escalar,direccional,tam,classTipo,Opciones,final);
end