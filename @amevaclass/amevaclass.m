classdef amevaclass < handle
    %   Methods for my ameva windows
    %   $Revision: 1.2.0.1 $  $Date: 07/Dec/2012 14:44:40 $
    
    properties(GetAccess='public', SetAccess='public')
        source='';
        carpeta='';
        workPath='';
        nmfctn='';
        state=false;
        timestate=[];
        var_name='';
        data=[];
    end
    methods
        function hf = amevaclass()%(carpeta)
            % Assign to object properties
            %hf.carpeta = carpeta;
        end % constructor
        %     function delete(obj)
        %         %ishandle(obj.plot),delete(obj.plot);return
        %     end
        %     function obj = set.carpeta(obj,val)
        %         obj.carpeta = val;
        %     end
        function AmevaUpdateName(obj,nval,ui_cm,ui_all,ui_td) %%Update cell,struct and matrix name
            %covgevid para gargar toda la matrix de covariables o solo
            %determinada columnas
            if ~isempty(ui_cm)
                set(ui_cm(nval),'string','(none)','Value',1,'visible','off');
            end
            ionea=get(ui_all(nval),'Value');
            sonea=get(ui_all(nval),'String');
            if ionea>1
                if isstruct(evalin('base',char(sonea(ionea))))
                    fld=fieldnames(evalin('base',char(sonea(ionea))),'-full');
                elseif strcmp(class(evalin('base',char(sonea(ionea)))),'double') && max(size(evalin('base',char(sonea(ionea)))))>1 && min(size(evalin('base',char(sonea(ionea)))))>1
                    fld=num2cell((1:min(size(evalin('base',char(sonea(ionea))))))');
                end
                s=get(ui_td(nval),'string');
                set(ui_td(nval),'string',{char(s(1,:)),['(',sprintf('%dx%d %s',size(evalin('base',char(sonea(ionea)))),class(evalin('base',char(sonea(ionea))))),')']});
            else
                s=get(ui_td(nval),'string');
                set(ui_td(nval),'string',s(1,:));
            end
            if  ~isempty(ui_cm)
                if exist('fld','var') && ~isempty(fld)
                    set(ui_cm(nval),'string',fld,'visible','on');
                    ST = dbstack('-completenames');
                    if  nval==4 && any(cell2mat(strfind({ST.file},'gev'))), 
                        set(ui_cm(nval),'Value',length(fld)); 
                        %disp('Modos ON, GEV Tool'); 
                    end 
                else
                    set(ui_cm(nval),'string','(none)','Value',1,'visible','off');
                end
            end
        end
        function obj=AmevaEvalData(obj,nval,ui_cm,ui_all) %% Eval data, pasarlos a la ventana
            sname=get(ui_all(nval),'String');
            vname=sname{get(ui_all(nval),'Value')};
            tipf=class(evalin('base',vname));
            tamf=size(evalin('base',vname));
            [fmps fmpi]=min(tamf);%find the position of minimum
            if strcmp(tipf,'double') && min(tamf)==1
                tpname='';
                vn=vname;
            elseif strcmp(tipf,'double') && min(tamf)>1
                ipos=get(ui_cm(nval),'Value');
                spos=get(ui_cm(nval),'String');
                if fmpi==1,
                    tpname=['(',num2str(ipos),',:)'];
                else
                    tpname=['(:,',num2str(ipos),')'];
                    ST = dbstack('-completenames');
                    if  nval==4 && any(cell2mat(strfind({ST.file},'gev'))), tpname=['(:,1:',num2str(ipos),')']; disp('Modos ON, GEV Tool'); end 
                end
                vn=vname;
            elseif strcmp(tipf,'struct')
                ipos=get(ui_cm(nval),'Value');
                spos=get(ui_cm(nval),'String');
                tpname=['.' spos{ipos}];
                vn=spos{ipos};
            else
                error('tipo de data de entrada erroneo!');
            end
            aux=strfind(vn,'_');if isempty(aux), aux=2; end
            vn=regexprep(vn,'_','','preservecase');
            obj.var_name=[vn(1) vn(aux:end)];
            data_aux=evalin('base',[vname tpname]);
            if min(size(data_aux))==1, data_aux=data_aux(:); end
            obj.data=data_aux;
        end
        function obj=AmevaPlotToPrint(obj,val1,val2,val3)%% Callback for list of fig to print
            %val2: 'Santander1'
            %val3: 'C:\Users\fernando\Documents\MATLAB\'
            %Ej1. awinc.ListBoxCallbackP(ui_pp,carpeta);
            %Ej2. awinc.ListBoxCallbackP(ui_pp,carpeta,workDir);
            aux=strfind(val1,filesep);%quito el path del val1 si existe
            if ~isempty(aux), val1=val1(aux(end)+1:end); end
            obj.source=val1;
            obj.carpeta=val2;
            if exist('val3','var') && ~isempty(val3), pathd=val3(1:end-1); else pathd=pwd; end
            ival=get(obj.source,'Value');
            iins=get(obj.source,'String');
            [ni mi]=size(iins);
            if ni>0
                figurename=char(iins(ival,1));
                if strcmpi(figurename(end-2:end),'fig')
                    open([obj.carpeta,filesep,figurename]);
                elseif strcmpi(figurename(end-2:end),'png')
                    if ispc,
                        winopen(fullfile(obj.carpeta,figurename));
                    elseif isunix
                        unix(strcat('eog "',fullfile(obj.carpeta,figurename),'" &'));
                    end
                end
            end
            if exist('val3','var') && ~isempty(val3), obj.carpeta=([pathd filesep obj.carpeta]); end
        end
        function obj=ListBoxCallback(obj,val1,objstr)%% Callback for list box
            % Load workspace vars into list box
            vars = evalin('base','who');
            if ~isempty(vars),
                li = length(vars);
                j=1;
                varsI{j}='(none)';
                j=j+1;
                for i=1:li,
                    if exist('objstr','var') && strcmp(objstr,'double')
                        if (strcmp(class(evalin('base',vars{i})),'double') && max(size(evalin('base',vars{i})))==1 && min(size(evalin('base',vars{i})))==1 )
                            varsI{j}=vars{i};
                            j=j+1;
                        end
                    elseif exist('objstr','var') && strcmp(objstr,'struct')
                        if strcmp(class(evalin('base',vars{i})),'struct')
                            varsI{j}=vars{i};
                            j=j+1;
                        end
                    elseif exist('objstr','var') && objstr,
                        if ( isstruct(evalin('base',vars{i})) || (strcmp(class(evalin('base',vars{i})),'double') && max(size(evalin('base',vars{i})))>1) )
                            varsI{j}=vars{i};
                            j=j+1;
                        end
                    else
                        if (strcmp(class(evalin('base',vars{i})),'double') && max(size(evalin('base',vars{i})))>1 )
                            varsI{j}=vars{i};
                            j=j+1;
                        end
                    end
                end
            else
                varsI{1}='(none)';
            end
            %compruebo que no se hayan borrado variables
            for i=1:length(val1)
                if get(val1(i),'value')>length(varsI)
                    set(val1(i),'String',varsI{1},'value',1);
                end
            end
            obj.source=val1;
            set(obj.source,'String',varsI);
        end
        function obj=ListBoxCallbackP(obj,val1,val2,val3,val4,val5)%% Callback for list plot box
            %val2: 'Santander1'
            %val3: 'C:\Users\fernando\Documents\MATLAB\'
            %val4: '*.fig' '*.png', etc etc
            %Ej1. awinc.ListBoxCallbackP(ui_pp,carpeta);
            %Ej2. awinc.ListBoxCallbackP(ui_pp,carpeta,workDir,'*.png');
            aux=strfind(val2,filesep);%quito el path del val1 si existe
            if ~isempty(aux), val2=val2(aux(end)+1:end); end
            obj.source=val1;
            obj.carpeta=val2;
            if exist('val3','var') && ~isempty(val3),
                if strcmp(val3(end),filesep)
                    pathd=val3(1:end-1);
                else
                    pathd=val3;
                end
            else
                pathd=pwd;
            end
            if exist('val4','var') && ~isempty(val4), defft=val4; else defft='*.fig'; end %defaul format .fig
            % Load workspace fig,png, etc into list box
            figlist=dir(fullfile(pathd,[obj.carpeta,filesep,defft]));
            %solo me quedo con los especificado en val5 ej: val5={'SOM';'MDA';'KMA'}
            if exist('val5','var') && ~isempty(val5),
                pos=[];
                for j=1:length(figlist)
                    thisString = figlist(j).name;
                    for k=1:length(val5)
                        if strcmp(thisString(1:length(val5{k})), val5{k}), pos(1+end)=j; break; end
                    end
                end
                figlist=figlist(pos);
            end
            if ~isempty(figlist),
                li = length(figlist);
                varsI{1}='(Plot to print)';
                for i=1:li,
                    varsI{i}=figlist(i,1).name;
                end
            else
                varsI{1}='(Plot to print)';
            end
            if exist('val3','var') && ~isempty(val3), obj.carpeta=([pathd filesep obj.carpeta]); end
            set(obj.source,'String',varsI,'Value',1);
        end
        function obj=AmevaClose(obj,val1,val2)%% Close all Ameva windows
            obj.source=val1;
            obj.nmfctn=val2;
            set(0,'ShowHiddenHandles','on');
            [ni mi]=size(obj.nmfctn);
            for i=1:ni,
                delete(findobj('type','figure','Name',obj.nmfctn{i}));
            end
            li = length(obj.source);
            for i=1:li,
                if ishandle(obj.source(i)), delete(obj.source(i)); end
            end
            if ishandle(findobj('Tag','Tag_amevaworkspace')), delete(findobj('Tag','Tag_amevaworkspace')); end
        end
        function obj=NewFolderCheck(obj,val1,val2,val3)%% Creo el driectorio de salida de resultados y actualizo el nombre del directorio
            aux=strfind(val1,filesep);%quito el path del val1 si existe
            if ~isempty(aux), val1=val1(aux(end)+1:end); end
            obj.carpeta=val1;
            obj.nmfctn=val2;
            if exist('val3','var') && ~isempty(val3),
                if strcmp(val3(end),filesep)
                    pathd=val3(1:end-1);
                else
                    pathd=val3;
                end
            else
                pathd=pwd;
            end
            if isempty(obj.carpeta), obj.carpeta=[obj.nmfctn,'[',datestr(now,'yyyymmddHHMMSS'),']']; end
            if ~exist([pathd filesep obj.carpeta],'dir'), mkdir([pathd filesep obj.carpeta]); end
            if exist([pathd filesep obj.carpeta],'dir') && ( ~isempty(dir(fullfile(pathd,[obj.carpeta,filesep,'*.mat']))) || ~isempty(dir(fullfile(pathd,[obj.carpeta,filesep,'*.fig']))) || ~isempty(dir(fullfile(pathd,[obj.carpeta,filesep,'*.txt']))) )
                NewDir=questdlg('Do you want to create a new folder?','Create folder Question','Yes', 'No','Yes');
                if strcmp(NewDir,'Yes'),
                    obj.carpeta=[obj.nmfctn,'[',datestr(now,'yyyymmddHHMMSS'),']'];
                    mkdir([pathd filesep obj.carpeta]);
                end
            end
            if exist('val3','var') && ~isempty(val3), obj.carpeta=([pathd filesep obj.carpeta]); end
        end
        function obj=LoadPath(obj)%% Path work directorio
            obj.workPath = uigetdir;
            if ~isempty(obj.workPath) && ~strcmp(obj.workPath(end),filesep)
                obj.workPath=[obj.workPath filesep];
            end
        end
        function obj=AmevaVarCheck(obj,val,var1,var2,str1,str2)%% Comprobar Variables
            %compruebo que dos variables son diferentes y no estan vacias
            obj.source=val;
            obj.state=false;
            uistate=false;
            if ~isempty(val) && ishandle(val), uistate=true; end
            if ~isempty(var1)
                if ~isempty(var2)
                    if isequal(var1,var2),
                        if uistate, set(obj.source,'String',['[Data]: >> ATENTION! they need different ',str1,' and ',str2,' vectors to continue!']); end
                        disp(['ATENTION! the vectors ',str1,' and ',str2,' must be different to continue!']);
                        obj.state=true; return;
                    else
                        [mi,ni]=size(var1);
                        [mr,nr]=size(var2);
                        if ni~=1 || nr~=1 || mi~=mr,
                            if uistate, set(obj.source,'String',['[Data]: >> ATENTION! not macht size array ' str1 '['  num2str(mi) ']; '  str2 '['  num2str(mr) ']']); end
                            disp(['ATENTION! not macht size array ' str1 '['  num2str(mi) ']; '  str2 '['  num2str(mr) ']']);
                            obj.state=true; return;
                        end
                        if strcmpi(str1,'Dir'), %Compruebo que es una direccion [0 180]
                            if  min(var1)<0 || max(var1)>360,
                                disp(['The Dir vector can only contain values between 0',char(176),' and 360',char(176),'. min(' str1 ')='  num2str(min(var1)) ' y max(' str1 ')='  num2str(max(var1))]);
                                obj.state=true; return;
                            end
                            if max(var1)<1, %Compruebo que es una direccion y que tiene valores mayores que 1 grado
                                disp(['The maximum value of the Dir vector must be greater than 1',char(176),'. max(' str1 ')='  num2str(max(var1))]);
                                obj.state=true; return;
                            end
                        end
                        if strcmpi(str2,'Dir'), %Compruebo que es una direccion [0 180]
                            if  min(var2)<0 || max(var2)>360,
                                disp(['The Dir vector can only contain values?? between 0',char(176),' and 360',char(176),'. min(' str2 ')='  num2str(min(var2)) ' y max(' str2 ')='  num2str(max(var2))]);
                                obj.state=true; return;
                            end
                            if max(var2)<1, %Compruebo que es una direccion y que tiene valores mayores que 1 grado
                                disp(['The maximum value of the Dir vector must be greater than 1',char(176),'. max(' str2 ')='  num2str(max(var2))]);
                                obj.state=true; return;
                            end
                        end
                        if ~isempty(find(isnan(var1),1)), disp(['nan exist in your ',str1]); end
                        if ~isempty(find(isnan(var2),1)), disp(['nan exist in your ',str2]); end
                        if ~isempty(find(isinf(var1),1)), disp(['inf exist in your ',str1]); end
                        if ~isempty(find(isinf(var2),1)), disp(['inf exist in your ',str2]); end
                        if(mi<50), disp('[Data]: >> WARNING! not enought data'); end
                    end
                end
            end
        end
        function obj=AmevaTimeCheck(obj,val,var1)%% Comprobar Time
            obj.source=val;
            obj.state=false;
            uistate=false;
            if ~isempty(val) && ishandle(val), uistate=true; end
            if ~isempty(var1)
                if ~isempty(find(isnan(var1),1)), disp(['nan exist in your ',str1]); end
                if ~isempty(find(isinf(var1),1)), disp(['inf exist in your ',str1]); end
                if length(var1)<50, disp(['[Data]: >> WARNING! not enought data: ' num2str(length(var1))]); end
                if not(isequal(var1,sort(var1))),
                    if uistate, set(obj.source,'String','[Data]: >> ATENTION! The temporary vector data must be sorted in ascending!'); end
                    disp('ATENTION! The temporary vector data must be sorted in ascending!');
                    obj.state=true; return;
                end
                if not(isfloat(var1)),
                    if uistate, set(obj.source,'String','[Data]: >> ATENTION! The temporary vector data must be a float vector!'); end
                    disp('ATENTION! The temporary vector data must be sorted in ascending!');
                    obj.state=true; return;
                end
                if not(isequal(var1,unique(var1))),
                    if uistate, set(obj.source,'String','[Data]: >> ATENTION! The temporary vector data must be diferents elements!'); end
                    disp('ATENTION! The temporary vector data must be diferents elements!');
                    obj.state=true; return;
                end
                minYear = [1000 6 1 0 0 0]; maxYear = [5000 6 1 0 0 0];
                if min(var1)>=datenum(minYear) && max(var1)<=datenum(maxYear);
                    if uistate, set(obj.source,'String','[Data]: >> Your temporary vector are in matlab time format!'); end
                    disp(['Your temporary vector are in matlab time format between: [' datestr(datenum(minYear)) '-' datestr(datenum(maxYear)) ']']);
                    obj.timestate=true;%true is a matlab datetime
                elseif min(var1)>=0 && max(var1)<=maxYear(1),
                    if uistate, set(obj.source,'String','[Data]: >> Your temporary vector are in yearly scale!'); end
                    disp(['Your temporary vector are in yearly scale between: [0-' num2str(maxYear(1)) ']']);
                    obj.timestate=false;%false-is  a rescale yearly time scale
                else
                    if uistate, set(obj.source,'String','[Data]: >> ATENTION! The temporary vector is not in the proper format!'); end
                    disp('ATENTION! The temporary vector is not in the proper format!');
                    disp(['Use yearly scale between: [0-' num2str(maxYear(1)) '] or']);
                    disp(['matlab time format between: [' datestr(datenum(minYear)) '-' datestr(datenum(maxYear)) ']']);
                    obj.state=true; return;
                end
            end
        end
    end
    methods (Static=true)
        %figure position of diferent windows for ameva
        function [figPos,xPos,btnWid,btnHt,top,spacing]=amevaconst(tipo)
            figPos=[];btnWid=[];btnHt=[];spacing=[];top=[];xPos=[];
            screenSize=get(0,'ScreenSize');
            switch tipo
                case 'mw', %main window
                    figPos=[9 screenSize(4)-590 800 500];
                case 'mlp', %main location point meshselection
                    figPos=[9 screenSize(4)-750 550 550];
                case 'setting', %setting
                    figPos=[570 screenSize(4)-610 250 350];
                    xPos=[0.02 0.45 0.82];
                    btnWid=[0.35 0.15 0.10 0.09 0.16 0.35 0.40];
                    btnHt=0.075;
                    top=0.98;
                    spacing=0.02;
                case 'ds', %data
                    figPos=[570 screenSize(4)-610 300 350];
                    xPos=[0.01 0.37 0.67 0.82 0.92];
                    btnWid=[0.30 0.15 0.10 0.09 0.14 0.27 0.35];
                    btnHt=0.075;
                    top=0.98;
                    spacing=0.02;
                case 'dsc', %data characterized data
                    figPos=[570 screenSize(4)-610 300 450];
                    xPos=[0.01 0.37 0.67 0.82 0.92];
                    btnWid=[0.30 0.15 0.10 0.09 0.14 0.27 0.35];
                    btnHt=0.075;
                    top=0.98;
                    spacing=0.02;
                case 'dda', %ameva-ihdata
                    figPos=[150 screenSize(4)-610 750 450];
                case 'dsa', %setting aeva
                    figPos=[500 screenSize(4)-600 300 360];
                case 'wf', %worldfilter [50 100 1250 800]
                    figPos=[50 100 1250 800];
                otherwise
                    figPos=[9 screenSize(4)-590 800 500];
                    btnWid=0.35;
                    btnHt=0.075;
                    spacing=0.02;
                    top=0.98;
                    xPos=[0.02 0.47 0.84];
            end
        end
        %%Load or not load ameva.gif ico
        function loadamevaico(hf,tipo)
            if not(exist('tipo','var')) || isempty(tipo), tipo=false; end
            if tipo && not(isempty(which('ameva.gif'))),
                % Can't proceed unless we have desktop java support
                if ~usejava('swing')
                    error('stats:dfittool:JavaSwingRequired','requires Java Swing to run.');
                end
                warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
                hfframe=get(hf,'javaframe');
                jIcon=javax.swing.ImageIcon(which('ameva.gif'));
                hfframe.setFigureIcon(jIcon);
            end
        end
        %%Load text idioma
        function txtDict = amevatextoidioma(varargin)
            %Fuciones para regresar solo texto especifico en el idioma que sequiera
            error(nargchk(0,2,nargin));
            if nargin == 0,
                idioma = 'eng';
            elseif nargin == 1,
                idioma = varargin{1};
            elseif nargin == 2,
                idioma = varargin{1};
            end
            funcFile = which('amevatest.m');
            pth = fileparts(funcFile);
            file = fullfile(pth,'texts',idioma,'main.txt');
            fid = fopen(file,'r','n','UTF-8');
            if fid == -1, error('FOPEN cannot open the file'); end
            txt = textscan(fid,'%s %s','Delimiter','=');
            fclose(fid);
            txtDict = containers.Map(txt{1},txt{2});
            
            if nargin == 2% Solo en el caso textType='fecha'
                if strcmp(varargin{2},'fecha')
                    texto = [datestr(date,'mmmm') ' ' num2str(str2num(datestr(date,'dd'))) ', ' datestr(date,'yyyy')];%eng por defecto
                    if strcmp(idioma,'esp')
                        aux = []; eval(['aux=' txtDict('meslong') ';']);
                        texto = [num2str(str2num(datestr(date,'dd'))) ' de '  aux{str2double( datestr(date,'mm') )} ' de ' datestr(date,'yyyy')];
                    end
                    clear txtDict;
                    txtDict = texto;
                end
            end
            
        end
        %Latex text selected model
        function [fmus,fsis] = textmodel(ID)
            fmus='';fsis='';
            if ID==1,
                fmus='$$f_\mu=p_1+p_2H_s^{R}$$';           %Model 1: fmu = p1+p2*x
                fsis='$$f_\sigma=p_3+p_4H_s^{R}$$';        %      fsigma = p3+p4*x
            elseif ID==3,
                fmus='$$f_\mu=p_1\left({H_s^{R}}\right)^{p_2}$$';%Model 3: fmu = p1*x^p2
                fsis='$$f_\sigma=p_3+p_4H_s^{R}$$';              %      fsigma = p3+p4*x
            elseif ID==2,
                fmus='$$f_\mu=p_1+p_2H_s^{R}$$';%Model 2: fmu = p1+p2*x
                fsis='$$f_\sigma=p_3+p_4\sqrt{H_s^{R}}$$';%      fsigma = p3+p4*sqrt(x)
            elseif ID==4,
                fmus='$$f_\mu=p_1\left({H_s^{R}}\right)^{p_2}$$';%Model 4: fmu = p1*x^p2
                fsis='$$f_\sigma=p_3+p_4\sqrt{H_s^{R}}$$';%      fsigma = p3+p4*sqrt(x)
            elseif ID==5,
                fmus='$$f_\mu=p_1\left(H_s^{R}\;\right)^{\;p_2}$$';%Model 5: fmu = p(1)*x.^p(2)
                fsis='$$f_\sigma=p_3\left(1-\exp\left(p_4H_s^{R}\right)\;\right)$$';%      fsigma = p(3)*(1-exp(-p(4)*x));
            elseif ID==6,
                fmus='$$f_\mu=p_1\left(H_s^{R}\;\right)^{\;p_2}$$';%Model 6: fmu = p(1)*x.^p(2)
                fsis='$$f_\sigma=p_3\left(H_s^{R}\;\right)^{\;p_4}$$';%      fsigma = p(3)*x.^p(4);
            end
        end
        %% Lectura de los caudales from xls files
        function [infoCaudal,N] = lecturaCaudales(cuenca,N,pth)
            if not(exist('cuenca','var')) || isempty(cuenca),
                error('Falta nombre de cuenta Ej: [N,infoCaudal] = lecturaCaudales(''miera''); for Qd_miera.xls');
            end
            if not(exist('pth','var')) || isempty(pth), pth=pwd; end
            
            % Time local format
            import java.text.DateFormat;
            import java.text.SimpleDateFormat;
            df = DateFormat.getDateInstance(DateFormat.SHORT);
            dateFormat = char(df.toPattern());
%             if strcmpi(dateFormat(1),'d') && strcmpi(dateFormat(4),'m'),
%                 dateFormat = 'dd/mm/yyyy';
%             elseif strcmpi(dateFormat(1),'m') && strcmpi(dateFormat(4),'d'),
%                 dateFormat = 'mm/dd/yyyy';
%             else
%                 error('No se puede determinar el formatl local de las fechas, use: dd/mm/yyyy o mm/dd/yyyy!');
%             end
            disp(['El formato local de las fechas detectado es: ' dateFormat]);
            
            if  not(exist(fullfile(pth,['Qd_',cuenca,'.xls']),'file')), 
                error(['No existe fichero: ','Qd_',cuenca,'.xls']);
            end
            % aqui leer los sheets y si son PC solo hacer eso con o sin espacios
            if not(exist('N','var')) || isempty(N),
                aux = 0;
                [status,sheets] = xlsfinfo(fullfile(pth,['Qd_',cuenca,'.xls']));
                for i=1:length(sheets),
                    auss=char(sheets(i));
                    if strcmp(auss(1:2),'PC'), aux = aux+1; end
                end
                N = 1:aux;
            end
            infoCaudal = struct('Time',[],'dataq',[],'TimeQo',[],'dataqQo',[], ...
                'Qo_PC',[],'PC',[],'N',N,'cuenca',cuenca);
            for i=N
                % Lectura de datos PC from xls
                [num,tex,raw] = xlsread(fullfile(pth,['Qd_',cuenca,'.xls']),['PC',num2str(i)]);
                raw = raw(:,2:3);
                raw = raw(not(any(cellfun(@isempty,raw),2)),:);
                raw(any(cellfun(@(raw) any(isnan(raw(:))),raw),2),:) = [];
                Time = datenum(raw(:,1),dateFormat);
                dataq = cell2mat(raw(:,2));
                % Lectura de Qo
                TimeQo = []; dataqQo = [];
                [status,sheets] = xlsfinfo(fullfile(pth,['Qd_',cuenca,'.xls']));
                if sum(ismember(sheets,['Qo_','PC',num2str(i)]))>0
                    [num,tex,raw]=xlsread(fullfile(pth,['Qd_',cuenca,'.xls']),['Qo_','PC',num2str(i)]);% Ej: Qo_PC9
                    raw = raw(:,2:3);
                    raw = raw(not(any(cellfun(@isempty,raw),2)),:);
                    raw(any(cellfun(@(raw) any(isnan(raw(:))),raw),2),:)=[];
                    TimeQo = datenum(raw(:,1),dateFormat);
                    dataqQo = cell2mat(raw(:,2));
                    infoCaudal(i).QoPC = ['Qo_','PC',num2str(i)];
                end
                infoCaudal(i).Time = Time;
                infoCaudal(i).dataq = dataq;
                infoCaudal(i).TimeQo = TimeQo;
                infoCaudal(i).dataqQo = dataqQo;
                infoCaudal(i).PC = ['PC',num2str(i)];
            end
            disp(['Se ha leido el fichero: ','Qd_',cuenca,'.xls']);
        end
        % Wind Convert
        function [Wspd,Wdir] = wind_convert(u,v)
            % convert the meridional and the zonal component of the wind to module and
            % direction
            Wdir=(atan2(-u,-v)).*180./pi;Wdir(Wdir<0)=Wdir(Wdir<0)+360;
            Wspd=sqrt(u.^2+v.^2);
            
        end
        %% Pinta punto lat,lon en el mapa con batimetrias
        function pintaLatLonMapa(namebuoy,pathPrint,lat,lon,depth,closetf)
            % pintaLatLonMapa(namebuoy,pathPrint,boya(iBuoy).lat,boya(iBuoy).lon,boya(iBuoy).depth,true)
            f4 = figure('Visible','on');
            axz = axes('Units','normalized','Parent',f4,'Position',[0.10 0.18 0.88 0.78],'Visible','Off');
            hold(axz,'on'); grid(axz,'on'); axis on;
            hil = 'i'; forcing = 'NCEP'; mesh = 'Global';
            Plot1name0 = [namebuoy '_Punto_Mapa'];
            myPathName = fullfile(tempdir,'gshhs_shape_ameva',['gshhs_',lower(hil),'_',forcing,'_',mesh,'.mat']); %medium resolution default
            %Pinto linea de costa
            Aux = load(myPathName);
            myShape = Aux.ShapeM;
            %Pinto Batimetria mallas GOW
            [LAT_bathy,LON_bathy,Depth_bathy] = fichasDG.amevaBathymetry(forcing,mesh);
            colormap(jet(64));
            pcolor(axz,LON_bathy,LAT_bathy,-Depth_bathy); hold on;
            caxis(axz,[-5000 0]);shading(axz,'interp');
            pcaux=colorbar('peer',axz,'location','SouthOutside','position',[0.17 0.07 0.7 0.02]);
            set(pcaux,'fontsize',7,'Color','k');
            geoshow(axz,myShape,'FaceColor',[0.7 0.7 0.7]); clear Aux; hold on;  axis equal;
            box(axz,'on');
            if ~isnan(lon) && ~isnan(lat)
                % factor para corregir deformacion
                zoomfactor = 8.5;
                x = lon; y = lat; xWidth = get(axz,'xlim'); yHeight = get(axz,'ylim');
                xlim(axz,x+diff([0 (xWidth(2)-xWidth(1))])/zoomfactor*[-.5 +.5]);
                ylim(axz,y+diff([0 (yHeight(2)-yHeight(1))])/zoomfactor*[-.5 +.5]);
                marcaColor = [255 0 0]/255; if abs(depth)<1500, marcaColor = [0 0 255]/255; end
                plot(axz,x,y,'o','Color',[0 0 0]/255,'MarkerFaceColor',marcaColor,'MarkerSize',9);
                plot(axz,x,y,'yx','MarkerSize',10);
            else
                disp(['NaN lat,lon - ' namebuoy]);
            end
            texto = amevaclass.amevatextoidioma('esp');
            xlabel(axz, [texto('longitude') ' ' '(',char(176),'E)']);
            ylabel(axz, [texto('latitude'), ' ' '(',char(176),'N)']);
            
            print(f4,'-dpng','-r150', fullfile(pathPrint,[Plot1name0 '.png']));
            if closetf, close(f4); clear axz; end
        end
        %% Pintar series de structura ihdata
        function PlotDataSeries(data,varargin)
            % data=wind.getTemporalSerie(43.5,-3.8,'NCEP','Global','ocean');
            % dateIni=datenum(2000,01,01);dateEnd=datenum(2000,12,31);
            % PlotDataSeries(data,'dateIni',dateIni,'dateEnd',dateEnd,'path',pathGraf,'time','month')
            % 
            vars = fieldnames(data);
            timeres='years';
            dateIni=data.time(1);
            initDate=data.time(1);
            dateEnd=data.time(end);
            endDate=data.time(end);
            path='off';
            d=1;
            S = 'r-';
            for k=1:2:length(varargin)
                argname = varargin{k};
                argvalue = varargin{k+1};
                switch argname
                    case 'vars'
                        vars = argvalue;
                    case 'dateIni'
                        dateIni=argvalue;
                        if (dateIni>initDate), initDate=dateIni;end
                    case 'dateEnd'
                        dateEnd=argvalue;
                        if (dateEnd<endDate), endDate=dateEnd;end
                    case 'time'
                        timeres=argvalue;
                    case 'path'
                        path=argvalue;
                    case 'typePlot'
                        S = argvalue;
                    otherwise %por compatibilidad con las versiones viejas
                        dateIni=argname;
                        dateEnd=argvalue;
                        if (dateIni>initDate), initDate=dateIni;end
                        if (dateEnd<endDate), endDate=dateEnd;end
                end
            end
            if strcmp(timeres,'days') && initDate==data.time(1) && endDate==data.time(end)
                ejex=unique(fix(time));
                timeFormat='dd';
            elseif strcmp(timeres,'days') && (initDate~=data.time(1) || endDate~=data.time(end))
                ejex=DayAxis(initDate,endDate,d);
                timeFormat='dd';
            elseif  strcmp(timeres,'hours')
                ejex=HourAxis(initDate,endDate,d);
                timeFormat='HH';
            elseif  strcmp(timeres,'month')
                ejex=MonthAxis(initDate,endDate,d);
                timeFormat='mm';
            else strcmp(timeres,'years')
                VecTime=datevec(data.time);
                VecTime=unique(VecTime(:,1));
                VecTime=[VecTime;VecTime(end)+1];
                VecTime=[VecTime,ones(size(VecTime,1),2)];
                ejex=datenum(VecTime);
                timeFormat='yyyy';
            end
            FolderOut=datestr(datenum(clock),'yyyy_mm_dd_HHMMSS');
            for i=1:length(vars)
                if ~ischar(data.(vars{i})) && length(data.(vars{i}))==length(data.time) && ~strcmp(vars{i},'time')
                    Fig=figure;
                    plot(data.time,data.(vars{i}),S)
                    xlim([ejex(1),ejex(end)])
                    set(gca,'XTick',ejex)
                    datetick('x',timeFormat,'keepticks')
                    xlabel(timeres)
                    ylabel(vars{i})
                    title(vars{i})
                    if ~strcmp(path,'off')
                        pathLocalGraphics = fullfile(path,FolderOut);
                        if not(isdir(pathLocalGraphics))
                            mkdir(pathLocalGraphics)
                        end
                        print(Fig,'-dpng','-r300',fullfile(path,FolderOut,[vars{i} '.png']));
                        saveas(Fig,fullfile(path,FolderOut,[vars{i} '.fig']));
                    end
                end
            end
        end
        
        %% Funciona para las tools de ameva_free
        function AMEVAscriptversions = ihameva_checkUpdates
            try
                fid_myversions=fopen(fullfile(regexprep(which('ameva.m'),'ameva.m',''),'README.txt'));
                myversionsinfo = textscan(fid_myversions,'%s');
                fclose(fid_myversions);
                
                %ameva,aeva,disfitanalysis,gev,pot,calibration,heteroscedasticmodel,amevadataselection
                scriptnames={myversionsinfo{1}{1} myversionsinfo{1}{143:2:155}};
                scriptversionsO={myversionsinfo{1}{2} myversionsinfo{1}{144:2:156}}; scriptversionsO=char(scriptversionsO);
                slvO=uint8([str2num(scriptversionsO(:,2)) str2num(scriptversionsO(:,4)) str2num(scriptversionsO(:,6))]);
                
                [READMEtxt,status]=urlread('https://bitbucket.org/ouc/ihameva_free/raw/master/README.txt');clear aux;
                
                if status
                    lastversionsinfo = textscan(READMEtxt,'%s');
                    scriptversionsN = char({lastversionsinfo{1}{2} lastversionsinfo{1}{144:2:156}});
                    slvN = uint8([str2num(scriptversionsN(:,2)) str2num(scriptversionsN(:,4)) str2num(scriptversionsN(:,6))]);
                    CheckTF = CheckFunctionVersion(scriptnames,scriptversionsN,scriptversionsO,slvN,slvO);
                    AMEVAscriptversions = scriptversionsO(1,:);%Devuelve la version solo de AMEVA
                    if CheckTF == 0,
                        disp([scriptnames{1},' ' AMEVAscriptversions ' is updated']);
                    end
                    return;
                end
                AMEVAscriptversions='unknown version';
                disp('Warning: ihameva_checkUpdates unable to successfully check for updates AMEVA functions');
                %% OJO OJO hacer para ameva_clima  falta introducir passw y añadir versiones en leeme.txt
                %[READMEtxt,status]=urlread('https://climaih@bitbucket.org/ouc/ihameva_clima/raw/master/README.txt','Password','climaclima')
                %exist(which('fichaclass'),'file')==2
                %fid_myversions=fopen(fullfile(regexprep(which('amevainfo.m'),'amevainfo.m',''),'leeme.txt'))
                
            catch E1
                AMEVAscriptversions='unknown version';
                disp('Warning: ihameva_checkUpdates unable to successfully check for updates AMEVA functions');
            end
            function CheckTF = CheckFunctionVersion(scriptnames,scriptversionsN,scriptversionsO,slvN,slvO)
                n=length(scriptnames);
                CheckTF = 0;
                for i = 1:n,
                    if not(isequal(slvN(i,:),slvO(i,:))),
                        CheckTF = CheckTF+1;
                        disp(['Warning:[Update AMEVA] You are using an outdated version of ',scriptnames{i},' ' scriptversionsO(i,:)]);
                        if isequal(slvN(i,1:2),slvO(i,1:2)) && slvN(i,3)>slvO(i,3),
                            disp([' It is ADVISABLE to download the latest version: ',scriptnames{i},' ' scriptversionsN(i,:)]);
                        elseif isequal(slvN(i,1),slvO(i,1)) && (slvN(i,2)>slvO(i,2) || slvN(i,1)>slvO(i,1)),
                            disp([' It is NECESSARY to download the latest version: ',scriptnames{i},' ' scriptversionsN(i,:)]);
                        end
                    end
                end
            end
        end
        %% Funciona para las tools de ameva_free
        function AMEVAscriptversions = ihclima_checkUpdates
            try
                fid_myversions=fopen(fullfile(regexprep(which('amevaclima.mat'),'amevaclima.mat',''),'README.txt'));
                myversionsinfo = textscan(fid_myversions,'%s');
                fclose(fid_myversions);
                
                %ameva,aeva,disfitanalysis,gev,pot,calibration,heteroscedasticmodel,amevadataselection
                scriptnames={myversionsinfo{1}{1} myversionsinfo{1}{64:2:74}};
                scriptversionsO={myversionsinfo{1}{2} myversionsinfo{1}{65:2:75}}; scriptversionsO=char(scriptversionsO);
                slvO=uint8([str2num(scriptversionsO(:,2)) str2num(scriptversionsO(:,4)) str2num(scriptversionsO(:,6))]);
                
                [READMEtxt,status]=urlread('https://bitbucket.org/ouc/ihameva_free/raw/master/README.txt');clear aux;
                !wget --http-user=climaih --http-password=Clima2121: -q -O - https://bitbucket.org/ouc/ihameva_clima/raw/master/README.txt  | egrep -m1 -o 'v[0-9](\.[0-9]+)+'
                % OJO OJO hacer para ameva_clima  falta introducir passw y añadir versiones en leeme.txt
                %[READMEtxt,status]=urlread('https://climaih@bitbucket.org/ouc/ihameva_clima/raw/master/README.txt','Password','climaclima')
                %exist(which('fichaclass'),'file')==2
                %fid_myversions=fopen(fullfile(regexprep(which('amevainfo.m'),'amevainfo.m',''),'leeme.txt'))
                
                if status
                    lastversionsinfo = textscan(READMEtxt,'%s');
                    scriptversionsN = char({lastversionsinfo{1}{2} lastversionsinfo{1}{144:2:156}});
                    slvN = uint8([str2num(scriptversionsN(:,2)) str2num(scriptversionsN(:,4)) str2num(scriptversionsN(:,6))]);
                    CheckTF = CheckFunctionVersion(scriptnames,scriptversionsN,scriptversionsO,slvN,slvO);
                    AMEVAscriptversions = scriptversionsO(1,:);%Devuelve la version solo de AMEVA
                    if CheckTF == 0,
                        disp([scriptnames{1},' ' AMEVAscriptversions ' is updated']);
                    end
                    return;
                end
                AMEVAscriptversions='unknown version';
                disp('Warning: ihameva_checkUpdates unable to successfully check for updates AMEVA functions');
                
            catch E1
                AMEVAscriptversions='unknown version';
                disp('Warning: ihameva_checkUpdates unable to successfully check for updates AMEVA functions');
            end
            function CheckTF = CheckFunctionVersion(scriptnames,scriptversionsN,scriptversionsO,slvN,slvO)
                n=length(scriptnames);
                CheckTF = 0;
                for i = 1:n,
                    if not(isequal(slvN(i,:),slvO(i,:))),
                        CheckTF = CheckTF+1;
                        disp(['Warning:[Update AMEVA] You are using an outdated version of ',scriptnames{i},' ' scriptversionsO(i,:)]);
                        disp('Importante, eliminar y volver a cargar la carpeta ameva_free al path de MATLAB!');
                        if isequal(slvN(i,1:2),slvO(i,1:2)) && slvN(i,3)>slvO(i,3),
                            disp([' It is ADVISABLE to download the latest version: ',scriptnames{i},' ' scriptversionsN(i,:)]);
                        elseif isequal(slvN(i,1),slvO(i,1)) && (slvN(i,2)>slvO(i,2) || slvN(i,1)>slvO(i,1)),
                            disp([' It is NECESSARY to download the latest version: ',scriptnames{i},' ' scriptversionsN(i,:)]);
                        end
                    end
                end
            end
            % -------------------------------------------------------------------------
%             persistent Timeout_error
%             Timeout=2;%seconds
%             t0_urlread=now;
%             if isempty(Timeout_error)
%                 try
%                     url=sprintf('https://bitbucket.org/ouc/ihameva_free/raw/master/README.txt');
%                     urlread(url);
%                 catch E2
%                     disp('Warning: urlread timeout error')
%                 end
%             end
%             if 24*3600*(now-t0_urlread)>Timeout
%                 Timeout_error=true;
%             end
        end
    end
end
