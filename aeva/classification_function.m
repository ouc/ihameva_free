function [final,bmus,acierto_cien]=classification_function(datos,direccional,t,classTipo,Opciones)
% funcion de calssificacion: classTipo 'SOM' 'MDA' 'KMA'
% datos.- Matrix of data  - Classification
% direccional.- Positon of directional vector in the matrix -datos-, if not exist set empty
% classTipo.-'SOM', 'MDA', o 'KMA'
% t.- size of claster en el caso SOM (t(1)*t(2))
% Opciones = ameva_options(svopt,carpeta,idioma,var_name,var_unit);
% Opciones = ameva_options([0 1 0],pwd,'esp','Hs','m');
% Examples 1
%   load ameva;
%   Opciones = ameva_options([1 1 0],pwd,'esp',{'Hs','Hr','Dir'},{'m','m',char(186)});
%   classification_function([excal_HsI excal_HsR excal_Dire],3,[7 7],'SOM',Opciones);
% Examples 2
%   Opciones = ameva_options([0 1 0],'sample_clssification','esp');
%   [final,bmus,acierto_cien]=classification_function([excal_HsI excal_HsR],[],[7 7],'SOM');
% Examples 3
%   [final,bmus,acierto_cien]=classification_function(exgev_Modos,[],[7 7],'SOM');
%
% See also:
%   castello@unican.es

if nargin<4, 
    error(['faltan parametros: classification_function(datos,direccional,t,classTipo,Opciones)' ...
           'help classification_function']); 
end
if not((strcmp(classTipo,'SOM') || strcmp(classTipo,'MDA') || strcmp(classTipo,'KMA'))), error('Seleccione correctamente el tipo de classificacion: SOM MDA O KMA'); end
if ( strcmp(classTipo,'MDA') || strcmp(classTipo,'KMA') ) && length(t) ~=1, error('En el caso KMA y MDA ej, t=9 -escalar de  un entero-'); end
if strcmp(classTipo,'SOM') && length(t)~=2, error('En el caso KMA y MDA ej, t=9 -escalar de  un entero-'); end
if length(t)==2 && (t(1)/floor(t(1))~=1 || t(2)/floor(t(2))~=1), error('En el caso SOM ej, t=[7 6] -vector de dos enteros-'); end
if length(t)==1 && t/floor(t)~=1, error('En el caso KMA y MDA ej, t=9 -escalar de  un entero-'); end
if ~exist('Opciones','var') || isempty(Opciones), Opciones=ameva_options; end
if isempty(direccional),
    escalar = (1:min(size(datos)));
else
    escalar = (1:min(size(datos)));
    escalar(direccional) = [];
end

N = length(datos);

if strcmp(classTipo,'SOM') && N>10001, classTipo = [classTipo '_PreMDA']; end
if strcmp(classTipo,'KMA') && N>10001, classTipo = [classTipo '_PreMDA']; end

switch classTipo
    case 'SOM_PreMDA',   
        [final,bmus,acierto_cien] = SOM_3D_PreMDA_function(datos,escalar,direccional,t,classTipo,Opciones);
    case 'SOM',          
        [final,bmus,acierto_cien] = SOM_3D_function       (datos,escalar,direccional,t,classTipo,Opciones);
    case 'MDA',          
        [final,bmus,acierto_cien] = MDA_3D_function       (datos,escalar,direccional,t,classTipo,Opciones);
    case 'KMA_PreMDA',   
        [final,bmus,acierto_cien] = KMA_3D_PreMDA_function(datos,escalar,direccional,t,classTipo,Opciones);
    case 'KMA',          
        [final,bmus,acierto_cien] = KMA_3D_function       (datos,escalar,direccional,t,classTipo,Opciones);
end
