function durationtime(varargin)
%   DURATIONTIME. Hours duration time vector for this threshold and condition
%   DURATIONTIME (durationtime.m) opens a graphical user interface for displaying the main program posibility.
%   Tested on Ubuntu trusty v.14.04.1(developed), Windows 7 and Mac Os X v10.9.4 (MATLAB:R2013a)
%   To load test data, please click on: -Help, Load ameva test data-, to send
%   data to matlab worksapce and work with this.
%
% Examples 1
%   Tools-->Statistics-->Durations Algorithms
% Examples 2
%   load ameva;
%   durationtime(data_gow.time,data_gow.hs);
% Examples 3
%   load ameva;
%   durationtime(data_dow_sdr1.time,data_dow_sdr1.hs,data_dow_sdr1.tp);
% Examples 4
%   load ameva;
%   Time = data_gow.time;	%Time
%   Hs= data_gow.hs;        %Variable X
%   Tm= data_gow.tm;        %Variable Y
%   Dir= data_gow.dir;      %Variable Dir
%   timedefine= true;       %Time exist
%   carpeta='sample_dir';	%Directory name-relative to work directory
%   idioma='eng';           %idioma-no implementado
%   var_name={'Hs' 'Tm' 'Dir'};     %Variable name
%   var_unit={'m' 's' 'deg.'};      %Variable unit
%   durationtime(Time,Hs,Tm,Dir,timedefine,carpeta,idioma,var_name,var_unit);
% See also:
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   durationtime(main program tool v1.2.1 (140820)).
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows 7
%   03-07-2011 - The first distribution version
%   22-04-2013 - Old distribution version
%   07-05-2013 - Old distribution version
%   19-07-2013 - Old distribution version
%   20-08-2014 - Last distribution version

versionumber='v1.2.1 (140820)';
nmfctn='durationtime';
Ax1Pos=[0.095 0.130 0.725 0.815 0.095 0.130 0.725 0.815 0.095 0.130 0.725 0.815;...
    0.095 0.583 0.725 0.341 0.095 0.110 0.725 0.341 0.095 0.110 0.725 0.341;...
    0.095 0.709 0.725 0.216 0.095 0.409 0.725 0.216 0.095 0.110 0.725 0.216];%Posiciones plot
figPos=[];

if ~isempty(findobj('Tag',['Tag_' nmfctn]));
    figPos=get(findobj('Tag',['Tag_' nmfctn]),'Position');
    close(findobj('Tag',['Tag_' nmfctn]));
end

%Lista de las figuras que se van a usar en este programa
hf=[];%ventana principal
hg=[];%ventana auxiliar-data
hi=[];%ventana auxiliar-setting
global hsp line_x line_y line_d;
% Information for all buttons and Spacing between the button
colorG=[0.94 0.94 0.94];	%Color general
btnWid=0.16;
btnHt=0.05;
spacing=0.01;
top=0.94;
xPos=[1-btnWid-spacing 1-btnWid-spacing];
var_lab={'Time:' '* X-Data:' 'Y-Data:' 'Dir'};
%GLOBAL VARIALBES solo usar las necesarias!!
awinc=amevaclass;%ameva windows commons methods
awinc.nmfctn=nmfctn;
xmin=0;xmax=0;qxmin=0;qxmax=1;%x minimo, maximo, quantil inferior y superior
ymin=0;ymax=0;qymin=0;qymax=1;%y minimo, maximo, quantil inferior y superior
sini=0;sfin=360;
carpeta=[];
Time=[];X=[];Y=[];Dir=[];
LabX='';LabY='';LabDir='';
ydefine=false;timedefine=false;
xdefine=false;dirdefine=false;
N=[];
Nmin=1;
idioma='eng'; usertype=1;
if length(varargin)==9,
    Time=varargin{1};
    X=varargin{2};
    Y=varargin{3};
    Dir=varargin{4};
    timedefine=varargin{5};
    carpeta=varargin{6};
    idioma=varargin{7};
    var_name=varargin{8};
    var_unit=varargin{9};
    texto=amevaclass.amevatextoidioma(idioma);
elseif nargin==4,
    Time=varargin{1};
    X=varargin{2};
    Y=varargin{3};
    Dir=varargin{4};
    timedefine=true;
    texto=amevaclass.amevatextoidioma(idioma);
elseif nargin==3,
    Time=varargin{1};
    X=varargin{2};
    Y=varargin{3};
    timedefine=true;
    texto=amevaclass.amevatextoidioma(idioma);
elseif nargin==2,
    Time=varargin{1};
    X=varargin{2};
    timedefine=true;
    texto=amevaclass.amevatextoidioma(idioma);
elseif nargin==1, %Los datos se seleccionan desde el espacio de trabajo
    idioma=varargin{1}.idioma;
    usertype=varargin{1}.usertype;
    texto=amevaclass.amevatextoidioma(idioma);
    disp(texto('dataClasif'));
elseif nargin==0, %Los datos se seleccionan desde el espacio de trabajo
    texto=amevaclass.amevatextoidioma(idioma);
    disp(texto('dataClasif'));
else
    texto=amevaclass.amevatextoidioma(idioma);
    error(texto('helpDuration'));
end
namesoftware=[texto('nameDuration') ' ',versionumber];
%compruebo
if ~exist('var_name','var') || isempty(var_name), var_name={'Time','Hs','Tm','Dir'}; end %Nombre de 3 variables por defecto sino existen 
if ~exist('var_unit','var') || isempty(var_unit), var_unit={'','m','s',char(176)}; end %Unidades por defecto sino existen 
if ~exist('svopt','var') || isempty(svopt), svopt=[1 1 0]; end %save option 1-fig 2-png 3-eps (1/0)
if ~isempty(find(isnan(Time),1)), disp(texto('nanTime')); end
if ~isempty(find(isnan(X),1)), disp(texto('nanX')); end
if ~isempty(find(isnan(Y),1)), disp(texto('nanY')); end
if ~isempty(find(isnan(Dir),1)), disp(texto('nanDir')); end

%---LA FIGURA-VENTANA PRINCIPAL-
hf=figure('Name',namesoftware,'Color',colorG,'CloseRequestFcn',@AmevaClose, ...
    'NumberTitle','Off','DockControls','Off','Tag',['Tag_' nmfctn],'NextPlot','new');
if ~isempty(figPos), set(hf,'Position',figPos); end
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

btnN=0;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_data=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'String','Data', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataOO('on'));
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_xtxt=uicontrol('Style','text','Units','normalized','Parent',hf,'String',LabX, ...
    'FontWeight','Bold','Position',[xPos(1) yPos btnWid btnHt]);

ui_xma_=uicontrol('Style','text','Units','normalized','Parent',hf,'String','Xmax QXmax', ...
    'Position',[xPos(1) yPos-0.02 btnWid btnHt*.75]);
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_xmi_=uicontrol('Style','text','Units','normalized','Parent',hf,'String','Xmin QXmin', ...
    'Position',[xPos(1) yPos-0.035 btnWid btnHt*.75]);
ui_xmax=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(xmax),'UserData',xmax, ...
    'Position',[xPos(1) yPos btnWid/2 btnHt],'Callback',@(source,event)ActXY('Xmax',source,event));
ui_qxmax=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(qxmax),'UserData',qxmax, ...
    'Position',[xPos(1)+btnWid/2 yPos btnWid/2 btnHt],'Callback',@(source,event)ActQxy('Xmax',source,event));
btnN=btnN+1.35;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_xmin=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(xmin),'UserData',xmin, ...
    'Position',[xPos(1) yPos btnWid/2 btnHt],'Callback',@(source,event)ActXY('Xmin',source,event));
ui_qxmin=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(qxmin),'UserData',qxmin, ...
    'Position',[xPos(1)+btnWid/2 yPos btnWid/2 btnHt],'Callback',@(source,event)ActQxy('Xmin',source,event));


btnN=btnN+1.75;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ytxt=uicontrol('Style','text','Units','normalized','Parent',hf,'String',LabY, ...
    'FontWeight','Bold','Position',[xPos(1) yPos btnWid btnHt]);

ui_yma_=uicontrol('Style','text','Units','normalized','Parent',hf,'String','Ymax QYmax', ...
    'Position',[xPos(1) yPos-0.02 btnWid btnHt*.75]);
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ymi_=uicontrol('Style','text','Units','normalized','Parent',hf,'String','Ymin QYmin', ...
    'Position',[xPos(1) yPos-0.04 btnWid btnHt*.75]);
ui_ymax=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(ymax),'UserData',ymax, ...
    'Position',[xPos(1) yPos btnWid/2 btnHt],'Callback',@(source,event)ActXY('Ymax',source,event));
ui_qymax=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(qymax),'UserData',qymax, ...
    'Position',[xPos(1)+btnWid/2 yPos btnWid/2 btnHt],'Callback',@(source,event)ActQxy('Ymax',source,event));
btnN=btnN+1.35;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ymin=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(ymin),'UserData',ymin, ...
    'Position',[xPos(1) yPos btnWid/2 btnHt],'Callback',@(source,event)ActXY('Ymin',source,event));
ui_qymin=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(qymin),'UserData',qymin, ...
    'Position',[xPos(1)+btnWid/2 yPos btnWid/2 btnHt],'Callback',@(source,event)ActQxy('Ymin',source,event));


btnN=btnN+1.75;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_stxt=uicontrol('Style','text','Units','normalized','Parent',hf,'String',LabDir, ...
    'FontWeight','Bold','Position',[xPos(1) yPos btnWid btnHt]);

ui_sfi_=uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('max'), ...
    'Position',[xPos(1) yPos-0.02 btnWid btnHt*.75]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_sin_=uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('min'), ...
    'Position',[xPos(1) yPos-0.04 btnWid btnHt*.75]);
ui_sfin=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(sfin),'UserData',sfin, ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)ActXY('Sfin',source,event));
btnN=btnN+1.35;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_sini=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(sini),'UserData',sini, ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)ActXY('Sini',source,event));

btnN=btnN+1.25;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_avm=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'String','Setting', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataO1('on'),'Enable','Off');
btnN=btnN+1.0; 
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_start=uicontrol('Style','push','Units','normalized','Parent',hf,'String','Start', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@DurationRun);
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_reset=uicontrol('Style','push','Units','normalized','Parent',hf,'String','Reset', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@ActDefOption);

ActPlots;%Actualiza los plot
ActButton;%Actualiza los botones
ActDefOption;%Actualiza defaul options

uicontrol('Style','frame','Units','normalized','Parent',hf,'Position',[0 0 1 0.04]);
uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.59 0.004 0.18 0.03],'String',texto('saveFigure'));
ui_ckeps=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.77 0.004 btnWid/2 0.03],'String','*.eps','Value',svopt(3));
ui_ckfig=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.85 0.004 btnWid/2 0.03],'String','*.fig','Value',svopt(1));
ui_ckpng=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.92 0.004 btnWid/2 0.03],'String','*.png','Value',svopt(2),'Callback',@(source,event)EnableButton([ui_data,ui_start,ui_reset],'Enable','on'));
SetSaveOpt;
ui_tb   =uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.01 0.004 0.58 0.03],...
    'String',[nmfctn,texto('dataDuration')],'HorizontalAlignment','left');

%% Data
[figPos,xPos,btnWid,btnHt,top,spacing]=awinc.amevaconst('ds');
hg=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Data'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataOO('off'),'WindowButtonMotionFcn',@ListBoxCallback1,...
    'NumberTitle','Off','MenuBar','none','Position',figPos,'Resize','Off','Visible','Off','NextPlot','new');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
amvcnf.idioma=idioma;amvcnf.usertype=usertype;

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(end) btnHt],...
    'String','Data & WorkSpace','Callback',@(source,event)amevaworkspace(amvcnf));
for ii=1:length(var_lab)
    btnN=btnN+1;
    yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
    ui_td(ii)=uicontrol('Style','text','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(6) btnHt],'String',var_lab(ii),'BackgroundColor',colorG,'HorizontalAlignment','right');
    ui_all(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid(1) btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',ii);
    ui_cm(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid(5) btnHt],'String','(none)','visible','off');
    ui_vn(ii)=uicontrol('Style','edit','Units','normalized','Parent',hg,'String',var_name(ii),'BackgroundColor','white','Position',[xPos(4) yPos btnWid(3) btnHt]);
    ui_vu(ii)=uicontrol('Style','edit','Units','normalized','Parent',hg,'String',var_unit(ii),'BackgroundColor','white','Position',[xPos(5) yPos btnWid(3) btnHt]);
end; clear ii;

btnN=btnN+2;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Set Data','Position',[xPos(2) yPos btnWid(1) btnHt],'Callback',@AmevaSetData);

uicontrol('Style','frame','Units','normalized','Parent',hg,'Position',[0 0 1 0.05]);
ui_tbd=uicontrol('Style','Text','Units','normalized','Parent',hg,'String',texto('dataWorkspace'),'Position',[0.01 0.004 0.99 0.04],'HorizontalAlignment','left');

if ~strcmp(get(hg,'NextPlot'),'new'), set(hg,'NextPlot','new'); end;

%% Setting
[figPos,xPos,btnWid,btnHt,top,spacing]=awinc.amevaconst('ds');
hi=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Setting'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataO1('off'),'WindowButtonMotionFcn',@ListBoxCallback1,...
    'NumberTitle','Off','MenuBar','none','Position',figPos,'Resize','Off','Visible','Off','NextPlot','new');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hi,'String','Settings','Position',[xPos(1)+btnWid(1) /2 yPos+btnHt btnWid(1) btnHt],'FontWeight','bold','BackgroundColor',colorG);
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
srN=uicontrol('Style','radiobutton','Units','normalized','Parent',hi, 'Callback',@UpdateStruct, ...
   'String','n bins (empty*)','Value',1,'Position',[xPos(1) yPos btnWid(1)*1.5 btnHt]);
srW=uicontrol('Style','radiobutton','Units','normalized','Parent',hi, 'Callback',@UpdateStruct, ...
   'String','bins size (hours)','Value',0,'Position',[xPos(2)+btnWid(1)*.5 yPos btnWid(1)*1.5 btnHt]);
UpdateStruct(srN);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_nbin=uicontrol('Style','edit','Units','normalized','Parent',hi,'String','','Position',[xPos(1) yPos btnWid(1) btnHt]);
ui_wbin=uicontrol('Style','edit','Units','normalized','Parent',hi,'String','','Position',[xPos(2)+btnWid(1)*.5 yPos btnWid(1) btnHt]);

btnN=btnN+2;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hi,'String','Close','Position',[xPos(2) yPos btnWid(1) btnHt],'Callback',@(source,event)FigDataO1('off'));

uicontrol('Style','frame','Units','normalized','Parent',hi,'Position',[0 0 1 0.05]);
uicontrol('Style','Text','Units','normalized','Parent',hi,'String',texto('settingDuration'),'Position',[0.01 0.004 0.99 0.04],'HorizontalAlignment','left');

if ~strcmp(get(hi,'NextPlot'),'new'), set(hi,'NextPlot','new'); end;

%% C0 Open,Close figure data
    function FigDataOO(state,source,event)
        set(hg,'Visible',state);
    end
%% C0 Open,Close figure Settings
    function FigDataO1(state,source,event)
        set(hi,'Visible',state);
    end
%% C0 Actulizo los botones Enable on/off & The figure open/close visible on/off
    function EnableButton(listbutton,statetyp,statebutton)
        set(listbutton,statetyp,statebutton);
    end
%% C0
    function ListBoxCallback1(source,event) % Load workspace vars into list box
        awinc.ListBoxCallback(ui_all,true);
    end
%% C0
    function SetSaveOpt
        if isdeployed,
            set(ui_ckfig,'Value',1,'Enable','On','Visible','On');
            set(ui_ckeps,'Value',0,'Enable','On','Visible','On');
        end
    end
%% C0
    function AmevaClose(source,event) % Close all Ameva windows
        awinc.AmevaClose([hf,hg,hi], ...
            {[upper(nmfctn(1)),nmfctn(2:end),'Data'],[upper(nmfctn(1)),nmfctn(2:end),'Setting']});
    end
%% C1 Update cell and matrix name
    function UpdateNameAll(source,event)
        nval=get(source,'UserData');
        awinc.AmevaUpdateName(nval,ui_cm,ui_all,ui_td)
    end
%% Update time format
    function UpdateTimeFormat(source,event)
        if timedefine,
            EnableButton([ui_wbin,srW,srN],'Enable','On')
            set(ui_wbin,'String','1');
        else
            UpdateStruct(srN);
            set(ui_wbin,'String','');
            EnableButton([ui_wbin,srW,srN],'Enable','Off')
        end
        set(ui_nbin,'String','');
    end
%% update entre struct y vectores
    function UpdateStruct(source,event)
        aux=get(source,'String');
        if strcmpi(aux(1),'n'),
            if get(source,'value'),  set(srW,'Value',0);
            else set(srW,'Value',1); end
        else
            if get(source,'value'),  set(srN,'Value',0);
            else set(srN,'Value',1); end
        end
    end
%% plots
    function ActPlots(source,event)
        %PLOT
        if length(Time)>1, xData=linspace(Time(1),Time(end),12); end; %tick of Time data
        LabX=labelsameva(var_name(2),var_unit(2));
        LabY=labelsameva(var_name(3),var_unit(3));
        LabDir=labelsameva(var_name(4),var_unit(4));
        dirtick=0:90:360;%ax1 tick
        N=length(X);
        nsp=0;
        if length(X)>1, nsp=nsp+1; end
        if length(Y)>1, nsp=nsp+1; end
        if length(Dir)>1, nsp=nsp+1; end
        for j=1:nsp,
            hsp(j)=subplot(nsp,1,j,'Parent',hf);
            set(hsp(j),'position',Ax1Pos(nsp,4*(j-1)+1:4*(j-1)+4))
        end
        xdefine=false;
        ydefine=false;
        dirdefine=false;
        
        if nsp==1,
            xdefine=true; xmin=min(X); xmax=max(X); qxmin=0; qxmax=1;
            plot(hsp(1),Time,X,'k');ylabel(hsp(1),LabX);
            line_x(1)=line([Time(1) Time(end)],[xmin xmin],'Color','b','Marker','.','LineStyle','-','Parent',hsp(1));
            line_x(2)=line([Time(1) Time(end)],[xmax xmax],'Color','r','Marker','.','LineStyle','-','Parent',hsp(1));
            line_x(3)=line([Time(1) Time(1)],[xmax xmax],'Color','b','LineStyle','-','Parent',hsp(1));
            line_x(4)=line([Time(1) Time(1)],[xmax xmax],'Color','r','LineStyle','-','Parent',hsp(1));
            set(hsp(1),'XGrid','on','YGrid','on');%,'XLim',[Time(1) Time(end)]
            if timedefine,
                set(hsp(1),'XTick',xData); datetick(hsp(1),'x','mmmyy','keepticks');
            end
            xlabel(hsp(1),var_name(1));
        elseif nsp==2,
            xdefine=true; xmin=min(X); xmax=max(X); qxmin=0; qxmax=1;
            plot(hsp(1),Time,X,'k');ylabel(hsp(1),LabX);
            line_x(1)=line([Time(1) Time(end)],[xmin xmin],'Color','b','Marker','.','LineStyle','-','Parent',hsp(1));
            line_x(2)=line([Time(1) Time(end)],[xmax xmax],'Color','r','Marker','.','LineStyle','-','Parent',hsp(1));
            line_x(3)=line([Time(1) Time(1)],[xmax xmax],'Color','b','LineStyle','-','Parent',hsp(1));
            line_x(4)=line([Time(1) Time(1)],[xmax xmax],'Color','r','LineStyle','-','Parent',hsp(1));
            set(hsp(1),'XGrid','on','YGrid','on','XLim',[Time(1) Time(end)])
            if timedefine,
                set(hsp(1),'XTick',xData); datetick(hsp(1),'x','mmmyy','keepticks');
            end
            if length(Y)>1,
                ydefine=true; ymin=min(Y); ymax=max(Y); qymin=0; qymax=1;
                plot(hsp(2),Time,Y,'k');  ylabel(hsp(2),LabY);
                line_y(1)=line([Time(1) Time(end)],[ymin ymin],'Color','b','Marker','.','LineStyle','-','Parent',hsp(2));
                line_y(2)=line([Time(1) Time(end)],[ymax ymax],'Color','r','Marker','.','LineStyle','-','Parent',hsp(2));
                line_y(3)=line([Time(1) Time(1)],[ymax ymax],'Color','b','LineStyle','-','Parent',hsp(2));
                line_y(4)=line([Time(1) Time(1)],[ymax ymax],'Color','r','LineStyle','-','Parent',hsp(2));
                set(hsp(2),'XGrid','on','YGrid','on','XLim',[Time(1) Time(end)])
                if timedefine,
                    set(hsp(2),'XTick',xData); datetick(hsp(2),'x','mmmyy','keepticks');
                end
                xlabel(hsp(2),var_name(1));
            end
            if length(Dir)>1,
                dirdefine=true;
                plot(hsp(2),Time,Dir,'k.','MarkerSize',4); ylabel(hsp(2),LabDir);
                line_d(1)=line([Time(1) Time(end)],[sini sini],'Color','b','Marker','.','LineStyle','-','Parent',hsp(2));
                line_d(2)=line([Time(1) Time(end)],[sfin sfin],'Color','r','Marker','.','LineStyle','-','Parent',hsp(2));
                line_d(3)=line([Time(1) Time(1)],[sfin sfin],'Color','b','LineStyle','-','Parent',hsp(2));
                line_d(4)=line([Time(1) Time(1)],[sfin sfin],'Color','r','LineStyle','-','Parent',hsp(2));
                set(hsp(2),'XGrid','on','YGrid','on','YTick',dirtick,'XLim',[Time(1) Time(end)],'Ylim',[0 360])
                if timedefine,
                    set(hsp(2),'XTick',xData); datetick(hsp(2),'x','mmmyy','keepticks');
                end
                xlabel(hsp(2),var_name(1));
            end
        elseif nsp==3,
            xdefine=true; xmin=min(X); xmax=max(X); qxmin=0; qxmax=1;
            plot(hsp(1),Time,X,'k');ylabel(hsp(1),LabX);
            line_x(1)=line([Time(1) Time(end)],[xmin xmin],'Color','b','Marker','.','LineStyle','-','Parent',hsp(1));
            line_x(2)=line([Time(1) Time(end)],[xmax xmax],'Color','r','Marker','.','LineStyle','-','Parent',hsp(1));
            line_x(3)=line([Time(1) Time(1)],[xmax xmax],'Color','b','LineStyle','-','Parent',hsp(1));
            line_x(4)=line([Time(1) Time(1)],[xmax xmax],'Color','r','LineStyle','-','Parent',hsp(1));
            set(hsp(1),'XGrid','on','YGrid','on','XLim',[Time(1) Time(end)])
            if timedefine,
                set(hsp(1),'XTick',xData); datetick(hsp(1),'x','mmmyy','keepticks');
            end
            ydefine=true; ymin=min(Y); ymax=max(Y); qymin=0; qymax=1;
            plot(hsp(2),Time,Y,'k'); ylabel(hsp(2),LabY);
            line_y(1)=line([Time(1) Time(end)],[ymin ymin],'Color','b','Marker','.','LineStyle','-','Parent',hsp(2));
            line_y(2)=line([Time(1) Time(end)],[ymax ymax],'Color','r','Marker','.','LineStyle','-','Parent',hsp(2));
            line_y(3)=line([Time(1) Time(1)],[ymax ymax],'Color','b','LineStyle','-','Parent',hsp(2));
            line_y(4)=line([Time(1) Time(1)],[ymax ymax],'Color','r','LineStyle','-','Parent',hsp(2));
            set(hsp(2),'XGrid','on','YGrid','on','XLim',[Time(1) Time(end)])
            if timedefine,
                set(hsp(2),'XTick',xData); datetick(hsp(2),'x','mmmyy','keepticks');
            end
            dirdefine=true;
            plot(hsp(3),Time,Dir,'k.','MarkerSize',4); ylabel(hsp(3),LabDir);
            line_d(1)=line([Time(1) Time(end)],[sini sini],'Color','b','Marker','.','LineStyle','-','Parent',hsp(3));
            line_d(2)=line([Time(1) Time(end)],[sfin sfin],'Color','r','Marker','.','LineStyle','-','Parent',hsp(3));
            line_d(3)=line([Time(1) Time(1)],[sfin sfin],'Color','b','LineStyle','-','Parent',hsp(3));
            line_d(4)=line([Time(1) Time(1)],[sfin sfin],'Color','r','LineStyle','-','Parent',hsp(3));
            set(hsp(3),'XGrid','on','YGrid','on','YTick',dirtick,'XLim',[Time(1) Time(end)],'Ylim',[0 360])
            if timedefine,
                set(hsp(3),'XTick',xData); datetick(hsp(3),'x','mmmyy','keepticks');
            end
            xlabel(hsp(3),var_name(1));
        end
    end

%% Reset default options
    function ActDefOption(source,event)
        xmin=min(X);xmax=max(X);
        ymin=min(Y);ymax=max(Y);
        qxmin=0;qxmax=1;
        qymin=0;qymax=1;
        sini=0;sfin=360;
        if xdefine,
            set(ui_xmin,'String',num2str(xmin),'UserData',xmin);
            set(ui_qxmin,'String',num2str(qxmin),'UserData',qxmin);
            set(ui_xmax,'String',num2str(xmax),'UserData',xmax);
            set(ui_qxmax,'String',num2str(qxmax),'UserData',qxmax);
            set(line_x(1),'YData',[xmin xmin]);
            set(line_x(2),'YData',[xmax xmax]);
        end
        if ydefine,
            set(ui_ymin,'String',num2str(ymin),'UserData',ymin);
            set(ui_qymin,'String',num2str(qymin),'UserData',qymin);
            set(ui_ymax,'String',num2str(ymax),'UserData',ymax);
            set(ui_qymax,'String',num2str(qymax),'UserData',qymax);
            set(line_y(1),'YData',[ymin ymin]);
            set(line_y(2),'YData',[ymax ymax]);
        end
        if dirdefine,
            set(ui_sini,'String',num2str(sini),'UserData',sini);
            set(ui_sfin,'String',num2str(sfin),'UserData',sfin);
            set(line_d(1),'YData',[sini sini]);
            set(line_d(2),'YData',[sfin sfin]);
            set(ui_sini,'UserData',sini);
        end
    end
%% acutaliza X Y Dir (MAX MIN)
    function ActXY(strxy,source,event)
        if strcmp(strxy(1),'X'),
            xmin=str2double(get(ui_xmin,'String'));
            xmax=str2double(get(ui_xmax,'String'));
        elseif strcmp(strxy(1),'Y'),
            ymin=str2double(get(ui_ymin,'String'));
            ymax=str2double(get(ui_ymax,'String'));
        elseif strcmp(strxy(1),'S')
            sini=str2double(get(ui_sini,'String'));
            sfin=str2double(get(ui_sfin,'String'));
        end
        if N<1,
            disp([texto('notData_1'),num2str(N)]); return;
        else
            [aax,lagsx]=ecdf(X);
            if ~isempty(Y), [aay,lagsy]=ecdf(Y); end
        end
        qxmin=interp1q(lagsx,aax,xmin);
        qxmax=interp1q(lagsx,aax,xmax);
        if ydefine,
            qymin=interp1q(lagsy,aay,ymin);
            qymax=interp1q(lagsy,aay,ymax);
        end
        if strcmp(strxy,'Xmin'),
            set(ui_xmin,'UserData',xmin);
            set(ui_qxmin,'UserData',qxmin,'String',num2str(qxmin));
            set(line_x(1),'YData',[xmin xmin]);
        elseif strcmp(strxy,'Xmax'),
            set(ui_xmax,'UserData',xmax);
            set(ui_qxmax,'UserData',qxmax,'String',num2str(qxmax));
            set(line_x(2),'YData',[xmax xmax]);
        elseif strcmp(strxy,'Ymin'),
            set(ui_ymin,'UserData',ymin);
            set(ui_qymin,'UserData',qymin,'String',num2str(qymin));
            set(line_y(1),'YData',[ymin ymin]);
        elseif strcmp(strxy,'Ymax'),
            set(ui_ymax,'UserData',ymax);
            set(ui_qymax,'UserData',qymax,'String',num2str(qymax));
            set(line_y(2),'YData',[ymax ymax]);
        elseif strcmp(strxy,'Sini'),
            set(ui_sini,'UserData',sini);
            set(line_d(1),'YData',[sini sini]);
        elseif strcmp(strxy,'Sfin'),
            set(ui_sini,'UserData',sini);
            set(line_d(2),'YData',[sfin sfin]);
        end
        EnableButton([ui_reset,ui_start,ui_avm],'Enable','On');
    end
%% acutaliza Qx Qy (MAX MIN) -->X Y introducido manualmente
function ActQxy(strxy,source,event)
    qxmin=str2double(get(ui_qxmin,'String'));
    qxmax=str2double(get(ui_qxmax,'String'));
    qymin=str2double(get(ui_qymin,'String'));
    qymax=str2double(get(ui_qymax,'String'));
    if N<Nmin, 
        disp([texto('notData_1'),num2str(N)]); return;
    else
        xmin=quantile(X,qxmin);
        xmax=quantile(X,qxmax);
        if ydefine,
            ymin=quantile(Y,qymin);
            ymax=quantile(Y,qymax);
        end
    end
	set(ui_qxmin,'UserData',qxmin);
	set(ui_qxmax,'UserData',qxmax);
	set(ui_qymin,'UserData',qymin);
	set(ui_qymax,'UserData',qymax);
	set(ui_xmin,'UserData',xmin,'String',num2str(xmin));
	set(ui_xmax,'UserData',xmax,'String',num2str(xmax));
	set(ui_ymin,'UserData',ymin,'String',num2str(ymin));
	set(ui_ymax,'UserData',ymax,'String',num2str(ymax));
    set(line_x(1),'YData',[xmin xmin]);
    set(line_x(2),'YData',[xmax xmax]);
    if ydefine, 
        set(line_y(1),'YData',[ymin ymin]); 
        set(line_y(2),'YData',[ymax ymax]); 
    end
    if dirdefine, 
        set(line_d(1),'YData',[sini sini]); 
        set(line_d(2),'YData',[sfin sfin]); 
    end
    EnableButton([ui_reset,ui_start,ui_avm],'Enable','On');
end
%% Actualiza los botones de la ventana principal
    function ActButton
        if xdefine
            EnableButton([ui_xtxt,ui_xmin,ui_xmi_,ui_xmax,ui_xma_,ui_qxmin,ui_qxmax],'Visible','On');
            EnableButton([ui_reset,ui_start],'Enable','On');
        else
            EnableButton([ui_xtxt,ui_xmin,ui_xmi_,ui_xmax,ui_xma_,ui_qxmin,ui_qxmax],'Visible','Off');
            EnableButton([ui_reset,ui_start],'Enable','Off');
        end
        if ydefine,
            EnableButton([ui_ytxt,ui_ymin,ui_ymi_,ui_ymax,ui_yma_,ui_qymin,ui_qymax],'Visible','On');
        else
            EnableButton([ui_ytxt,ui_ymin,ui_ymi_,ui_ymax,ui_yma_,ui_qymin,ui_qymax],'Visible','Off');
        end
        if dirdefine
            EnableButton([ui_stxt,ui_sfin,ui_sfi_,ui_sini,ui_sin_],'Visible','On');
        else
            EnableButton([ui_stxt,ui_sfin,ui_sfi_,ui_sini,ui_sin_],'Visible','Off');
        end
    end
%% P0 Set data comprobamos que los Data para calcular la calibaracion son
    function AmevaSetData(source,event)
        var_name=cell(1,1);var_unit=cell(1,1);
        
        for i=1:4
            var_name(i)=get(ui_vn(i),'String');
            var_unit(i)=get(ui_vu(i),'String');
        end
        ione=get(ui_all(1),'Value');
        itwo=get(ui_all(2),'Value');
        ithr=get(ui_all(3),'Value');
        ifou=get(ui_all(4),'Value');
        
        if itwo>1,
            awinc.AmevaEvalData(2,ui_cm,ui_all);
            X=awinc.data;
            xdefine=true;
        else
            xdefine=false;
            error(texto('xData'));
        end
        set(hg,'Visible','off');set(ui_tbd,'String',texto('applyData'));drawnow;
        if ione>1,
            awinc.AmevaEvalData(1,ui_cm,ui_all);
            Time=awinc.data;
            timedefine=true;
            awinc.AmevaTimeCheck(ui_tbd,Time);
            if awinc.state==true,
                disp(texto('dataFormat_1'));
                set(hg,'Visible','on'); return;
            end
        else
            Time=(1:length(X))';
            timedefine=false;
        end
        if ithr>1,
            awinc.AmevaEvalData(3,ui_cm,ui_all);
            Y=awinc.data;
            ydefine=true;
        else
            Y=[];
            ydefine=false;
        end
        if ifou>1,
            awinc.AmevaEvalData(4,ui_cm,ui_all);
            Dir=awinc.data;
            dirdefine=true;
        else
            Dir=[];
            dirdefine=false;
        end
        %compruebo que las tres variables sean diferentes
        awinc.AmevaVarCheck(ui_tbd,X,Time,'X-Data','Time');      if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,Y,Time,'Y-Data','Time');      if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,Y,X,'Y-Data','X-Data');       if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,Dir,Time,'Dir','Time');       if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,Dir,X,'Dir','X-Data');        if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,Dir,Y,'Dir','Y-Data');        if awinc.state, set(hg,'Visible','on'); return; end,
        %set new data
        set(ui_tbd,'String',texto('timeOptions'));drawnow ;
        ActPlots;
        ActButton;
        ActDefOption;
        UpdateTimeFormat;
        EnableButton([ui_reset,ui_start,ui_avm],'Enable','On');
    end

%% Run data
    function DurationRun(source,event)
        %Creo el directorio de trabajo para almacenamiento
        awinc.NewFolderCheck(carpeta,nmfctn);carpeta=awinc.carpeta;% Methods for "amevaclass"
        set(ui_tb,'String',[nmfctn,texto('run')]);drawnow ;
        %N-size of data vector
        %n-size of duration vector
        %Duration vector of 1
        try
            EnableButton([ui_reset,ui_start,ui_avm],'Enable','Off');
            set(hg,'Visible','off');
            set(hi,'Visible','off');
            set(ui_tb,'String',[nmfctn,texto('run')]);drawnow;
            
            aux_=( X >= xmin & X <= xmax );
            xpos_up=find( X > xmax );
            xpos_mi=find( aux_== 1 );
            xpos_lo=find( X < xmin );
            if sum(aux_)<1
                disp([texto('xMin'),num2str(xmin),texto('xMax'),num2str(xmax)]);
                return;
            end
            duration_v=aux_;
            
            set(line_x(4),'YData',X(duration_v),'XData',Time(duration_v),'LineStyle','none','Marker','^','MarkerSize',1);
            set(line_x(3),'YData',X(aux_),'XData',Time(aux_),'LineStyle','none','Marker','^','MarkerSize',2);
            if ydefine,
                aux_=( Y >= ymin & Y <= ymax );
                ypos_up=find( Y > ymax );
                ypos_mi=find( aux_== 1 );
                ypos_lo=find( Y < ymin );
                if sum(aux_)<1
                    disp([texto('yMin'),num2str(ymin),texto('yMax'),num2str(ymax)]);
                    return;
                end
                duration_v=duration_v & aux_;
                set(line_x(4),'YData',X(duration_v),'XData',Time(duration_v),'LineStyle','none','Marker','^','MarkerSize',1);
                set(line_y(3),'YData',Y(aux_),'XData',Time(aux_),'LineStyle','none','Marker','^','MarkerSize',2);
                set(line_y(4),'YData',Y(duration_v),'XData',Time(duration_v),'LineStyle','none','Marker','^','MarkerSize',1);
            end
            if dirdefine,
                if (sini<sfin),
                    aux_=(sini<=Dir & Dir<=sfin);
                    dpos_up=find( Dir > sfin );
                    dpos_mi=find( aux_== 1 );
                    dpos_lo=find( Dir < sini );
                    if sum(aux_)<1
                        disp([texto('sIni'),num2str(sini),texto('sFin'),num2str(sfin)]);
                        return;
                    end
                else
                    aux_=(sini<=Dir | Dir<=sfin);
                    if sum(aux_)<1
                        disp([texto('sIni')',num2str(sini),texto('sFin'),num2str(sfin)]);
                        return;
                    end
                end
                duration_v=duration_v & aux_;
                set(line_x(4),'YData',X(duration_v),'XData',Time(duration_v),'LineStyle','none','Marker','^','MarkerSize',1);
                set(line_d(3),'YData',Dir(aux_),'XData',Time(aux_),'LineStyle','none','Marker','^','MarkerSize',2);
                set(line_d(4),'YData',Dir(duration_v),'XData',Time(duration_v),'LineStyle','none','Marker','^','MarkerSize',1);
            end
            
            %duraton vector
            dvec=duration_t(duration_v,true,X);
            %Formta del vector de persistencias/duraciones si existe vector de tiempo
            if timedefine,
                duration_vector=etime(datevec(Time(dvec(:,3))),datevec(Time(dvec(:,2))))/3600; %(duration en horas)
                if get(srW,'Value')
                    dt=str2num(get(ui_wbin,'String'));%leo el numero de horas
                    var_unit(1)={['Every ' get(ui_wbin,'String') ' hours']};
                    duration_vector=duration_vector/dt;
                    ndx=ceil(min(duration_vector))+1/2:floor(max(duration_vector))-1/2;
                else
                    var_unit(1)=get(ui_vu(1),'String');
                    ndx=str2num(get(ui_nbin,'String'));%leo el numero de bins
                    duration_vector=dvec(:,1);
                end
            else
                var_unit(1)=get(ui_vu(1),'String');
                ndx=str2num(get(ui_nbin,'String'));%leo el numero de bins
                duration_vector=dvec(:,1);
            end
            
            if isempty(duration_vector), 
                msgbox(texto('dataChange'));
                EnableButton([ui_reset,ui_start,ui_avm],'Enable','On'); return; 
            end

            if ~exist('ndx','var') || isempty(ndx) || sum(ndx)==0 % utilizo nbins automatico
                ndx=round(3/2+log(length(duration_vector))/log(2))*2;
            end
            
            %Elimino los cero de duration_vector OJO OJO
            duration_vector(duration_vector==0) = [];
            
            if length(duration_vector)<Nmin,
                msgbox(['only ',num2str(length(duration_vector)),texto('dataChange')]);
                EnableButton([ui_reset,ui_start,ui_avm],'Enable','On'); return; 
            end
            
            [yout,xx]=hist_n(duration_vector,ndx);%pdf histogram duration
            
            %save options
            svopt(1)=get(ui_ckfig,'Value');%fig
            svopt(2)=get(ui_ckpng,'Value');%png
            svopt(3)=get(ui_ckeps,'Value');%eps
            if sum(svopt)>0,  %save duration vector
                xyd_pos=find( duration_v== 1 );%posiciones de las intercepciones conjuntas
                save(fullfile(carpeta,'duration_data.mat'),'duration_vector','dvec','xyd_pos','xpos_up','xpos_mi','xpos_lo','yout','xx');
                if ydefine, save(fullfile(carpeta,'duration_data.mat'),'ypos_up','ypos_mi','ypos_lo','-append'); end
                if dirdefine, save(fullfile(carpeta,'duration_data.mat'),'dpos_up','dpos_mi','dpos_lo','-append'); end
            end %data .mat
            
            h=figure;   % save plot pdf
            bar(xx,yout,1.0,'b');
            grid on;
            title([texto('title_5') ' ',num2str(length(xx))],'FontWeight','bold');
            ylabel(texto('yLabel_5'));
            xlabel(labelsameva(var_name(1),var_unit(1)));%plot fin
            amevasavefigure(h,carpeta,svopt,['duration_histogram_',var_name{2}]);
            h=figure('Visible','off');
            cpaxis=findall(hf,'Type','axes');
            for j=1:length(cpaxis)
                copyobj(cpaxis(length(cpaxis)+1-j),h);
            end
            amevasavefigure(h,carpeta,svopt,['duration_',var_name{2}]);
            % send to ws. save var to workspace
            assignin('base',[var_name{2},'_d_vector'],duration_vector);
            assignin('base',[var_name{2},'_hist_xx'],xx);
            assignin('base',[var_name{2},'_hist_yout'],yout);
            disp([datestr(now,'yyyymmddHHMMSS') texto('dataSituation')]);
            amevaws_update_listbox_ext;
        catch ME
            EnableButton([ui_reset,ui_start,ui_avm],'Enable','On');
            rethrow(ME)
        end
        set(ui_tb,'String',[nmfctn, texto('readyStart')]);drawnow ;
        set(ui_tb,'String',[nmfctn, texto('start')]);drawnow ;
        EnableButton([ui_reset,ui_start,ui_avm],'Enable','On');     
    end

%% Help
    function DurationHelp(source,event)
        ttlStr=texto('durationHelp');
        hlpStr= ...
            {texto('descriDuraHelp_0') ' '
            ' '
            texto('descriDuraHelp_1') ' '
            texto('descriDuraHelp_2') ' '
            texto('descriDuraHelp_3') ' '
            texto('descriDuraHelp_4') ' '
            '                                                '};
        helpwin(hlpStr,ttlStr);
    end
end
