function [Tnew,Xnew,AnualTimeMax,txk,indices]=maximostempfunction(Time,X,tipo,maxtype,porcentage,idtest,ndaysi,dayhouri,nDays,p,umbral)
% Examples 1
%   load ameva;
%   Time=data_gow.time;
%   X=data_gow.hs;
%   tipo='annual'; %(annual, monthly, days)
%   maxtype='maximum'; %(threshold, maximum, mean, quantile)
%   porcentage=80; % (0-100 %)
%   idtest=1; %(0,1 -> usar o no test de independencias)
%   ndaysi=3; %(numero de dias/horas de independecias de los maximos)
%   dayhouri='day'; %tipo de intervalos de independencia: day o hour
%   nDays=7; %(intervalo de dias para buscar maximos)
%   p=0.95; %En caso de selecionar quantile 0-1
%   umbral=mean(X); %El umbral en caso de selecinar por umbral
%   [Tnew,Xnew]=maximostempfunction(Time,X,tipo,maxtype,porcentage,idtest,ndaysi,dayhouri,nDays,p,umbral)
% Examples 2
%   load ameva;
%   [Tnew,Xnew]=maximostempfunction(data_gow.time,data_gow.hs,'monthly','maximum',80,1,3,'day');
%   [Tnew,Xnew,AnualTimeMax]=maximostempfunction(data_gow.time,data_gow.hs,'days','maximum',80,1,24,'hour',7);
%   [Tnew,Xnew]=maximostempfunction(data_gow.time,data_gow.hs,[],'threshold',[],[],[],[],[],[],4);
%   [Tnew,Xnew]=maximostempfunction(data_gow.time,data_gow.hs,[],'pot',[],1,3,'day',[],[],4);
%   %AnualTimeMax-vector temporal para usar en los progrmas de roberto pot, gev. Year normalize time.

if ~exist('idtest','var') || isempty(idtest), idtest=0; end
if ~exist('nDays','var') || isempty(nDays), nDays=1; end % Por defecto 1 dia - maximo diario
if ~exist('p','var') || isempty(p), p=0.95; end
if ~exist('umbral','var') || isempty(umbral), umbral=[]; end
    
    %Ecuentro del paso de tiempo mas frecuente y numero de datos minimos sino es threshold
    nmindat=1;
    if   ~strcmp(maxtype,'threshold') && ~strcmp(maxtype,'pot')
        ptimeu=unique(abs(diff(Time)));
        ptimeu=ptimeu(find(ptimeu));
        if strcmp(tipo,'annual'),                 %1-annual maximum
            nmindat=round(365*1/ptimeu(1)*porcentage/100);
        elseif strcmp(tipo,'monthly'),              %2-monthly maximum
            nmindat=round(28*1/ptimeu(1)*porcentage/100);
        elseif strcmp(tipo,'days'),              %3-N days maximum
            nmindat=round(nDays*1/ptimeu(1)*porcentage/100);
        elseif strcmp(tipo,'N_data'),              %4-N data
            nmindat=1;
        else
            error('No exist this case of maximum. Please Select: annual, monthly, days o threshold'); 
        end
        if not(isempty(nmindat)) && nmindat>length(Time) && porcentage>0, 
            msgbox({'Problemas con la discretizacion de vector temporal.' 'Selecciones min porcentage of data = 0'});
            error('Problemas con la discretizacion de vector temporal. Selecciones min porcentage of data = 0'); 
        end
    end
    TimeVec=datevec(Time);
    tini=TimeVec(1,1);
    tfin=TimeVec(end,1);
    Xnew=[];
    Tnew=[];txk=[];AnualTimeMax=[];
    indices=cell(12,1);%indice de cada mes
    
    if  idtest && ~strcmp(maxtype,'threshold')
        if strcmp(tipo,'days') && strcmp(dayhouri,'day') && ndaysi>=round(nDays/2), msgbox('Select number of separate days less than the number of days/2'); return;
        elseif strcmp(tipo,'monthly') && strcmp(dayhouri,'day') && ndaysi>=30/2, msgbox('Select number of separate days less than the 15 days'); return;
        elseif strcmp(tipo,'annual') && strcmp(dayhouri,'day') && ndaysi>=364/2, msgbox('Select number of separate days less than the 182 days'); return;
        end
    end
    
    if strcmp(maxtype,'threshold'),              %4-Threshold maximum
        if isempty(umbral), error('Missing umbral!. (umbral)'); end
        idx=X>=umbral;
        if ~isempty(idx)
            Xnew=X(idx);
            Tnew=Time(idx);
        end
    elseif strcmp(maxtype,'pot'),              % peak over threshold
        if isempty(umbral), error('Missing umbral!. (umbral POT)'); end
        if not(idtest), ndaysi=0; end
        [Xnew,Tnew]=SeleccionaMaximosIndepPOT([Time TimeVec(:,1) TimeVec(:,2) TimeVec(:,3) TimeVec(:,4) X],umbral,ndaysi);
        Tnew=Tnew(:,1);% En la primera posicion esta el timpo en juliano
    elseif strcmp(tipo,'annual'),                 %1-annual maximum
        k=1;
        for i=tini:tfin,
            indice=find(TimeVec(:,1)==i);
            indicD=find(TimeVec(:,1)==i+1);
            indicI=find(TimeVec(:,1)==i-1);
            indice1 = []; indice2 = []; indice3 = []; indice4 = []; indice5 = []; indice8 = []; indice9 = [];
            if idtest
                indice1=find(Time(indice)>=datenum(i,1,1,0,0,0)                              & Time(indice)<=addtodate(datenum(i,1,0,0,0,0),ndaysi,dayhouri));
                indice2=find(Time(indice)>addtodate(datenum(i,1,0,0,0,0),ndaysi,dayhouri)    & Time(indice)<=addtodate(datenum(i+1,1,1,0,0,0),-ndaysi,dayhouri));
                indice3=find(Time(indice)>addtodate(datenum(i+1,1,1,0,0,0),-ndaysi,dayhouri) & Time(indice)<datenum(i+1,1,1,0,0,0));
                indice4=find(Time(indicD)>=datenum(i+1,1,1,0,0,0)                            & Time(indicD)<=addtodate(datenum(i+1,1,0,0,0,0),ndaysi,dayhouri));
                indice5=find(Time(indicD)>addtodate(datenum(i+1,1,0,0,0,0),ndaysi,dayhouri)  & Time(indicD)<=addtodate(datenum(i+2,1,1,0,0,0),-ndaysi,dayhouri));
                indice8=find(Time(indicI)>addtodate(datenum(i-1,1,0,0,0,0),ndaysi,dayhouri)  & Time(indicI)<=addtodate(datenum(i,1,1,0,0,0),-ndaysi,dayhouri));
                indice9=find(Time(indicI)>addtodate(datenum(i,1,1,0,0,0),-ndaysi,dayhouri)   & Time(indicI)<datenum(i,1,1,0,0,0));
            end
            if ~isempty(indice) && length(indice)>=nmindat,
                switch maxtype
                    case 'maximum'
                    [xmax_,tmax_,tx_]=func_itmax(X(indice),Time(indice),X(indicD),X(indicI),indice1,indice2,indice3,indice4,indice5,indice8,indice9,indice);
                    xmax(k)=xmax_;
                    idxk(k)=tmax_;
                    if ~isempty(tx_), 
                        txk(end+1,:)=tx_; 
                    end;
                    case 'mean'
                    xmax(k)=mean(X(indice));
                    idxk(k)=mean(Time(indice));
                    case 'quantile'
                    xmax(k)=quantile(X(indice),p);
                    idxk(k)=mean(Time(indice));
                    otherwise
                    error('No exist this case of maximum. Please Select: maximum, mean o quantile');
                end
                k=k+1;
            end
            
        end
        if exist('idxk','var')
            Xnew=xmax;
            Tnew=idxk;
        end
    elseif strcmp(tipo,'monthly'),              %1-monthly maximum
        k=1;
        for i=tini:tfin,
            for j=1:12
                indice=find(Time>=datenum(i,j,1,0,0,0) & Time<addtodate(datenum(i,j,1,0,0,0),1,'month'));%mes
                indicD=find(Time>=addtodate(datenum(i,j,1,0,0,0),1,'month') & Time<addtodate(datenum(i,j,1,0,0,0),2,'month'));%mes posterior
                indicI=find(Time>addtodate(datenum(i,j,1,0,0,0),-1,'month') & Time<=datenum(i,j,1,0,0,0));%mes anterior
                indice1 = []; indice2 = []; indice3 = []; indice4 = []; indice5 = []; indice8 = []; indice9 = [];
                if idtest
                    indice1=find(Time(indice)>=datenum(i,j,1,0,0,0)                              & Time(indice)<=addtodate(datenum(i,j,0,0,0,0),ndaysi,dayhouri));
                    indice2=find(Time(indice)>addtodate(datenum(i,j,0,0,0,0),ndaysi,dayhouri)    & Time(indice)<=addtodate(datenum(i,j+1,1,0,0,0),-ndaysi,dayhouri));
                    indice3=find(Time(indice)>addtodate(datenum(i,j+1,1,0,0,0),-ndaysi,dayhouri) & Time(indice)<datenum(i,j+1,1,0,0,0));
                    indice4=find(Time(indicD)>=datenum(i,j+1,1,0,0,0)                            & Time(indicD)<=addtodate(datenum(i,j+1,0,0,0,0),ndaysi,dayhouri));
                    indice5=find(Time(indicD)>addtodate(datenum(i,j+1,0,0,0,0),ndaysi,dayhouri)  & Time(indicD)<=addtodate(datenum(i,j+2,1,0,0,0),-ndaysi,dayhouri));
                    indice8=find(Time(indicI)>addtodate(datenum(i,j-1,0,0,0,0),ndaysi,dayhouri)  & Time(indicI)<=addtodate(datenum(i,j,1,0,0,0),-ndaysi,dayhouri));
                    indice9=find(Time(indicI)>addtodate(datenum(i,j,1,0,0,0),-ndaysi,dayhouri)   & Time(indicI)<datenum(i,j,1,0,0,0));
                end
                if ~isempty(indice) && length(indice)>=nmindat,
                    switch maxtype
                        case 'maximum'
                        [xmax_,tmax_,tx_,index_]=func_itmax(X(indice),Time(indice),X(indicD),X(indicI),indice1,indice2,indice3,indice4,indice5,indice8,indice9,indice);
                        xmax(k)=xmax_;
                        idxk(k)=tmax_;
                        if ~isempty(tx_), 
                            txk(end+1,:)=tx_; 
                        end;
                        indices{j}=[indices{j} index_]; clear xmax_ tmax_ tx_ index_;
                        case 'mean'
                        xmax(k)=mean(X(indice));
                        idxk(k)=mean(Time(indice));
                        case 'quantile'
                        xmax(k)=quantile(X(indice),p);
                        idxk(k)=mean(Time(indice));
                        otherwise
                        error('No exist this case of maximum. Please Select: maximum, mean o quantile');
                    end
                    k=k+1;
                end
            end
        end
        if exist('idxk','var')
            Xnew=xmax;
            Tnew=idxk;
        end
    elseif strcmp(tipo,'days'),              %1-N days maximum
        if isempty(nDays) || nDays<0, error('Missing or empty nDays (n days maximum)'); end
        k=1;
        aux=Time(1);
        while aux<= Time(end),
            indice=find(Time>=aux                         & Time<addtodate(aux,nDays,'day'));
            indicD=find(Time>=addtodate(aux,nDays,'day') & Time<addtodate(aux,2*nDays,'day'));
            indicI=find(Time<aux                          & Time>addtodate(aux,-nDays,'day'));
            indice1 = []; indice2 = []; indice3 = []; indice4 = []; indice5 = []; indice8 = []; indice9 = [];
            if idtest
                indice1=find(Time(indice)>=aux                                                     & Time(indice)<=addtodate(aux,ndaysi,dayhouri));
                indice2=find(Time(indice)>=addtodate(aux,ndaysi,dayhouri)                          & Time(indice)<=addtodate(addtodate(aux,nDays,'day'),-ndaysi,dayhouri));
                indice3=find(Time(indice)>addtodate(addtodate(aux,nDays,'day'),-ndaysi,dayhouri)  & Time(indice)<addtodate(aux,nDays,'day'));
                indice4=find(Time(indicD)>=addtodate(aux,nDays,'day')                             & Time(indicD)<addtodate(addtodate(aux,nDays,'day'),ndaysi,dayhouri));
                indice5=find(Time(indicD)>=addtodate(addtodate(aux,nDays,'day'),ndaysi,dayhouri)  & Time(indicD)<addtodate(addtodate(aux,2*nDays,'day'),-ndaysi,dayhouri));
                indice8=find(Time(indicI)>=addtodate(addtodate(aux,-nDays,'day'),ndaysi,dayhouri) & Time(indicI)<addtodate(aux,-ndaysi,dayhouri));
                indice9=find(Time(indicI)>=addtodate(aux,-ndaysi,dayhouri)                         & Time(indicI)<aux);
            end
            if ~isempty(indice) && length(indice)>=nmindat,
                switch maxtype
                    case 'maximum'
                    [xmax_,tmax_,tx_]=func_itmax(X(indice),Time(indice),X(indicD),X(indicI),indice1,indice2,indice3,indice4,indice5,indice8,indice9,indice);
                    xmax(k)=xmax_;
                    idxk(k)=tmax_;
                    if ~isempty(tx_), 
                        txk(end+1,:)=tx_; 
                    end;
                    case 'mean'
                    xmax(k)=mean(X(indice));
                    idxk(k)=mean(Time(indice));
                    case 'quantile'
                    xmax(k)=quantile(X(indice),p);
                    idxk(k)=mean(Time(indice));
                    otherwise
                    error('No exist this case of maximum. Please Select: maximum, mean o quantile');
                end
                k=k+1;
            end
            aux=addtodate(aux,nDays,'day');
        end
        if exist('idxk','var')
            Xnew=xmax;
            Tnew=idxk;
        end
    elseif strcmp(tipo,'N_data'),              %1-N data
        if isempty(nDays) || nDays<=1 || nDays>length(Time), error('Missing, empty or nDays > length(data) or nDays <=1'); end
        indice=1:nDays;k=1;
        while indice(end)<= length(Time),
            switch maxtype
                case 'maximum'
                    [xmax(k),posi]=max(X(indice));
                    idxk(k)=Time(indice(posi));
                    
                case 'mean'
                    xmax(k)=mean(X(indice));
                    idxk(k)=mean(Time(indice));
                case 'quantile'
                    xmax(k)=quantile(X(indice),p);
                    idxk(k)=mean(Time(indice));
                otherwise
                    error('No exist this case of maximum. Please Select: maximum, mean o quantile');
            end
            indice=indice+nDays;k=k+1;
        end
        if exist('idxk','var')
            Xnew=xmax;
            Tnew=idxk;
        end
    end
    if ~isempty(Tnew)
        AnualTimeMax=yearlytimescale(Tnew,'A');%maximum annual time scale normalizado
        Tnew=Tnew(:);Xnew=Xnew(:);AnualTimeMax=AnualTimeMax(:);
    end
    if ~isempty(txk) && not(isequal(Tnew,txk(:,2))),
        disp(['found ',num2str(length(setdiff(Tnew,txk(:,2)))),' elements for ',dayhouri,' = ' num2str(ndaysi)]);
    end
% test de independencia
function [ymax,tmax,tx,ind]=func_itmax(xnew,tnew,newD,newI,i1,i2,i3,i4,i5,i8,i9,indice)
    ymax=[];
    tmax=[];tx=[];ind=[];
%     if ~isempty(find(tnew([i1',i2',i3'])==7.272005000000000e+005))
%         disp('este');
%     end
    if ~isempty(xnew) && idtest
        if max(xnew(i2))>=max(xnew(i1)) %2>=1
            if max(xnew(i2))>=max(xnew(i3)); %2>=3
                [ymax,index]=max(xnew(i2)); %max(-2-)
                tmax=tnew(i2(index));ind=indice(i2(index));
            else %<--10
                if isempty(i4)  || (~isempty(i5) && max(newD(i5))>=max(newD(i4)) ) %5>=4
                    [ymax,index]=max(xnew(i3));  %max(-3-)
                    tmax=tnew(i3(index));ind=indice(i3(index));
                else
                    if max(xnew(i3))>=max(newD(i4)) %3>=4
                        [ymax,index]=max(xnew(i3));  %max(-3-)
                        tmax=tnew(i3(index));ind=indice(i3(index));
                    else
                        if  max(newD(i5))>=max(newD(i4)) %5>=4
                            [ymax,index]=max(xnew(i3));  %max(-3-)
                            tmax=tnew(i3(index));ind=indice(i3(index));
                        else
                            [ymax,index]=max(xnew(i2)); %max(-2-)
                            tmax=tnew(i2(index));ind=indice(i2(index));
                        end
                    end
                end
            end
        else
            if max(xnew(i1))>=max(xnew(i3)) %1>=3
                if isempty(i9) || max(newI(i9))>=max(xnew(i1)) %9>=1
                    if  isempty(i9) || isempty(i8) || max(newI(i8))>=max(newI(i9)) %8>=9
                        [ymax,index]=max(xnew(i1)); %max(-1-)
                        tmax=tnew(i1(index));ind=indice(i1(index));
                    else
                        if max(xnew(i3))>=max(newD(i4)) %3>4
                            if max(xnew(i3))>max(xnew(i2)) %3>2
                                [ymax,index]=max(xnew(i3)); %max(-3-)
                                tmax=tnew(i3(index));ind=indice(i3(index));
                            else
                                [ymax,index]=max(xnew(i2)); %max(-2-)
                                tmax=tnew(i2(index));ind=indice(i2(index));
                            end
                        else
                            [ymax,index]=max(xnew(i2)); %max(-2-)
                            tmax=tnew(i2(index));ind=indice(i2(index));
                        end
                    end
                else
                    [ymax,index]=max(xnew(i1)); %max(-1-)
                    tmax=tnew(i1(index));ind=indice(i1(index));
                end
            else
                %10-->
            end
        end
    end
    if isempty(ymax) && ~isempty(xnew)
        [ymax,index]=max(xnew);
        tmax=tnew(index);ind=indice(index);
    end
    if idtest
        %tx contine el maximo para comprobar el resultado de test de
        %independencia. NO es necesario sacarlo
        [ymax_,index]=max(xnew);
        tx(1,1)=tmax;
        tx(1,2)=tnew(index);
        tx(1,3)=xnew(index);
    end
    
end

end
