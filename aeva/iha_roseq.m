function varargout = iha_roseq(D,F,varargin)
%iha_roseq   directional wind rose
% 
%   Syntax:
%      [HANDLES,DATA] = iha_roseq(D,F,varangin)
%
%   Inputs:
%      D   Directions
%      F   Intensities
%      VARARGIN:
%       -dtype, type of input directions D, standard or meteo,
%            if meteo, the conversion dnew=mod(-90-D,360) is done;
%            if not meteo, standard is used (default)
%       -n, number of D subdivitions. number of sector divitions
%       -di, percentiles subdivitions (ordenados de menor a mayor)
%       -ci, percentage circles to draw, default is automatic
%       -labtitle, main title
%       -lablegend, legend title
%       -cmap, colormap [jet]
%       -colors, to use instead of colormap, for each di
%       -quad, Quadrant to show percentages [1]
%       -legtype, legend type: 1, continuous, 2, separated boxes [2]
%       -bcolor, full rectangle border color ['none']
%       -lcolor, line colors for axes and circles ['k']
%       -percbg, percentage labels bg ['w']
%       -ax, to place wind rose on pervious axes, the input for ax
%            must be [theax x y width], where theax is the previous
%            axes, x and y are the location and width is the wind
%            rose width relative to theax width (default=1/5)
%       -parent, by default a new axes is created unless parent is
%                given, ex, parent may be a subplot
%       -iflip, flip the intensities as they go outward radially, ie,
%                highest values are placed nearest the origin [{0} 1]
%
%   Output:
%      HANDLES   Handles of all lines, fills, texts
%      DATA   Wind rose occurences per direction and intensity
%
%   Examle:
%      d=0:10:350;
%      D=[];
%      V=[];
%      for i=1:length(d)
%        n=d(i)/10;
%        D=[D ones(1,n)*d(i)];
%        V=[V 1:n];
%      end
%
%      figure
%      wind_rose(D,V)
%
%      figure
%      wind_rose(D,V,'iflip',1)
%
%      figure
%      wind_rose(D,V,'ci',[1 2 7],'dtype','meteo')
%
%      % place it on a previous axes:
%      ax=axes;
%      plot(lon,lat)
%      wind_rose(D,V,'ax',[ax x y 1/3])
%
%   MMA 6-06-2011, castello@unican.es
%
%   IH Cantabria, Instituto de Hidraulica de Cantabria
%   Santander, Espania

handles=[];

% varargin options:
dtype='standard';
nAngles=16;
ri=1/30;%distancia al borde o al centro del radio
quad=2;%quad 1-5 (0 45 135 225 315) gradus
legType=2;
percBg='w';
titStr='';
legStr='';
cmap=jet;% color por defecto
colors=[];
Ag=[]; % percentiles.
ci=[]; % circle for input var
lineColors='k';
borderColor='none';
onAxes=false;
parent=0;

vin=varargin;
for i=1:length(vin)
  if isequal(vin{i},'dtype')
    dtype=vin{i+1};
  elseif isequal(vin{i},'n')
    nAngles=vin{i+1};
  elseif isequal(vin{i},'ri')
    ri=vin{i+1};
  elseif isequal(vin{i},'quad')
    quad=vin{i+1};
  elseif isequal(vin{i},'legtype')
    legType=vin{i+1};
  elseif isequal(vin{i},'percbg')
    percBg=vin{i+1};
  elseif isequal(vin{i},'labtitle')
    titStr=vin{i+1};
  elseif isequal(vin{i},'lablegend')
    legStr=vin{i+1};
  elseif isequal(vin{i},'cmap')
    cmap=vin{i+1};
  elseif isequal(vin{i},'colors')
    colors=vin{i+1};
  elseif isequal(vin{i},'di')
    Ag=sort(vin{i+1});% ordenadas de menor a mayor los percentiles a calcular
  elseif isequal(vin{i},'ci')
    ci=vin{i+1};
  elseif isequal(vin{i},'lcolor')
    lineColors=vin{i+1};
  elseif isequal(vin{i},'bcolor')
    borderColor=vin{i+1};
  elseif isequal(vin{i},'ax')
    ax=vin{i+1};
    try
      onAxes=ax(1);
      onAxesX=ax(2);
      onAxesY=ax(3);
      onAxesR=ax(4);
    catch
      disp(':: cannot place wind rose on axes, bad argument for ax')
      return
    end
  elseif isequal(vin{i},'parent')
    parent=vin{i+1};
  end
end

% other options:
% size of the full rectangle:
rs=1.2;
rl=1.7;

% directions conversion:
if isequal(dtype,'meteo')
  D=mod(-270-D,360);
end

% angles subdivisons:
D=mod(D,360);
Ay=linspace(0,360,nAngles+1)-0.5*360/nAngles;
Ay_=linspace(0,360,nAngles+1);
% percentiles subdivisions:
if isempty(Ag)
  % gen Ag:
  Ag=[5    25    50    75    90    95    99]/100;
end
% quantiles by directional sector & porcentage
for i=1:length(Ay)-1
  if i==1
     I=find( (D>=Ay(i) & D<Ay(i+1)) | D>=Ay(end));
  else
    I=find(D>=Ay(i) & D<Ay(i+1));
  end
  b=F(I);
  for j=1:length(Ag)
    E(i,j)=quantile(b,Ag(j));% quantiles
  end
  P(i)=length(b)/length(F)*100;% porcentage
end

% circles 
if isempty(ci)
    ncircles=5;
	dcircles=ceil(linspace(min(F),min(max(F),max(max(E))),ncircles));
    if ~isempty(find(dcircles==0))
        dcircles(find(dcircles==0))=[];
        ncircles=ncircles-1;
    end
    ci=dcircles;
    g=dcircles(end);
else
  ncircles=length(ci);
  g=max(max(ci),max(max(E)));
end

% plot axes, percentage circles and percent. data:
if parent
  wrAx=parent;
  set(wrAx,'units','normalized');
else
  wrAx=axes('units','normalized');
end
ri=g*ri;
handles(end+1)=fill([-rs*g rl*g rl*g -rs*g],[-rs*g -rs*g rs*g rs*g],'w',...
                     'EdgeColor',borderColor);
if onAxes
  set(handles(end),'facecolor','w')
end
hold on;

%plot spokes. oc he anadido las lines de 45/2 en 45/2 como
rmax=g;labelrotate=90;
dnA=360/nAngles;
nAp=round(nAngles/2);
th = (1:nAp)*2*pi/nAngles;
cst = cos(th); snt = sin(th);
h1=plot(repmat([ 0 rmax],nAp,1)'.*repmat(cst,2,1),repmat([ 0 rmax],nAp,1)'.*repmat(snt,2,1),':','color',lineColors);
h2=plot(repmat([ 0 rmax],nAp,1)'.*repmat(-cst,2,1),repmat([ 0 rmax],nAp,1)'.*repmat(-snt,2,1),':','color',lineColors);
handles=[handles h1' h2'];
labs=[];
rt = 1.15*rmax;% annotate spokes in degrees
for i = 1:length(th)
    angTextNum =labelrotate-(i*dnA)    ;
    if angTextNum<0, angTextNum=angTextNum+360; end
    labs(2*i-1)=text(rt*cst(i),rt*snt(i),[num2str(angTextNum) char(176)],'horizontalalignment','center',...
         'handlevisibility','off','fontsize',8);
    if i == length(th)
        loc = num2str(0+labelrotate);
    else
        angTextNum = labelrotate-(i*dnA)+180    ;
        loc = num2str(angTextNum);
    end
    if str2num(loc)<0, loc=num2str(str2num(loc)+360); end
    labs(2*i)=text(-rt*cst(i),-rt*snt(i),[loc char(176)],'horizontalalignment','center',...
         'handlevisibility','off','fontsize',8);
end 
handles=[handles labs];%end plot spokes.
    
t0=[0:360]*pi/180;
labs=[];
Ang=[0 1/4 3/4 5/4 7/4]*pi;
Valign={'bottom' 'top' 'top' 'bottom' 'bottom'};
Halign={'right' 'right' 'left' 'left' 'right'};
for i=1:ncircles %circles and it label
  x=(ci(i)+ri)*cos(t0);
  y=(ci(i)+ri)*sin(t0);
  if i==ncircles
    circles(i)=plot(x,y,'color',lineColors);
  else
    circles(i)=plot(x,y,':','color',lineColors);
  end
  handles(end+1)=circles(i);

  labs(i)=text((ci(i)+ri)*cos(Ang(quad)),(ci(i)+ri)*sin(Ang(quad)),[num2str(ci(i)),legStr],...
      'VerticalAlignment',Valign{quad},'HorizontalAlignment',Halign{quad},...
      'BackgroundColor',percBg,'FontSize',8);
end
handles=[handles labs];
handles(end+1)=plot(x,y,'color',lineColors,'MarkerEdgeColor',...
'r','MarkerFaceColor','r');%oc ultimo circulo linea continua

% calc colors:
% if isempty(colors)
%   cor={};
%   for j=1:length(Ag)-1
%     cor{j}=caxcolor(Ag(j),[Ag(1) Ag(end-1)],cmap);
%   end
% else
%   cor=colors;
% end
colors=jet(length(Ag));
for i=1:length(Ag)
    cor{i}=colors(i,:);
end

% plot the percentiles data:
n=sum(E,2);
t=(Ay+(Ay(2)-Ay(1))/2)*pi/180;t(end)=[];
for j=1:length(Ag)
    x=E(:,j).*(cos(t))';x(end+1)=x(1);
	y=E(:,j).*(sin(t))';y(end+1)=y(1);
    jcor=j;
    handles(end+1)=plot(x,y,'color',cor{jcor},'LineWidth',2);
end
axis equal
axis off

%uistack has problems in some matlab versions, so:
%uistack(labs,'top')
%uistack(circles,'top')
ch=get(wrAx,'children');
for i=1:length(labs)
  ch(ch==labs(i))=[]; ch=[labs(i); ch];
end
set(wrAx,'children',ch);


% gradus labels:
bg='none';
args={'BackgroundColor',bg,'FontSize',8};
h=[];
for j=1:length(Ay_)-1
    if Ay_(j)>=0 && Ay_(j)<90
        h(j)=text((g+ri)*cos(t(j)), (g+ri)*sin(t(j)),[num2str(P(j),'%2.1f') char(37)], 'VerticalAlignment','top',   'HorizontalAlignment','right', args{:});
    elseif Ay_(j)==90
        h(j)=text((g+ri)*cos(t(j)), (g+ri)*sin(t(j)),[num2str(P(j),'%2.1f') char(37)], 'VerticalAlignment','top',   'HorizontalAlignment','center', args{:});
    elseif Ay_(j)>90 && Ay_(j)<180
        h(j)=text((g+ri)*cos(t(j)), (g+ri)*sin(t(j)),[num2str(P(j),'%2.1f') char(37)], 'VerticalAlignment','top',   'HorizontalAlignment','left', args{:});
    elseif Ay_(j)>=180 && Ay_(j)<270
        h(j)=text((g+ri)*cos(t(j)), (g+ri)*sin(t(j)),[num2str(P(j),'%2.1f') char(37)], 'VerticalAlignment','bottom',   'HorizontalAlignment','left', args{:});
    elseif Ay_(j)==270
        h(j)=text((g+ri)*cos(t(j)), (g+ri)*sin(t(j)),[num2str(P(j),'%2.1f') char(37)], 'VerticalAlignment','bottom',   'HorizontalAlignment','center', args{:});
    elseif Ay_(j)>270 && Ay_(j)<360
        h(j)=text((g+ri)*cos(t(j)), (g+ri)*sin(t(j)),[num2str(P(j),'%2.1f') char(37)], 'VerticalAlignment','bottom',   'HorizontalAlignment','right', args{:});
    end
end
handles=[handles h];
% scale legend:
L=(g*rl-g-ri)/7;
h=(g+ri)/10;
dy=h/3;

x0=g+ri*3.5+(g*rl-g-ri)/7;
x1=x0+L;
y0=-g-ri;

if legType==1 % contimuous.
  for j=1:length(Ag)
    lab=num2str(Ag(j));
    y1=y0+h;
    handles(end+1)=fill([x0 x1 x1 x0],[y0 y0 y1 y1],cor{j});
    handles(end+1)=text(x1+L/4,y0,lab,'VerticalAlignment','middle','fontsize',8);
    y0=y1;
  end
elseif legType==2 % separated boxes.
  for j=1:length(Ag)
    lab=[num2str(Ag(j)*100) ' %'];
    y1=y0+h;
    handles(end+1)=fill([x0 x1 x1 x0],[y0+dy y0+dy y1 y1],cor{j});
    handles(end+1)=text(x1+L/4,(y0+dy+y1)/2,lab,'VerticalAlignment','middle','fontsize',8);
    y0=y1;
  end

end

% title and legend label:
x=mean([-g*rs,g*rl]);
y=mean([g+3*ri,g*rs]);
handles(end+1)=text(x,y,titStr,'HorizontalAlignment','left','FontWeight','bold');

x=x0;
y=y1+dy;
%handles(end+1)=text(x,y,legStr,'HorizontalAlignment','left','VerticalAlign
%ment','bottom');%no se usa lo uso para la dimension de la variable

if onAxes
  place_wr(onAxes,wrAx,onAxesX,onAxesY,onAxesR);
end

if nargout>=1
  varargout{1}=handles;
end
if nargout>=2
  varargout{2}=E;
end

function place_wr(ax,ax2,x,y,width)
if nargin < 5
  width=1/5;
end
uax=get(ax,'units');
pax=get(ax,'position');
set(ax,'units',uax)
axXlim=get(ax,'xlim');
axYlim=get(ax,'ylim');

x_ax2=pax(1)+pax(3)*(x-axXlim(1))/diff(axXlim);
y_ax2=pax(2)+pax(4)*(y-axYlim(1))/diff(axYlim);

pax2=get(ax2,'position');
width=pax(3)*width;
height=pax2(4)*width/pax2(3);
pax2=[x_ax2 y_ax2 width height];

if 1
  % place at centre of the wr, not the bottom left corner:
  ax2Xlim=get(ax2,'xlim');
  ax2Ylim=get(ax2,'ylim');
  dx=(0-ax2Xlim(1))/diff(ax2Xlim)*pax2(3);
  dy=(0-ax2Ylim(1))/diff(ax2Ylim)*pax2(4);
  x_ax2=x_ax2-dx;
  y_ax2=y_ax2-dy;
  pax2=[x_ax2 y_ax2 width height];
end
set(ax2,'position',pax2)

function cor = caxcolor(val,cax,cmap)
%CAXCOLOR   Caxis color for value
%   Find the color for a given value in a colormap.
%
%   Syntax:
%     COLOR = CAXCOLOR(VALUE,CAXIS,COLORMAP)
%
%   Inputs:
%      VALUE
%      CAXIS   Default is current caxis
%      COLORMAP   Default is current colormap
%
%   Output:
%      COLOR   RGB color vector
%
%   Example:
%      figure
%      pcolor(peaks)
%      color=caxcolor(0);
%      set(gcf,'color',color)
%
%   MMA 28-5-2007, martinho@fis.ua.pt

% Department of Physics
% University of Aveiro, Portugal

if nargin < 3
  cmap = get(gcf,'colormap');
end
if nargin < 2
  cax = caxis;
end

n=size(cmap,1);
i= (val-cax(1))/diff(cax) * (n-1) +1;
a=i-floor(i);
i=floor(i);

i=min(i,n);
i=max(i,1);

if i==n
  cor=cmap(n,:);
elseif i==1
  cor=cmap(1,:);
else
  cor=cmap(i,:)*(1-a) + cmap(i+1,:)*a;
end
