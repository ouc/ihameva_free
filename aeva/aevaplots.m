function [Xxx,Xyout,Yxx,Yyout,Dxx,Dyout,XYnn,XYctrs,XDnn,XDctrs,YDnn,YDctrs,Xq,Yq]= ...
    aevaplots(X,Y,Dir,Time,statmoments,tpstring,timedefine,ndx,nsec,sini,sfin,svopt,carpeta,idioma,var_name,var_unit,Isdire,ax1)
%Regimen Medio plot's
[ni,mi]=size(tpstring);
if mi<1, return; end;
h=[];

%variables de salida
Xxx=[];Xyout=[];Yxx=[];Yyout=[];Dxx=[];Dyout=[];
XYnn=[];XYctrs=[];XDnn=[];XDctrs=[];YDnn=[];YDctrs=[];
Xq=[];Yq=[];
N=length(X);

xData=linspace(Time(1),Time(end),12);%Tick of time data

Lab=labelsameva(var_name(2:end),var_unit(2:end));%Labels

if ~isempty(X), dirtickx=linspace(round(min(X)),ceil(max(X)),6); end
if ~isempty(Y), dirticky=linspace(round(min(Y)),ceil(max(Y)),6); end
if strcmp(idioma,'esp')
    dirtickt={'','E','','F','','M','','A','','M','','J','','J','','A','','S','','O','','N','','D',''};
    dirtickn={'N','E','S','O','N'};
else
    dirtickt={'','J','','F','','M','','A','','M','','J','','J','','A','','S','','O','','N','','D',''};
    dirtickn={'N','E','S','W','N'};
end
az = 142.5;el = 30;% Display the default 3-D perspective view

if timedefine, %Tiempo en escala anual
    AnualTime=yearlytimescale(Time,'A');
end
if Isdire
    dirtick=0:90:360;
    Dlim=[0 360];
else
    dirtick=[];
    Dlim=[min(Dir) max(Dir)];
    dirtickn=[];
end
%marker size depende de size de la serie
markesz=6;
if N>2000, markesz=4; end

savename='';
for i=1:mi,
    plothc=false;
    plotname=tpstring{i};
    if ~exist('ax1','var') || ~ishandle(ax1), h=figure('Visible','Off'); ax1 = axes('Parent',h); end
    switch plotname
        case 'Serie(X)',
        plot(Time,X,'k');
        grid on;
        ylabel(Lab{1});
        xlabel(var_name{1});
        xlim([Time(1) Time(end)]);
        title(['Serie of ',var_name{2}],'FontWeight','bold');
        if timedefine,
            set(ax1,'XTick',xData);
            datetick(ax1,'x','mmmyy','keepticks');
        end
        savename=['Serie_',var_name{2}];
        case 'Serie(Y)'
        plot(Time,Y,'k');
        grid on;
        ylabel(Lab{2});
        xlabel(var_name{1});
        xlim([Time(1) Time(end)]);
        title(['Serie of ',var_name{3}],'FontWeight','bold');
        if timedefine,
            set(gca,'XTick',xData);
            datetick(gca,'x','mmmyy','keepticks');
        end
        savename=['Serie_',var_name{3}];
        case 'Serie(X,Y)'
        XYPlot(Time,X,Y,'-',var_name,var_unit,gca);
        savename=['Serie_',var_name{2},var_name{3}];
        case 'Serie(Dir)'
        plot(ax1,Time,Dir,'k.','MarkerSize',markesz);
        set(ax1,'XColor','k','YColor','k','XLim',[Time(1) Time(end)],'Ylim',Dlim);
        if Isdire,
            set(ax1,'YTick',dirtick);
        end
        title(ax1,['Serie of ',var_name{4}],'FontWeight','bold'); 
        grid on;box on;
        ylabel(Lab{3});
        xlabel(var_name{1});
        if Isdire
            axdir=axes('Position',get(ax1,'Position'),'YTick',dirtick,'YTickLabel',dirtickn,...
                'XLim',[Time(1) Time(end)],'Ylim',Dlim,'YAxisLocation','right','Color','none','XColor','k','YColor','k');
        end
        if timedefine,
            set(ax1,'XTick',xData); set(axdir,'XTick',xData);
            datetick(ax1,'x','mmmyy','keepticks'); datetick(axdir,'x','mmmyy','keepticks');
        end
        savename=['Serie_',var_name{4}];
        case 'YearSerie(X)'
        plot(mod(AnualTime,1),X,'k.','MarkerSize',markesz);
        grid on;
        ylabel(Lab{1});
        xlabel(var_name{1});
        xlim([0 1]);
        title(['Serie of ',var_name{2}],'FontWeight','bold'); 
        set(gca,'XTick',0:1/24:1)
        set(gca,'XTickLabel',dirtickt);
        savename=['YearSerie_',var_name{2}];
        case 'YearSerie(Y)'
        plot(mod(AnualTime,1),Y,'k.','MarkerSize',markesz);
        grid on;
        xlim([0 1]);
        ylabel(Lab{2});
        xlabel(var_name{1});
        title(['Serie of ',var_name{3}],'FontWeight','bold'); 
        set(gca,'XTick',0:1/24:1)
        set(gca,'XTickLabel',dirtickt);
        savename=['YearSerie_',var_name{3}];
        case 'YearSerie(Dir)'
        plot(ax1,mod(AnualTime,1),Dir,'k.','MarkerSize',markesz);
        set(ax1,'XColor','k','YColor','k','XTickLabel',dirtickt,'XTick',0:1/24:1,'XLim',[0 1],'Ylim',Dlim);
        if Isdire,
            set(ax1,'YTick',dirtick);
        end
        title(ax1,['Serie of ',var_name{4}],'FontWeight','bold'); 
        grid on;box on;
        ylabel(Lab{3});
        xlabel(var_name{1});
        if Isdire
        axes('Position',get(ax1,'Position'),'YTick',dirtick,'YTickLabel',dirtickn,...
            'XTick',0:1/24:1,'XTickLabel',dirtickt,'XLim',[0 1],'Ylim',Dlim,'YAxisLocation','right','Color','none','XColor','k','YColor','k');
        end
        savename=['YearSerie_',var_name{4}];
        case 'PDF(X)'
        [Xyout,Xxx]=hist_n(X,ndx);
        bar(gca,Xxx,Xyout,1.0,'b');
        grid on;
        ylabel('Relative Frequency');
        xlabel(Lab{1});
        title(['Histogram of ',var_name{2}],'FontWeight','bold');
        str2(1) = {['\mu(',var_name{2},') = ',sprintf('%1.2f',statmoments(1,1)),' ']};
        str2(2) = {['\sigma(',var_name{2},') = ',sprintf('%1.2f',statmoments(2,1)),' ']};
        str2(3) = {['\chi(',var_name{2},') = ',sprintf('%1.2f',statmoments(3,1)),' ']};
        str2(4) = {['\beta(',var_name{2},') = ',sprintf('%1.2f',statmoments(4,1)),' ']};
        str2(5) = {' '};
        text(max(get(gca,'Xlim')),min(get(gca,'Ylim')),str2,'HorizontalAlignment','Right','VerticalAlignment','Bottom');
        savename=['PDF_',var_name{2}];
        case 'PDF(Y)'
        [Yyout,Yxx]=hist_n(Y,ndx);
        bar(gca,Yxx,Yyout,1.0,'b');
        grid on;
        xlabel(Lab{2});
        ylabel('Relative Frequency');
        title(['Histogram of ',var_name{3}],'FontWeight','bold');
        str2(1) = {['\mu(',var_name{3},') = ',sprintf('%1.2f',statmoments(1,2)),' ']};
        str2(2) = {['\sigma(',var_name{3},') = ',sprintf('%1.2f',statmoments(2,2)),' ']};
        str2(3) = {['\chi(',var_name{3},') = ',sprintf('%1.2f',statmoments(3,2)),' ']};
        str2(4) = {['\beta(',var_name{3},') = ',sprintf('%1.2f',statmoments(4,2)),' ']};
        str2(5) = {' '};
        text(max(get(gca,'Xlim')),min(get(gca,'Ylim')),str2,'HorizontalAlignment','Right','VerticalAlignment','Bottom');
        savename=['PDF_',var_name{3}];
        case 'PDF(Dir)'
        [Dyout,Dxx]=hist_n(Dir,ndx);
        bar(gca,Dxx,Dyout,1.0,'b');
        grid on;
        xlim(Dlim);
        ylabel('Relative Frequency');
        xlabel(Lab{3});
        title(['Histogram of ',var_name{4}],'FontWeight','bold');
        savename=['PDF_',var_name{4}];
        case 'CDF(X)' %Funcion de distribucion
        Xq(1)=quantile(X,0.05);
        Xq(2)=quantile(X,0.25);
        Xq(3)=quantile(X,0.50);
        Xq(4)=quantile(X,0.75);
        Xq(5)=quantile(X,0.95);
        [aa,lags]=ecdf(X);
        line([0 Xq(1)],[0.05 0.05],'Color','k','LineStyle','--');hold on;
        line([0 Xq(2)],[0.25 0.25],'Color','k','LineStyle','--');
        line([0 Xq(3)],[0.50 0.50],'Color','k','LineStyle','--');
        line([0 Xq(4)],[0.75 0.75],'Color','k','LineStyle','--');
        line([0 Xq(5)],[0.95 0.95],'Color','k','LineStyle','--');
        plot(gca,lags,aa,'LineWidth',2);
        line([Xq(1) Xq(1)],[0 0.05],'Color','k','LineStyle','--');
        line([Xq(2) Xq(2)],[0 0.25],'Color','k','LineStyle','--');
        line([Xq(3) Xq(3)],[0 0.50],'Color','k','LineStyle','--');
        line([Xq(4) Xq(4)],[0 0.75],'Color','k','LineStyle','--');
        line([Xq(5) Xq(5)],[0 0.95],'Color','k','LineStyle','--');hold off;
        legend(['Percentile ( 5%) = ',sprintf('%1.2f',Xq(1))],...
            ['Percentile (25%) = ',sprintf('%1.2f',Xq(2))],['Percentile (50%) = ',sprintf('%1.2f',Xq(3))], ...
            ['Percentile (75%) = ',sprintf('%1.2f',Xq(4))],['Percentile (95%) = ',sprintf('%1.2f',Xq(5))],...
            'Location','SouthEast');
        axis(gca,'normal');box on;
        ylabel('CDF');
        xlabel(Lab{1});
        title(['Cumulative Distribution Function of ',var_name{2}],'FontWeight','bold');
        savename=['CDF_',var_name{2}];
        case 'CDF(Y)'
        Yq(1)=quantile(Y,0.05);
        Yq(2)=quantile(Y,0.25);
        Yq(3)=quantile(Y,0.50);
        Yq(4)=quantile(Y,0.75);
        Yq(5)=quantile(Y,0.95);
        [aa,lags]=ecdf(Y);
        line([0 Yq(1)],[0.05 0.05],'Color','k','LineStyle','--');hold on;
        line([0 Yq(2)],[0.25 0.25],'Color','k','LineStyle','--');
        line([0 Yq(3)],[0.50 0.50],'Color','k','LineStyle','--');
        line([0 Yq(4)],[0.75 0.75],'Color','k','LineStyle','--');
        line([0 Yq(5)],[0.95 0.95],'Color','k','LineStyle','--');
        plot(gca,lags,aa,'LineWidth',2);
        line([Yq(1) Yq(1)],[0 0.05],'Color','k','LineStyle','--');
        line([Yq(2) Yq(2)],[0 0.25],'Color','k','LineStyle','--');
        line([Yq(3) Yq(3)],[0 0.50],'Color','k','LineStyle','--');
        line([Yq(4) Yq(4)],[0 0.75],'Color','k','LineStyle','--');
        line([Yq(5) Yq(5)],[0 0.95],'Color','k','LineStyle','--');hold off;
        legend(['Percentile ( 5%) = ',sprintf('%1.2f',Yq(1))],...
            ['Percentile (25%) = ',sprintf('%1.2f',Yq(2))],['Percentile (50%) = ',sprintf('%1.2f',Yq(3))], ...
            ['Percentile (75%) = ',sprintf('%1.2f',Yq(4))],['Percentile (95%) = ',sprintf('%1.2f',Yq(5))],...
            'Location','SouthEast');
        axis(gca,'normal');box on;
        ylabel('CDF');
        xlabel(Lab{2});
        title(['Cumulative Distribution Function of ',var_name{3}],'FontWeight','bold');
        savename=['CDF_',var_name{3}];
        case 'Scatter(X,Y)'
        axis square
        plot(X,Y,'k.','MarkerSize',markesz);
        grid on;
        BIAS=statmoments(1,1)-statmoments(1,2);
        RMS=sqrt( (1/N)*sum( ( X - Y ).^2 ) );
        rho=sqrt(sum( (X - mean(Y) ).^2 ) / sum( (Y - X ).^2 + (X - mean(Y) ).^2 ));
        SI=RMS/mean(X);

        str2(1) = {['BIAS = ',sprintf('%1.2f',BIAS),' ']};
        str2(2) = {['RMS = ',sprintf('%1.2f',RMS),' ']};
        str2(3) = {['\rho = ',sprintf('%1.2f',rho),' ']};
        str2(4) = {['SI = ',sprintf('%1.2f',SI),' ']};
        str2(5) = {' '};
        text(max(get(gca,'Xlim')),min(get(gca,'Ylim')),str2,'HorizontalAlignment','Right','VerticalAlignment','Bottom');
        ylabel(Lab{2});
        xlabel(Lab{1});        
        title(['Scatter plot of ' var_name{2} ' & ' var_name{3}],'FontWeight','bold');
        savename=['Scatter_',var_name{2},var_name{3}];
        case 'Scatter(X,Dir)'
        axis square
        plot(X,Dir,'k.','MarkerSize',markesz);
        grid on;
        set(gca,'Ylim',Dlim);
        if Isdire,
            set(gca,'YTick',dirtick);
        end
        ylabel(Lab{3});
        xlabel(Lab{1});
        title(['Scatter plot of ' var_name{2} ' & ' var_name{4}],'FontWeight','bold');
        savename=['Scatter_',var_name{2},var_name{4}];
        case 'Scatter(Y,Dir)'
        axis square
        plot(Y,Dir,'k.','MarkerSize',markesz);
        grid on;
        set(gca,'Ylim',Dlim);
        if Isdire,
            set(gca,'YTick',dirtick);
        end
        ylabel(Lab{3});
        xlabel(Lab{2});
        title(['Scatter plot of ' var_name{3} ' & ' var_name{4}],'FontWeight','bold');
        savename=['Scatter_',var_name{3},var_name{4}];
        case 'Scatter3d(X,Y,Dir)'
        axis square
        plot3(Dir,Y,X,'k.','MarkerSize',markesz);
        view(az, el);grid on;box on;
        set(gca,'Xlim',Dlim)
        if Isdire,
            set(gca,'YTick',dirtick);
        end
        xlim(Dlim)
        zlabel(Lab{1})
        ylabel(Lab{2})
        xlabel(Lab{3})
        title(['Scatter plot of  ' var_name{2} ', ' var_name{3} ' & ' var_name{4}],'FontWeight','bold');
        savename=['Scatter3d_',var_name{2},var_name{3},var_name{4}];
        case 'PDF3d(X,Y)'
        XY(:,1)=X;
        XY(:,2)=Y;
        [nn,XYctrs]=hist3(XY,[ndx ndx],'FaceAlpha',.65); % Draw histogram in 2D 
        hist3_nor(nn,XYctrs,min(XY),max(XY),N,[ndx ndx]);
        XYnn=hist3_nor(nn,XYctrs,min(XY),max(XY),N,[ndx ndx]);
        zlabel('Relative Frequency');
        ylabel(Lab{2});
        xlabel(Lab{1});
        title(['Density Histogram of ',var_name{2},' & ',var_name{3}],'FontWeight','bold');
        view(az, el);grid on;box on;
        savename=['PDF3d_',var_name{2},var_name{3}];
        case 'PDF3d(X,Dir)'
        XY(:,1)=X;
        XY(:,2)=Dir;
        [nn,XDctrs]=hist3(XY,[ndx ndx],'FaceAlpha',.65); % Draw histogram in 2D
        hist3_nor(nn,XDctrs,min(XY),max(XY),N,[ndx ndx]);
        XDnn=hist3_nor(nn,XDctrs,min(XY),max(XY),N,[ndx ndx]);
        zlabel('Relative Frequency');
        ylabel(Lab{3});
        xlabel(Lab{1});
        title(['Density Histogram of ',var_name{2},' & ',var_name{4}],'FontWeight','bold');
        view(az, el);grid on;box on;
        set(gca,'Ylim',Dlim);
        if Isdire,
            set(gca,'YTick',dirtick);
        end
        savename=['PDF3d_',var_name{2},var_name{4}];
        case 'PDF3d(Y,Dir)'
        XY(:,1)=Y;
        XY(:,2)=Dir;
        [nn,YDctrs]=hist3(XY,[ndx ndx],'FaceAlpha',.65); % Draw histogram in 2D
        hist3_nor(nn,YDctrs,min(XY),max(XY),N,[ndx ndx]);
        YDnn=hist3_nor(nn,YDctrs,min(XY),max(XY),N,[ndx ndx]);
        zlabel('Relative Frequency');
        ylabel(Lab{3});
        xlabel(Lab{2});
        title(['Density Histogram of ',var_name{3},' & ',var_name{4}],'FontWeight','bold');
        view(az, el);grid on;box on;
        set(gca,'Ylim',Dlim);
        if Isdire,
            set(gca,'YTick',dirtick);
        end
        savename=['PDF3d_',var_name{3},var_name{4}];
        case 'PDF2d(X,Y)'
        XY(:,1)=X;
        XY(:,2)=Y;
        [nn,ctrs]=hist3(XY,[ndx ndx],'FaceAlpha',.65); % Draw histogram in 2D
        nn=hist3_nor(nn,ctrs,min(XY),max(XY),N,[ndx ndx]);
        imagesc(cell2mat(ctrs(1)),cell2mat(ctrs(2)),nn'); axis xy
        zlabel('Relative Frequency');
        ylabel(Lab{2});
        xlabel(Lab{1});
        title(['Density Histogram of ',var_name{2},' & ',var_name{3}],'FontWeight','bold');
        grid on;
        colormap(flipud(bone(30)));
        if ~isempty(h)
        %colorbar('peer',ax1);%OJO OJO JOJO  18-Apr-2011 Da problemas con los ejes SIN RESOLVER!       
            colorbar;
        end
        savename=['PDF2d_',var_name{2},var_name{3}];
        case 'PDF2d(X,Dir)'
        XY(:,1)=X;
        XY(:,2)=Dir;
        [nn,ctrs]=hist3(XY,[ndx ndx],'FaceAlpha',.65); % Draw histogram in 2D
        nn=hist3_nor(nn,ctrs,min(XY),max(XY),N,[ndx ndx]);
        imagesc(cell2mat(ctrs(1)),cell2mat(ctrs(2)),nn'); axis xy
        zlabel('Relative Frequency');
        ylabel(Lab{3});
        xlabel(Lab{1});
        title(['Density Histogram of ',var_name{2},' & ',var_name{4}],'FontWeight','bold');
        set(gca,'Ylim',Dlim);
        if Isdire,
            set(gca,'YTick',dirtick);
        end
        grid on;
        colormap(flipud(bone(30)));
        if ~isempty(h)
        %colorbar('peer',ax1);%OJO OJO JOJO  18-Apr-2011 Da problemas con los ejes SIN RESOLVER!       
            colorbar;
        end
        savename=['PDF2d_',var_name{2},var_name{4}];
        case 'PDF2d(Y,Dir)'
        XY(:,1)=Y;
        XY(:,2)=Dir;
        [nn,ctrs]=hist3(XY,[ndx ndx],'FaceAlpha',.65); % Draw histogram in 2D
        nn=hist3_nor(nn,ctrs,min(XY),max(XY),N,[ndx ndx]);
        imagesc(cell2mat(ctrs(1)),cell2mat(ctrs(2)),nn'); axis xy
        zlabel('Relative Frequency');
        ylabel(Lab{3});
        xlabel(Lab{2});
        title(['Density Histogram of ',var_name{3},' & ',var_name{4}],'FontWeight','bold');
        set(gca,'YTick',dirtick,'Ylim',Dlim)
        if Isdire,
            set(gca,'YTick',dirtick);
        end
        grid on;
        colormap(flipud(bone(30)));
        if ~isempty(h)
        %colorbar('peer',ax1);%OJO OJO JOJO  18-Apr-2011 Da problemas con los ejes SIN RESOLVER!       
            colorbar;
        end
        savename=['PDF2d_',var_name{3},var_name{4}];
        case 'RoseP(X,Dir)'
        [ticks,etiqueta3]=PintaRosasProbabilisticas(Dir,X,idioma,var_name{2},var_unit{2},ax1);
        set(gca,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio',[1 1 1]);
        if ~isempty(h)
            colorbar('peer',ax1,'location','southoutside','position',[1/3 0.065 1/3 0.03],'xtick',[ticks(1:3:end) ticks(end)],'XtickLabel',['0.0' etiqueta3(4:3:end) strcat(etiqueta3(end),' (f)')],'FontSize',7);
        end
        savename=['RoseP_',var_name{2}]; plothc=true;
        case 'RoseP(Y,Dir)'
        [ticks,etiqueta3]=PintaRosasProbabilisticas(Dir,Y,idioma,var_name{3},var_unit{3},ax1);
        set(gca,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio',[1 1 1]);
        if ~isempty(h)
            colorbar('peer',ax1,'location','southoutside','position',[1/3 0.065 1/3 0.03],'xtick',[ticks(1:3:end) ticks(end)],'XtickLabel',['0.0' etiqueta3(4:3:end) strcat(etiqueta3(end),' (f)')],'FontSize',7);
        end
        savename=['RoseP_',var_name{3}]; plothc=true;
        case 'Rose(X,Dir)'
        deltaH=round((ceil(max(X))-floor(min(X)))/8);
        VH=(floor(min(X)):deltaH:ceil(max(X)));
        if deltaH == 0
            deltaH=(max(X)-min(X))/8;
            VH=(min(X):deltaH:max(X));
        end
        iha_wind_rose(Dir,X,'ax',[ax1 0.45 0.5 1.2],'percbg','none','n',nsec,'di',VH,'dtype','meteo','quad',1,'idioma',idioma,'lablegend',Lab{1},'labtitle',['Rose of ' var_name{2}]);
        axis(ax1,'off');
        set(ax1,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio',[1 1 1]);
        savename=['Rose_',var_name{2}]; plothc=true;
        case 'Rose(Y,Dir)'
        deltaH=round((ceil(max(Y))-floor(min(Y)))/8);
        VH=(floor(min(Y)):deltaH:ceil(max(Y)));
        if deltaH == 0
            deltaH=(max(Y)-min(Y))/8;
            VH=(min(Y):deltaH:max(Y));
        end
        iha_wind_rose(Dir,Y,'ax',[ax1 0.45 0.5 1.2],'percbg','none','n',nsec,'di',VH,'dtype','meteo','quad',1,'idioma',idioma,'lablegend',Lab{2},'labtitle',['Rose of ' var_name{3}]);
        axis(ax1,'off');
        set(ax1,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio',[1 1 1]);
        savename=['Rose_',var_name{3}]; plothc=true;
        case 'RoseQ(X,Dir)'
        VH=[0.05 0.25 0.50 0.75 0.90 0.95 0.99];%seleccion de percentiles
        col=jet(10);col2=[1 1 1; col];%col2=flipud(bone(11));,'cmap',col2
        iha_roseq(Dir,X,'ax',[ax1 0.45 0.5 1.2],'percbg','none','n',nsec,'di',VH,'dtype','meteo','quad',1,'lablegend',var_unit{2},'labtitle',['Quantile Rose of ' var_name{2}]);
        axis(ax1,'off');
        set(ax1,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio',[1 1 1]);
        savename=['RoseQ_',var_name{2}]; plothc=true;
        case 'RoseQ(Y,Dir)'
        VH=[0.05 0.25 0.50 0.75 0.90 0.95 0.99];%seleccion de percentiles
        col=jet(10);col2=[1 1 1; col];%col2=flipud(bone(11));,'cmap',col2
        iha_roseq(Dir,Y,'ax',[ax1 0.45 0.5 1.2],'percbg','none','n',nsec,'di',VH,'dtype','meteo','quad',1,'lablegend',var_unit{3},'labtitle',['Quantile Rose of ' var_name{3}]);
        axis(ax1,'off');
        set(ax1,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio',[1 1 1]);
        savename=['RoseQ_',var_name{3}]; plothc=true;
        case 'BoxPlot(sector)'
        BoxPlotXY(X,Y,Dir,'sector',nsec,idioma,var_name(2:3),var_unit(2:3),ax1);
        savename='BoxPlot_sector';
        case 'BoxPlot(monthly)'
        BoxPlotXY(X,Y,Time,'monthly',nsec,idioma,var_name(2:3),var_unit(2:3),ax1);
        savename='BoxPlot_monthly';
        otherwise
        disp(['Warnig!.Not exist this plot case ',plotname]);
    end
    if ~isempty(h)
        set(gcf,'Color','w');%solo en aevaplot
        if plothc, set(h, 'Renderer', 'ZBuffer','InvertHardcopy','off'); end;
        if svopt(3), saveas(h,fullfile(pwd,carpeta,[savename,'.eps']),'psc2'); end
        if svopt(2), saveas(h,fullfile(pwd,carpeta,[savename,'.png'])); end
        set(h,'Visible','on');
        if svopt(1), saveas(h,fullfile(pwd,carpeta,[savename,'.fig'])); end
        close(h);
    end
end

end
