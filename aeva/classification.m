function classification(varargin)
%   CLASSIFICATION Algoritms SOM, MDA, KMA
%   CLASSIFICATION (classification.m) opens a graphical user interface for displaying the main
%   program posibility.
%   To load test data, please click on: -Help, Load ameva test data-, to send
%   data to matlab worksapce and work with this.
%   Tested on Ubuntu trusty v.14.04.1(developed), Windows 7 and Mac Os X v10.9.4 (MATLAB:R2013a)
%   Examples 1
%     ameva-->Tools-->Statistics->Classification
%   Examples 2
%     load ameva;
%     classification([data_gow.hs,data_gow.tm,data_gow.dir],'esp')
%   Examples 3
%     load ameva;
%     X1= data_gow.hs;              %Variable X1
%     X2= data_gow.tm;              %Variable X2
%     X3 = data_gow.dir;	        %Variable X3
%     carpeta='sample_dir';	  %Directory name-relative to work directory
%     idioma='eng';             %idioma-no implementado
%     var_name={'Hs' 'Tm' 'Dir'};   %Variable name
%     var_unit={'m' 's' 'grad'};    %Variable unit
%     svopt=[1 1 0];                %To so save figure (fig, png, eps)
%     classification([X1,X2,X3],carpeta,idioma,var_name,var_unit,svopt,3)
% See also:
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   classification (main window v1.7.2 (140820)).
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows 7
%   04-07-2011 - The first distribution version
%   10-04-2013 - Old distribution version
%   12-06-2013 - Old distribution version
%   20-08-2014 - Old distribution version
%   12-11-2015 - Last distribution version

versionumber='v1.7.2 (151112)';
nmfctn='classification';
Ax1Pos=[0.095 0.130 0.725 0.815];%Posiciones plot
figPos=[];

if ~isempty(findobj('Tag',['Tag_' nmfctn]));
    figPos=get(findobj('Tag',['Tag_' nmfctn]),'Position');
    close(findobj('Tag',['Tag_' nmfctn]));
end

%Lista de las figuras que se van a usar en este programa
hf=[];%ventana principal
hg=[];%ventana auxiliar
% Information for all buttons and Spacing between the button
colorG=[0.94 0.94 0.94];	%Color general
btnWid=0.16;
btnHt=0.05;
spacing=0.01;
top=0.94;
xPos=[1-btnWid-spacing 1-btnWid/2-spacing];
%GLOBAL VARIALBES solo usar las necesarias!!
awinc=amevaclass;%ameva windows commons methods
awinc.nmfctn=nmfctn;
carpeta=[];
ckvs=[1 2];ckvd=3;%por defecto
XA=[];final=[];
Labs={'';'';''};
nv=8;
var_lab=cell(1,nv);
for ii=1:nv
    var_lab(ii)={['X',num2str(ii),'-Data:']};
end
for ii=1:3, var_lab(ii)={['* ' var_lab{ii}]}; end; clear ii;
N=[];
t=7;%size, clusters por defecto
titulo='';
idioma='eng'; usertype=1;
if length(varargin)==7,
    XA=varargin{1};
    carpeta=varargin{2};
    idioma=varargin{3};
    var_name=varargin{4};
    var_unit=varargin{5};
    svopt=varargin{6};
    ckvd=varargin{7};
    texto=amevaclass.amevatextoidioma(idioma);
elseif nargin==2
    XA=varargin{1};
    idioma=varargin{2};
    texto=amevaclass.amevatextoidioma(idioma);
elseif nargin==1, %Los datos se seleccionan desde el espacio de trabajo
    idioma=varargin{1}.idioma;
    usertype=varargin{1}.usertype;
    texto=amevaclass.amevatextoidioma(idioma);
    disp(texto('dataClasif'));
elseif nargin==0, %Los datos se seleccionan desde el espacio de trabajo
    texto=amevaclass.amevatextoidioma(idioma);
    disp(texto('dataClasif'));
else
    texto=amevaclass.amevatextoidioma(idioma);
    error(texto('misData'));
end
namesoftware=[texto('classifName') ' ',versionumber];
%compruebo
if ~exist('var_name','var') || isempty(var_name), var_name={'Hs','Tm','Dir'}; end %Nombre de 3 variables por defecto sino existen
if ~exist('var_unit','var') || isempty(var_unit), var_unit={'m','s',char(176)}; end %Unidades por defecto sino existen
var_name(end+1:length(var_lab))={''};
var_unit(end+1:length(var_lab))={''};
if ~exist('svopt','var') || isempty(svopt), svopt=[1 1 0]; end %save option 1-fig 2-png 3-eps (1/0)
if ~isempty(find(isnan(XA),1)), disp(texto('nan')); end

%---LA FIGURA-VENTANA PRINCIPAL-
hf=figure('Name',namesoftware,'Color',colorG,'CloseRequestFcn',@AmevaClose, ...
    'NumberTitle','Off','DockControls','Off','Tag',['Tag_' nmfctn],'NextPlot','new');
if ~isempty(figPos), set(hf,'Position',figPos); end

if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

btnN=0;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'String','Data', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataOO('on'));

btnN=btnN+1;
yPos=top-btnHt/2-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hf,'String','N=', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid/2 btnHt/2]);
ui_nn=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(N), ...
    'Position',[xPos(2) yPos btnWid/2 btnHt/2],'Callback',@ActN,'Enable','Off');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hf,'String','Algorithm Type:', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt*0.66]);
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_algtipo=uicontrol('Style','popup','Units','normalized','Parent',hf,'String',{'SOM';'MDA';'KMA'}, ...
    'FontWeight','Bold','Position',[xPos(1) yPos btnWid btnHt],'Value',2,'Callback',@UpdateATipo);

btnN=btnN+1.5;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_scs=uicontrol('Style','text','Units','normalized','Parent',hf,'String','ClusterSize=49', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos-spacing btnWid btnHt]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_sz(1)=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(t), ...
    'Position',[xPos(1) yPos btnWid/2 btnHt],'Callback',@UpdateATipo);
ui_sz(2)=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(t), ...
    'Position',[xPos(2) yPos btnWid/2 btnHt],'Callback',@UpdateATipo);

btnN=btnN+1.5;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_start=uicontrol('Style','push','Units','normalized','Parent',hf,'String','Start', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@ClassificationStart);

btnN=btnN+2;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('buttonPlot_1b'), ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos-spacing btnWid btnHt]);
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_open=uicontrol('Style','push','Units','normalized','Parent',hf,'String','open', ...
    'Position',[xPos(1) yPos btnWid/2 btnHt],'Callback',@AmevaPlotToPrint);
ui_del=uicontrol('Style','push','Units','normalized','Parent',hf,'String','delete', ...
    'Position',[xPos(2) yPos btnWid/2 btnHt],'Callback',@AmevaPlotToPrintDel);

btnN=btnN+5;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_pp=uicontrol('Style','listbox','Units','normalized','Parent',hf,'String',texto('buttonPlot_2'), ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt*6],'Min',1,'Max',1);

ActPlots;%Actualiza los plot

uicontrol('Style','frame','Units','normalized','Parent',hf,'Position',[0 0 1 0.04]);
uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.59 0.004 0.18 0.03],'String',texto('saveFigure'));
ui_ckeps=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.77 0.004 btnWid/2 0.03],'String','*.eps','Value',svopt(3));
ui_ckfig=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.85 0.004 btnWid/2 0.03],'String','*.fig','Value',svopt(1));
ui_ckpng=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.92 0.004 btnWid/2 0.03],'String','*.png','Value',svopt(2),'Callback',@(source,event)EnableButton([ui_start,ui_open,ui_del,ui_nn],'Enable','on'));
SetSaveOpt;
ui_tb   =uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.01 0.004 0.58 0.03],...
    'String',[nmfctn,texto('selectData')],'HorizontalAlignment','left');

%% Data
[figPos,xPos,btnWid,btnHt,top,spacing]=awinc.amevaconst('ds');
hg=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Data'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataOO('off'),'WindowButtonMotionFcn',@ListBoxCallback1,...
    'NumberTitle','Off','MenuBar','none','Position',figPos,'Resize','Off','Visible','Off','NextPlot','new');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
amvcnf.idioma=idioma;amvcnf.usertype=usertype;

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(end) btnHt],...
    'String','Data & WorkSpace','Callback',@(source,event)amevaworkspace(amvcnf));
rbS=uicontrol('Style','radiobutton','Units','normalized', 'Callback',@UpdateStruct, ...
   'String','Matrix','Value',0,'Position',[xPos(2) yPos btnWid(1) btnHt]);
rbM=uicontrol('Style','radiobutton','Units','normalized', 'Callback',@UpdateStruct, ...
   'String','Vector','Value',1,'Position',[xPos(3) yPos btnWid(1) btnHt]);

for ii=1:length(var_lab)
    btnN=btnN+1;
    yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
    ui_td(ii)=uicontrol('Style','text','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(6) btnHt],'String',var_lab(ii),'BackgroundColor',colorG,'HorizontalAlignment','right');
    ui_all(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid(1) btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',ii);
    ui_cm(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid(5) btnHt],'String','(none)','visible','off');
    ui_ck(ii)=uicontrol('Style','check','Units','normalized','Parent',hg,'Position',[xPos(1)+btnWid(6) yPos btnWid(4) btnHt],'String','D','Value',0);
    ui_vn(ii)=uicontrol('Style','edit','Units','normalized','Parent',hg,'String',var_name(ii),'BackgroundColor','white','Position',[xPos(4) yPos btnWid(3) btnHt]);
    ui_vu(ii)=uicontrol('Style','edit','Units','normalized','Parent',hg,'String',var_unit(ii),'BackgroundColor','white','Position',[xPos(5) yPos btnWid(3) btnHt]);
    if ii==1,
        ui_tds=uicontrol('Style','text','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(6) btnHt],'String','* XA:','BackgroundColor',colorG,'HorizontalAlignment','right');
        ui_alls=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid(1) btnHt],'String','(none)','Callback',@UpdateNameAll1);
        ui_dirpos=uicontrol('Style','edit','Units','normalized','Parent',hg,'String','','BackgroundColor','white','Position',[xPos(3)+spacing yPos btnWid(1) btnHt]);
    end
end; clear ii;
set(ui_ck(3),'Value',not(isempty(ckvd)));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Set Data','Position',[xPos(2) yPos btnWid(1) btnHt],'Callback',@AmevaSetData);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Close','Position',[xPos(1) yPos btnWid(1) btnHt],'Callback',@(source,event)FigDataOO('off'));

uicontrol('Style','frame','Units','normalized','Parent',hg,'Position',[0 0 1 0.05]);
ui_tbd=uicontrol('Style','Text','Units','normalized','Parent',hg,'String',texto('dataWorkspace'),'Position',[0.01 0.004 0.99 0.04],'HorizontalAlignment','left');

if ~strcmp(get(hg,'NextPlot'),'new'), set(hg,'NextPlot','new'); end;

UpdateStruct(rbS);

UpdateATipo

%% C0 Open,Close figure data
    function FigDataOO(state,source,event)
        set(hg,'Visible',state);
    end
%% C0 Actulizo los botones Enable on/off & The figure open/close visible on/off
    function EnableButton(listbutton,statetyp,statebutton)
        set(listbutton,statetyp,statebutton);
    end
%% C0
    function ListBoxCallback1(source,event) % Load workspace vars into list box
        awinc.ListBoxCallback([ui_all,ui_alls],true);
    end
%% C0
    function SetSaveOpt
        if isdeployed,
            set(ui_ckfig,'Value',1,'Enable','On','Visible','On');
            set(ui_ckeps,'Value',0,'Enable','On','Visible','On');
        end
    end
%% C0
    function AmevaClose(source,event) % Close all Ameva windows
        awinc.AmevaClose([hf,hg], ...
            {[upper(nmfctn(1)),nmfctn(2:end),'Data']});
    end
%% C0
function AmevaPlotToPrint(source,event) % List of fig
    awinc.AmevaPlotToPrint(ui_pp,carpeta);
end
%% C1 Update cell and matrix name
    function UpdateNameAll(source,event)
        nval=get(source,'UserData');
        awinc.AmevaUpdateName(nval,ui_cm,ui_all,ui_td);
    end
%% C1 Update cell and matrix name
    function UpdateNameAll1(source,event)
        awinc.AmevaUpdateName(1,[],ui_alls,ui_tds);
    end
%% Open figure .fig
    function AmevaPlotToPrintDel(source,event)
        ival=get(ui_pp,'Value');
        iins=get(ui_pp,'String');
        [ni mi]=size(iins);
        if ni>=1
            for i=1:length(ival)
                figurename=char(iins(ival(i),1));
                delete(fullfile(pwd,carpeta,[figurename(1:end-3),'*']));
            end
        end
        %listar los plot para imprimir (solo SOM KMA MDA)
        awinc.ListBoxCallbackP(ui_pp,carpeta,[],'*.fig',{'SOM';'KMA';'MDA'});% Methods for "amevaclass"

    end
%% update entre struct y vectores
function UpdateStruct(source,event)
        aux=get(source,'String');
    if strcmpi(aux(1),'M'),
        if get(source,'value'),  set(rbM,'Value',0);
        else set(rbM,'Value',1); end
    else
        if get(source,'value'),  set(rbS,'Value',0);
        else set(rbS,'Value',1); end
    end
    if get(rbM,'Value')
        EnableButton([ui_td,ui_all,ui_cm,ui_ck,ui_vn,ui_vu],'Visible','on');
        EnableButton([ui_tds,ui_alls,ui_dirpos],'Visible','off');
    else
        EnableButton([ui_td,ui_all,ui_cm,ui_ck,ui_vn,ui_vu],'Visible','off');
        EnableButton([ui_tds,ui_alls,ui_dirpos],'Visible','on');
    end
end
%% Actualiza el tipo de claster
    function UpdateATipo(varargin)
        if get(ui_algtipo,'Value')==1,
            t(1)=round(str2double(get(ui_sz(1),'String'))); %Size of clauster
            t(2)=round(str2double(get(ui_sz(2),'String'))); %Size of clauster
            set(ui_sz(2),'Visible','On');
            set(ui_scs,'String',['ClusterSize=',num2str(t(1)*t(2))]);
            
        else
            t=round(str2double(get(ui_sz(1),'String')));%Size of clauster
            set(ui_sz(2),'Visible','Off');
            set(ui_scs,'String',[texto('elements'),num2str(t)]);
        end
    end
%% actualiza N
    function ActN(source,event)
        n=str2num(get(ui_nn,'String'));
        if n>0 && n<max(size(XA))
            disp([texto('size_1') ' ' num2str(N) texto('to') num2str(n) texto('size_2')]);
            XA=XA(1:n,:);
            ActPlots;
        end
        if n>max(size(XA))
            disp([texto('size_3') ' ' num2str(n) texto('size_4') ' ' num2str(N) texto('size_5')]);
            set(ui_nn,'String',num2str(N));
        end
    end
%% P0 Set data comprobamos que los Data para calcular la calibaracion son
    function AmevaSetData(source,event)
        XA=[];final=[];ckvd=[];ckvs=[];var_name=cell(3,1);var_unit=cell(3,1);
        if get(rbS,'Value'), % caso Matrix
            itwo=get(ui_alls,'Value');
            stwo=get(ui_alls,'String');
            if itwo==1, disp(texto('notDataStruc')); return; end
            XA=evalin('base',stwo{itwo});
            ckvd=str2num(get(ui_dirpos,'String'));
            ckvs=(1:min(size(XA)));
            if not(isempty(ckvd))
                ckvs=(1:min(size(XA)));
                ckvs(ckvd)=[];
            end
            nv=min(size(XA));
            for i=1:nv, var_name(i)={['X',num2str(i),'-data']}; end
            var_unit(1:nv)={'n'};
        else % caso vectores
            is=1;
            id=1;
            for i=1:nv
                iall=get(ui_all(i),'Value');
                if iall>1
                    awinc.AmevaEvalData(i,ui_cm,ui_all);
                    XA(:,i)=awinc.data;
                else
                    break;
                end
                if get(ui_ck(i),'Value');
                    ckvd(id)=i;
                    id=id+1;
                else
                    ckvs(is)=i;
                    is=is+1;
                end
            var_name(i)=get(ui_vn(i),'String');
            var_unit(i)=get(ui_vu(i),'String');
            end
        end
        %compruebo que al menos haya dos variables
        if min(size(XA))==length(ckvd), disp(texto('directionVector')); end
        if min(size(XA))<2, error(texto('minVarible')); end
        set(hg,'Visible','off');set(ui_tbd,'String',texto('applyData'));drawnow;
        
        %set new data
        set(ui_tbd,'String',texto('timeOptions'));drawnow ;
        set(0,'CurrentFigure',hf)
        ActPlots;
        %ActButton;
        %ActDefOption;
        EnableButton(ui_start,'Enable','On');
    end
%% plots
    function ActPlots(source,event)
        %PLOT
        for i=1:(min(size(XA)))
            Labs(i)=labelsameva(var_name(i),var_unit(i));
        end
        if max(size(XA))>1,
            N=max(size(XA));
            set(ui_nn,'String',num2str(N));
        end
        if not(isempty(XA))
            newchildren=get(hf,'children');%reset all axis
            %legend off;
            for i=1:length(newchildren)%reset only axes !!
                if strcmp(get(newchildren(i),'type'),'axes')
                    delete(newchildren(i));
                end
            end
            if ~strcmp(get(hf,'NextPlot'),'add'), set(hf,'NextPlot','add'); end;
            dim=min(size(XA));
            if dim==3,
                Dibuja_2D3D_DistribucionPtos(XA,final,'.k', '.r',0.5, floor(min(XA)), ceil(max(XA)),Labs,titulo,ckvd,'3D',hf);
            elseif dim==2
                Dibuja_2D3D_DistribucionPtos(XA,final,'.k', '.r',0.5, floor(min(XA)), ceil(max(XA)),Labs,titulo,ckvd,'2D',hf);
            end
            newchildren=get(hf,'children');%reset all axis
            for i=1:length(newchildren)%reset only axes !!
                if strcmp(get(newchildren(i),'type'),'axes')
                    set(newchildren(i),'Position',Ax1Pos);
                end
            end
            if dim==3, legend off; end
            if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;
        end
    end
%% ClassificationStart Plots
    function ClassificationStart(source,event)
        %Creo el directorio de trabajo para almacenamiento
        awinc.NewFolderCheck(carpeta,nmfctn);carpeta=awinc.carpeta;% Methods for "amevaclass"
        awinc.ListBoxCallbackP(ui_pp,carpeta,[],'*.fig',{'SOM';'KMA';'MDA'});drawnow;
        
        iins=get(ui_algtipo,'String');
        classTipo=iins{get(ui_algtipo,'Value')};%algoritmo tipo: 'SOM','KMA','MDA'
        UpdateATipo;
        
        set(ui_tb,'String',[nmfctn,texto('run')]);drawnow;
        
        %Run function
        try
            final=[];
            set(hg,'Visible','off');
            EnableButton([ui_start,ui_open,ui_del],'Enable','off');drawnow;
            %save options
            svopt(1)=get(ui_ckfig,'Value');%fig
            svopt(2)=get(ui_ckpng,'Value');%png
            svopt(3)=get(ui_ckeps,'Value');%eps
            titulo=[classTipo,' ',num2str(t)];
            Opciones = ameva_options(svopt,carpeta,idioma,var_name,var_unit);
            [final,bmus,acierto_cien]=classification_function(XA,ckvd,t,classTipo,Opciones);
            
            %Dibuja_DistribucionPtos_3variables_3D
            ActPlots;
%             legend(gca,'All data','Selected data','Location','NorthWest');
        catch ME
            EnableButton([ui_start,ui_open,ui_del],'Enable','on');
            rethrow(ME);
        end
        %listar los plot para imprimir (solo SOM KMA MDA)
        awinc.ListBoxCallbackP(ui_pp,carpeta,[],'*.fig',{'SOM';'KMA';'MDA'});% Methods for "amevaclass"
        
        %save
        save([pwd,filesep,carpeta,filesep,'classification_N=',num2str(N),'_t=',num2str(t),'_',classTipo,'.mat'], ...
            'Labs','final','bmus','acierto_cien');
        set(ui_tb,'String',[nmfctn, texto('ready_1')]);drawnow ;
        if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
        EnableButton([ui_start,ui_open,ui_del],'Enable','on');
    end

end
