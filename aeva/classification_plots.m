function classification_plots(datos,escalar,direccional,t,tipo,Opciones,final2,sM,SM2,acierto_cien2)
if isempty(Opciones.carpeta), Opciones.carpeta='classification_sample_dir'; end;
if ~exist(fullfile(pwd,Opciones.carpeta),'dir'),  mkdir(Opciones.carpeta); end;

%Size
if sum(strcmpi(tipo,{'SOM','SOM_PreMDA'}))>0
    tam = t(1)*t(2);
else
    tam = t;
end

minimos=zeros(length(escalar),1);
maximos=zeros(length(escalar),1);
for i=1:length(escalar)
    minimos(i)=min(datos(:,escalar(i)));
    maximos(i)=max(datos(:,escalar(i)));
end

lab = labelsameva(Opciones.var_name,Opciones.var_unit);

titulo=[tipo,' ',num2str(tam)];
if not(isempty(strfind(tipo,'_')))
    titulo=[regexprep(tipo,'_',' ') ,' ',num2str(tam)];
end

if sum(strcmpi(tipo,{'SOM','SOM_PreMDA'}))>0
    if not(isempty(escalar)) && length(escalar)==2 && escalar(1)==1 && escalar(2)==2 && (not(isempty(direccional)) && direccional(1)==3)
        h=figure ('color',[1 1 1],'visible','off');
        set(h, 'Renderer', 'ZBuffer');
        amevaSOM.grafica(final2, acierto_cien2, t,lab{1},lab{2},titulo);
        
        set(h, 'Renderer', 'ZBuffer');
        amevasavefigure(h,Opciones.carpeta,Opciones.svopt,[tipo,num2str(size(datos,2)),'_',Opciones.var_name{1},'_',num2str(tam)]);
    elseif not(isempty(escalar)) && length(escalar)==3  && escalar(1)==1 && escalar(2)==2  && ...
            escalar(3)==4 && (not(isempty(direccional)) && direccional(1)==3 && direccional(2)==5)
        h=figure ('color',[1 1 1],'visible','off');
        set(h, 'Renderer', 'ZBuffer');
        amevaSOM.grafica(final2, acierto_cien2, t,lab{1},lab{2},titulo);
        set(h, 'Renderer', 'ZBuffer');
        amevasavefigure(h,Opciones.carpeta,Opciones.svopt,[tipo,num2str(size(datos,2)),'_',Opciones.var_name{1},'_',num2str(tam)]);
    end
    
    h=figure ('color',[1 1 1],'visible','off');
    set(h, 'Renderer', 'ZBuffer');
    if strcmpi(tipo,'SOM_PreMDA') && not(isempty(direccional)), % OJO OJO comprobar OJO OJO
        sM.codebook=[SM2(:,escalar) SM2(:,direccional)*180/pi acierto_cien2(:)];%dir en grados
    else
        sM.codebook=[SM2 acierto_cien2(:)];
    end
    sM.comp_names(end+1)={'Frequency'};
    sM.comp_norm(end+1)=sM.comp_norm(end);
    sM.mask(end+1)=sM.mask(end);
    ncp=65;
    cm=1-copper(ncp+10);%Escala de azules (65 gamas)%acierto mes/anual
    cprob=cm(1:ncp,:);%Se eliminan las gamas mas oscuras
    som_show(sM,'comp',1:min(size(sM.codebook)),'colormap',cprob);
    set(h, 'Renderer', 'ZBuffer');
    amevasavefigure(h,Opciones.carpeta,Opciones.svopt,[tipo,num2str(size(datos,2)),'_',Opciones.var_name{1},'_',num2str(tam),'_']);
end
    
h=figure ('visible','off');
Dibuja_2D3D_DistribucionPtos(datos,final2, 'k.', 'r.', 0.5, floor(minimos), ceil(maximos),lab,titulo,direccional,'2D');
amevasavefigure(h,Opciones.carpeta,Opciones.svopt,[tipo,'_',num2str(tam),'_2D']);

h=figure ('visible','off');
Dibuja_2D3D_DistribucionPtos(datos,final2, 'k.', 'r.', 0.5, floor(minimos), ceil(maximos),lab,titulo,direccional,'3D');
amevasavefigure(h,Opciones.carpeta,Opciones.svopt,[tipo,'_',num2str(tam),'_3D']);

end