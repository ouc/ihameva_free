function [yout,xx] = hist_n(t,M)
%   HIST_N histograma normalizado
% -------------------------------------------------------------------------
%   Fisica Aplicada(MEPE). Universidad de Cantabria
%   Santander, Spain. 
% -------------------------------------------------------------------------
%   castello@unican.es
%   02-02-2009 - The first distribution version

if nargin == 1
    M = 10;
end
if isvector(t), t = t(:); end

[f,x]=ecdf(t,'alpha',0.05);
[yout,xx]=ecdfhist(f,x,M);
if nargout==0,
    plot(xx,yout,'x');
    auxi=0;
    for k=1:length(xx)-1, auxi=auxi+yout(k)*(xx(k+1)-xx(k));end
    auxi=auxi+yout(end)*(xx(end)-xx(end-1));
    disp(['sum(f(x)*dx)=' num2str(auxi)]);
end

end