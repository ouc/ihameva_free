function amevaws_update_listbox_ext
    if ~exist('MaxListBox','var'), MaxListBox = 35; end;
    if ~exist('vars_aux','var'), 
        vars_aux=cell(2,1);
        vars_aux{1}=sprintf(' %s       %s      %s','Name','Size','Class');
        vars_aux{2}='---- workspace -----------------------';
    end;

    if ~isempty(findobj('Tag','tg_wslbox')),
        s = evalin('base','whos');
        set(findobj('Tag','tg_wslbox'),'String',evalin('base','who'));

        list_entries = get(findobj('Tag','tg_wslbox'),'String');
        num_elementos=length(list_entries)+2;
        if num_elementos>MaxListBox, disp('Aumente MaxListBox 35. No se muestra todas las varibles!'); end;
        if num_elementos <= 2
            disp([datestr(now,'yyyymmddHHMMSS') '... No existen datos para utilizar con las herramientas de ameva']);
            %errordlg('No data in workspace',...
            %        'Incorrect Selection','modal')
        else
            varsI=cell(num_elementos,1);
            varsI{1}=vars_aux{1};
            varsI{2}=vars_aux{2};
            for k=1:num_elementos-2,
                varsI{k+2}=(sprintf(' %s     %dx%d     %s',s(k).name,s(k).size,s(k).class));
            end
            set(findobj('Tag','tg_wslbox'),'String',varsI,'Value',3)
        end 
    end
end
