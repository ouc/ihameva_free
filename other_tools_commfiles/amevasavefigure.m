function amevasavefigure(h,carpeta,svopt,savename,plothc,closefigtf,renderoc,fichastf)
% amevasavefigure(h,carpeta,svopt,savename,plothc,closefigtf,renderoc)

if nargin<3, error('amevasavefigure(h,carpeta,svopt); min 3 arg. ej: amevasavefigure(gcf,[],[0 1 0])'); end
if sum(svopt) <= 0, return; end
if not(exist('savename','var')) || isempty(savename), savename = ['[NewFigure',datestr(now,'yyyymmddHHMMSS'),']']; end
if not(exist('plothc','var')) || isempty(plothc), plothc = false; end
if not(exist('renderoc','var')) || isempty(renderoc), renderoc = 'zbuffer'; end
if not(exist('fichastf','var')) || isempty(fichastf), fichastf = false; end
set(h, 'Renderer',renderoc);%opengl
if plothc, set(h,'InvertHardcopy','off'); end;

[pathstr,folderstr] = fileparts(carpeta);
if isempty(pathstr), pathstr=pwd; end
if isempty(folderstr), disp('WARNING Folder name is empty, save in de current path!'); end

if svopt(3), saveas(h,fullfile(pathstr,folderstr,[savename,'.eps']),'psc2'); end

if svopt(2),
    if fichastf,
        print(h,'-dpng','-r300',fullfile(pathstr,folderstr,[savename,'.png']));
    else
        saveas(h,fullfile(pathstr,folderstr,[savename,'.png']));
    end
end

set(h,'Visible','on');
if svopt(1), saveas(h,fullfile(pathstr,folderstr,[savename,'.fig'])); end

%cierro la ventana, salvo que exista y sea closefigtf=false;
if exist('closefigtf','var') && islogical(closefigtf)
    if closefigtf, delete(h); end
else %sum(ismember(get(get(h,'Children'),'Tag'),'Colorbar'))~=0, 
    delete(h);
end