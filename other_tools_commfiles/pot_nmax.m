function [xumb,tumb,n,upc]=pot_nmax(x,Time,n0,Dupc)
% Examples 1
%   POT_NMAX to find a maximun serie of length n0
%   load ameva;
%   [xumb,tumb,n,upc]=pot_nmax(data_dow_sdr1.hs,data_dow_sdr1.time);
%
% See also:
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   castello@unican.es
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   created with MATLAB ver.: 7.7.0.471 (R2011a) on Windows 7
%   15-11-2012 - The first distribution version

if ~exist('n0','var') || isempty(n0), n0=183; disp(['default length of maximum serie is ' num2str(n0)]); end
if ~exist('Dupc','var') || isempty(Dupc), Dupc=0.2; end
upc=99.2;% 99.5 porciento 99.8
desv=ceil(n0*0.01);%desviacion respecto al n0 por defecto el 1 porciento
iter=20;iter_max=600;
dupc=zeros(1,iter);
for i=1:iter, dupc(i)=Dupc/(2*i); end;
%Time=(datenum([1948 2 1 0 0 0]):1/24:datenum([2008 12 31 23 0 0]));
TimeVec=datevec(Time);
xor=sort(x);%umbral = prctile(x,upc);% -Anual-Escalar-
N=length(x);
xn=N-round(upc*N/100);
umbral=xor(end-xn);
[xumb,tumb]=SeleccionaMaximosIndepPOT([Time(:) TimeVec(:,1) TimeVec(:,2) TimeVec(:,3) TimeVec(:,4) x(:)],umbral,3);
cont=1;
n=length(xumb);
for i=1:iter;
    %disp(['n_' num2str(i) '=' num2str(n) ' quantile=' num2str(upc)]);
    encuentranmax(dupc(i));
	if cont>iter_max, disp(['itermax=' num2str(iter) ' incremente itermax!']); break; end;
    if n<=n0+desv && n>=n0-desv
        disp(['n_' num2str(i) '=' num2str(n) ' quantile=' num2str(upc)]);
        break;
    end
end
if n>n0+desv || n<n0-desv, disp(['iter=' num2str(iter) ' incremente el numero de iteraciones iter!']); end;

disp(['iter=' num2str(cont)]);
%%
    function encuentranmax(Dupc)
        if n>n0+desv
            while n>n0+desv
                upc=upc+Dupc;
                xn=N-round(upc*N/100);
                umbral=xor(end-xn); %umbral=quantile(x,upc/100)
                [xumb,tumb]=SeleccionaMaximosIndepPOT([Time(:) TimeVec(:,1) TimeVec(:,2) TimeVec(:,3) TimeVec(:,4) x(:)],umbral,3);
                n=length(xumb);
                cont=cont+1;
                if cont>iter_max, disp(['itermax=' num2str(iter) ' incremente itermax!']); break; end;
                %disp(num2str(cont));
            end
        elseif n<n0-desv
            while n<n0-desv
                upc=upc-Dupc;
                xn=N-round(upc*N/100);
                umbral=xor(end-xn);
                [xumb,tumb]=SeleccionaMaximosIndepPOT([Time(:) TimeVec(:,1) TimeVec(:,2) TimeVec(:,3) TimeVec(:,4) x(:)],umbral,3);
                n=length(xumb);
                cont=cont+1;
                if cont>iter_max, disp(['itermax=' num2str(iter) ' incremente itermax!']); break; end;
%                 disp(num2str(n));
            end
        end
    end
end
