function amevaclear(tipo)
%function to clear all configure file for ameva programs
if ~exist('tipo','var'), tipo=''; end

if exist(fullfile(tempdir,'bathygshhsconf.txt'),'file')==2
    delete(fullfile(tempdir,'bathygshhsconf.txt'));
end

%eliminamos los ficheros descomprimidos zip
if strcmpi(tipo,'ALL')
    if exist([tempdir,'batimetrias_ameva'],'dir')==7
        delete([tempdir,'batimetrias_ameva',filesep,'*.dat'])
        delete([tempdir,'batimetrias_ameva',filesep,'*.mat'])
        rmdir([tempdir,'batimetrias_ameva']);
    end
    if exist([tempdir,'gshhs_shape_ameva'],'dir')==7
        delete([tempdir,'gshhs_shape_ameva',filesep,'*.shp'])
        delete([tempdir,'gshhs_shape_ameva',filesep,'*.dbf'])
        delete([tempdir,'gshhs_shape_ameva',filesep,'*.shx'])
        delete([tempdir,'gshhs_shape_ameva',filesep,'*.mat'])
        rmdir([tempdir,'gshhs_shape_ameva']);
    end
    delete([tempdir,'matrix_*.mat'])
end