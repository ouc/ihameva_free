classdef ameva_options
    properties
        svopt = [0 0 0];
        carpeta = '';
        idioma = 'eng';
        var_name = {'Data','',''};
        var_unit = {'','',''};
        cityname = '';
        poolparalelo = false; %Enables the parallel language
        forzarAQ = true; %Forzar calcular el cuantil agregado siempre (true)
        Ts = []; %Periodos de retorno vacio para usar el que se define en la funcion amevaAgregateQuantiles.aqPlot ln15
        myMeses = []; %Meses para calcular el Periodos de retorno. Vacio para usar todos los meses
        myPlots = 1; % Plot to calculate 0-to plot basic, 1-plots enough (*por defecto), 2-to plot all
        luint = []; %Low Upper bound chi0 GEV amevaFuncAutoAdjust ln314
    end
    methods(Static)
        function ao_obj = ameva_options(svopt,carpeta,idioma,var_name,var_unit,cityname,poolparalelo,forzarAQ,Ts,myMeses,myPlots,luint)
            %  Structura con las Opciones generales para usa con ameva
            %  ao_obj.svopt = [1 1 1]; (1 save .fig; 1 save .png; 1 save .eps), 0 para no guardar en ese formato
            %  ao_obj.carpeta = pwd; nombre de la carpeta donde guardar las figuras/datos de salida (carpeta actual)
            %  ao_obj.idioma = 'eng'; idioma de los texto eng/esp
            %  ao_obj.var_name = {'Hs','Tp','Dir'}; Nombre de las variables (Cell string array)
            %  ao_obj.var_unit = {'m','s','grad.'}; Unidades de las variables (Cell string array)
            %  ao_obj.cityname = 'Santander_1'; Titulo en las figuras
            %  ao_obj.poolparalelo = false; activar o deter calculo en paralelo true/false
            %  ao_obj.forzarAQ = true; forzar el calculo de los quantiles agregados true/false
            %  ao_obj.TS = []; Periodos de retorno para calcular quantiles agregados []/2/2 10 100 500
            %  ao_obj.myMeses = []; Meses para calcular el Periodos de retorno. Vacio para usar todos los meses
            %  ao_obj.myPlots = 1; % Plot to calculate 0-to plot all, 1-plots enough (*por defecto), 2-to plot basic
            %  ao_obj.luint = []; Low Upper bound chi0 0.2
            %
            % Ex : opciones = ameva_options([0 1 0],'prueba_m','esp',{'Hs'},{'m'},'Santander1');

            ao_obj.svopt = [0 0 0];
            ao_obj.carpeta = '';
            ao_obj.idioma = 'eng';
            ao_obj.var_name = {'Data','',''};
            ao_obj.var_unit = {'','',''};
            ao_obj.cityname = '';
            ao_obj.poolparalelo = false; %Enables the parallel language
            ao_obj.forzarAQ = true; %Forzar calcular el cuantil agregado siempre (true)
            ao_obj.Ts = []; %Periodos de retorno vacio para usar el que se define en la funcion amevaAgregateQuantiles.aqPlot ln15
            ao_obj.myMeses = []; %Meses para calcular el Periodos de retorno. Vacio para usar todos los meses
            ao_obj.myPlots = 1; % Plot to calculate 0-to plot basic, 1-plots enough (*por defecto), 2-to plot all
            ao_obj.luint = []; %Low Upper bound chi0 GEV amevaFuncAutoAdjust ln314
            %if exist var input
            if exist('svopt','var') && ~isempty(svopt),
                ao_obj.svopt = svopt;
            end
            if exist('carpeta','var') && ~isempty(carpeta),
                ao_obj.carpeta = carpeta;
            end
            if exist('idioma','var') && ~isempty(idioma),
                ao_obj.idioma = idioma;
            end
            if exist('var_name','var') && ~isempty(var_name),
                ao_obj.var_name  = var_name;
            end
            if exist('var_unit','var') && ~isempty(var_unit),
                ao_obj.var_unit  = var_unit;
            end
            if exist('cityname','var') && ~isempty(cityname),
                ao_obj.cityname = cityname;
            end
            if exist('poolparalelo','var') && ~isempty(poolparalelo),
                ao_obj.poolparalelo = poolparalelo;
            end
            if exist('forzarAQ','var') && ~isempty(forzarAQ),
                ao_obj.forzarAQ = forzarAQ;
            end
            if exist('Ts','var') && ~isempty(Ts),
                ao_obj.Ts = Ts;
            end
            if exist('myMeses','var') && ~isempty(myMeses),
                ao_obj.myMeses = myMeses;
            end
            if exist('myMeses','var') && ~isempty(myPlots),
                ao_obj.myPlots = myPlots;
            end
            if exist('luint','var') && ~isempty(luint),
                ao_obj.luint = luint;
            end
        end
        function [svopt,carpeta,idioma,var_name,var_unit,cityname,poolparalelo,forzarAQ,Ts,myMeses,myPlots,luint] = ameva_options_toarray(s)
            svopt=existevar('svopt');
            carpeta=existevar('carpeta');
            idioma=existevar('idioma');
            var_name=existevar('var_name');
            var_unit=existevar('var_unit');
            cityname=existevar('cityname');
            poolparalelo=existevar('poolparalelo');
            forzarAQ=existevar('forzarAQ');
            Ts=existevar('Ts');
            myMeses=existevar('myMeses');
            myPlots=existevar('myPlots');
            luint=existevar('luint');
            function obj=existevar(vn)
                if isprop(s,vn)%isfield(s,vn),
                    obj=s.(vn);
                else
                    obj=[];
                end
            end
        end
    end
end