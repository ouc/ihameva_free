function aevadsf=aeva_ds_functions
aevadsf.CallXY= @CallXY;
aevadsf.ActXY_Qxy = @ActXY_Qxy;
aevadsf.SetQXY_Enable = @SetQXY_Enable;
aevadsf.ActSe = @ActSe;
aevadsf.ActTinifin = @ActTinifin;
aevadsf.AmevadsApplyData = @AmevadsApplyData;
%% call X Y
    function CallXY(Typo,val,ui_v,ui_min,ui_max,ui_qmin,ui_qmax)
        if val==1,
            set(ui_v.(Typo)(2),'Value',0);
        else
            set(ui_v.(Typo)(1),'Value',0);
        end
        SetQXY_Enable(Typo,ui_v.(Typo)(1),ui_min,ui_max,ui_qmin,ui_qmax);
    end
%% call X Y
    function SetQXY_Enable(Typo,ui_v,ui_min,ui_max,ui_qmin,ui_qmax)
        set(ui_min.(Typo),'Enable','on');
        set(ui_max.(Typo),'Enable','on');
        set(ui_qmin.(Typo),'Enable','on');
        set(ui_qmax.(Typo),'Enable','on');
        if get(ui_v,'Value')
            set(ui_qmin.(Typo),'Enable','off');
            set(ui_qmax.(Typo),'Enable','off');
        else
            set(ui_min.(Typo),'Enable','off');
            set(ui_max.(Typo),'Enable','off');
        end
    end
%% acutaliza Qx Qy (MAX MIN) -->X Y introducido manualmente && acutaliza X Y (MAX MIN) --> Qx Qy introducido manualmente
    function [xmax,xmin,ymax,ymin,qxmax,qxmin,qymax,qymin]=ActXY_Qxy(tipo,strxy,ui_ckv,Xaux,Yaux,ui_min,ui_max,ui_qmin,ui_qmax)
        xmax=[];xmin=[];ymax=[];ymin=[];qxmax=[];qxmin=[];qymax=[];qymin=[];
        if strcmp(strxy(1),'X'), set(ui_ckv.x,'Value',1); end
        if strcmp(strxy(1),'Y'), set(ui_ckv.y,'Value',1); end
        if strcmp(strxy(1),'Y') && isempty(Yaux), return; end
        
        auxs='';
        if strcmp(tipo,'Qxy'), auxs='q'; end
        eval([auxs lower(strxy) '=str2double(get(ui_' auxs strxy(2:end) '.' lower(strxy(1))  ',''String''));']);

        if length(Xaux)<10,
            disp(['WARNING! not enought data to calculete quantile! N=',num2str(length(Xaux))]);
            return;
        end
        if strcmp(tipo,'XY'),
            aa_=[];lags_=[];
            eval(['[aa_,lags_]=ecdf(' strxy(1) 'aux);q' lower(strxy),'=interp1q(lags_,aa_,' lower(strxy) ');']);
            eval(['set(ui_' strxy(2:end) '.' lower(strxy(1))  ',''UserData'',' lower(strxy),');'])
            eval(['set(ui_q' strxy(2:end) '.' lower(strxy(1))  ',''UserData'',q' lower(strxy),',''String'',num2str(q' lower(strxy),'));'])
        else
            eval([lower(strxy) '=quantile(' strxy(1) 'aux,q' lower(strxy(1)) strxy(2:end) ');']);           
            eval(['set(ui_q' strxy(2:end) '.' lower(strxy(1))  ',''UserData'',q' lower(strxy),');'])
            eval(['set(ui_' strxy(2:end) '.' lower(strxy(1))  ',''UserData'',' lower(strxy),',''String'',num2str(' lower(strxy),'));'])
         
        end
    end
%% actualizar nsectores a grados sectores
    function [sini,sfin]=ActSe(nsec,ui_ckv,ui_nsi,ui_sini,ui_sfin)
        if str2double(get(ui_sini,'String'))<0, disp('ATENTION! sector(ini)>=0. Set default 0'); set(ui_sini,'String','0'); end
        if str2double(get(ui_sfin,'String'))>360, disp('ATENTION! sector(fin)<=360. Set default 360'); set(ui_sfin,'String','360'); end
        sini=str2double(get(ui_sini,'String'));
        sfin=str2double(get(ui_sfin,'String'));
        set(ui_ckv,'Value',1);
        set(ui_nsi,'String',num2str(abs(360-0)/nsec),'UserData',abs(360-0)/nsec);
        if sini==sfin, displ('ATENTION! No data select: sector(ini)=sector(fin)'), end
    end
%% acutaliza tini and tfin
    function ActTinifin(strtif,timedefine,ui_ckv,ui_imy,ui_tini,ui_tfin)
        if strcmp(strtif(1),'T'), 
            set(ui_ckv,'Value',1);
            set(ui_imy(1),'Value',1);
            set(ui_imy(2),'Value',0);
            set(ui_imy(3),'Value',0);
        end
        if strcmp(strtif,'Tini'),
            if timedefine,
                set(ui_tini,'UserData',datenum(get(ui_tini,'String'),'dd/mm/yyyy'));
            else
                set(ui_tini,'UserData',str2num(get(ui_tini,'string')));
            end
        elseif strcmp(strtif,'Tfin'),
            if timedefine,
                set(ui_tfin,'UserData',datenum(get(ui_tfin,'String'),'dd/mm/yyyy'));
            else
                set(ui_tfin,'UserData',str2num(get(ui_tfin,'string')));
            end
        end
    end
%% Select data for diferent options (Time-Data, directions, XData, YData)
    function [newTime,newX,newY,newD] = AmevadsApplyData(ifcimy,ifcyvq,ifcxvq,cktime,ckxopt,ckyopt,cksect,timedefine,tini,tfin,newTime,newX,newY,newD,Isdire,sini,sfin,montsc,seassc,xmin,xmax,qxmin,qxmax,ymin,ymax,qymin,qymax)
        %APLICO LAS DISTINTAS OPCIONES SELECCIONADAS
        if cktime,%TIME OPTIONS
            newPos = time_data_selection(newTime,ifcimy,timedefine,tini,tfin,montsc,seassc);
            newTime=newTime(newPos);
            newX=newX(newPos);
            if ~isempty(newY), newY=newY(newPos); end
            if ~isempty(newD), newD=newD(newPos); end
        end
        if cksect && ~isempty(newD),%SECTOR OPTIONS
            newPos = dir_data_selection(newD,Isdire,sini,sfin);
            newTime = newTime(newPos);
            newX = newX(newPos);
            if ~isempty(newY), newY = newY(newPos); end
            newD = newD(newPos);
        end
        if ckxopt, %X OPTIONS
            newPos = x_data_selection(newX,ifcxvq,xmin,xmax,qxmin,qxmax);
            newTime = newTime(newPos);
            newX = newX(newPos);
            if ~isempty(newY), newY = newY(newPos); end
            if ~isempty(newD), newD = newD(newPos); end
        end
        if ckyopt && ~isempty(newY), %Y OPTIONS
            newPos = x_data_selection(newY,ifcyvq,ymin,ymax,qymin,qymax);
            newTime = newTime(newPos);
            newX = newX(newPos);
            newY = newY(newPos);
            if ~isempty(newD), newD = newD(newPos); end
        end
        newTime = newTime(:);
    end
%% Select data for diferent options (Time-Data)
    function [newPos] = time_data_selection(newTime,ifcimy,timedefine,tini,tfin,montsc,seassc)
        %montsc --> 1,2,3,.....12  -->Enero,Febrero,....
        %seassc --> 1,2,3,4 -->DJF,MAM,JJA,SON
        %por defecto se busca el intervalo temporal seleccionado
        newPos = newTime == newTime;
        if ifcimy(1) == 1,
            newPos = find(tini<=newTime & newTime<=tfin);
            if isempty(newPos)
                disp(['selecte a valid tini=',datestr(tini,'dd/mm/yyyy'),' or tfin=',datestr(tfin,'dd/mm/yyyy')]);
                return;
            end
            %if month
        elseif ifcimy(2)==1 && timedefine,
            TimeVec=datevec(newTime);
            newPos=find(TimeVec(:,2)==montsc);
            if isempty(newPos)
                disp(['No data in this month=',montsc]);
                return;
            end
            %if season
        elseif ifcimy(3)==1 && timedefine,
            TimeVec=datevec(newTime);
            if seassc==1, %DJF
                newPos=find(TimeVec(:,2)==12 | TimeVec(:,2)==1 | TimeVec(:,2)==2);
            elseif seassc==2, %MAM
                newPos=find(TimeVec(:,2)==3 | TimeVec(:,2)==4 | TimeVec(:,2)==5);
            elseif seassc==3, %JJA
                newPos=find(TimeVec(:,2)==6 | TimeVec(:,2)==7 | TimeVec(:,2)==8);
            elseif seassc==4, %SON
                newPos=find(TimeVec(:,2)==9 | TimeVec(:,2)==10 | TimeVec(:,2)==11);
            elseif seassc==5, %DJFMAM Wet Season - Estacion lluviosa
                newPos=ismember(TimeVec(:,2),[12 1 2 3 4 5]);
            elseif seassc==6, %JJASON Dry Season - Estacion seca
                newPos=ismember(TimeVec(:,2),[6 7 8 9 10 11]);
            end
            if isempty(newPos)
                disp(['No data in this season=',montsc]);
                return;
            end
        end
    end
%% Select data for diferent options (Dereccion-Data)
    function [newPos] = dir_data_selection(newD,Isdire,sini,sfin)
        newPos = newD == newD;
        if Isdire
            %por defecto se busca el intervalo del sector seleccionado
            %sini=0;sfin=360;newD=DirM;
            if (sini<sfin),
                newPos=find(sini<=newD & newD<=sfin);
                if isempty(newPos)
                    disp(['selecte a valid sini=',num2str(sini),' or sfin=',num2str(sfin)]);
                    return;
                end
            else
                newPos=find(sini<=newD | newD<=sfin);
                if isempty(newPos)
                    disp(['selecte a valid sini=',num2str(sini),' or sfin=',num2str(sfin)]);
                    return;
                end
            end
        end
    end
%% Select data for diferent options (X-Data,Y-Data,...) by value or by quantil
    function [newPos] = x_data_selection(newX,ifczvq,zmin,zmax,qzmin,qzmax)
        newPos = newX == newX;
        if ifczvq(1) == 1, %by value zmin zmax
            newPos = find(zmin <= newX & newX <= zmax);
            if isempty(newPos)
                disp(['selecte a valid zmin=',num2str(zmin),' or zmax=',num2str(zmax)]);
                return;
            end
        elseif ifczvq(2) == 1, %by quantil qzmin qzmax
            newPos = find(quantile(newX,qzmin) <= newX & newX <= quantile(newX,qzmax));
            if isempty(newPos)
                disp(['selecte a valid qzmin=',num2str(qzmin),' or qzmax=',num2str(qzmax)]);
                return;
            end
        end
    end

end