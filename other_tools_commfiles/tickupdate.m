function [xtick,nx]=tickupdate(x)
%OC 2012
h=figure('visible','off');
ax = axes;
plot(ax,x,x);
L = get(ax,'ytick');
if length(L) < 7
    L1 = linspace(L(1),L(end),11);
    dt = round((L1(2)-L1(1))*10)/10; %intento redondear
    xtick = L1(1):dt:L1(end);
    if length(xtick)<9
        xtick = L1;
    end
    if xtick(end) < max(x),
        xtick(end+1) = xtick(end)+dt;
    end
else
    xtick = L;
end
nx = length(xtick)-1;
%set(gca,'xtick',xtick,'xlim',[xtick(1) xtick(end)]);
close(h);
end
