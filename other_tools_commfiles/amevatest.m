%To test ameva or ameva function, load ameva.mat
%STATISTIC, GEV, POT, CALIBRATION Y RM(regimen medio), otros
%close all; clear all; clc;
%Importante AMEVA:
%En general si se usa svopt(save options), tiene el siguiente formato:
%  svopt=[1 1 1];(1 save .fig; 1 save .png; 1 save .eps), 0 para no guardar en ese formato
%Importante GEV:
%v. pos. 1 2 3 ............10
%  info=[1 1 1 1 9 1 0 0 0 0];
%        |___________________ 1/0 Profilike/Akaike criterio
%          |_________________ 0-1 IntrAnual-Armonicos    0-No, 1-Si
%            |_______________ 0-1 LongTerm-Tendencias    0-No, 1-Si
%              |_____________ 0-2 InterAnual-Covariables 0-No, 1-(Loc./Scale), *2-(Loc./Scale/Shape)
%                |___________ 1-12 Maximos numero de armonicos
%                  |_________ 1/0 AutoArmonicos Si/No
%                    |_______ 1-6 Armonicos en localizacion (AutoA->0)
%                      |_____ 1-6 Armonicos en escala (AutoA->0)
%                        |___ 1-6 Armonicos en forma (AutoA->0)
%                          |_ 0-2 Forzar Gamma0. 0-Auto, 1-Forzar Gamma0=0, 2-Forzar Gamma0~=0
%%
tStart = tic; %Do some things...
%%%%%
load ameva.mat; %sample data
%%%%%
%%%%%
ihclima=false;
if exist(which('amevainfo'),'file') == 2, ihclima = true; end
if ihclima
    fichasDG.UnZipBatiCoastLineHI; %Cargo batimetrias/linea de costa H o I Solo para fichas
end
ihclima=false;
%%%%%
mapinstall=amevaTheseToolboxesInstalled('Mapping Toolbox');
%% GEV [27-Ene-2012]
% [exgev_datemax,ny]=yearlytimescale(Time_juliano,'A'); % Importante que
% los datos temporales deben estar normalizado. Utilize esta funcion
%info necesario para c y matlab
svopt=[0 1 0]; %save figures option 1-fig 2-png 3-eps (1/0)
info = [1 1 0 0 12 1 0 0 0 0];%2 Forzamos no gumbel para usar cov. en forma
criterio = 'Akaike'; if info(1) == 1, criterio = 'ProfLike'; end
confidenceLevel = 0.95; confidenceLevelAlpha=1-confidenceLevel; % 0.95 <-> 0.05
Opciones = ameva_options(svopt,'prueba_old_gev','esp',{'Hs'},{'m'},'Santander1'); %Fichero de opciones
Opciones.luint = 0.2; %low up bound limit on gamma (GEV autoajuste)
exgev_Modos = [];%no va con kt Bretagne
%example 1 [Old codes]: (matlab & c++ gev auto ajuste)
FuncAutoAdjust(exgev_Hsmax,exgev_datemax,exgev_kt1,exgev_Modos,info,'monthly',confidenceLevel,criterio,Opciones);%MatLab
if not(isdeployed) && ispc && isempty(strfind(computer,'64')) && exist(which('c_ophgev'),'file')==2,
    Opciones.carpeta='prueba_gev_ccp';
    amevaFuncAutoAdjust.c_AutoAjuste(exgev_Hsmax,exgev_datemax,[],exgev_Modos,info,confidenceLevel,Opciones);%c++
end
%sample 3 [New codes]
Opciones.carpeta='prueba_gev';
[OPHr,OPHobj] = amevaFuncAutoAdjust.AutoAjuste(exgev_Hsmax,exgev_datemax,[],exgev_Modos,info,confidenceLevel,Opciones);
figure; amevaAgregateQuantiles.aqPlot(OPHobj,OPHr,confidenceLevelAlpha,Opciones,gca,true,pwd);clear auxi;
%example 2
info = [1 1 1 1 12 1 0 0 0 0];%necesario para c++(c_FuncAutoAdjust)
criterio = 'Akaike'; if info(1) == 1, criterio = 'ProfLike'; end
carpeta = 'C:\IHCantabria\amevadatos\gev\';ciudadG = 'cristina215w55n';%
if exist(carpeta,'dir')==7
    exgev_Hsmax = load([carpeta,ciudadG,'Y.txt']);
    exgev_datemax = load([carpeta,ciudadG,'T.txt']);
    exgev_kt = load([carpeta,ciudadG,'N.txt']);
    exgev_Modos = load([carpeta,ciudadG,'Modos.txt']);
    Opciones.carpeta=['sample_GEV_' ciudadG 'M'];
    FuncAutoAdjust(exgev_Hsmax,exgev_datemax,exgev_kt,exgev_Modos,info,'monthly',confidenceLevel,criterio,Opciones);%MatLab
    if not(isdeployed) && ispc && isempty(strfind(computer,'64')),
        Opciones.carpeta=['sample_GEV_' ciudadG 'C'];
        amevaFuncAutoAdjust.c_AutoAjuste(exgev_Hsmax,exgev_datemax,exgev_kt,exgev_Modos,info,'monthly',confidenceLevel,Opciones);%c++
    end
    %c++ code varios
    figure; plot(exgev_datemax,exgev_Hsmax,'.-r',exgev_datemax,exgev_Hsmax.*exgev_kt1,'.-g',exgev_datemax,exgev_kt1,'.-b');xlabel(['T a',char(241),'os']);ylabel('Hs m');title(ciudadG);
    if not(isdeployed) && ispc && isempty(strfind(computer,'64')),
        [exito,P,LNG,ALLP,Loglikeobj,XG,XH,mut,psit,epst,IL,IE] = c_ophgev(exgev_Hsmax,exgev_datemax,info,exgev_kt1,exgev_Modos,exgev_Modos);
    end
    if not(isdeployed) && ispc && isempty(strfind(computer,'64')) && (~isempty(XH) && exito==0),
        %   ATENCION no se debe utilizar valores menores  que 2... las aprox. deja de ser validas. Maximos mensuales
        Ts = [2.0 5.0 10.0 20.0 25.0 75.0 100.0 200.0 300.0 400.0 500.0];
        [Z,ZLO,ZUP,Ts] = c_ConfidInterQuanAggregate(XH,exgev_datemax,P,LNG,0,1,Ts,exgev_Modos,exgev_Modos,'monthly');
        figure;semilogx(Ts,Z,'--r',Ts,ZLO,'--b',Ts,ZUP,'--g');xlabel(['T a',char(241),'os']);
    end
end
%% CALIBRATION [27-Ene-2012]
%example 1
excal_nq = 20; excal_increthet = 22.5;excal_qlim = 0;excal_nivconf = 0.95; svopt=[0 1 0];
carpeta = 'sample_calibrationW2';if ~exist([pwd filesep carpeta],'dir'), mkdir(carpeta); end
[Hscal,thetaj,pdef,maxHsR,deltamaxHsR] = CalibraSplineQuantile(excal_HsI,excal_HsR,excal_Dire,excal_nq,excal_increthet,excal_qlim,excal_nivconf,svopt,carpeta,'eng',{'Hs'},{'m'});
%Calibrando una nueva variable excal_HsI,excal_Dire con el resultado del calibrado anterior
Hscaln1 = CalibraSplineNLP(excal_HsI,excal_Dire,thetaj,pdef,maxHsR,deltamaxHsR,excal_HsR,excal_Dire,Hscal,svopt,carpeta);
Hscaln1 = CalibraSplineNLP(data_gow.hs,data_gow.dir,thetaj,pdef,maxHsR,deltamaxHsR,excal_HsR,excal_Dire,Hscal,svopt,carpeta);
%example 2 Se necesita DATA.mat
dirscript = 'C:\IHCantabria\amevadatos\calibration\antoniot\';%directorio de los datos DATA.mat cambiar
if exist(dirscript,'dir')==7
    
    confidenceLevel = 0.95; confidenceLevelAlpha=1-confidenceLevel; % 0.95 <-> 0.05
    
    carpeta = 'sample_calibration1';if ~exist([pwd filesep carpeta],'dir'), mkdir(carpeta); end
    load([dirscript,'DATA.mat'])
    HsI = DATA(:,4);HsR = DATA(:,5);Dir = DATA(:,6);
    GOW = load([dirscript,'GOW_DATA.dat']);%GOW_Valencia II Ext
    HsGow = GOW(:,5);thetaGOW = GOW(:,8);
    nq = 20;increthet = 22.5;qlim = 0;
    [Hscal,thetaj,pdef,maxHsR,deltamaxHsR,stdvHs,A,stdev,residuo,HsI,HsR,Hlim,calidadRHO,statmoments,DirQ,...
        nq,Hs_NCQ,Hs_CQ,Hs_SATQ,aj,ajlo,ajup,bj,bjlo,bjup,dj,djlo,djup,DirNoQ,np,Hsup,Hslo,n,x_G,y_G,stdvHs_G] = ...
        CalibraSplineQuantile(HsI,HsR,Dir,nq,increthet,qlim,confidenceLevelAlpha,svopt,carpeta);
    %Calibrando una nueva variable HsGow,thetaGOW con el resultado del calibrado anterior
    Hscal_n = CalibraSplineNLP(HsGow,thetaGOW,thetaj,pdef,maxHsR,deltamaxHsR);
    GraficosAdicionales(HsGow,thetaGOW,HsR,Dir,maxHsR,Hscal,Hscal_n,svopt,carpeta);
    
    %Filtro de ouuliers
    conf = [0.90 0.95 0.99 0.999 0.9999];
    ID = 6;%Kind of model
    if not(isdeployed) && ispc, worldfilter(excal_HsI,excal_HsR,excal_Dire,excal_datime,conf,confidenceLevel,carpeta,'esp','Hs','m'); end
    [idx,p,rN,fmu,upper,lower,Derivatives] = outlierfilterRNoCte(excal_HsR,excal_HsI,confidenceLevel,ID,conf);
    % excal_HsI(idx{end},:) = [];%se remplazan los outlier por vacios
    % excal_HsR(idx{end},:) = [];
    % excal_Dire(idx{end},:) = [];
    % excal_datime(idx{end},:) = [];
end

%% PRUEBA STATISTIC AMEVA DATA AND REGIMEN MEDIO
%Buenas Omar, te mando un fichero de ejemplo (DOW en Virgen del Mar), tiene
%5 columnas: t[datenum], Hs[m], Tm[s], Tp[s], DirM[grad], (cabria la posibilidad de
%que el delta t no fuese regular, como las boyas, o que no se tenga tiempo)
carpeta = 'sample_statistic_roses';if ~exist([pwd filesep carpeta],'dir'), mkdir(carpeta); end
%AEVA,CALIBRATION PLOT pruebas
if exist('aj','var') && not(isempty(aj))
    figure;Pinta_felpudo(aj,bj,thetaj,np,[Hs_NCQ Hs_NCQ(:,1)]',Hlim,'m');
    amevasavefigure(gcf,carpeta,svopt,'Pinta_felpudo');
end
figure;iha_roseq(data_gow.dir,data_gow.hs,'gca','quad',1,'dtype','meteo','n',16,'lablegend','m');%Rosa de Quantiles
set(gcf,'Color','w');amevasavefigure(gcf,carpeta,svopt,'iha_roseq');
figure;iha_wind_rose(data_gow.dir,data_gow.hs,'gca','idioma','esp','quad',1,'dtype','meteo','n',16,'lablegend','m');%Rosa
set(gcf,'Color','w');amevasavefigure(gcf,carpeta,svopt,'iha_wind_rose');
figure;PintaRosasProbabilisticas(data_gow.dir,data_gow.hs,'esp','N','m');%Rosa de Probabilidades
amevasavefigure(gcf,carpeta,svopt,'PintaRosasProbabilisticas_data_gow');
figure;PintaRosasProbabilisticas(excal_Dire,excal_HsR,'esp','N','m',gca,[0 4],true);
amevasavefigure(gcf,carpeta,svopt,'PintaRosasProbabilisticas');
%% find de best fit model DISTRIBUTION FIT ANALISYS
carpeta = 'sample_disfitanalysis';if ~exist([pwd filesep carpeta],'dir'), mkdir(carpeta); end
disp('disfitanalysis tool') %%disfitanalysis tool
confidenceLevel = 0.95; confidenceLevelAlpha=1-confidenceLevel; % 0.95 <-> 0.05
DistriName = {'normal','lognormal','weibull','weibullmin','weibullmax','gumbel','extreme value','pareto',...
    'exponential','rayleigh','gamma','logistic','t-student'};
X = excal_HsI; %X = random('gev',0,0.4065,0.6474,1,20000);
ndx = round(3/2+log(length(X))/log(2))*2;
for i_ = 1:length(DistriName)
    alldistri_result(i_).name = DistriName{i_};
    [param,paramstr,pci] = disfitparam(DistriName{i_},X,confidenceLevelAlpha);
    [y,x,nlogL,AVAR] = pdfcdffit(DistriName{i_},X,param,confidenceLevelAlpha,'pdf');
    alldistri_result(i_).param = param;
    alldistri_result(i_).nlogL = nlogL;
    alldistri_result(i_).paramstr = paramstr;
    alldistri_result(i_).pci = pci;
    alldistri_result(i_).AVAR = AVAR;
end
alldistri_result(1).data = X;
%mejor ajuste and loglikelihood
[aux,auxi] = max(disfitlike(DistriName,X,alldistri_result,confidenceLevelAlpha));
disp(['The best fit is: ' upper(char(DistriName(auxi))) ' distribution, with loglikelihood = ' sprintf('%4.2f',aux)]);
plotname = {'Serie(X)','PDF(X)','CDF(X)','ProbabilityPlot','QQplot','PPplot'};
disfitanalysisplots(X,DistriName{auxi},alldistri_result(auxi).param,confidenceLevelAlpha,ndx,plotname,[],svopt,carpeta,'esp','Hs','m');

%Clasificacion SOM -SOM_3D_function-
carpeta = 'sample_clasification';if ~exist([pwd filesep carpeta],'dir'), mkdir(carpeta); end
t = 7; %t*t-number of cluster
Opciones = ameva_options(svopt,carpeta,'esp',{'Hs' 'Tp' 'Dir'},{'m' 's' char(186)});
[final,bmus,acierto_cien] = classification_function([excal_HsI excal_HsR excal_Dire],3,[t t],'SOM',Opciones);

%% Fichas MEL
carpeta = 'sample_fichas_plots';if ~exist([pwd filesep carpeta],'dir'), mkdir(carpeta); end
figure;iha_wind_rose(data_dow_sdr1.wave_dir,data_dow_sdr1.hs,'gca','idioma','esp','quad',1,'dtype','meteo','n',16,'lablegend','m');%Rosa
set(gcf,'Color','w');
amevasavefigure(gcf,carpeta,svopt,'rosa')
%DISTRIBUCION CONJUNTA -Hs vs Tp-
var_name = {'Hs','Tp'};var_unit = {'m','s'};
ttm = 20;Tlim = (0:2:ttm);
Hlim = tickupdate(data_dow_sdr1.hs);
isdire = false; fs = 10;
scattipo = 'AT';%OJO AT va lento, poner OC va rapido
figure;polycTp = scattertable(data_dow_sdr1.hs,data_dow_sdr1.tp,Hlim,[],Tlim,isdire,var_name,var_unit,fs,scattipo);
amevasavefigure(gcf,carpeta,svopt,'hsvstp')
%% Scatter sample
[statmoments,Hlim,rho] = statmoments_hlim_rho(excal_HsI,excal_HsR);
figure;ScatterQQ(excal_HsI,excal_HsR,Hlim,rho(1),statmoments,'Hs','m',{'I' 'R'}); %instrumental vs reanalisis
amevasavefigure(gcf,carpeta,svopt,'ScatterQQ')
figure;ScatterReaCal(excal_HsI,excal_HsR,[],[],Hlim,[],{'Hs' 'Hs'},{'m' 'm'},{'I' 'R'},7,'AT'); %instrumental vs reanalisis
amevasavefigure(gcf,carpeta,svopt,'ScatterReaCal')
figure;ScatterReaCal(excal_HsI,excal_HsR);
amevasavefigure(gcf,carpeta,svopt,'ScatterReaCal_default')

% REGIMEN MEDIO MENSUAL -rmgevajuste- Hs and Tp
carpeta = 'sample_rmgevajuste';if ~exist([pwd filesep carpeta],'dir'), mkdir(carpeta); end
ns = 12; maxtype = 'monthly';
var_name = {'Hs','Tp'};var_unit = {'m','s'};
confidenceLevel = 0.95; confidenceLevelAlpha=1-confidenceLevel; % 0.95 <-> 0.05

% Probability plot ajustado a distintas distribuciones
[param,paramstr,pci] = disfitparam('lognormal',data_dow_sdr1.hs,confidenceLevelAlpha);%parama.-parametros optimos
figure;siplot = gumbelprobplot('lognormal',data_dow_sdr1.hs,param,confidenceLevelAlpha,'esp',var_name(1),var_unit(1),gca);%lognormal,normal,weibull
amevasavefigure(gcf,carpeta,svopt,'rmgev_prob_plot_hs')

%% Solo IH - ihameva_clima % Regimen medio mensual de Hs y Tp
if not(isdeployed) && ihclima,
    ttm = 20;Tlim = (0:2:ttm);
    Hlim = tickupdate(data_dow_sdr1.hs);
    figure;[gevcoefhs,frechs] = rmgevajuste(data_dow_sdr1.hs,data_dow_sdr1.time,[Hlim(1) Hlim(end)],maxtype,ns,'esp',var_name{1},var_unit{1});
    amevasavefigure(gcf,carpeta,svopt,'rmgevtp')
    figure;[gevcoeftp,frectp] = rmgevajuste(data_dow_sdr1.tp,data_dow_sdr1.time,[Hlim(1) Hlim(end)],maxtype,ns,'esp',var_name{2},var_unit{2});
    amevasavefigure(gcf,carpeta,svopt,'rmgevtp')
end

%% GEV Maximos MENSUALES -extremos-
carpeta = 'sample_extremos';if ~exist([pwd filesep carpeta],'dir'), mkdir(carpeta); end
Opciones = ameva_options([0 0 0],carpeta,'esp',{'Hs'},{'m'},'Santander1');
maxtype = 'monthly';
[datemax_j,hsmax] = maximostempfunction(data_dow_sdr1.time,data_dow_sdr1.hs,maxtype,'maximum',0,1,5,'day');
datemax = yearlytimescale(datemax_j,'R');%escala anual
info(1) = 1;%aik Chi2 check (0 = aik, 1 = Chi2)
info(2) = 0;%intra_annual check
info(3) = 0;%long term(Trend) check
info(4) = 0;%inter-annual check
info(5) = 16;%max. armonicos X3(se usa en c++). En Matlab itermax = 100
info(6) = 0;%auto-armonicos
info(7) = 0;%arm. location
info(8) = 0;%arm. scale
info(9) = 0;%arm. shape
info(10) = 0;%force gamma0
criterio = 'Akaike'; if info(1) == 1, criterio = 'ProfLike'; end
confidenceLevel = 0.95; confidenceLevelAlpha=1-confidenceLevel; % 0.95 <-> 0.05

OPHr = FuncAutoAdjust(hsmax,datemax,[],[],info,maxtype,confidenceLevel,criterio,Opciones);
if sum(sqrt(OPHr.gradiente.*OPHr.gradiente))>10, %sino ajusta bien con el modelo fijo entonces hago autoajuste
    info(6) = 1; %autoajuste
    OPHr = FuncAutoAdjust(hsmax,datemax,[],[],info,maxtype,confidenceLevel,criterio,Opciones);
end; clear auxi;
nameficha = 'extremos';
pathresult = pwd;
figure;
[auxi,quanGEVX,stdDqX] = AgregateQuantiles(hsmax,datemax,confidenceLevelAlpha,[],[],OPHr,nameficha,'','',gca,true,pathresult);clear auxi;
title(['R',char(233),'gimen extremal ajustado a la funci',char(243),'n GEV'],'fontsize',11,'fontweight','b');
xlabel(['Periodos de retorno, T_r (a',char(241),'os)'],'fontsize',11,'fontweight','b');
ylabel([],'fontsize',11,'fontweight','b');
%         hlim_ = get(gca,'ylim');
%         if not(exist('hlim','var') || hlim_(2)>hlim(2)), hlim = [0 hlim_(2)];  end
%         set(gca,'ylim',hlim);htck = get(gca,'ytick');
%set(gcf,'Color','w');%set(h, 'Renderer', 'ZBuffer','InvertHardcopy','off');
amevasavefigure(gcf,carpeta,svopt,'gev_mes')
Opciones = ameva_options([0 0 0],carpeta,'esp',{'Hs'},{'m'},'Santander1');
[OPHr,OPHobj] = amevaFuncAutoAdjust.AutoAjuste(hsmax,datemax_j,[],[],info,confidenceLevel,Opciones);
figure; amevaAgregateQuantiles.aqPlot(OPHobj,OPHr,confidenceLevelAlpha,Opciones,gca,true,pwd);clear auxi;
amevasavefigure(gcf,carpeta,svopt,'gev_mes_new')

%GEV Maximos ANUALES
maxtype = 'annual';
%OJO datemax_ tiene que ser igual o muy parecido datemax
[datemax_j,hsmax] = maximostempfunction(data_gow.time,data_gow.hs,maxtype,'maximum',0,1,5,'day');
datemax = yearlytimescale(datemax_j,'R');%escala anual
info = [1 0 0 0 16 0 0 0 0 0];
criterio = 'Akaike'; if info(1) == 1, criterio = 'ProfLike'; end
confidenceLevel = 0.95; confidenceLevelAlpha=1-confidenceLevel; % 0.95 <-> 0.05
OPHr = FuncAutoAdjust(hsmax,datemax,[],[],info,maxtype,confidenceLevel,criterio,Opciones);
figure; amevaAgregateQuantilesAnuales.aAQA(hsmax,datemax,confidenceLevelAlpha,[],[],OPHr,'CityName','Hs','m',gca);
title(['R',char(233),'gimen extremal ajustado a la funci',char(243),'n GEV'],'fontsize',11,'fontweight','b');
xlabel(['Periodos de retorno, T_r (a',char(241),'os)'],'fontsize',11,'fontweight','b');
ylabel('Hs (m)','fontsize',11,'fontweight','b');
amevasavefigure(gcf,carpeta,svopt,'gev_anual');
%Un hs para un periodo de retorno dado: Ts = 100;
[quanaggr,stdup,stdlo] = amevaAgregateQuantilesAnuales.AQA(100,hsmax,datemax,confidenceLevelAlpha,[],[],OPHr);
% Ej new
Opciones = ameva_options([0 0 0],carpeta,'esp',{'Hs'},{'m'},'Santander1');
[OPHr,OPHobj] = amevaFuncAutoAdjust.AutoAjuste(hsmax,datemax_j,[],[],info,confidenceLevel,Opciones);
figure; amevaAgregateQuantiles.aqPlot(OPHobj,OPHr,confidenceLevelAlpha,Opciones,gca,true,pwd);clear auxi;
amevasavefigure(gcf,carpeta,svopt,'gev_anual_new');
[quanaggr_n,stdup_n,stdlo_n] = amevaAgregateQuantiles.aqAnual(100,OPHobj,OPHr,confidenceLevelAlpha);


%% Test POT Pareto-Poisson
carpeta = 'sample_pareto_poisson';if ~exist([pwd filesep carpeta],'dir'), mkdir(carpeta); end
global neps0; neps0 = 1;
Opciones = ameva_options([0 0 0],carpeta,'esp',{'Hs'},{'m'},'Guayaquil');
confidenceLevel = 0.95; confidenceLevelAlpha=1-confidenceLevel; % 0.95 <-> 0.05
% 1)
ny = expot_datemax(end);%ny numero de anos neps0 = 1;
[lambda0,sigma0,gamma0,loglikeobj,gradiente,Hessiano] = OptiParamHessianPOTsimple(expot_Hsmax,expot_umbral,ny);
[quan95,stdDq,mut,psit,epst,damax,xamax,invI0] = potpreplot(expot_datemax,expot_Hsmax,lambda0,sigma0,gamma0,confidenceLevelAlpha,Hessiano,loglikeobj,gradiente,expot_umbral);
figure;
[Ts,quanaggr,stdlo,stdup] = amevaAgregateQuantilesPOT(xamax,expot_umbral,confidenceLevelAlpha,lambda0,sigma0,gamma0,invI0,ny,Opciones);
amevasavefigure(gcf,carpeta,svopt,'amevaAgregateQuantilesPOT_G');
% 2)
% Seleccion de maximos POT
% TimeVec = datevec(data_gow.time);
% timedata_aux = [data_gow.time TimeVec(:,1) TimeVec(:,2) TimeVec(:,3) TimeVec(:,4) data_gow.hs];
%[xumbo,tumbo] = SeleccionaMaximosIndepPOT(timedata_aux,umbral,3);
%figure;plot(tumb,xumb,'or',tumbo(:,1),xumbo,'+b');
upc = 99.5;
umbral = prctile(data_gow.hs,upc);%calculo el umbral
[tumb,xumb,Dmax] = maximostempfunction(data_gow.time,data_gow.hs,'','pot',[],1,3,'day',[],[],umbral);%maximos anuales COMPROBAR
[tumb,ny] = yearlytimescale(tumb,'R');%transformo el tumb a anos y encuentro el nuemro de anos years
[lambda0,sigma0,gamma0,loglikeobj,gradiente,Hessiano] = OptiParamHessianPOTsimple(xumb,umbral,ny);
[quan95,stdDq,mut,psit,epst,damax,xamax,invI0] = potpreplot(tumb,xumb,lambda0,sigma0,gamma0,confidenceLevelAlpha,Hessiano,loglikeobj,gradiente,umbral);
figure;
[Ts,quanaggr,stdlo,stdup] = amevaAgregateQuantilesPOT(xamax,umbral,confidenceLevelAlpha,lambda0,sigma0,gamma0,invI0,ny,Opciones,gca,upc);
amevasavefigure(gcf,carpeta,svopt,'amevaAgregateQuantilesPOT');

%% test fichas:
% 1 conociendo la malla
forcing = 'SW_NCEP'; mesh = 'Iberia';
MallaDOW = 'D0131';
allmesh = [1 0 0 0 0 0 0 0 0 0 0 0];%
%allmesh = ones(1,12);
name_FBP.nameficha = 'Santander_1';
name_FBP.gd_datagd = MallaDOW;
name_FBP.gd_prof = num2str(data_dow_sdr1.bat);
costLineResol = 'I'; %Cost Line resolution: 'H' - high, 'I' -medium resolution
if not(isdeployed) && ihclima
    rmall_fig(data_dow_sdr1,data_dow_sdr1.lat,data_dow_sdr1.lon,data_dow_sdr1.bat,forcing,mesh,[],costLineResol,MallaDOW,allmesh,name_FBP);
end

% 2 datos con lat, lon (ficha 9,10 -> extremos gev y pot)
allmesh = [0 0 0 0 0 0 0 0 1 1 0 0];%ficha 9,10
name_FBP.nameficha = 'GevPotFichas';
name_FBP.autoArmonicos=false;% false->autoarmonicos/true->armonicos 3,3,1 locatio,scale,shape
name_FBP.gd_prof=''; %Profundidad: Es un string
if not(isdeployed) && ihclima
    rmall_fig(data_dow_sdr1,data_dow_sdr1.lat,data_dow_sdr1.lon,[],'','',[],'','',allmesh,name_FBP);
end
%% Extremos simples
% 1
extremos_simple(data_gow,'gev');
% 2 con opciones de entrada
Opciones = ameva_options();
Opciones.Ts = [1.1 2:1:9 10:10:90 100 150 200];
setGamma0 = 0; %0/1/2 %Forzar Gamma0. 0-Auto, 1-Forzar Gamma0=0, 2-Forzar Gamma0~=0
extremos_simple(data_gow,'gevpot',[],[],Opciones,setGamma0);

%% Modelo Hetersocedastico [ X(t) ]
ecoinstall = amevaTheseToolboxesInstalled('Econometrics Toolbox');
km = bin2dec([num2str(1) num2str(1) num2str(1) num2str(1)]);
%mo = dec2bin(km,4);%   Defino el modelo binario
%model = str2num(mo(1:4)');
conf = 0.90;
confidenceLevelOutlier = 0.95;
TimeR = yearlytimescale(data_dow_sdr1.time,'R');%Cambiar la escala temporal para aplicar al modelo
[idx,pnew,model,pvalue,fval,exitflag,output,lambda,gradiente,Hessiano,residuo,rN,fmunew,fsigmanew,...
    Omega,upperp,lowerp,uppermean,lowermean,upperpredict,lowerpredict] = outlierfilterRNoCteC3E (TimeR,data_dow_sdr1.hs,km,conf,confidenceLevelOutlier);%0.9,0.95
%   Kolmogorov-Smirnov test sobre normalidad del residuo
[hr pvaluer] = kstest((residuo-mean(residuo))/std(residuo),[],1-conf);
%   Si hr = 0 el residuo es normal. pvalue>1-alpha
%   Si hr = 1 se rechaza la hipotesis de que sea normal el residuo.
%   pvalue<1-alpha
if hr
    ksteststr = {'Model error Kolmogorov-Smirnov test:', ...
        'the assumption of normality is rejected',['P-value = ' num2str(pvaluer)]};
else
    ksteststr = {'Model error Kolmogorov-Smirnov test:', ...
        'the assumption of normality is accepted',['P-value = ' num2str(pvaluer)]};
end
Hr = [];pValuer = [];Qstatr = [];CriticalValuer = [];
if ecoinstall, [Hr,pValuer,Qstatr,CriticalValuer] = lbqtest(residuo,[1 2 3 4 5],1-conf); end
%save plots
modelstr = ['$$\renewcommand{\arraystretch}{1.4}\begin{array}{rcccccl}\mu(t) &=&p_1 ' pnew(1) ...
    ' \\ \sigma(t) &=& p_4 ' pnew(4) ' \end{array} $$'];
frec{1} = 'P_1';frec{2} = 'P_2';frec{3} = 'P_3';frec{4} = 'P_4';frec{5} = 'P_3';frec{6} = 'P_4';frec{7} = 'P_4';
tpstring = {'Results' 'Autocorrelation' 'Partial Autocorrelation' 'Residue' 'Residual normplot'};
cityname = 'Santander';var_name = {'Hs','Tp'};var_unit = {'m','s'};
carpeta = 'sample_HeteroscedasticModel';if ~exist([pwd filesep carpeta],'dir'), mkdir(carpeta); end
heteroscedasticplots(data_dow_sdr1.hs,data_dow_sdr1.time,idx,fmunew,uppermean,lowermean,upperpredict,lowerpredict,...
    residuo,lowerp,pnew,upperp,Hr,pValuer,Qstatr,modelstr,frec,tpstring,svopt,carpeta,cityname,var_name,var_unit);

%Ejemplo 2 Heterocedatico Modelo X and Y (excal_HsI , excal_HsR)
km = bin2dec([num2str(1) num2str(1) num2str(1) num2str(1)]);
conf = 0.90;
confidenceLevelOutlier = 0.95;
aux = sortrows([excal_HsI excal_HsR],1);% Para los plots ordeno respecto a la primera variable
excal_HsI = aux(:,1);
excal_HsR = aux(:,2);
[idx,pnew,model,pvalue,fval,exitflag,output,lambda,gradiente,Hessiano,residuo,rN,fmunew,fsigmanew,...
    Omega,upperp,lowerp,uppermean,lowermean,upperpredict,lowerpredict] = outlierfilterRNoCteC3E (excal_HsI,excal_HsR,km,conf,confidenceLevelOutlier);%0.9,0.95


disp([datestr(now,'yyyymmddHHMMSS') ' Exito!']);
if exist('tStart','var') && not(isempty(tStart))
    tEnd = toc(tStart);
    fprintf('%d minutes and %f seconds\n',floor(tEnd/60),rem(tEnd,60));
end

%% Meshselection: Calibrar + Ficha de calibration  + Ficha de filtro de outliers
if ihclima
    forcing = 'CFS';meshes = 'Canarias'; latcn = 28.875; loncn = -16;
    sel_mode = 'circle';    %sel_mode: 'circle' -> [vc, radio], 'rectangle' -> [lat1 lon1 lat2 lon2]
    sel_par = [0 .5]; %sel_par: [0 1.0], [1.9 1.0]
    % forcing = 'NCEP';meshes = 'Caribe';latcn = -2.75; loncn = -81.25; sel_mode = 'circle';  sel_par = [1.8 1.0]; %Ecuador
    pb2 = [];%vector auxiliar solo en el caso de vc
    if strcmp(sel_mode,'circle') && sel_par(1)>0, %solo si es vc selection
        [satpairsdb,latb,lonb,pb,pb2] = getSatellitePairsVCR_ToPlot(latcn,loncn,sel_mode,sel_par,forcing,meshes);%solo para pintar y vc
    end  %solo si es vc selection and plots
    satpairs = getSatellitePairsVCR(latcn,loncn,forcing,meshes,sel_mode,sel_par,pb2);
    data_gow = gow.getTemporalSerie(latcn,loncn,forcing,meshes);
    % Ficha de calibration y de filtro de outliers
    carpeta = 'sample_calibration_outlier_fichas';if ~exist([pwd filesep carpeta],'dir'), mkdir(carpeta); end
    var_name = 'Hs';var_unit = 'm';idioma = 'esp';svopt = [0 1 0];
    isfilteron = true;ID = 6;%filtro configuration
    % aqui se calcula la calibracion, se le aplica al punto y se pinta la ficha de calibracion y de filtro de outlier (si se ha seleccionado)
    meshCalibrateFunction(satpairs,data_gow,forcing,meshes,sel_mode,sel_par,isfilteron,ID,svopt,carpeta,idioma,var_name,var_unit);
    %% Ficha de calibracion con los resultados
    carpeta = 'sample_calibration_outlier_fichas';if ~exist([pwd filesep carpeta],'dir'), mkdir(carpeta); end
    % load data_gow xxxx;%From a valid path result file.mat %Load you data, by
    if exist(carpeta,'dir')==7 && exist(fullfile(carpeta,'data_gow[28.875][-16].mat'),'file')
        load(fullfile(carpeta,'data_gow[28.875][-16].mat'));
        calibraciongowfig(data_gow_cal.lat,data_gow_cal.lon,output,latpairs,lonpairs,data_gow_info,forcing,meshes,[0 1 0],carpeta,'esp','Hs','m');
    end
end

%% Gev Mis pruebas OLD vs NEW
if not(isdeployed) && ihclima
    clear all; clc; load ameva,
    info = [1 0 0 0 12 1 0 0 0 0];%2 Forzamos no gumbel para usar cov. en forma
    criterio = 'Akaike'; if info(1) == 1, criterio = 'ProfLike'; end
    confidenceLevel = 0.95; confidenceLevelAlpha=1-confidenceLevel; % 0.95 <-> 0.05
    Opciones = ameva_options(); maxtype = 'annual'; % daily monthly annual
    [datemaxA,HsmaxA] = maximostempfunction(exgev_datemax_j,exgev_Hsmax,maxtype,'maximum',0,1,5,'day');
    datemaxA = yearlytimescale(datemaxA,'R'); %escala anual
    Ts = 10;
    tic
    OPHr = FuncAutoAdjust(HsmaxA,datemaxA,[],[],info,maxtype,confidenceLevel,criterio,Opciones);
    toc
    [quanaggrOld,stdupOld,stdloOld] = amevaAgregateQuantilesAnuales.AQA(Ts,HsmaxA,datemaxA,confidenceLevelAlpha,[],[],OPHr);%Solo max Anual
    
    tic
    [OPHr,OPHobj] = amevaFuncAutoAdjust.AutoAjuste(HsmaxA,datemaxA,[],[],info,confidenceLevel,Opciones);
    toc
    [quanaggrNew,stdupNew,stdloNew] = amevaAgregateQuantiles.aqAnual(Ts,OPHobj,OPHr,confidenceLevelAlpha);%Solo max Anual
end