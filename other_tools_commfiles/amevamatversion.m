function amevamatversion
%   Comprueba la version de matlab y los problemas que da con ameva.

disp(['MATLAB Version: ' version]);
disp(['Computer type: ' computer]);
disp(['Computer architecture: ' computer('arch')]);

end