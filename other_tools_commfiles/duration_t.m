function [signal] = duration_t(y,cntar1,Y,Tipo)
%   DURATION_T (duration_t.m) Busca los grupos de unos en un vector de ceros y unos.
%   y devuleve la cantidad de unos y las posiciones de inicio y fin
%   dentro de cada grupo de unos, dentro del vector.
%   cntar1 .- true/false, por defecto -true- cuenta un solo valor como un
%   paso, en caso contrario para contar un paso almenos necesitas dos unos: Obligatorio
%   Si ademas se introduce la serie completa, en el resultado puede
%   obtenerse el valor del maximo, la posicion del maximo y la media en las
%   columnas 4, 5, 6 respectivamente.
%
%   Ouptup: Signal(:,1:6) <-> 'N datos','posIni','posFin','Max','posMax','Media'
%   Ej. 1
%       Y = [0 1 2 3 2 1 0 0 3 6 9 0];
%       dvec = duration_t( Y > 0.1, true, Y)
%   Ej. 2
%       Y = 10*randn(1000000,1);
%       dvec = duration_t( Y > 0.1, true, Y);
% -------------------------------------------------------------------------
%   Fisica Aplicada(MEPE). Universidad de Cantabria
%   Santander, Spain.
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows
%   02-02-2009 - The first distribution version
%   22-03-2013 - Old distribution version
%   5-11-2015 - Last distribution version (OJO OJO Comprobar otros casos OC)

if ~exist('Tipo','var') || isempty(Tipo), Tipo='Jrg'; end
error(nargchk(2,4,nargin));% errror numero de argumentos min dos argumentos
validateattributes(y,{'logical'},{'vector'});
validateattributes(cntar1,{'logical'},{'scalar'});

if strcmpi(Tipo,'Jrg'),
    xaux=[0;y(:);0];
    starts = find(diff(xaux) == 1);
    ends = find(diff(xaux) == -1)-1;
    if cntar1
        dur = ends-starts+1;
        signal = [dur,starts,ends];
    else
        dur = ends-starts;
        ok = dur~=0;
        signal = [dur(ok),starts(ok),ends(ok)];
    end
else
    N = length(y);
    signal = zeros(length(y),3);
    k2=0;
    k=1;
    while k <= N && N>=2
        ctr=1;
        if y(k)==0,
            k=k+1;
            continue
        else
            if k<=1 && k2>0,
                continue;
            end;
            j=k+1;
            while j<=N && y(j)==1
                ctr=ctr+1;
                if j<N
                    j=j+1;
                else
                    j=j+1;
                    break
                end
            end
            k=j;
        end
        %para contar o no un solo dato, por defecto cuenta un solo dato 1 paso (true)
        if cntar1,
            k2=k2+1;
            signal(k2,1)=(ctr);
            signal(k2,2) = k-ctr;
            signal(k2,3) = k-1;
        else
            if ctr>1,
                k2=k2+1;
                signal(k2,1)=(ctr-1);
                signal(k2,2) = k-ctr;
                signal(k2,3) = k-1;
            end
        end
        k=k+1;
    end
    signal(k2+1:end,:) = [];
end

%Ecuentro el maximo y la posicion del mismo
if exist('Y','var') && ~isempty(Y)
    aux=size(signal);
    for i=1:aux(1)
        pos=signal(i,2):signal(i,3);
        [ymax,posi]=max(Y(pos));
        signal(i,4)=ymax;
        signal(i,5)=pos(posi);
        signal(i,6)=mean(Y(pos));
    end
end

end