function [tiempo,datos]=inputfiletxt(nombre_archivo,nlabel,verlabel)

if ~exist('nlabel','var') || isempty(nlabel), nlabel=1; end;
if ~exist('verlabel','var') || isempty(verlabel), verlabel=false; end;

fid = fopen(nombre_archivo);
for kk=1:nlabel+1
    tline = fgetl(fid);
    if verlabel,
        disp(tline);
    end
end
i=1;
datos=[];
tiempo=[];
while ischar(tline)
    tiempo(i)=datenum(tline(1:19),'yyyy-mm-dd HH:MM:SS');
    datos(i,:)=str2num(tline(20:end));
    tline = fgetl(fid);
    i=i+1;
end
tiempo=tiempo(:);

fclose(fid);

end