function colorDiferente = colores_diferentes_Javi(t,pintaColorTF)
%% Funcion que encuentra el numero de colores mas diferentes estadisticamente mediante MDA
if not(exist('pintaColorTF','var')) || isempty(pintaColorTF), pintaColorTF = true; end
matriz=colorcube(100000);
colorDiferente = classification_function([matriz(:,1), matriz(:,2), matriz(:,3)],[],t,'MDA');
X = 1:t;
Y = 1:t;

if pintaColorTF,
    figure;
    axis on;
    grid on
    hold on;
    for i=1:length(X);
        color=colorDiferente(i,:);
        scatter(X(i),Y(i),50,color,'filled')
    end
    colormap(colorDiferente);
    colorbar;
end

end

