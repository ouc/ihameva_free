function [AnualTime,ny]=yearlytimescale(Time,tipo,Tyear0)
%   YEARLYTIMESCALE to convert a Time vector to yearly scale(normalize/not normalize):
%   1) tipo='R'.- Devuelve el tiempo estandarizado anual: no es formato de datos de matlab sino transformado a un vector en year:
%   T=[2011,6,30,23,59,0; 2012,6,30,23,59,0;];% las fechas [2011/6/30:23:59:0 2012/6/30:23:59:0]
%   Tj=[7.346849993055556e+005 7.350509993055556e+005];%En formato de datos de matlab (Tj=datenum(T))
%   Ty=[0.501035505657713 1.500372410296979];%'R' En este formato especial
%   El 0.5010... indica que este es el menor de los anos de la serie, en la que cada unidad es un ano.
%   Las herramientta GEV y POT, Pareto-Poisson usan este formato de tiempo
%   2) tipo='A'.- Devuelve el tiempo en escala anual sin normalizar. POR DEFECTO
%   3) tipo='N'.- Tiempo en escala anual: Standard (-normalizado- empieza en 0)
%   4) tipo='RR'.-Reverse, from normalize vector to julian time
% Examples 1
%   T=[2011,6,30,23,59,0; 2012,6,30,23,59,0; 2013,6,30,23,59,0];Tj=datenum(T);
%   [AnualTime,ny]=yearlytimescale(Tj,'R');
% Examples 2
%   [AnualTime,ny]=yearlytimescale(Tj,'A');
%   mod(AnualTime,1); %para verlos reducido a un ano.
%
% See also:
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   castello@unican.es
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   created with MATLAB ver.: 7.7.0.471 (R2011a) on Windows 7
%   01-12-2012 - The first distribution version
%   20-08-2014 - Last distribution version

if ~exist('tipo','var') || isempty(tipo), tipo='A'; end;
% Comprobamos el vector de tiempo
if isvector(Time)
    if ~isempty(find(isnan(Time),1)), error('The vector can not contain nans Time'); end
    if ~isempty(find(isinf(Time),1)), error('The vector can not contain inf Time'); end
    if not(isequal(Time,sort(Time))), error('The Time vector must be sorted in ascending');end
    if nargin<3
        if datenum(1000,1,1)>Time(1), disp(['Your julian Time vector starts in the date ',datestr(Time(1)),'?']);end
        if datenum(3000,1,1)<Time(end), disp(['Your julian Time vector ends in the date ',datestr(Time(end)),'?']);end
    else
        if ~exist('Tyear0','var'), Tyear0=2013; end
        if Time(1)<=0, disp(['Your Time vector starts in the date ',datestr(Time(1)),'?']);end
        if Time(end)>3000, disp(['Your Time vector ends in the date ',datestr(Time(end)),'?']);end
    end
end
if strcmpi(tipo,'R'),
	%R - Tiempo en escala anual: Roberto GEV y POT, Pareto-Poisson (-normalizado- empieza en 0)
    disp('R - Tiempo en escala anual: Roberto GEV y POT, Pareto-Poisson (-normalizado- empieza en 0)');
	posi=datevec(Time(1));
	AnualTime=zeros(size(Time));
	for i=1:length(Time),
	    AnualTime(i)=addtodate(Time(i),-posi(1),'year')/365.242190402;%paso a escala Anual(Roberto gev)
	end
	ny=ceil(AnualTime(end));%years % OJO OJO poner ceil a ny
elseif strcmpi(tipo,'N'),
	%N - Tiempo en escala anual: Standard (-normalizado- empieza en 0)
    disp('N - Tiempo en escala anual: Standard (-normalizado- empieza en 0)');
    TimeVec=datevec(Time);
    AnualTime=zeros(size(Time));
    Tyear0=min(TimeVec(:,1));
    for i=TimeVec(1,1):TimeVec(end,1),
        posi=find(TimeVec(:,1)==i);
        AnualTime(posi)=i-Tyear0+(Time(posi)-datenum(i,1,1,1,0,0))/(datenum(i+1,1,1,1,0,0)-datenum(i,1,1,1,0,0));
    end
    ny=(TimeVec(end,1)-TimeVec(1,1))+(Time(posi)-datenum(i,1,1,1,0,0))/(datenum(i+1,1,1,1,0,0)-datenum(i,1,1,1,0,0));
elseif strcmpi(tipo,'RR'), % From normalize vector to julian time
    disp('RR - From normalize vector to julian time). Funcion incompleta!');
%     if ~exist('Tyear0','var'), Tyear0=2013; end
%     TimeVec=floor(Time);
%     AnualTime=zeros(size(Time));
% 	if TimeVec(1) == 0, TimeVec=TimeVec+Tyear0; end
%     for i=TimeVec(1):TimeVec(end),
%         posi=TimeVec==i;
%         %  OJO OJO pasar los meses, dias, horas, min, sec correctamente OJO OJO
%         AnualTime(posi)=[(TimeVec(posi)-datenum(i,1,1,1,0,0))/(datenum(i+1,1,1,1,0,0)-datenum(i,1,1,1,0,0))];
%         AnualTime(posi)= (TimeVec(posi)-datenum(i,1,1,1,0,0))       /(datenum(i+1,1,1,1,0,0)-datenum(i,1,1,1,0,0));
%     end
%     ny=(TimeVec(end,1)-TimeVec(1,1))+(Time(posi)-datenum(i,1,1,1,0,0))/(datenum(i+1,1,1,1,0,0)-datenum(i,1,1,1,0,0));
else
	%A - Tiempo en escala anual: Standard (-Sin normalizar- empieza en el ano menor)
    %disp('A - Tiempo en escala anual: Standard (-Sin normalizar- empieza en el ano menor)');
    TimeVec=datevec(Time);
    AnualTime=zeros(size(Time));
    for i=TimeVec(1,1):TimeVec(end,1),
        posi=find(TimeVec(:,1)==i);
        AnualTime(posi)=i+(Time(posi)-datenum(i,1,1,1,0,0))/(datenum(i+1,1,1,1,0,0)-datenum(i,1,1,1,0,0));
    end
    ny=(TimeVec(end,1)-TimeVec(1,1))+(Time(posi)-datenum(i,1,1,1,0,0))/(datenum(i+1,1,1,1,0,0)-datenum(i,1,1,1,0,0));
end

end
