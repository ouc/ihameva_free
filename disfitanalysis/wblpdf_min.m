function p = wblpdf_min(x,A,B,C)
%WBLCDF_MIN

if nargin<4
    error('stats:wblcdfM:TooFewInputs Input argument X is undefined.');
end

% Force a zero for x menores que A.
x(x <= A) = 0;

try
    z = ((x-A)./B).^C;
    zn = ((x-A)./B).^(C-1);
catch
    error('stats:wblcdf:InputSizeMismatch Non-scalar arguments must match in size.');
end
p = (C/B)*exp(-z).*zn;
