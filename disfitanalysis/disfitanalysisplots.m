function [mypdftxt, Result2Plot] = disfitanalysisplots(X,DistributionName,param,alfa,ndx,tpstring,hsp,svopt,carpeta,idioma,var_name,var_unit,ax1,hf)
%Regimen Medio plot's
mypdftxt = '';
[ni,mi]=size(tpstring);
if mi<1, return; end;

%variables aux
Yaux=[]; newfig=false;

%struct name para guardar las salidas
Result2Plot = [];

texto=amevaclass.amevatextoidioma(idioma);

XLabel=labelsameva(var_name,var_unit);%Labels
N=length(X);

for i=1:mi,
    plotname=tpstring{i};
    if ~exist('ax1','var') || ~ishandle(ax1), h=figure('Visible','Off'); ax1 = axes('Parent',h); newfig=true; end
    switch plotname
        case 'Serie(X)',
            plot(ax1,X,'k');
            grid(ax1,'on');
            ylabel(ax1,XLabel);
            title(ax1,['Serie of ',var_name],'FontWeight','bold');
        case 'PDF(X)'
            [Yaux,Xxx]=hist_n(X,ndx);%empirical histogram
            [Xyfit,Xxfit]=pdfcdffit(DistributionName,X,param,alfa,'pdf');%fit pdf
            Result2Plot = struct('Yaux',Yaux,'Xxx',Xxx,'Xyfit',Xyfit,'Xxfit',Xxfit);
            if ~isempty(Xxx) && ~isempty(Xxfit) && ~strcmp(DistributionName,'poisson')
                bar(ax1,Xxx,Yaux,1.0,'FaceColor',0.75*[1 1 1],'EdgeColor',0.5*[1 1 1]);hold(ax1,'on');
                plot(ax1,Xxfit,Xyfit,'k','LineWidth',1.8);hold(ax1,'off');
                legend(ax1,texto('empData'),[DistributionName,' ',lower(texto('fit'))]);
            else
                [Xxx,Yaux]=unique(X);%empirical histogram PARETO DISTRIBUTION
                bar(ax1,Xxx,Yaux,1.0,'BarWidth',1,'FaceColor',0.75*[1 1 1],'EdgeColor',0.5*[1 1 1]);
                legend(ax1,texto('empData'));
            end
            xlim(ax1,[Xxx(1),Xxx(end)]);
            grid(ax1,'on');
            ylabel(ax1,texto('freq'));
            xlabel(ax1,XLabel);
            title(ax1,[texto('probDensity'),' ',texto('of'),' ',var_name],'FontWeight','bold');
            str2(1)={[texto('moment'),' ',var_name]};
            str2(2)={['\mu = ',sprintf('%1.2f',mean(X))]};
            str2(3)={['\sigma = ',sprintf('%1.2f',std(X))]};
            str2(4)={['\chi = ',sprintf('%1.2f',skewness(X))]};
            str2(5)={['\beta = ',sprintf('%1.2f',kurtosis(X))]};
            mypdftxt = text(max(Xxx)-0.01*max(Xxx),mean(get(ax1,'ylim')),str2,'HorizontalAlignment',...
                'right','BackgroundColor',[.7 .9 .7],'EdgeColor','k','Parent',ax1);%
        case 'CDF(X)' %Funcion de distribucion
            Xq(1)=quantile(X,0.05);
            Xq(2)=quantile(X,0.25);
            Xq(3)=quantile(X,0.50);
            Xq(4)=quantile(X,0.75);
            Xq(5)=quantile(X,0.95);
            [aa,lags]=ecdf(X);%empirical comulative distribution function
            [Yaux,Xaux]=pdfcdffit(DistributionName,X,param,alfa,'cdf');%fit cdf
            Result2Plot = struct('aa',aa,'lags',lags,'Yaux',Yaux,'Xaux',Xaux);
            if ~isempty(Yaux),
                plot(ax1,lags,aa,'--b','LineWidth',1.5,'MarkerSize',1.8); hold(ax1,'on');
                plot(ax1,Xaux,Yaux,'-r','LineWidth',2);
                line([min(get(ax1,'Xlim')) Xq(1) Xq(1) Xq(1)],[0.05 0.05 0 0.05],'Color',[0 0 .0],'LineStyle','--');
                line([min(get(ax1,'Xlim')) Xq(2) Xq(2) Xq(2)],[0.25 0.25 0 0.25],'Color',[0 0 .1],'LineStyle','--');
                line([min(get(ax1,'Xlim')) Xq(3) Xq(3) Xq(3)],[0.50 0.50 0 0.50],'Color',[0 0 .2],'LineStyle','--');
                line([min(get(ax1,'Xlim')) Xq(4) Xq(4) Xq(4)],[0.75 0.75 0 0.75],'Color',[0 0 .3],'LineStyle','--');
                line([min(get(ax1,'Xlim')) Xq(5) Xq(5) Xq(5)],[0.95 0.95 0 0.95],'Color',[0 0 .4],'LineStyle','--');hold(ax1,'off');
                legend(ax1,'empirical data',[DistributionName ' fit'],['Percentile ( 5%)=',sprintf('%1.2f',Xq(1))],...
                    ['Percentile (25%)=',sprintf('%1.2f',Xq(2))],['Percentile (50%)=',sprintf('%1.2f',Xq(3))], ...
                    ['Percentile (75%)=',sprintf('%1.2f',Xq(4))],['Percentile (95%)=',sprintf('%1.2f',Xq(5))],'Location','SouthEast');
                axis(ax1,'normal');box on;
                grid(ax1,'on');
                ylabel(ax1,'Probability');
                xlabel(ax1,XLabel);
                title(ax1,['Cumulative Distribution Function of ',var_name],'FontWeight','bold');
            end
        case 'QQplot'
            [Yaux,Xaux,my,mx]=pdfcdffit(DistributionName,X,param,alfa,'qqplot');
            Result2Plot = struct('Yaux',Yaux,'Xaux',Xaux,'my',my,'mx',mx);
            if ~isempty(Yaux)
                plot(ax1,Xaux,Yaux,'bo','MarkerSize',1.8);
                line(mx,my,'LineStyle','-.','Marker','none','Color','k');
                grid(ax1,'on');
                ylabel(ax1,[DistributionName ' fit']);
                xlabel(ax1,'Empirical data');
                title(ax1,['QQ Plot of ',var_name],'FontWeight','bold');
            end
        case 'PPplot'
            Yaux=pdfcdffit(DistributionName,sort(X),param,alfa,'ppplot');
            Xaux=((1:N)-0.5)/(N+1);
            Result2Plot = struct('Yaux',Yaux,'Xaux',Xaux);
            if ~isempty(Yaux)
                plot(ax1,Xaux,Yaux,'bo','MarkerSize',1.8);
                line([0 1],[0 1],'LineStyle','-.','Marker','none','Color','k');
                grid(ax1,'on');
                ylabel(ax1,[DistributionName ' fit']);
                xlabel(ax1,'Empirical data');
                title(ax1,['PP Plot of ',var_name],'FontWeight','bold');
            end
        case 'ProbabilityPlot'
            [Yaux,Result2Plot]=gumbelprobplot(DistributionName,X,param,alfa,idioma,var_name,var_unit,ax1);
        otherwise
        disp(['Warnig!.Not exist this plot case ',plotname]);
    end
    
    if sum(svopt)<=0, return; end % OJO OJO OJO nov/2015
    
    %save the current plot
    savename=[plotname,'_',DistributionName,'_',var_name];
    
    if newfig
        %save the current plot
        amevasavefigure(h,carpeta,svopt,savename)  
    else
        %NO se guardan plot sino existen datos o es tipo data
        if strcmp(plotname,'Serie(X)') || isempty(Yaux)
            return;
        end            
        %save only for pdf exist un bag en saveas para hacer el grafico de la pdf provisionalmente asi 22/09
        if strcmp(plotname,'PDF(X)') && ~isempty(Xxx) && ~isempty(Xxfit)
            h=figure('Visible','off');
            if ~isempty(Xxx) && ~isempty(Xxfit) && ~strcmp(DistributionName,'poisson')
                bar(gca,Xxx,Yaux,1.0,'b','FaceColor','r','EdgeColor','w');hold(gca,'on');
                plot(gca,Xxfit,Xyfit,'r','LineWidth',1.5);hold(gca,'off');
                legend(gca,'empirical data',[DistributionName,' fit']);
            else
                [Xxx,Yaux]=unique(X);%empirical histogram PARETO DISTRIBUTION
                bar(gca,Xxx,Yaux,1.0,'b','FaceColor','r','EdgeColor','w');
                legend(gca,'empirical data');
            end
            xlim(gca,[Xxx(1),Xxx(end)]);
            grid(gca,'on');
            ylabel(gca,'Frequency');
            xlabel(gca,XLabel);
            title(gca,['Probability Density Function of ',var_name],'FontWeight','bold');
            str2(1)={['Moments of ',var_name]};
            str2(2)={['\mu = ',sprintf('%1.2f',mean(X))]};
            str2(3)={['\sigma = ',sprintf('%1.2f',std(X))]};
            str2(4)={['\chi = ',sprintf('%1.2f',skewness(X))]};
            str2(5)={['\beta = ',sprintf('%1.2f',kurtosis(X))]};
            text(max(Xxx)-0.01*max(Xxx),mean(get(gca,'ylim')),str2,'HorizontalAlignment',...
                'right','BackgroundColor',[.7 .9 .7],'EdgeColor','k','Parent',gca);
            
            %save the current plot
            amevasavefigure(h,carpeta,svopt,savename)           
            return;
        end
            h=figure('Visible','off');
            cpaxis=findall(hf,'Type','axes');
            if exist('hsp','var') && ~isempty(findobj(hsp)),  cpaxis(find(cpaxis==hsp))=[]; end
            for j=1:length(cpaxis)
                copyobj(cpaxis(length(cpaxis)+1-j),h);
            end
            if strcmp(plotname,'CDF(X)')
                h0=findall(h,'Type','axes');
                set(h0(1),'Position',[0.55    0.16   0.3482    0.233]);
                set(h0(2),'Position',[0.13    0.11    0.775    0.815]);
            else
                set(gca,'Position',[0.13    0.11    0.775    0.815]);
            end

            %save the current plot
            amevasavefigure(h,carpeta,svopt,savename)        
    end
end

end