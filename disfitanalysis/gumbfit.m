

function [phat, pci] = gumbfit(data,alpha,trunc)
%GUMBFIT Parameter estimates and confidence intervals for Gumbel data.
%
% CALL: [phat, pci] = gumbfit(data,alpha, trunc) 
%
%   phat = the maximum likelihood estimates of the  
%          parameters of the Gumbel distribution given the data.
%   pci  = (1-alpha)*100 percent confidence intervals of the estimated parameters.
%   data = data vector or matrix
%  alpha = confidense level constant (default 0.05)
%  trunc = 0  regular gumbel distribution (default)
%          1  truncated gumbel distribution 
% 
%   Gumbel CDF  is given by :                           
%      F(x) = exp(-exp(-(x-b)/a)    -inf < x < inf,  a>0
%    or the truncated
%      F(x) = [exp(-exp(-(x-b)/a)) -exp(-exp(b/a)) ]/(1-exp(-exp(b/a)))
%      0 < x < inf, a>0     
%
% See also: gumblike, gumbpdf, gumbcdf, gumbinv, gumbpdf

%   Reference:
%      [1]  Michel K. Ochi,
%       OCEAN TECHNOLOGY series 6
%      "OCEAN WAVES, The stochastic approach", Cambridge
%      1998 p. 167-170.

%  tested on: matlab 5.2
% history
% revised pab 8.11.1999
% updated header info
%   Per A. Brodtkorb 17.10.98

if nargin < 3 | isempty(trunc),
    trunc=0; % default value is not truncated
end

if nargin < 2 | isempty(alpha),
    alpha = 0.05;
end
p_int = [alpha/2; 1-alpha/2];
[m,n] = size(data);

if m == 1
  m = n;
  n = 1;
  data = data(:);
end

if 0,
  eprob = [0.5./m:1./m:(m - 0.5)./m]';
  w  = log(log(1./(1-eprob)));
  z  = sort(log(data));
  c  = polyfit(z,w,1);
  % this is not correct
  pinit  = [exp(c(2)./c(1)) c(1)];
else
  tmp=std(data)*sqrt(6)/pi ;
  pinit=[ tmp  mean(data)- tmp*0.5772 ];
end

%phat = fmins('gumblike',pinit,[],[],data,trunc);% MLE
phat = fminsearch(@(phat)gumblike(phat,data),pinit);%OJO Yo matlab 2008b

if nargout == 2
   [logL,info]=gumblike(phat,data,trunc);
   % MLE are asymptotically normal with variance 
    % the inverted observed Fisher info
   sigma = sqrt(diag(info));
   pci = norminv([p_int p_int],[phat; phat],[sigma';sigma']);
end

