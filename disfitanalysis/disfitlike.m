function nlogL=disfitlike(DistributionName,data,param,alfa)

if ~exist('alfa','var'), alfa=0.05; end

if iscell(DistributionName)
    nlogL=zeros(1,length(DistributionName));
    for i=1:length(DistributionName)
        nlogL(i)=LogLikehoodDistriName(DistributionName{i},data,param(i).param,alfa);
    end    
else
    nlogL=LogLikehoodDistriName(DistributionName,data,param,alfa);
end


%si existen inf los pongo como nans
nlogL(find(nlogL==inf))=NaN;

function nlog=LogLikehoodDistriName(DistributionName,data,param,alfa)
switch DistributionName
    case 'normal'
        nlog=normlike(param,data);
    case 'lognormal'
        nlog=lognlike(param,data);
    case 'weibull'
        nlog=wbllike(param,data);
    case 'weibullmin'
        nlog=-sum(log(wblpdf_min(data,param(1),param(2),param(3))));
    case 'weibullmax'
        nlog=-sum(log(wblpdf_max(data,param(1),param(2),param(3))));
    case 'gumbel'
        nlog=gevlike([0,param(1),param(2)],data);
    case 'extreme value'
        nlog=gevlike(param,data);
    case 'pareto'
        nlog=gplike(param,data);
    case 'exponential'
        nlog=explike(param,data);
    case 'rayleigh'
        nlog=-sum(log(raylpdf(data,param)));
    case 'gamma'
        nlog=gamlike(param,data);
    case 'beta'
        nlog=betalike(param,data);
    case 'logistic'
        nlog=-sum(log(logisticpdf(data,param(1),param(2))));
    case 't-student'
        nlog=-sum(log(pdf('tlocationscale',data,param(1),param(2),param(3))));
    case 'poisson'
        %nlog=-sum(log(poisspdf(data,poissfit(data,alfa))));
        nlog=NaN;
    otherwise
        nlog=NaN;
end
nlog=-nlog;

end

end