function x = gumbinv(p,a,b,trunc)
%GUMBINV Inverse of the Gumbell cumulative distribution function (cdf).
%
%  CALL:  x = gumbinv(p,a,b,trunc) 
%
%   x    = Inverse Gumbel evaluated at p
%  a, b  = parameters of the Gumbel distribution.
%  trunc = 0  regular gumbel distribution (default)
%          1  truncated gumbel distribution 
%
%    Gumbel cdf  is given by :    
%            F(x) = exp(-exp(-(x-b)/a)    -inf < x < inf,  a>0
%    or the truncated
%           F(x) = [exp(-exp(-(x-b)/a)) -exp(-exp(b/a)) ]/(1-exp(-exp(b/a)))
%       0 < x < inf, a>0     
%
%   The size of X is the common size of the input arguments. A scalar input
%   functions as a constant matrix of the same size as the other inputs.    

%   Reference:
%      [1]  Michel K. Ochi,
%       OCEAN TECHNOLOGY series 6
%      "OCEAN WAVES, The stochastic approach", Cambridge
%      1998 p. 167-170.


%  tested on: matlab 5.2
% history
% revised pab 8.11.1999
% updated header info
%   Per A. Brodtkorb 17.10.98

if nargin < 3,    
    error('Requires three input arguments.'); 
end
if nargin < 4 | isempty(trunc),
    trunc=0; % default value is not truncated
end
[errorcode p a b] = distchck(3,p,a,b);

if errorcode > 0
    error('Requires non-scalar arguments to match in size.');
end

%   Initialize X to zero.
x = zeros(size(p));

%   Return NaN when the arguments are outside their respective limits.
k1 = find(a <= 0  | p < 0 | p > 1);
if any(k1)
    tmp   = NaN;
    x(k1) = tmp(ones(size(k1)));
end

k = find(a > 0 &  p > 0 & p < 1);
if any(k)
  if trunc,
    tmp=exp(-exp(b(k)./a(k)));
    x(k) =-a(k).* log(-log((1-tmp).* p(k) +tmp) ) + b(k); 
  else
    x(k) =-a(k).* log(-log( p(k)) ) + b(k); 
  end
end

if trunc,
  k2 = find(p == 1 & a > 0 );
else
  k2 = find(((p == 1)|(p==0)) & a > 0 );
end
if any(k2)
  tmp   = Inf;
  x(k2) = tmp(ones(size(k2)));
end
  
