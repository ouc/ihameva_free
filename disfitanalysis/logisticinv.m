function t = logisticinv(Y,U,S)
%LOGISTICCDF Logistic culmulative density function.
%   Y = Logisticcdf(t,U,S) returns the CDF of the Logistic Distribution
%   with location parameter U and scale parameter S, evaluated at the
%   values in t.  
%   
%   The size of Y is the common size of the input arguments.  A scalar input
%   functions as a constant matrix of the same size as the other inputs.  Each
%   element of Y contains the probability density evaluated at the
%   corresponding elements of the inputs.

if nargin<1
    error('logisticcdf:Insufficient number of parameters.');
end
if nargin < 2
    U = 0;
end
if nargin < 3
    S = 1;
end

if any(Y<0) || any(Y>1), error('Y must be between 0 and 1'); end

% Return NaN for out of range parameters.
S(S <= 0) = NaN;

try
    for i=1:length(Y)
        t(i) = fzero(@(x) logisticcdf(x,U,S)-Y(i),0.5); 
    end
catch ME
    error('stats:logisticcdf:Non-scalar arguments must match in size.');
end
