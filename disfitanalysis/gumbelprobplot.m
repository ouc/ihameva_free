function [siplot, Result2Plot] = gumbelprobplot(DistributionName,data,param,alfa,idioma,var_name,var_unit,ax1)
% gumbelprobplot Displays a Gumbel probability plot.
% figure;[param, pci]=gumbfit(data_gow.hs,0.05);
% gumbelprobplot('gumbel',data_gow.hs,param,0.05,'','',gca)
if nargin~=8, error('many input argument'); end
if ~exist('alfa','var') || isempty(alfa), alfa = 0.05; end
if ~exist('var_name','var'), var_name = ''; end
if ~exist('var_unit','var'), var_unit = ''; end
if isempty(DistributionName), error('No distribution name exist'); end
siplot = 1;
Result2Plot = [];
texto=amevaclass.amevatextoidioma(idioma);

if strcmp(DistributionName,'beta') || strcmp(DistributionName,'gumbel') || strcmp(DistributionName,'t-student') ...
         || strcmp(DistributionName,'gamma') || strcmp(DistributionName,'pareto') || strcmp(DistributionName,'weibull') ...
         || strcmp(DistributionName,'weibullmin') || strcmp(DistributionName,'weibullmax') || strcmp(DistributionName,'extreme value') || strcmp(DistributionName,'poisson')
    p = [0.001 0.02 0.05 0.10 0.25 0.5 0.75 0.90 0.95 0.98 0.99];
    lista=3:floor(log10(length(data)));
    if ~isempty(lista), p=[p 1-10.^(-lista)]; end
    
    ylbl = num2str(p');
    [LowUp,x,p]=pdfcdffit(DistributionName,data,param,alfa,'gumbelplot',p);
    Result2Plot = struct('Xaux',[],'Xyfit',[],'Yaux',[],'LowUpX',[],'Low',[],'Up',[]);
    if ~isempty(x) && min(size(x))>1
        Result2Plot.Xaux = x(:,1);
        Result2Plot.Xyfit = x(:,2);
        Result2Plot.Yaux = x(:,3);
        plot(x(:,1),x(:,3),'o','Color',[0 0 1],'MarkerSize',1.8);hold on;
        plot(x(:,1),x(:,2),'Color',[1 0 0],'LineStyle','-', 'LineWidth',1.2);
        myXtick = get(gca,'XTick');
        myXtickLabel = num2str(myXtick)';
        myYtick = unique(p);
        myYtickLabel = ylbl;
        set(gca,'XGrid','on','YGrid','on','YTick',myYtick,'YTickLabel',myYtickLabel);
        if strcmp(DistributionName,'weibull')
            myXtickLabel = num2str(exp(get(gca,'XTick'))');
        end
        if strcmp(DistributionName,'weibullmin')
            myXtickLabel = num2str(exp(get(gca,'XTick'))'+param(1));
        end
        if strcmp(DistributionName,'weibullmax')
            myXtickLabel = num2str(param(1)-exp(-get(gca,'XTick'))');
        end
        set(gca,'XTick',myXtick,'XTickLabel',myXtickLabel);
        Result2Plot.myXtick = myXtick;
        Result2Plot.myXtickLabel = myXtickLabel;
        Result2Plot.myYtick = myYtick;
        Result2Plot.myYtickLabel = myYtickLabel;
    end
    if ~isempty(LowUp)
        hold on;
        plot(LowUp(:,1),LowUp(:,2),'r--');
        plot(LowUp(:,1),LowUp(:,3),'r--');
        hold off;
        ylim([min(x(:,3)) max(x(:,3))]);
        xlim([min(x(:,1)) max(x(:,1))]);
        Result2Plot.LowUpX = LowUp(:,1);
        Result2Plot.Low = LowUp(:,2);
        Result2Plot.Up = LowUp(:,3);
    end
    ylabel('Probability')
    title([DistributionName,' distribution ','in gumbel probability plot']);
    %ver si se guarga el plot
    if isempty(LowUp), siplot=[]; end
elseif strcmp(DistributionName,'normal') || strcmp(DistributionName,'lognormal') || strcmp(DistributionName,'exponential') ...
         || strcmp(DistributionName,'rayleigh') || strcmp(DistributionName,'logistic')
    probplot(DistributionName);%probplot('normal');
    h_=probplot(ax1,data,[],[],'noref');
    set(h_,'Color',[0 0 1],'Marker','o','MarkerSize',1.8);
    % Nudge axis limits beyond data limits
    xlim_ = get(ax1,'XLim');
    if all(isfinite(xlim_))
       xlim_ = xlim_ + [-1 1] * 0.01 * diff(xlim_);
       set(ax1,'XLim',xlim_);
    end
    % Fit this distribution to get parameter values
    h_=pdfcdffit(DistributionName,data,param,alfa,'probplot');
    ylim_ = get(ax1,'YLim');
    set(h_,'Color',[1 0 0],'LineStyle','-','LineWidth',1.2);
    box(ax1,'on');
    [LowUp,x] = pdfcdffit(DistributionName,data,param,alfa,'gumbelplot');
    if min(size(x)) == 3,
        Result2Plot = struct('Xaux',x(:,1),'Xyfit',x(:,2),'Yaux',x(:,3));
    else
        Result2Plot = struct('Xaux',x);
    end
    if ~isempty(LowUp)
        grid on;
        hold on;
        plot(LowUp(:,1),LowUp(:,2),'r--');
        plot(LowUp(:,1),LowUp(:,3),'r--');
        hold off;
        ylim(ax1,ylim_);
        xlim(ax1,[min(x) max(x)]);
        Result2Plot.LowUpX = LowUp(:,1);
        Result2Plot.Low = LowUp(:,2);
        Result2Plot.Up = LowUp(:,3);
    end
    set(ax1,'XtickLabel',num2cell(get(ax1,'Xtick')));
    ylabel(texto('yLabel_5'))
    title(ax1,[regexprep(DistributionName,'(\<[a-z])','${upper($1)}'),', ',texto('distriProPlot'),' ',var_name],'FontWeight','bold');
else
    disp('No plot');
    return;
end


if ~isempty(var_unit), 
    xlabel(ax1,[char(var_name) ' (' char(var_unit) ')']);
else
    xlabel(ax1,char(var_name));
end

end