function p = wblcdf_max(x,A,B,C)
%WBLCDFM

if nargin<4
    error('stats:wblcdfM:TooFewInputs Input argument X is undefined.');
end

% Force a zero for x mayores que A.
x(x >= A) = 0;

try
    z = ((A-x)./B).^C;
catch
    error('stats:wblcdf:InputSizeMismatch Non-scalar arguments must match in size.');
end
p = exp(-z);
