function ameva(varargin)
%   AMEVA tool. Mathematical and Statistical Analysis of Environmental Variables
%   AMEVA(ameva.m) opens a graphical user interface for displaying the main program posibility.
%   Tested on Ubuntu trusty v.14.04.1(developed), Windows 7 and Mac Os X v10.9.4 (MATLAB:R2013a)
%   To load test data, please click on: -Help, Load ameva test data-, to send
%   data to matlab worksapce and work with this.
%   From this display you can execute:
%   aeva--------------------->Statistical Analysis of Environmental variables
%   gev---------------------->Generalized Exreme Value tool
%   pot---------------------->Peak Over Threshold tool
%   disfitanalysis----------->Distribution Fit Analysis of Environmental Variables
%   calibration-------------->CALIBRATION tool. (CalibraSplineQuantile,CalibraSplineNLP)
%   classification----------->CLASSIFICATION Algoritms SOM, MDA, KMA
%   durationtime------------->Hours duration time vector for this threshold and condition
%   heteroscedasticmodel----->Heteroscedastic Model
%   occurrencetable---------->Occurrence Table
%   maximostemporales-------->Temporary Maximum
%   worldfilter-------------->Outlier filter
%   amevadataselection------->Ameva Data Save Selection
%   amevaworkspace----------->Windows to view, save the ws variables and work with this variables
%   meshselection*----------->FAST VECTORIAL CORRELATION SELECTION
%   amevaihdata*------------->ihAmeva - ihData
%   flowanalysis*------------>Flow analysis
% Examples 1
%   ameva
%
% See also:
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   ameva (main program).
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB Version: 8.1.0.604 (R2013a) on Ubuntu trusty
%   04-07-2011 - The first distribution version
%   17-12-2012 - Old distribution version
%   25-03-2013 - Old distribution version
%   09-05-2013 - Old distribution version
%   12-06-2013 - Old distribution version
%   20-08-2014 - Old distribution version
%   20-04-2015 - Old distribution version
%   04-12-2015 - Last distribution version

nmfctn = 'ameva';
namesoftware = ['AMEVA ' amevaclass.ihameva_checkUpdates];
awinc = amevaclass;%ameva windows commons methods
awinc.nmfctn = nmfctn;
% Cpyright
rutasCpyright = fullfile(fileparts(mfilename('fullpath')),'Copyright.txt');
LicEspStr = ...
    [' AMEVA is free software: you can redistribute it and/or modify it under the terms of'
    ' the GNU General Public License as published by the Free Software Foundation;       '
    ' either version 3 of the License, or (at your option) any later version.            '
    '                                                                                    '
    ' AMEVA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; '
    ' without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR   '
    ' PURPOSE.  See the GNU General Public License for more details.                     '
    '                                                                                    '
    ' You should have received a copy of the GNU General Public License along with AMEVA.'
    ' If not, see http://www.gnu.org/licenses/.                                          '
    '                                                                                    '];
if exist(rutasCpyright,'file') == 2
    fid = fopen(rutasCpyright,'r','n','UTF-8'); % read text file lines as cell array of strings
    LicEspStr = textscan(fid, '%s', 'Delimiter','\n'); LicEspStr = LicEspStr{1};
    fclose(fid);
end
LicEngStr = LicEspStr;

%--name of all ih ameva windows to close only this windows OJO aumentar nuevas
nameallsoftware = {'meshselection';'calibration';'worldfilter';...
    'aeva';'maximostemporales';'durationtime';'classification';...
    'occurrencetable';'occtable';'disfitanalysis';...
    'gev';'pot';'amevaworkspace';'heteroscedasticmodel';...
    'ennera_clima';'characterized_data';'flowanalysis';'interpolation';'amevads';'amevaihdata'};
LabelMenu.WebSite = {'Ameva Web Site',1};
LabelMenu.UserManual = {'User Manual (PDF)',1};
LabelMenu.Idioma = {'Language',1};
LabelMenu.Esp = {'Spanish',1,'b'};
LabelMenu.Eng = {'English',1,'k'};
LabelMenu.About = {'About',1};
LabelMenu.LIC = {'License',1,LicEngStr};
LabelMenu.File = {'File',1};
LabelMenu.ImpData = {'Import Data',1};
LabelMenu.MeshDataSel = {'Mesh Data Selection',0};%conf file
LabelMenu.LoadTestData = {'Load ameva test data',1};
LabelMenu.Workspace = {'View Data (workspace)',1};
LabelMenu.Save = {'Save (.mat)',1};
LabelMenu.PWD = {'Set Work Directory',1};
LabelMenu.Quit = {'Quit',1};
LabelMenu.DataPrePro = {'Data Preprocessing',1};
LabelMenu.DataSel = {'Data Selection...',1};
LabelMenu.Maximum = {'Maximum,Threshold,POT...',1};
LabelMenu.Duration = {'Persistence/Durations Algorithms',1};
LabelMenu.Outlier = {'Outlier Filter Detection X vs Y',1};
LabelMenu.Tool = {'Tool',1};
LabelMenu.DStat = {'Descriptive Statistic',1};
LabelMenu.Occurrence = {'Occurrence Table',1};
LabelMenu.DisFit = {'Distribution Fit',1};
LabelMenu.ExtStat = {'Extreme Value Analysis',1};
LabelMenu.GEV = {'GEV Generalized Extreme Value Tool',1};
LabelMenu.POT = {'POT, Pareto-Poisson Tool.',1};
LabelMenu.Regression = {'Regression',1};
LabelMenu.HModel = {'Homocedastic/Heterocedastic Model',1};
LabelMenu.DataMining = {'Data Mining',1};
LabelMenu.Classification = {'Classification',1};
LabelMenu.Interpolation = {'Interpolation',0};%conf file
LabelMenu.Toolbox = {'Toolbox',1};
LabelMenu.Calibration = {'Calibration',1};
LabelMenu.ChaDat1 = {'Marine Climate. Ports',0};
LabelMenu.ChaDat2 = {'Wave Energy',0};
LabelMenu.Caudal = {'Flow analysis',0};%conf file
LabelMenu.EnneraClima = {'Ennera-Clima Tool',0};%conf file

error(nargchk(0,2,nargin));
idioma = 'eng'; OC = [];
if nargin==1
    idioma = varargin{1};
elseif nargin==2,
    idioma = varargin{1};
    OC = varargin{2};
end
if ~exist('idioma','var') || isempty(idioma) || ~ischar(idioma), idioma = 'eng'; end;

ReadAmevaConf;%to set usertype

if strcmp(idioma,'esp'),
    LabelMenu.WebSite = {'Sitio web de AMEVA',1};
    LabelMenu.UserManual = {'Manual de usuario (PDF)',1};
    LabelMenu.Idioma = {'Idioma',1};
    LabelMenu.Esp = {['Espa',char(241),'ol'],1,'k'};
    LabelMenu.Eng = {['Ingl',char(233),'s'],1,'b'};
    LabelMenu.About{1} = 'Acerca de AMEVA';
    LabelMenu.LIC = {'Licencia',1,LicEspStr};
    LabelMenu.File{1} = 'Archivo';
    LabelMenu.ImpData{1} = 'Importar datos';
    LabelMenu.MeshDataSel{1} = ['Selecci',char(243),'n de datos de las mallas'];
    LabelMenu.LoadTestData{1} = 'Cargar datos para testear AMEVA';
    LabelMenu.Workspace{1} = 'Ver Datos (workspace)';
    LabelMenu.Save{1} = 'Guardar (.mat)';
    LabelMenu.PWD = {'Configurar el directorio de trabajo',1};
    LabelMenu.Quit{1} = 'Salir de AMEVA';
    LabelMenu.DataPrePro{1} = 'Prep. datos';
    LabelMenu.DataSel{1} = ['Selecci',char(243),'n de datos'];
    LabelMenu.Maximum{1} = ['M',char(225),'ximo,Umbral,POT...'];
    LabelMenu.Duration{1} = 'Persistencias/Duraciones';
    LabelMenu.Outlier{1} = ['Detecci',char(243),'n de Outlier X vs Y'];
    LabelMenu.Tool{1} = 'Mod. independientes';
    LabelMenu.DStat{1} = ['Estad',char(237),'stica descriptiva'];
    LabelMenu.Occurrence{1} = 'Tabla de encuentros';
    LabelMenu.DisFit{1} = 'Ajuste de distribuciones';
    LabelMenu.ExtStat{1} = ['An',char(225),'lisis de valores extremos'];%['Estad',char(237),'stica extrema'];
    LabelMenu.GEV{1} = ['Herramienta GEV Funci',char(243),'n generalizada de valores extremos'];
    LabelMenu.POT{1} = 'Herramienta POT, Pareto-Poisson';
    LabelMenu.Regression{1} = ['Regresi',char(243),'n'];
    LabelMenu.HModel{1} = ['Modelo homoced',char(225),'stico/heteroced',char(225),'stico'];
    LabelMenu.DataMining{1} = ['Miner',char(237),'a de datos'];
    LabelMenu.Classification{1} = ['Clasificaci',char(243),'n'];
    LabelMenu.Interpolation{1} = ['Interpolaci',char(243),'n'];
    LabelMenu.Toolbox{1} = ['Mod. metodol',char(243),'gicos'];
    LabelMenu.Calibration{1} = ['Calibraci',char(243),'n'];
    LabelMenu.ChaDat1{1} = ['Clima mar',char(237),'timo. Puertos'];
    LabelMenu.ChaDat2{1} = ['Energ',char(237),'a del oleaje'];
    LabelMenu.Caudal{1} = ['An',char(225),'lisis de caudales'];
    LabelMenu.EnneraClima{1} = 'Herramienta Ennera-Clima';
end

%---LA PANTALLA---
screenSize = get(0,'ScreenSize');
figPos = [15 screenSize(4)-80 550 1];

if ~isempty(findobj('Tag',['Tag_' nmfctn]))
    figPos = get(findobj('Tag',['Tag_' nmfctn]),'Position');
    close(findobj('Tag',['Tag_' nmfctn]));
end

%Lista de las figuras que se van a usar en este programa
hf = [];%ventana principal
%Global variables

%---LA FIGURA-VENTANA PRINCIPAL-
hf = figure('Name',namesoftware,'MenuBar','None','CloseRequestFcn',@AmevaClose,...
    'NumberTitle','Off','DockControls','Off','Position',figPos,'Tag',['Tag_' nmfctn],'NextPlot','new',...
    'Resize','off');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
amvcnf.idioma=idioma;amvcnf.usertype=usertype;

%% AMEVA
mj = uimenu('Label','AMEVA');
uimenu(mj,'Label',LabelMenu.About{1},'Callback',@FigAbModeOn);
uimenu(mj,'Label',LabelMenu.WebSite{1},'CallBack','web(''http://ihameva.ihcantabria.com'',''-browser'')');
uimenu(mj,'Label',LabelMenu.UserManual{1},'CallBack','open(''muser_ameva.pdf'')','Separator','on');
mk=uimenu(mj,'Label',LabelMenu.Idioma{1},'Separator','on');

uimenu(mk,'Label',LabelMenu.Esp{1},'Callback',@AmevaIdioma,'UserData','esp','ForegroundColor',LabelMenu.Esp{3});
uimenu(mk,'Label',LabelMenu.Eng{1},'Callback',@AmevaIdioma,'UserData','eng','ForegroundColor',LabelMenu.Eng{3});
uimenu(mj,'Label',LabelMenu.PWD{1},'Callback',@AmevaPWD,'Separator','on');
uimenu(mj,'Label',LabelMenu.Quit{1},'Callback',@AmevaClose,'Accelerator','Q','Separator','on');
srtcal='msgbox(''Working!'',''W'')';%mensage de ventana sin terminar
% FILE
me =uimenu('Label',LabelMenu.File{1});
ImpDatCall=['[auxFileName,auxPathName] = uigetfile(',...
    '{''*.dat;*.txt;*.mat'';''*.dat'';''*.txt'';''*.mat'';''*.*''},''File Selector'',''MultiSelect'', ''on'');',...
    'if isempty(auxFileName), return; end;',...
    'if length(auxFileName)==1, clear auxFileName auxPathName; return; end;',...
    'h=waitbar(0.3,''Please wait...'');',...
    'auxs = whos(''-var'', ''auxFileName'');',...
    'if auxs.class==''char''',...
    'load([auxPathName,auxFileName]);',...
    'elseif auxs.class==''cell''',...
    'for auxi=1:length(auxFileName), load([auxPathName,auxFileName{auxi}]); end;',...
    'else,',...
    'disp(''NO DATA FILE!'');',...
    'end;','waitbar(1,h);','close(h);','clear auxFileName auxPathName auxs h;'];
uimenu(me,'Label',LabelMenu.ImpData{1},'Callback',ImpDatCall,'Separator','on');
if LabelMenu.MeshDataSel{2} && exist(which('amevainfo'),'file') == 2,
    mf=uimenu(me,'Label','ihdata_toolbox');
    mg=uimenu(mf,'Label','Viento');
    uimenu(mg,'Label','NCEP','Callback',@(source,event)amevaihdata({'Viento','NCEP'},amvcnf));
    uimenu(mg,'Label','SeaWind','Callback',@(source,event)amevaihdata({'Viento','SW_NCEP'},amvcnf));
    uimenu(mg,'Label','SeaWind-HR','Callback',@(source,event)amevaihdata({'Viento','HR_SW_NCEP'},amvcnf));
    uimenu(mg,'Label','CFS','Callback',@(source,event)amevaihdata({'Viento','CFS'},amvcnf));
    mh=uimenu(mf,'Label','Oleaje');
    uimenu(mh,'Label','GOW','Callback',@(source,event)amevaihdata({'Oleaje','GOW'},amvcnf));
    uimenu(mh,'Label','DOW','Callback',@(source,event)amevaihdata({'Oleaje','DOW'},amvcnf));
    mi=uimenu(mh,'Label','Intrumental');
    uimenu(mi,'Label','Intrumental boyas','Callback',@(source,event)amevaihdata({'Oleaje','Instrumental boyas'},amvcnf));
    uimenu(mi,'Label','Intrumental satelites','Callback',@(source,event)amevaihdata({'Oleaje','Instrumental satelites'},amvcnf));
    mj=uimenu(mf,'Label','Nivel del mar');
    uimenu(mj,'Label','GOS','Callback',@(source,event)amevaihdata({'Nivel del mar','GOS'},amvcnf));
    uimenu(mj,'Label','GOT','Callback',@(source,event)amevaihdata({'Nivel del mar','GOT'},amvcnf));
    uimenu(mj,'Label','MSL','Callback',@(source,event)amevaihdata({'Nivel del mar','MSL'},amvcnf));
    ee=uimenu(me,'Label',LabelMenu.MeshDataSel{1});
    infor=amevainfo('ameva');
    MESHES=infor.MESHES;
    for i=1:length(MESHES),
        uimenu(ee,'Label',[MESHES{i,1},' ',MESHES{i,2}],'Callback',@(source,event)meshselection(i,amvcnf));
    end
end
if usertype==0,
    uimenu(me,'Label','ToolsMallas','Callback','ToolsMallas');
end
uimenu(me,'Label',LabelMenu.LoadTestData{1},'Callback','load(''ameva'')','Separator','on');
uimenu(me,'Label',LabelMenu.Workspace{1},'CallBack',@(source,event)amevaworkspace(amvcnf),'Separator','on');
uimenu(me,'Label',LabelMenu.Save{1},'Callback','uisave','Separator','on');
%% DATA PREPROCESSING
mg =uimenu('Label',LabelMenu.DataPrePro{1});
% eval(['matlabpool local ' num2str(feature('numcores')-2)']);
% batch('gev.m','matlabpool',1)
% spmd
%   classification;
% end
% spmd
%   gev;
% end
% if matlabpool('size'), matlabpool close; end
uimenu(mg,'Label',LabelMenu.DataSel{1},'Callback',@(source,event)amevadataselection(amvcnf));
uimenu(mg,'Label',LabelMenu.Maximum{1},'Callback',@(source,event)maximostemporales(amvcnf));
uimenu(mg,'Label',LabelMenu.Duration{1},'Callback',@(source,event)durationtime(amvcnf));
uimenu(mg,'Label',LabelMenu.Outlier{1},'Callback',@(source,event)worldfilter(amvcnf));
%% TOOL - Modulos independientes
mh =uimenu('Label',LabelMenu.Tool{1});
fh=uimenu(mh,'Label',LabelMenu.DStat{1});
uimenu(fh,'Label','AEVA','Callback',@(source,event)aeva(amvcnf));
uimenu(fh,'Label',LabelMenu.Occurrence{1},'Callback',@(source,event)occurrencetable(amvcnf));
uimenu(mh,'Label',LabelMenu.DisFit{1} ,'Callback',@(source,event)disfitanalysis(amvcnf));
ff=uimenu(mh,'Label',LabelMenu.ExtStat{1});
uimenu(ff,'Label',LabelMenu.GEV{1},'Callback',@(source,event)gev(amvcnf));
uimenu(ff,'Label',LabelMenu.POT{1},'Callback',@(source,event)pot(amvcnf));
mf =uimenu(mh,'Label',LabelMenu.Regression{1});
if LabelMenu.HModel{2},
    uimenu(mf,'Label',LabelMenu.HModel{1},'Callback',@(source,event)heteroscedasticmodel(amvcnf));
end
if usertype==0,
    uimenu(mh,'Label','*Modelos Autoregresivos*','Callback',srtcal);
end
mj=uimenu(mh,'Label',LabelMenu.DataMining{1});
uimenu(mj,'Label',LabelMenu.Classification{1},'Callback',@(source,event)classification(amvcnf));
if usertype==0,
    uimenu(mj,'Label','*PCA*','Callback',srtcal);
    uimenu(mj,'Label','*CCA*','Callback',srtcal);
    mk=uimenu(mh,'Label','*Interpolation*');
    uimenu(mk,'Label','*Analog*','Callback',srtcal);
    uimenu(mk,'Label','*RBF*','Callback',srtcal);
    uimenu(mk,'Label','propagation/interpolation','Callback',@(source,event)interpolation(amvcnf));
end
%% TOOLBOX - Modulos metodologicos
mi =uimenu('Label',LabelMenu.Toolbox{1});
uimenu(mi,'Label',LabelMenu.Calibration{1},'Callback',@(source,event)calibration(amvcnf));
if LabelMenu.Caudal{2},
    uimenu(mi,'Label',LabelMenu.ChaDat1{1},'Callback',@(source,event)characterized_data(1,amvcnf));
    uimenu(mi,'Label',LabelMenu.ChaDat2{1},'Callback',@(source,event)characterized_data(2,amvcnf));
end
if usertype==0,
    uimenu(mi,'Label','*Marine Climate, Beach*','Callback',srtcal);
end
if LabelMenu.Caudal{2},
    uimenu(mi,'Label',LabelMenu.Caudal{1},'Callback',@(source,event)flowanalysis(amvcnf));
end
if LabelMenu.EnneraClima{2},
    uimenu(mi,'Label',LabelMenu.EnneraClima{1},'Callback',@(source,event)ennera_clima(amvcnf));
end
if usertype==0,
    uimenu(mi,'Label','*Downscaling Estadistico*','Callback',srtcal);
end

%% Figure About Ameva
hi = figure('Name',['About',upper(nmfctn(1)),nmfctn(2:end)],'MenuBar','None','NumberTitle','Off','Resize','Off',...
    'CloseRequestFcn',@FigAbMode,'Resize','Off','Visible','Off');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
rgb = imread(which('aboutameva.png'));
image(rgb);
set(gca,'Position',[0 0 1 1]);
axis off;          % Remove axis ticks and numbers
axis image;
uicontrol('Style','pushbutton','Units','normalized','Parent',hi,'Enable','On','String',LabelMenu.LIC{1}, ...
    'Position',[0.02 0.02 0.135 0.05],'Callback',@FigLicModeOn);
if not(isdeployed)
    uicontrol('Style','pushbutton','Units','normalized','Parent',hi,'Enable','On','String','Close', ...
        'Position',[0.845 0.02 0.135 0.05],'Callback',@FigAbMode);
end
set(gca,'NextPlot','new');
set(hi,'NextPlot','new');

%% Figure Ameva License
hg = figure('Name',['License',upper(nmfctn(1)),nmfctn(2:end)],'MenuBar','None', 'Toolbar','none','NumberTitle','Off','Resize','Off',...
    'CloseRequestFcn',@FigLicMode,'Resize','Off','Visible','Off');
set(hg,'DefaultTextInterpreter','tex')
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
hPan = uipanel(hg, 'Title','License', ...
    'Units','normalized', 'Position',[0.05 0.075 0.9 0.9]);
uicontrol(hPan, 'Style','listbox', 'FontSize',9, ...
    'Min',0, 'Max',2, 'HorizontalAlignment','left', ...
    'Units','normalized', 'Position',[0 0 1 1], ...
    'String',LabelMenu.LIC{3});%,'Enable','On'
uicontrol('Style','pushbutton','Units','normalized','Parent',hg','String','Close', ...
    'Position',[0.845 0.02 0.135 0.05],'Callback',@FigLicMode);
set(hg,'NextPlot','new');

%% Configuration Options
    function ReadAmevaConf(source,event)
        % decidir las configuraciones de acuerdo al tipo de usuario
        usertype = 1; %student
        LabelMenu.MeshDataSel{2} = 0;
        LabelMenu.Caudal{2} = 0;
        LabelMenu.EnneraClima{2} = 0;
        LabelMenu.Interpolation{2} = 0;
        if exist(which('amevainfo'),'file') == 2,
            usertype = 2; %ih
            LabelMenu.MeshDataSel{2} = 1;
            LabelMenu.Caudal{2} = 1;
            if not(isempty(OC)) && strcmp(varargin{2},'rootoc'),
                usertype = 0; %OC
                LabelMenu.EnneraClima{2} = 1;
                LabelMenu.Interpolation{2} = 1;
            end
        end
    end
    function FigAbModeOn(source,event)
        set(hi,'Visible','on');
    end
    function FigAbMode(source,event)
        set(hi,'Visible','off');
    end
    function FigLicModeOn(source,event)
        set(hg,'Visible','on');
    end
    function FigLicMode(source,event)
        set(hg,'Visible','off');
    end
%% Set PWD
    function AmevaPWD(source,event)
        workPath = uigetdir;
        if ~isempty(workPath) && length(workPath)>1 && ~strcmp(workPath(end),filesep)
            cd(workPath);
        end
    end
%% Set idoma
    function AmevaIdioma(source,event)
        aux = get(source,'UserData');
        if strcmp(aux,'eng') && strcmp(idioma,'esp'),
            aux_ = questdlg('AMEVA will restart in english, you want to continue?','Language Restart Window','Yes','No','Yes');
            if strcmp(aux_,'Yes'), idioma = aux; ameva(idioma); end
        elseif strcmp(aux,'esp') && strcmp(idioma,'eng'),
            aux_ = questdlg(['AMEVA se va a reiniciar en espa',char(241),'ol, ',char(191),'desea continuar?'],'Idioma, Ventana de reinicio','Si','No','Si');
            if strcmp(aux_,'Si'), idioma = aux; ameva(idioma); end;
        end
    end
%% Close only all Ameva windows
    function AmevaClose(source,event)
        set(0,'ShowHiddenHandles','on')
        try
            for j = 1:length(nameallsoftware)
                if ishandle(findobj('Tag',['Tag_' char(nameallsoftware(j))])),
                    close(findobj('Tag',['Tag_' char(nameallsoftware(j))]));
                end
            end
            delete(findobj('type','figure','Name',['About',upper(nmfctn(1)),nmfctn(2:end)]));
            if ishandle(hi), delete(hi); end
            if ishandle(hf), delete(hf); end
            if ishandle(hg), delete(hg); end
        catch ME
            delete(get(0,'Children'));
            rethrow(ME);
        end
    end
end