function [Dermut,Derpsit,Derepst] = Dmupsiepst (t,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,indices,indices2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Function Dmupsiepst calculates the derivatives of the standardized maximum
%   with respect to parameters
%
%   Input:
%       x -> Maximum data
%       t -> time when the data occur within a yearly scale, so that
%       covariates can be used
%       kt -> Frequency parameter to measure the importance od the number
%       of points
%       beta0 -> Optimal constant parameter related to location
%       beta -> Optimal harmonic vector associated with location
%       alpha0 -> Optimal constant parameter related to scale
%       alpha -> Optimal harmonic vector associated with scale
%       gamma0 -> Optimal constant parameter related to shape
%       gamma -> Optimal harmonic vector associated with shape
%       betaT -> Optimal location trend parameter
%       varphi -> Optimal location covariate vector
%       betaT2 -> Optimal scale trend parameter
%       varphi2 -> Optimal scale covariate vector
%       indices -> covariates data related to the location parameter, a
%       matrix including the data at time t for each covariate
%       indices2 -> covariates data related to the scale parameter, a
%       matrix including the data at time t for each covariate
%       
%   Output:
%
%       Dq -> Quantile gradients with respect to parameters
%
%   Authors: R. Minguez, F.J. Mendez, C. Izaguirre, M. Menendez,
%   and I.J. Losada 
%   Environmental and Hydraulics Institute "IH Cantabria"
%   University of Cantabria 
%   E.T.S. de Ingenieros de Caminos, Canales y Puertos 
%   Avda de los Castros, s/n 
%   39005 Santander, Spain
%   Tfno.: +34 942 20 18 52 
%   Fax: +34 942 20 18 60 
%   Corresponding author email: roberto.minguez@unican.es
%
%   Created: 09/09/2009
%
%   For more details see the paper:
%   "Pseudo-Optimal Parameter Selection of Non-Stationary 
%   Generalized Extreme Value Models for Environmental Variables". 
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Check the number of input arguments
    if nargin<2, kt = []; end 
    if nargin<3, beta0 = []; end 
    if nargin<4, beta = []; end 
    if nargin<5, alpha0 = []; end 
    if nargin<6, alpha = [];end
    if nargin<7, gamma0 = [];end
    if nargin<8, gamma = [];end
    if nargin<9, betaT = [];end
    if nargin<10, varphi = [];end
    if nargin<11, betaT2 = [];end
    if nargin<12, varphi2 = [];end
    if nargin<13, indices = [];end
    if nargin<14, indices2 = [];end

    global neps0
    
    %   Check consistency of data
    [na nind] = size(indices);
    if ~isempty(indices),
    if na~=length(t),
        error('Check data t, indices: funcion quantile')
    end
    end
    [na2 nind2] = size(indices2);
    if ~isempty(indices2),
    if na2~=length(t),
        error('Check data t, indices2: funcion quantile')
    end
    end
    
    nd = length(t);
    
    if isempty(kt),
        kt = ones(nd,1);
    end

    nmu=length(beta);
    npsi=length(alpha);
    neps=length(gamma);
    ntend=length(betaT);
    nind=length(varphi);
    ntend2=length(betaT2);
    nind2=length(varphi2);
    
    %   Evaluate the location parameter at each time t as a function
    %   of the actual values of the parameters given by p
    mut1 = parametro(t,beta0,beta,betaT,indices,varphi);
    %   Evaluate the scale parameter at each time t as a function
    %   of the actual values of the parameters given by p
    psit1 = exp(parametro(t,alpha0,alpha,betaT2,indices2,varphi2));
    %   Evaluate the shape parameter at each time t as a function
    %   of the actual values of the parameters given by p
    epst = parametro(t,gamma0,gamma);

    %   The values whose shape parameter is almost cero corresponds to
    %   the GUMBEL distribution, locate their positions if they exist
    posG = find(abs(epst)<=1e-8);
    %   The remaining values correspond to WEIBULL or FRECHET
    pos  = find(abs(epst)>1e-8);
    %   The corresponding GUMBEl values are set to 1 to avoid 
    %   numerical problems, note that for those cases the GUMBEL
    %   expressions are used
    epst(posG)=1;
    
    %   Modifico los parametros para incluir el numero de datos
    mut = mut1;
    psit = psit1;
    mut(pos) = mut1(pos)+psit1(pos).*(kt(pos).^epst(pos)-1)./epst(pos);
    psit(pos) = psit1(pos).*kt(pos).^epst(pos);
    %   Modifico los parametros para incluir el numero de datos para GUMBEL
    mut(posG) = mut(posG)+psit(posG).*log(kt(posG));
    
    %   Evaluate auxiliary variables
% % %     xn = (x-mut)./psit; 
% % %     z = 1 + epst.*xn; 
% % %     %   Since the z-values must be greater than zero in order to avoid
% % %     %   numerical problems their values are set to be greater than 1e-4
% % %     z = max(1e-4,z);
% % %     zn =z.^(-1./epst); 
    
    %   Derivatives of the quantile function with respect to location,
    %   scale and shape parameters
% % %     Dmut(pos,1) = -1./(z(pos).*psit(pos));
% % %     Dpsit(pos,1) =  xn(pos).*Dmut(pos);
% % %     Depst(pos,1) = (1-1./z(pos)-log(z(pos)))./(epst(pos).*epst(pos));
% % %     
% % %     %   Gumbel derivatives
% % %     Dmut(posG,1) = -1./psit(posG);
% % %     Dpsit(posG,1) =-xn(posG)./psit(posG);
% % %     Depst(posG,1) =0;
    
    Dmut = ones(nd,1);
    Dpsit = ones(nd,1);
    Depst = ones(nd,1);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   NUEVAS DERIVADAS
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Dmutastmut = ones(size(kt));
    Dmutastpsit = (-1+kt.^epst)./epst;
    Dmutastepst = psit1.*(1+kt.^epst.*(epst.*log(kt)-1))./(epst.^2);

    Dpsitastpsit = kt.^epst;
    Dpsitastepst = log(kt).*psit1.*kt.^epst;

    Dmutastpsit(posG) = log(kt(posG));
    Dmutastepst(posG) = 0;

    Dpsitastpsit(posG) = 1;
    Dpsitastepst(posG) = 0;

%   Derivadas de los parametros de localizacion, forma y escala con
%   respecto a los parametros del modelo beta0, betaj, ...

    Dermut = zeros(1+nmu+ntend+nind,nd);
    Derpsit = zeros(1+npsi+ntend2+nind2,nd);
    Derepst = zeros(neps0+neps,nd);
    %   Jacobian elements related to the location parameters beta0
    %   and beta
    Dermut(1,:) = Dmut'.*Dmutastmut';
    %   If location harmonics are included
    if nmu>0,
        for i = 1:nmu,
            for k = 1:length(t),
                %   Funtion Dparam is explained below
                Dermut(1+i,k) = Dmut(k)*Dmutastmut(k)*Dparam (t(k),i);
            end
        end    
    end
    %   Jacobian elements related to the location parameters betaT,
    %   and varphi
    if ntend>0
        Dermut(2+nmu,:) =  (Dmut.*t.*Dmutastmut)';
    end
    if nind>0
        for i = 1:nind,
            Dermut(1+nmu+ntend+i,:) =  (Dmut.*indices(:,i).*Dmutastmut)';
        end
    end
    %   Jacobian elements related to the scale parameters alpha0
    %   and alpha
    Derpsit(1,:) = (psit1.*(Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit))';
    %   If scale harmonics are included
    if npsi>0,
        for i = 1:npsi,
            for k = 1:length(t),
                Derpsit(1+i,k) = Dparam (t(k),i)*psit1(k)*(Dpsit(k)*Dpsitastpsit(k)...
                                +Dmut(k)*Dmutastpsit(k));
            end
        end
    end
    %   Jacobian elements related to the scale parameters alphaT,
    %   and varphi, equation A.10
    if ntend2>0,
%         Derpsit(1+ntend2+npsi,:) =  (Dpsit.*t.*psit)';
        Derpsit(1+ntend2+npsi,:) =  ((Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit).*t.*psit1)';
    end
    if nind2>0,
        for i = 1:nind2,
%             Derpsit(1+ntend2+npsi+i,:) =  (Dpsit.*indices2(:,i).*psit)';
            Derpsit(1+ntend2+npsi+i,:) =  ((Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit).*indices2(:,i).*psit1)';      
        end
    end
    %   Jacobian elements related to the shape parameters gamma0
    %   and gamma
    if neps0 == 1,
%         Derepst(neps0,:) = (Depst)';
        Derepst(neps0,:) = (Depst+Dpsit.*Dpsitastepst+Dmut.*Dmutastepst)';
    end  
    %   If shape harmonics are included
    if neps>0,
        for i = 1:neps,
            for k = 1:length(t),
%                 Derepst(neps0+i,k) = Depst(k)*Dparam (t(k),i);
                Derepst(neps0+i,k) = (Depst(k)+Dpsit(k)*Dpsitastepst(k)+Dmut(k)*Dmutastepst(k))*Dparam (t(k),i);
            end
        end
    end  
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function dp = Dparam (t,j)
    %
    %   Derivative of the location, scale and shape functions with respect
    %   to harmonic parameters. It correspond to the right hand side 
    %   in equation A.11 of the paper    
    %
    %   Input:
    %        t-> Time (Yearly scale)
    %        j-> Harmonic number
    %
    %   Output:
    %        dp-> Corresponding derivative
    %
        if mod(j,2) == 0,
            dp = sin(j/2*2*pi*t);
        else
            dp = cos((j+1)/2*2*pi*t);
        end

    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end



