function jacobiano = amevaConfidInterQuanAggregate (q,t0,t1,OPHobj,pS)
% OPTIMIZAR ESTA FUNCION OC UC
%   Vector lengths
np=length(OPHobj.pv);
nmu=length(pS.mu.alpha0);
npsi=length(pS.psi.alpha0);
neps=length(pS.chi.alpha0);
nind=length(OPHobj.lista);
nind2=length(OPHobj.lista2);
nind3=length(OPHobj.lista3);
%   Total length
q=q(:);
t0=t0(:);
t1=t1(:);
if min(size(q))>1,
    error('the quantile must be a double or a vector');
end
if min(size(t0))>1 || min(size(t1))>1,
    error('Initial/Final quantile aggregated integration time must be a double or a vector');
end
if max(size(t0))~=max(size(t1)),
    error('Initial/Final quantile aggregated integration time size must be equal size');
end
m = length(q);
n = length(t0);

%   Initializing the Jacobian
jacobiano = zeros(np,n,m);

epsi = 10^(-4);
%   beta0 derivative
aux = 1;
% beta0=OPHr.beta0;
% OPHr_Up_=OPHr; OPHr_Up_.beta0=beta0*(1+epsi); OPHr_Up_.pS.mu.alpha0=beta0*(1+epsi);
% OPHr_Lw_=OPHr; OPHr_Lw_.beta0=beta0*(1-epsi); OPHr_Lw_.pS.mu.alpha0=beta0*(1-epsi);
% jacobiano_ = (amevaQuantileAgregado (q,t0,t1,OPHobj,OPHr_Up_.pS) - amevaQuantileAgregado(q,t0,t1,OPHobj,OPHr_Lw_.pS))/(2*beta0*epsi);
jacobiano(aux,:,:)=CIQuanAggregate(pS,'mu','alpha0');
% if jacobiano(aux)~=jacobiano_, disp(['distinto jacobiano_ de jacobiano(aux) ' num2str(jacobiano_) '-' num2str(jacobiano(aux))]); end
%   beta derivative
if ~isempty(pS.mu.alpha),
    for i = 1:nmu,
        aux = aux+1;
        jacobiano(aux,:,:)=CIQuanAggregate(pS,'mu','alpha',i);
    end
end
%   betaT derivative
if ~isempty(pS.mu.betaT),
    aux = aux+1;
    jacobiano(aux,:,:)=CIQuanAggregate(pS,'mu','betaT');
end
%   varphi derivative
if ~isempty(pS.mu.varphi),
    for i = 1:nind,
        aux = aux+1;
        if pS.mu.varphi(i)~=0,
            jacobiano(aux,:,:)=CIQuanAggregate(pS,'mu','varphi',i);
        else
            jacobiano(aux,:,:) = 0;
        end
    end
end
%   alpha0 derivative
aux = aux+1;
jacobiano(aux,:,:)=CIQuanAggregate(pS,'psi','alpha0');
%   alpha derivative
if ~isempty(pS.psi.alpha),
    for i = 1:npsi,
        aux = aux+1;
        jacobiano(aux,:,:)=CIQuanAggregate(pS,'psi','alpha',i);
    end
end
%   betaT2 derivative
if ~isempty(pS.psi.betaT),
    aux = aux+1;
    jacobiano(aux,:,:)=CIQuanAggregate(pS,'psi','betaT');
end
%   varphi2 derivative
if ~isempty(pS.psi.varphi),
    for i = 1:nind2,
        aux = aux+1;
        if pS.psi.varphi(i)~=0,
            jacobiano(aux,:,:)=CIQuanAggregate(pS,'psi','varphi',i);
        else
            jacobiano(aux,:,:) = 0;
        end
    end
end
%   gamma0 derivative
if ~isempty(pS.chi.alpha0) && pS.chi.alpha0 ~= 0, %% OJO OJO OJO OC 12/2015 add  && pS.chi.alpha0 ~= 0
    aux = aux+1;
    jacobiano(aux,:,:)=CIQuanAggregate(pS,'chi','alpha0');
end
%   gamma derivative
if ~isempty(pS.chi.alpha),
    for i = 1:neps,
        aux = aux+1;
        jacobiano(aux,:,:)=CIQuanAggregate(pS,'chi','alpha',i);
    end
end
%   betaT3 derivative
if ~isempty(pS.chi.betaT),
    aux = aux+1;
    jacobiano(aux,:,:)=CIQuanAggregate(pS,'chi','betaT');
end
%   varphi3 derivative
if ~isempty(pS.chi.varphi),
    for i = 1:nind3,
        aux = aux+1;
        if pS.chi.varphi(i)~=0,
            jacobiano(aux,:,:)=CIQuanAggregate(pS,'chi','varphi',i);
        else
            jacobiano(aux,:,:) = 0;
        end
    end
end

%
    function  obj=CIQuanAggregate(pS,vn1,vn2,i_)
        pS_Up = pS; pS_Lw = pS;
        auxchar='';
        if nargin<4
            auxchar=['[' vn1 '.' vn2 ']'];
            auxi=pS.(vn1).(vn2);
            pS_Up.(vn1).(vn2)=auxi*(1+epsi);
            pS_Lw.(vn1).(vn2)=auxi*(1-epsi);
        else
            auxchar=['[' vn1 '.' vn2 '(' num2str(i_) ')]'];
            auxi=pS.(vn1).(vn2)(i_);
            pS_Up.(vn1).(vn2)(i_)=auxi*(1+epsi);
            pS_Lw.(vn1).(vn2)(i_)=auxi*(1-epsi);
        end
        tic
        obj=(amevaQuantileAgregado (q,t0,t1,OPHobj,pS_Up) - amevaQuantileAgregado(q,t0,t1,OPHobj,pS_Lw))/(2*auxi*epsi);
        toc
        disp(['[CIQuanAggregate] amevaConfidInterQuanAggregate / -> ' auxchar datestr(now,0)]);%% COMENTAR
    end
end