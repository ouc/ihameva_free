function [mufinal,xifinal,psifinal,PP,Yn]=ajusteGEVAT(Y)
P1=0.05;
Pend=0.9999;
Clas=20;
global Yord Prob Yp P;
[Yord,Prob,Yp,P]=DetClas2(Y,Clas,P1,Pend);
PP=[0.0001 0.01:0.01:0.15 P P(end):0.00001:0.99999 0.99999];
[fstorage,bestsolution,bestf] =sce_YoGEV;
psifinal=bestsolution(1);
xifinal=bestsolution(2);
mufinal=bestsolution(3);
for i=1:length(PP)
	Yn(i)=mufinal- (psifinal./xifinal.* ( 1-(-log(    PP(i)     )).^(-xifinal) ) );
end
clear global;

function [fstorage,bestsolution,bestf] =sce_YoGEV
% global tipdis
%=====================================================
% The Shuffled Complex Evolution Method, SCE-UA v2.1
%=====================================================
%
% Method developed and coded in fortran by Q. Duan (April, 1992)
% at the Department of Hydrology & Water Resources of the
% University of Arizona 
%
% References:
%  Duan, Q., Sorooshian, S., Gupta, V. (1992) "Effective and efficient
%   global optimization for conceptual rainfall-runoff models". Water
%   Resources Research, 28(4), 1015-1031.
%  Duan, Q., Sorooshian, S., Gupta, V. (1994) "Optimal use of the SCE-UA 
%   global optimization method for calibrating watershed models".
%   Journal of Hydrology, 158, 265-284.
% 
% Recoded in MATLAB by Fernando J. Mendez (September, 2003) at Cornell
% University
% - Notation of variables follows Duan et al. (1992) paper
% 
% Comments: September 12, 2003
%   - Check convergence is implemented considering 
%        * maximum number of trials
%        * Absotule error of objective function
%   - "No improvement in KSTOP shuffling loops" is NOT implemented yet
%
% e-mail: mendezf@unican.es
%
% See the structure of the functions and scripts in sce-matlab.pdf
% 
% Definition of variables

nitemax=1500; %Maximum number of iterations
numberofloops=1; % Number of loops repeating the whole process



% Load the lower and upper bounds
% if length(tipdis)<10
    [x_min,x_max] = boundsGEV ;% Feasible space Omega in R^n
% else
%     [x_min,x_max] = bounds2;
% end


% 0. Initiate the variables of the method

n=length(x_min);    % Dimension of the problem

p=2;    % Initial number of complexes
pmin=p; % Minimum number of complexes (recommended in Duan et al, 1994)
m=2*n+1;    % Number of points in each complex (recommended in Duan et al, 1994)
s=m*p;  % Sample size
beta=2*n+1; % Number of evolution steps allowed for each complex before complex shuffling (Duan et al, 1994)
q=n+1;      % Number of points in each sub-complex (Duan et al, 1994)
alfa=1;     % Internal parameter of CCE (Duan et al, 1994)



fopt=-100; % for the minimum value of the objective function (lower)

fstorage=ones(nitemax,numberofloops)*1000000; % initialize the array with the values of the objective function for every trial 


for iloop=1:numberofloops % loop repeating the run



% 1. Initiate the process

errmin=.01; % porcentage error
%kstop=5; % number of shuffling loops in which the criterion value must change by 'pecnto' before optimization is terminated
%pcento=.005; % porcentage by which the criterion value must change in 'kstop' shuffling loops (in o/1) 



ite=1;
%criter=1000.*ones(20,1);

% 2. Generation of a sample
for i=1:s
 x(i,1:n)=generateuniformsample(x_min,x_max); % sample s points x_1,...,x_s in the feasible space Omega
                                              % using a uniform sampling distribution
                                                
 f(i)=objectivefunction_YoGEV(x(i,:));%,Y_vector); % compute the function value f_i at each point x_i
 ite=ite+1;  % Calculate the number of iterations
 fstorage(ite,iloop)=min([f(i); fstorage(ite-1,iloop)]);

end

% 3. Rank the points: sort the s points in order of increasing function value
% and store them in an array D={x_i,f_i} where i=1 represent the point with
% the smallest function value
[faux index]=sort(f);
D=[x(index,1:n) faux'];

err=10000;
nloop=0;
%timeou=Inf;

while (err>=errmin)&(ite<=nitemax) % 7. Check convergence

    nloop=nloop+1;  % number of loops - shufflings

% 4. Partition D into p complexes A(1),...A(p) each containing m points;
clear A;
for ip=1:p
    for im=1:m
        A(ip).D(im,1:n+1)=D(ip+p*(im-1),1:n+1);
    end
end

% 5. Evolve each complex A(k) according to the competitive complex
% evolution CCE algorithm
for ip=1:p
    complexA=A(ip).D;
    for i=1:beta
        L=simulatrapezoidal(m,q); % Select q points from complex according to the trapezoidal distr. 
                                  % Them the q points define a sub-complex
                                  % Storage the positions in L
                                  % m is the number of points of each complex
        B=complexA(L,1:n+1);      % B is the sub-complex
        
        [baux index]=sort(B(:,n+1));
        B=[B(index,1:n) baux];    % Now B is the sub-complex sorted
        
        L=sort(L);                % Now L is sorted        
        
        for j=1:alfa
            g=mean(B(1:(end-1),1:n));  % Calculate the centroid of q-1 best points
            uq=B(end,1:n);             % uq is the worst point in B
            fq=B(end,n+1);             % fq is the function value in the worst point in B
            r=2*g-uq;                  % r is the reflection step
            iflag=0;                   % flag to check if r is in the feasible space (0 = yes, 1 = no)
            
             
            for k=1:n                  % check if the point 'r' is in the feasible space
                    if(r(k)<x_min(k)|r(k)>x_max(k))
                        iflag=iflag+1;
                    end
            end
            
            
            if iflag==0
                fr=objectivefunction_YoGEV(r);%,Y_vector);
                ite=ite+1;  % Calculate the number of iterations
                fstorage(ite,iloop)=min([fr ; fstorage(ite-1,iloop)]);
            else
                
                % Generate a point z in Omega according to a Normal distribution with best point of the sub-complex                                                                    % mal
                % as mean and standard deviation of the population as std                                                                     
                             
                for in=1:n
                    sigma(in)=std(D(1:s,in),1)./(x_max(in)-x_min(in));
                end
                
                
                z=generatenormalsample(complexA(1,1:n),sigma,x_min,x_max);
                
   
                r=z;
                fr=objectivefunction_YoGEV(r);%,Y_vector);
                ite=ite+1;  % Calculate the number of iterations
                fstorage(ite,iloop)=min([fr ; fstorage(ite-1,iloop)]);
            end
            if (fr<fq)
                uq=r;
                fq=fr;
            else
                c=(g+uq)/2;             % c is the contraction step
                fc=objectivefunction_YoGEV(c);%,Y_vector);
                ite=ite+1;  % Calculate the number of iterations
                fstorage(ite,iloop)=min([fc ; fstorage(ite-1,iloop)]);

                if(fc<fq)
                    uq=c;
                    fq=fc;
                else
                    
                    
                    % Generate a point z in Omega according to a Normal distribution with best point of the sub-complex                                                                    % mal
                    % as mean and standard deviation of the population as std                                                                     
                             
                    for in=1:n
                        sigma(in)=std(D(1:s,in),1)./(x_max(in)-x_min(in));
                    end
                    
                    
                    z=generatenormalsample(complexA(1,1:n),sigma,x_min,x_max);
                    

                    fz=objectivefunction_YoGEV(z);%,Y_vector);
                    ite=ite+1;  % Calculate the number of iterations
                    fstorage(ite,iloop)=min([fz ; fstorage(ite-1,iloop)]);

                    uq=z;
                    fq=fz;
                end
            end  
             
            B(end,1:n)=uq;
            B(end,n+1)=fq;
         
        end  % loop for alfa
        
        complexA(L,:)=B;  % Replace B into A according to L
        
        [faux index]=sort(complexA(:,n+1)); % Sort A in order of increasing function value
        complexA=[complexA(index,1:n) faux];
        
       
    end % loop for beta
    
    
    A(ip).D=complexA;
    
end  % loop for each complex A(k)


 
  
    

% 6. Shuffle the complexes replacing A(i) into D and sorting D in order of
% increasing function value

x2=[];
for ip=1:p
    x2=[x2; A(ip).D(:,:)];
end
[faux index]=sort(x2(:,n+1));
D2=[x2(index,1:n) faux];

%err=abs(fopt-D2(1,n+1))/abs(fopt)*100; % Relative error in %
err=abs(fopt-D2(1,n+1)); % Absolute error in %


%criter(20)=D2(1,n+1);
%if(nloop>=(kstop+1))
%    denomi=abs(criter(20-kstop)+criter(20))/2;
%    timeou=abs(criter(20-kstop)-criter(20))/denomi;
%end
%for l=1:19
%    criter(l)=criter(l+1);
%end


D=D2;

if(p>pmin) % Reduction in the number of complexes (Duan et al., 1994)
    p=p-1;
    s=p*m;
end


end % close loop about err>=errmin 


end % close loop of 'iloop'

bestsolution=D(1,1:n);
bestf=D(1,n+1);

function [Yord,Prob,Yp,P]=DetClas2(Y,Clas,P1,Pend)

% P1=0.1;
% Pend=0.999;
% Clas=20;

Yord=sort(Y);

% for i=1:length(Y)
%     Prob(i)=i/(length(Y)+1);
% end

Prob=cumsum(ones(1,length(Y)))./(length(Y)+1);


X1=-log(log(1./P1));
Xend=-log(log(1./Pend));
Delta=(Xend-X1)/(Clas-1);
X=[X1:Delta:Xend];
P=1./(exp(exp(-X)));

i=1;
while i<=length(P)

    pos=find(Prob>=P(i));
    
    if length(pos)>=5
        Yp(i)=Yord(min(pos));
        i=i+1;
    else
        i=length(P)+1;
    end
    
end

P=P(1:length(Yp));

P=P(length(find(Yp==Yp(1))):end);
Yp=Yp(length(find(Yp==Yp(1))):end);


function [x_min,x_max] = boundsGEV
% This scriopt reads from an external file the lower and upper bound for
% every parameter. Two columns: x_min x_max
%
% The variable 'file' must contain the name of the file

% file='limites.txt';
% aux=load(file);
aux=[-10 20;-1 1;0.001 20];

x_min=aux(1:end,1)';     
x_max=aux(1:end,2)';


function [x] = generateuniformsample (x_min,x_max)

n=length(x_min);

x(1:n)=x_min+(x_max-x_min).*rand(1,n);


function f =objectivefunction_YoGEV(x);%,Y_vector);

% global psi xi mu;

global Yord Prob Yp P

psi=x(1);
xi=x(2);
mu=x(3);


% P1=0.1;
% Pend=0.999;
% Clas=20;
% 
% [Yord,Prob,Yp,P]=DetClas(Y_vector,Clas,P1,Pend);

N=length(Yp);
for i=1:N
    
    Yn(i)=mu- (psi./xi.* ( 1-(-log(    P(i)     )).^(-xi) ) );
  
end

% f=sum((Yp-Yn)./Yn)^2;
f=sum((Yp-Yn).^2);


function lista = simulatrapezoidal(m,q)
% Function to simulate q points in a trapezoidal distribution of m elements
% without replacement


lista=zeros(1,q);
lista(1)=trapezoidal(m);
for i=2:q
    lista(i)=trapezoidal(m);
    while isempty(find(lista(1:i-1)==lista(i)))==0
        lista(i)=trapezoidal(m);
    end
end


function inv = trapezoidal (m)
% This function calculates the random value associated to a discrete
% trapezoidal distribution
% m is the dimension of the complex
im=1:m;
prob=2*(m+1-im)/(m*(m+1)); 
F=cumsum(prob);
xr=rand(1,1);
if(xr<=F(1))
    iposi=1;
end
for i=2:m
    if(xr>F(i-1)&xr<=F(i))
        iposi=i;
    end
end
inv=iposi;


function  [z] = generatenormalsample(z0,sigma,x_min,x_max);

n=length(z0);
for k=1:n
    z(k)=z0(k)+sigma(k).*(x_max(k)-x_min(k)).*normrnd(0,1,1,1); 
    while (z(k)<x_min(k)|z(k)>x_max(k))
        z(k)=z0(k)+sigma(k).*(x_max(k)-x_min(k)).*normrnd(0,1,1,1); 
    end
end