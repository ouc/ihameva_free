function [Dq] = DQuantile (F,t,OPHr,kt,indices)
%   Function DQuantile calculates the quantile q  derivative associated with a given
%   parameterization with respect model parameters
%
%   Input:
%       F -> Quantile probability
%       t -> time when the data occur within a yearly scale, so that
%       covariates can be used
%       kt -> Frequency parameter to measure the importance od the number of points
%       indices -> covariates data
%       OPHr -> Optimal solution
%
%   Output:
%
%       Dq -> Quantile gradients with respect to parameters
%
%   Created: xx/xx/2014
%
%   For more details see the paper:
%   "Pseudo-Optimal Parameter Selection of Non-Stationary
%   Generalized Extreme Value Models for Environmental Variables".
%
%
global neps0

nd=length(t);
nmu=length(OPHr.beta);
npsi=length(OPHr.alpha);
neps=length(OPHr.gamma);
lista=OPHr.lista;
lista2=OPHr.lista2;
lista3=OPHr.lista3;
nind = length(lista);
nind2 = length(lista2);
ntend = length(OPHr.betaT);
ntend2 = length(OPHr.betaT2);
if ~exist('kt','var') || isempty(kt),  kt = ones(size(t)); end
if ~exist('indices','var'), indices=[]; end
%
[mut,psit,epst,posG,pos,mut1,psit1]=amevaOPH.parameters_t(t,OPHr.pS,kt,indices,lista,lista2,lista3);

%   Derivatives of the quantile function with respect to location,
%   scale and shape parameters
Dqmut(pos) = ones(size(mut(pos)));
Dqpsit(pos) = -(1-(-log(F)./kt(pos)).^(-epst(pos)))./epst(pos);
Dqepst(pos) = psit(pos).*(1-(-log(F)./kt(pos)).^(-epst(pos)).*(1+epst(pos).*log(-log(F)./kt(pos))))./(epst(pos).*epst(pos));

%   Gumbel derivatives
Dqmut(posG) = ones(size(mut(posG)));
Dqpsit(posG) = -log(-log(F)./kt(posG));
Dqepst(posG) =zeros(size(mut(posG)));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   NUEVAS DERIVADAS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Dmutastmut = ones(size(kt));
Dmutastpsit = (-1+kt.^epst)./epst;
Dmutastepst = psit1.*(1+kt.^epst.*(epst.*log(kt)-1))./(epst.^2);

Dpsitastpsit = kt.^epst;
Dpsitastepst = log(kt).*psit1.*kt.^epst;

Dmutastpsit(posG) = log(kt(posG));
Dmutastepst(posG) = 0;

Dpsitastpsit(posG) = 1;
Dpsitastepst(posG) = 0;

%   Derivadas de los parametros de localizacion, forma y escala con
%   respecto a los parametros del modelo beta0, betaj, ...

Dq = sparse(zeros(2+neps0+nmu+npsi+neps+ntend+nind+ntend2+nind2,nd));
%   Jacobian elements related to the location parameters beta0
%   and beta
%     Dq(1,:) = Dqmut;
Dq(1,:) = Dqmut.*Dmutastmut';
%   If location harmonics are included
if nmu>0,
    for i = 1:nmu,
        for k = 1:length(t),
            %   Funtion Dparam is explained below
            %                 Dq(1+i,k) = Dqmut(k)*Dparam (t(k),i);
            Dq(1+i,k) = Dqmut(k)*Dmutastmut(k)*Dparam (t(k),i);
        end
        
    end
end
%   Jacobian elements related to the location parameters betaT,
%   and varphi
if ntend>0
    %         Dq(2+nmu,:) =  Dqmut.*(t');
    Dq(2+nmu,:) =  Dqmut.*(t.*Dmutastmut)';
end
if nind>0
    for i = 1:nind,
        %             Dq(1+nmu+ntend+i,:) =  Dqmut.*(indices(:,lista(i))');
        Dq(1+nmu+ntend+i,:) =  Dqmut.*(indices(:,lista(i)).*Dmutastmut)';
    end
end
%   Jacobian elements related to the scale parameters alpha0
%   and alpha
%     Dq(2+ntend+nind+nmu,:) = Dqpsit.*(psit');
Dq(2+ntend+nind+nmu,:) = (psit1.*(Dqpsit'.*Dpsitastpsit+Dqmut'.*Dmutastpsit))';
%   If scale harmonics are included
if npsi>0,
    for i = 1:npsi,
        for k = 1:length(t),
            %                 Dq(2+nmu+ntend+nind+i,k) =Dqpsit(k)*Dparam
            %                 (t(k),i)*psit(k);
            Dq(2+nmu+ntend+nind+i,k) =Dparam (t(k),i)*psit1(k)*(Dqpsit(k)*Dpsitastpsit(k)...
                +Dqmut(k)*Dmutastpsit(k));
        end
    end
end
%   Jacobian elements related to the scale parameters alphaT,
%   and varphi, equation A.10
if ntend2>0,
    %         Dq(2+neps0+nmu+ntend+nind+npsi,:) =  Dqpsit.*(t.*psit)';
    Dq(2+neps0+nmu+ntend+nind+npsi,:) =  ((Dqpsit'.*Dpsitastpsit+Dqmut'.*Dmutastpsit).*t.*psit1)';
end
if nind2>0,
    for i = 1:nind2,
        %             Dq(2+nmu+ntend+nind+npsi+ntend2+i,:) =
        %             Dqpsit.*(indices(:,lista2(i)).*psit)';
        Dq(2+nmu+ntend+nind+npsi+ntend2+i,:) =  ((Dqpsit'.*Dpsitastpsit+Dqmut'.*Dmutastpsit).*indices(:,lista2(i)).*psit1)';
    end
end
%   Jacobian elements related to the shape parameters gamma0
%   and gamma
if neps0 == 1,
    %         Dq(2+neps0+ntend+nind+nmu+npsi+ntend2+nind2,:) = Dqepst;
    Dq(2+neps0+ntend+nind+nmu+npsi+ntend2+nind2,:) = (Dqepst'+Dqpsit'.*Dpsitastepst+Dqmut'.*Dmutastepst)';
end
%   If shape harmonics are included
if neps>0,
    for i = 1:neps,
        for k = 1:length(t),
            %                 Dq(neps0+2+nmu+npsi+ntend+nind+ntend2+nind2+i,k) = Dqepst(k)*Dparam (t(k),i);
            Dq(neps0+2+nmu+npsi+ntend+nind+ntend2+nind2+i,k) = (Dqepst(k)+Dqpsit(k)*Dpsitastepst(k)+Dqmut(k)*Dmutastepst(k))*Dparam (t(k),i);
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function dp = Dparam (t,j)
        %
        %   Derivative of the location, scale and shape functions with respect
        %   to harmonic parameters. It correspond to the right hand side
        %   in equation A.11 of the paper
        %
        %   Input:
        %        t-> Time (Yearly scale)
        %        j-> Harmonic number
        %
        %   Output:
        %        dp-> Corresponding derivative
        %
        if mod(j,2) == 0,
            dp = sin(j/2*2*pi*t);
        else
            dp = cos((j+1)/2*2*pi*t);
        end
        
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end
