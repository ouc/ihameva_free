function q = CDFAgregado (zq,t0,t1,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices,indices2,times)

if nargin<11, betaT = 0;end
if nargin<12, betaT2 = 0;end
if nargin<13, varphi2 = [];end
if nargin<14, varphi = [];end
if nargin<15, indices = [];end
if nargin<16, indices2 = [];end
if nargin<17, times = [];end


[m,n] = size(zq);
[m1,n1] = size(t0);
if m1~=m | n1~=n,
    error('Initial CDF aggregated integration time size must be equal than the quantile size');
end
[m2,n2] = size(t1);
if m2~=m | n2~=n,
    error('Final CDF aggregated integration time size must be equal than the quantile size');
end

%   For the required period the mean value of the corresponding covariates
%   is calculated and considered constant for the rest of the study
if ~isempty(times),
    pos = find(times>=t0 & times<=t1);
    indicesint = zeros(length(varphi),1);
    indices2int = zeros(length(varphi2),1);
    if ~isempty(pos),
        for i = 1:length(varphi),
            indicesint(i)=mean(indices(pos,i));
        end
        for i = 1:length(varphi2),
            indices2int(i)=mean(indices2(pos,i));
        end
    end
else
    indicesint = [];
    indices2int = [];
end    
 
if isempty(kt),
    kt = ones(size(x));
end

%   Requires quantile
qout = zeros(m,n);
       
for i1 = 1:m,
    for j1 = 1:n,
        [int cnt]=bsimp(@(x)fzeroquanint(x,zq(i1,j1),kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi2,varphi,indices,indices2,indicesint,indices2int),t0(i1,j1),t1(i1,j1));
        q(i1,j1) = exp(-12*int);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Function to solve the quantile
    function zn = fzeroquanint(t,zq,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi2,varphi,indices,indices2,indicesint,indices2int)
        %   Location
        mut1 = parametro(t,beta0,beta,betaT,indices,varphi,indicesint);
        %   Scale
        psit1 = exp(parametro(t,alpha0,alpha,betaT2,indices2,varphi2,indices2int));
        %   Shape
        epst = parametro(t,gamma0,gamma);

        %   The values whose shape parameter is almost cero corresponds to
        %   the GUMBEL distribution, locate their positions if they exist
        posG = find(abs(epst)<=1e-8);
        %   The remaining values correspond to WEIBULL or FRECHET
        pos  = find(abs(epst)>1e-8);
        %   The corresponding GUMBEl values are set to 1 to avoid 
        %   numerical problems, note that for those cases the GUMBEL
        %   expressions are used
        epst(posG)=1;

        %   Modifico los parametros para incluir el numero de datos
        mut = mut1;
        psit = psit1;
        mut(pos) = mut1(pos)+psit1(pos).*(kt(pos).^epst(pos)-1)./epst(pos);
        psit(pos) = psit1(pos).*kt(pos).^epst(pos);
        %   Modifico los parametros para incluir el numero de datos para GUMBEL
        mut(posG) = mut(posG)+psit(posG).*log(kt(posG));
        
        %   Evaluate auxiliary variables
        xn = (zq-mut)./psit; 
        z = 1 + epst.*xn; 
        %   Since the z-values must be greater than zero in order to avoid
        %   numerical problems their values are set to be greater than 1e-4
        z = max(1e-8,z);
        zn =z.^(-1./epst); 
        %    For the Gumbel case
        zn(posG) = exp(-xn(posG));
        %   
        zn = zn.*kt;
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Function to solve the quantile
    function zn = fzeroderiquanint(t,zq,kt,q,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi2,varphi,indices,indices2,indicesint,indices2int)
        %   Location
        mut1 = parametro(t,beta0,beta,betaT,indices,varphi,indicesint);
        %   Scale
        psit1 = exp(parametro(t,alpha0,alpha,betaT2,indices2,varphi2,indices2int));
        %   Shape
        epst = parametro(t,gamma0,gamma);

        %   The values whose shape parameter is almost cero corresponds to
        %   the GUMBEL distribution, locate their positions if they exist
        posG = find(abs(epst)<=1e-8);
        %   The remaining values correspond to WEIBULL or FRECHET
        pos  = find(abs(epst)>1e-8);
        %   The corresponding GUMBEl values are set to 1 to avoid 
        %   numerical problems, note that for those cases the GUMBEL
        %   expressions are used
        epst(posG)=1;
        
        %   Modifico los parametros para incluir el numero de datos
        mut = mut1;
        psit = psit1;
        mut(pos) = mut1(pos)+psit1(pos).*(kt(pos).^epst(pos)-1)./epst(pos);
        psit(pos) = psit1(pos).*kt(pos).^epst(pos);
        %   Modifico los parametros para incluir el numero de datos para GUMBEL
        mut(posG) = mut(posG)+psit(posG).*log(kt(posG));
        
        %   Evaluate auxiliary variables
        xn = (zq-mut)./psit; 
        z = 1 + epst.*xn; 
        %   Since the z-values must be greater than zero in order to avoid
        %   numerical problems their values are set to be greater than 1e-4
        z = max(1e-8,z);
        zn =-z.^(-1-1./epst)./psit; 
        %    For the Gumbel case
        zn(posG) = -exp(-xn(posG))./psit(posG);
        
        %   
        zn = zn.*kt;
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Function to integrate
    function [I,cnt]=bsimp(funfcn,a,b,n,epsilon,trace)
    %	BSIMP   Numerically evaluate integral, low order method.
    %   I = BSIMP('F',A,B) approximates the integral of F(X) from A to B 
    %   within a relative error of 1e-3 using an iterative
    %   Simpson's rule.  'F' is a string containing the name of the
    %   function.  Function F must return a vector of output values if given
    %   a vector of input values.%
    %   I = BSIMP('F',A,B,EPSILON) integrates to a total error of EPSILON.  %
    %   I = BSIMP('F',A,B,N,EPSILON,TRACE,TRACETOL) integrates to a 
    %   relative error of EPSILON, 
    %   beginning with n subdivisions of the interval [A,B],for non-zero 
    %   TRACE traces the function 
    %   evaluations with a point plot.
    %   [I,cnt] = BSIMP(F,a,b,epsilon) also returns a function evaluation count.%
    %   Roberto Minguez Solana%   Copyright (c) 2001 by Universidad de Cantabria
        if nargin < 4, n=2; epsilon =10^(-8); trace = 0;end
        if nargin < 5, epsilon =10^(-8); trace = 0; end
        if nargin < 6,trace = 0; end
        if isempty(epsilon), epsilon =10^(-8); end
        if isempty(trace), trace = 0; end
        if rem(n,2)~= 0 
           warning('The number of initial subintervals must be pair');
        end
        %Step 1
        h = (b-a)/n;
        % Step 3
        x = linspace(a,b,n+1);
        y = feval(funfcn,x);
        if trace 
           figure(1)  
           hold on
           lims1 = [min(x) max(x) min(y) max(y)];
           axis(lims1);
           plot(x,y,'.')
           drawnow
        end
        AuxI=0;
        Ainit=y(1)+y(n+1);
        AuxI1=0;
        AuxI2=0;
        for i=1:1:n/2,
           AuxI1=AuxI1+y(2*i);
        end
        for i=1:1:n/2-1,
           AuxI2=AuxI2+y(2*i+1);
        end
        cnt=n+1;
        %Step 4
        I=(Ainit+4*AuxI1+2*AuxI2)*h/3;
        AuxTot=AuxI1+AuxI2;
        %Step 5
        I1=I+epsilon*2;j=2;
        %Step 6
        error = 1;
        while error>epsilon,
           cnt=cnt+j*n/2; 
           %Step 7  
           I1=I;
           %Step 8 
           x=linspace(a+h/j,b-h/j,j*n/2);  
           y = feval(funfcn,x);
           for i=1:1:j*n/2  
              AuxI=AuxI+y(i);  
           end  
           %Step 9 
           I=(Ainit+4*AuxI+2*AuxTot)*h/(3*j);
           %Step 10  
           j=j*2; 
           %Step 11  
           AuxTot=AuxTot+AuxI;
           AuxI=0;
           %    Error
           if abs(I1)>10^(-5),
               error = abs((I - I1)/I1);
           else
               error = abs((I - I1));
           end
           %Plot   
           if trace   
              figure(1)   
              hold on 
              plot(x,y,'.')  
              drawnow 
           end    
        end
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end