function [f Jx Hxx] = amevaloglikelihood(p,OPHobj)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Loglikelihood function definition of the GEV distribution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Aqui se modifica el neps0, y la función que use amevaloglikelihood
%%%%%% tiene que tener declarada nesp0 de forma global
global neps0;
if isempty(neps0), [neps0,OPHobj.gamma0]=amevaOPH.check_gumbel_model(OPHobj.gamma0); warning('No global neps0 define'); end
x=OPHobj.x; t=OPHobj.t; kt=OPHobj.kt; indices=OPHobj.indices; %indicesF=OPHobj.indicesF;
%   Related to armonic
nmu=length(OPHobj.beta);
npsi=length(OPHobj.alpha);
neps=length(OPHobj.gamma);
%   Related to the covariate
lista=OPHobj.lista; lista2=OPHobj.lista2; lista3=OPHobj.lista3;
nind = length(lista);
nind2 = length(lista2);
nind3 = length(lista3);
%   Related to the tendency
ntend = length(OPHobj.betaT);
ntend2 = length(OPHobj.betaT2);
ntend3 = length(OPHobj.betaT3);
if isempty(kt),
    kt = ones(size(x));
end
if not(isempty(p)),
    OPHr = amevaOPH.oph_r(p,neps0,nmu,ntend,nind,npsi,ntend2,nind2,neps,ntend3,nind3,lista,lista2,[],[],[],[],[]);
    pS = OPHr.pS; 
    ps = OPHr.ps; clear OPHr;
else
    [pS,ps] = amevaOPH.objparam2pSps(OPHobj);
end
%   Evaluate the location,scale and shape parameter at each time t as a function
%   of the actual values of the parameters given by p
[mut,psit,epst,posG,pos,mut1,psit1]=amevaOPH.parameters_t(pS,OPHobj);
%   Evaluate auxiliary variables
xn = (x-mut)./psit;
z = 1 + epst.*xn;
%   Since the z-values must be greater than zero in order to avoid
%   numerical problems their values are set to be greater than 1e-4
z = max(amevaOPH.loglikeSizeZval,z);
zn =z.^(-1./epst);

%   Evaluate the loglikelihood function with the sign changed, not
%   that the general and Gumbel expresions are used
f = sum(-log(kt(pos)) +log(psit(pos)) +(1+1./epst(pos)).*log(z(pos))+kt(pos).*zn(pos))...
    +sum(-log(kt(posG))+log(psit(posG))+xn(posG)+kt(posG).*exp(-xn(posG)));

%   Gradient of the loglikelihood function
if nargout>1,
    %   Derivatives given by equations (A.1)-(A.3) in the paper
    Dmut = (1+epst-kt.*zn)./(psit.*z);
    Dpsit = -(1-xn.*(1-kt.*zn))./(psit.*z);
    Depst = zn.*(xn.*(kt-(1+epst)./zn)+z.*(-kt+1./zn).*log(z)./epst)./(epst.*z);
    
    %   GUMBEL derivatives given by equations (A.4)-(A.5) in the paper
    Dmut(posG) = (1-kt(posG).*exp(-xn(posG)))./(psit(posG));
    Dpsit(posG) = (xn(posG)-1-kt(posG).*xn(posG).*exp(-xn(posG)))./(psit(posG));
    Depst(posG)=0;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   NUEVAS DERIVADAS
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Dmutastmut = ones(size(kt));
    Dmutastpsit = (-1+kt.^epst)./epst;
    Dmutastepst = psit1.*(1+kt.^epst.*(epst.*log(kt)-1))./(epst.^2);
    
    Dpsitastpsit = kt.^epst;
    Dpsitastepst = log(kt).*psit1.*kt.^epst;
    
    Dmutastpsit(posG) = log(kt(posG));
    Dmutastepst(posG) = 0;
    
    Dpsitastpsit(posG) = 1;
    Dpsitastepst(posG) = 0;
    
    %   Set the Jacobian to zero
    Jx = sparse(zeros(2+neps0+nmu+npsi+neps+ntend+nind+ntend2+nind2+ntend3+nind3,1));
    %   Jacobian elements related to the location parameters beta0
    %   and beta, equation A.6 in the paper
    Jx(1,1) = sum(Dmut.*Dmutastmut);
    %   If location harmonics are included
    if nmu>0,
        for i = 1:nmu,
            aux = 0;
            for k = 1:length(t),
                %   Funtion Dparam is explained below
                aux = aux + Dmut(k)*Dmutastmut(k)*Dparam (t(k),i);
            end
            Jx(1+i,1) = aux;
        end
    end
    %   Jacobian elements related to the location parameters betaT,
    %   and varphi, equation A.9
    if ntend>0
        Jx(2+nmu,1) =  sum(Dmut.*t.*Dmutastmut);
    end
    if nind>0
        for i = 1:nind,
            Jx(1+nmu+ntend+i,1) =  sum(Dmut.*indices(:,lista(i)).*Dmutastmut);
        end
    end
    %   Jacobian elements related to the scale parameters alpha0
    %   and alpha, equation A.7 in the paper
    Jx(2+ntend+nind+nmu,1) = sum(psit1.*(Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit));
    %   If scale harmonics are included
    if npsi>0,
        for i = 1:npsi,
            aux = 0;
            for k = 1:length(t),
                aux = aux + Dparam (t(k),i)*psit1(k)*(Dpsit(k)*Dpsitastpsit(k)...
                    +Dmut(k)*Dmutastpsit(k));
            end
            Jx(2+nmu+ntend+nind+i,1) = aux;
        end
    end
    %   Jacobian elements related to the scale parameters alphaT,
    %   and varphi, equation A.10
    if ntend2>0,
        Jx(2+ntend2+nmu+ntend+nind+npsi,1) =  sum((Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit).*t.*psit1);
    end
    if nind2>0,
        for i = 1:nind2,
            Jx(2+nmu+ntend+nind+npsi+ntend2+i,1) =  sum((Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit).*indices(:,lista2(i)).*psit1);
        end
    end
    %   Jacobian elements related to the shape parameters gamma0
    %   and gamma, equation A.10 in the paper
    if neps0 == 1,
        Jx(2+neps0+ntend+nind+nmu+npsi+ntend2+nind2,1) = sum(Depst+Dpsit.*Dpsitastepst+Dmut.*Dmutastepst);
    end
    %   If shape harmonics are included
    if neps>0,
        for i = 1:neps,
            aux = 0;
            for k = 1:length(t),
                aux = aux + (Depst(k)+Dpsit(k)*Dpsitastepst(k)+Dmut(k)*Dmutastepst(k))*Dparam (t(k),i);
            end
            Jx(neps0+2+nmu+npsi+ntend+nind+ntend2+nind2+i,1) = aux;
        end
    end
    %   Jacobian elements related to the shape parameters alphaT,
    %   and varphi, equation A.xx
    if ntend3>0, %OJO OJO nuevas derivadas
        %Jx(neps0+2+nmu+npsi+ntend+nind+ntend2+nind2+neps,1) =  sum(Dmut.*t.*Dmutastmut);
        %Jx(neps0+2+nmu+npsi+ntend+nind+ntend2+nind2+neps,1) =  sum((Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit).*t.*psit1);
        Jx(neps0+2+nmu+npsi+ntend+nind+ntend2+nind2+neps+1,1) =  sum((Depst+Dpsit.*Dpsitastepst+Dmut.*Dmutastepst).*t.*psit1);
    end
    if nind3>0, %OJO OJO nuevas derivadas
        for i = 1:nind3,
            Jx(neps0+2+nmu+npsi+ntend+nind+ntend2+nind2+neps+ntend3+i,1) =  sum((Depst+Dpsit.*Dpsitastepst+Dmut.*Dmutastepst).*indices(:,lista3(i)).*psit1); %%OJO OJO indicesF x indices
        end
    end
    %   Since the numerical problem is a minimization problem the
    %   Jacobian sign must be changed
    Jx = -Jx;
end
%   Hessian matrix of the loglikelihood function (Esta listo sin Tendencias ni Covariables en forma)
if nargout>2 %&& ntend3==0 && nind3==0,
    %   Derivatives given by equations A.13-A.17 in the paper
    D2mut = (1+epst).*zn.*(-1+epst.*z.^(1./epst))./((z.*psit).^2);
    D2psit = (-zn.*xn.*((1-epst).*xn-2)+(((1-2*xn)-epst.*(xn).^2)))./((z.*psit).^2);
    D2epst = -zn.*(...
        +xn.*(xn.*(1+3*epst)+2+(-2-epst.*(3+epst).*xn).*z.^(1./epst))...
        +z./(epst.*epst).*log(z).*(2*epst.*(-xn.*(1+epst)-1+z.^(1+1./epst))+z.*log(z))...
        )./(epst.*epst.*z.^2);
    Dmutpsit = -(1+epst-(1-xn).*zn)./((z.*psit).^2);
    Dmutepst = -zn.*(epst.*(-(1+epst).*xn-epst.*(1-xn).*z.^(1./epst))+z.*log(z))./(epst.*epst.*psit.*z.^2);
    Dpsitepst = xn.*Dmutepst;
    %   Corresponding Gumbel derivatives given by equations
    %   A.18-A.20 in the paper
    D2mut(posG) = -(exp(-xn(posG)))./(psit(posG).^2);
    D2psit(posG) = ((1-2*xn(posG))+exp(-xn(posG)).*(2-xn(posG)).*xn(posG))./(psit(posG).^2);
    D2epst(posG) = 0;
    Dmutpsit(posG) = (-1+exp(-xn(posG)).*(1-xn(posG)))./(psit(posG).^2);
    Dmutepst(posG) = 0;
    Dpsitepst(posG) = 0;
    %   Initialize the hessian matrix
    np=2+neps0+nmu+npsi+neps+ntend+nind+ntend2+nind2+ntend3+nind3;
%     Hxx = sparse(zeros(np,np));
    Hxx(np,np) = 0;
    %   Elements of the Hessian matrix
    %   Sub-blocks following the order shown in Table 4 of the
    %   Diag --ini--
    %   Sub-block number 1,1(1)
    Hxx(1,1) = sum(D2mut);
    %   Sub-block number 3,3(2)
    if ntend>0
        Hxx(2+nmu,1+nmu+ntend) =  sum(D2mut.*(t.^2));
    end
    %   Sub-block number 4.4(3)
    if nind>0
        for i = 1:nind,
            for j = 1:i,
                Hxx(1+nmu+ntend+i,1+nmu+ntend+j) =  sum(D2mut.*indices(:,lista(i)).*indices(:,lista(j)));
            end
        end
    end
    %   Sub-block number 5,5(6) (Scale exponential involved)
    Hxx(2+nmu+ntend+nind,2+nmu+ntend+nind) = sum((D2psit.*psit+Dpsit).*psit);
    %   Sub-block number 7,7(4) (Scale exponential involved)
    if ntend2>0
        Hxx(2+ntend2+nmu+npsi+ntend+nind,2+nmu+npsi+ntend+nind+ntend2) =  sum((D2psit.*psit+Dpsit).*psit.*(t.^2));
    end
    %   Sub-block number 8,8(5) (Scale exponential involved)
    if nind2>0
        for i = 1:nind2,
            for j = 1:i,
                Hxx(2+nmu+npsi+ntend+nind+ntend2+i,2+nmu+npsi+ntend+nind+ntend2+j) =  sum((D2psit.*psit+Dpsit).*psit.*indices(:,lista2(i)).*indices(:,lista2(j)));
            end
        end
    end
    %   Sub-block number 9,9(7)
    if neps0==1,
        Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,2+neps0+nmu+npsi+ntend+nind+ntend2+nind2) = sum(D2epst);
    end
    %   Sub-block number 11,11(new) (Scale exponential involved)
    if ntend3>0
        Hxx(2+neps0+ntend2+nmu+npsi+ntend+nind+ntend2+nind2,2+neps0+ntend2+nmu+npsi+ntend+nind+ntend2+nind2) =  sum((D2epst.*epst+Depst).*epst.*(t.^2));
    end
    %   Sub-block number 12,12(new) (Scale exponential involved)
    if nind3>0
        for i = 1:nind3,
            for j = 1:i,
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+ntend3+i,2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+ntend3+j) =  sum((D2epst.*epst+Depst).*epst.*indices(:,lista3(i)).*indices(:,lista3(j)));
            end
        end
    end
    %   Diag --fin--
    %   Sub-block number 3,1(11)
    if ntend>0,
        Hxx(1+nmu+ntend,1) = sum(D2mut.*t);
    end
    %   Sub-block number 4,1(13)
    if nind>0,
        for i = 1:nind,
            Hxx(1+nmu+ntend+i,1) = sum(D2mut.*indices(:,lista(i)));
        end
    end
    %   Sub-block number 4,3(15)
    if nind>0 && ntend>0,
        for i = 1:nind,
            Hxx(1+nmu+ntend+i,1+nmu+ntend) = sum(D2mut.*t.*indices(:,lista(i)));
        end
    end
    %   Sub-block number 5,1(8) (Scale exponential involved)
    Hxx(2+nmu+ntend+nind,1) = sum(Dmutpsit.*psit);
    %   Sub-block number 5,3(17)
    if ntend>0,
        Hxx(2+nmu+ntend+nind,1+nmu+ntend) = sum(Dmutpsit.*t.*psit);
    end
    %   Sub-block number 5,4(20) (Scale exponential involved)
    if nind>0,
        for i = 1:nind,
            Hxx(2+nmu+ntend+nind,1+nmu+ntend+i) = sum(Dmutpsit.*indices(:,lista(i)).*psit);
        end
    end
    %   Sub-block number 7,1(12) (Scale exponential involved)
    if ntend2>0,
        Hxx(2+nmu+ntend+nind+npsi+ntend2,1) = sum(Dmutpsit.*t.*psit);
        %   Sub-block number 7,3(48) (Scale exponential involved)
        if ntend>0,
            Hxx(2+nmu+ntend+nind+npsi+ntend2,1+nmu+ntend) = sum(Dmutpsit.*t.*t.*psit);
        end
        %   Sub-block number 7,4(50) (Scale exponential involved)
        if nind>0,
            for i = 1:nind,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dmutpsit(k)*t(k)*indices(k,lista(i))*psit(k);
                end
                Hxx(2+nmu+ntend+nind+npsi+ntend2,1+nmu+ntend+i) = aux;
            end
        end
        %   Sub-block number 7,5(52) (Scale exponential involved)
        Hxx(2+nmu+ntend+nind+npsi+ntend2,2+nmu+ntend+nind) = sum((D2psit.*psit+Dpsit).*t.*psit);
    end
    %   Sub-block number 8,1(14) (Scale exponential involved)
    if nind2>0,
        for i = 1:nind2,
            Hxx(2+nmu+npsi+ntend+nind+ntend2+i,1) = sum(Dmutpsit.*indices(:,lista2(i)).*psit);
        end
        %   Sub-block number 8,3(49) (Scale exponential involved)
        if ntend>0,
            for i = 1:nind2,
                Hxx(2+nmu+npsi+ntend+nind+ntend2+i,1+nmu+ntend) = sum(Dmutpsit.*t.*indices(:,lista2(i)).*psit);
            end
        end
        %   Sub-block number 8,4(51) (Scale exponential involved)
        if nind>0,
            for i = 1:nind,
                for j = 1:nind2,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dmutpsit(k)*indices(k,lista2(j))*indices(k,lista(i))*psit(k);
                    end
                    Hxx(2+nmu+ntend+nind+npsi+ntend2+j,1+nmu+ntend+i) = aux;
                end
            end
        end
        %   Sub-block number 8,5(53) (Scale exponential involved)
        for i = 1:nind2,
            Hxx(2+nmu+npsi+ntend+nind+ntend2+i,2+nmu+ntend+nind) = sum((D2psit.*psit+Dpsit).*indices(:,lista2(i)).*psit);
        end
        %   Sub-block number 8,7(16)
        if ntend2>0,
            for i = 1:nind2,
                Hxx(2+nmu+npsi+ntend+nind+ntend2+i,2+nmu+npsi+ntend+nind+ntend2) = sum((D2psit.*psit+Dpsit).*t.*indices(:,lista2(i)).*psit);
            end
        end
    end
    if neps0 == 1,
        %   Sub-block number 9,1(9)
        Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,1) = sum(Dmutepst);
        %   Sub-block number 9,3(18)
        if ntend>0, 
            Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,1+nmu+ntend) = sum(Dmutepst.*t); 
        end
        %   Sub-block number 9,4(21)
        if nind>0,
            for i = 1:nind,
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,1+nmu+ntend+i) = sum(Dmutepst.*indices(:,lista(i)));
            end
        end
        %   Sub-block number 9,5(10) (Scale exponential involved)
        Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,2+nmu+ntend+nind) = sum(Dpsitepst.*psit);
        %   Sub-block number 9,7(19) (Scale exponential involved)
        if ntend2>0,
            Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,2+nmu+npsi+ntend+nind+ntend2) = sum(Dpsitepst.*t.*psit);
        end
        %   Sub-block number 9,8(22) (Scale exponential involved)
        if nind2>0,
            for i = 1:nind2,
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,2+nmu+ntend+nind+npsi+ntend2+i) = sum(Dpsitepst.*indices(:,lista2(i)).*psit);
            end
        end
    end
    %   Sub-block number 11,1(new) (Scale exponential involved)
    if ntend3>0,
        Hxx(2+neps0+nmu+ntend+nind+npsi+ntend2+nind2+ntend3,1) = sum(Dmutepst.*t.*epst);
        %   Sub-block number 11,3(new) (Scale exponential involved)
        if ntend>0,
            %Hxx(2+nmu+ntend+nind+npsi+ntend2,1+nmu+ntend) = sum(Dmutpsit.*t.*t.*psit);
        end
        %   Sub-block number 11,4(new) (Scale exponential involved)
        if nind>0 && ntend3>0,
            for i = 1:nind,
                %aux = 0;
                for k = 1:length(t),
                    %aux = aux + Dmutpsit(k)*t(k)*indices(k,lista(i))*psit(k);
                end
                %Hxx(2+nmu+ntend+nind+npsi+ntend2,1+nmu+ntend+i) = aux;
            end
        end
        %   Sub-block number 11,5(new)
        %   Sub-block number 11,7(new)
        %   Sub-block number 11,8(new)
        %   Sub-block number 11,9(new)
    end
    %   Sub-block number 12,1(new) (Scale exponential involved)
    if nind3>0,
        for i = 1:nind3,
            Hxx(2+neps0+nmu+ntend+nind+npsi+ntend2+nind2+ntend3+i,1) = sum(Dmutepst.*indices(:,lista3(i)).*epst);
        end
        %   Sub-block number 12,3(new) (Scale exponential involved)
        if ntend>0,
            for i = 1:nind3,
                %Hxx(2+nmu+npsi+ntend+nind+ntend2+i,1+nmu+ntend) = sum(Dmutpsit.*t.*indices(:,lista2(i)).*psit);
            end
        end
        %   Sub-block number 12,4(new)
        %   Sub-block number 12,5(new)
        %   Sub-block number 12,7(new)
        %   Sub-block number 12,8(new)
        %   Sub-block number 12,9(new)
        %   Sub-block number 12,11(new)
        if ntend3>0,
            for i = 1:nind3,
                %Hxx(2+nmu+npsi+ntend+nind+ntend2+i,2+nmu+npsi+ntend+nind+ntend2) = sum((D2psit.*psit+Dpsit).*t.*indices(:,lista3(i)).*psit);
            end
        end
    end
    if nmu>0,
        for i = 1:nmu,
            aux = 0;
            for k = 1:length(t),
                aux = aux + D2mut(k)*Dparam (t(k),i);
            end
            %   Sub-block number 23
            Hxx(1+i,1) = aux;
            for j = 1:i,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + D2mut(k)*Dparam (t(k),i)*Dparam (t(k),j);
                end
                %   Sub-block number 24
                Hxx(1+i,1+j) = aux;
            end
        end
        for i = 1:nmu,
            aux = 0;
            for k = 1:length(t),
                aux = aux + Dmutpsit(k)*Dparam (t(k),i)*psit(k);
            end
            %   Sub-block number 25 (Scale exponential involved)
            Hxx(2+nmu+ntend+nind,1+i) = aux;
        end
        if neps0 == 1,
            for i = 1:nmu,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dmutepst(k)*Dparam (t(k),i);
                end
                %   Sub-block number 26 (Scale exponential involved)
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,1+i) = aux;
            end
        end
        if ntend>0,
            for i = 1:nmu,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + D2mut(k)*t(k)*Dparam (t(k),i);
                end
                %   Sub-block number 27
                Hxx(2+nmu,1+i) = aux;
            end
        end
        if ntend2>0,
            for i = 1:nmu,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dmutpsit(k)*t(k)*Dparam (t(k),i)*psit(k);
                end
                %   Sub-block number 46 (Scale exponential involved)
                Hxx(2+nmu+ntend+nind+npsi+ntend2,1+i) = aux;
            end
        end
        if ntend3>0, %   New Sub-block OJO OJO
            for i = 1:nmu,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dmutepst(k)*t(k)*Dparam (t(k),i)*epst(k);
                end
                %   New Sub-block number xx (Scale exponential involved) OJO OJO
                Hxx(2+neps0+nmu+ntend+nind+npsi+ntend2+nind2+neps+ntend3,1+i) = aux;
            end
        end
        if nind>0,
            for i = 1:nmu,
                for j = 1:nind,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + D2mut(k)*indices(k,lista(j))*Dparam (t(k),i);
                    end
                    %   Sub-block number 28
                    Hxx(1+nmu+ntend+j,1+i) = aux;
                end
            end
        end
        if nind2>0,
            for i = 1:nmu,
                for j = 1:nind2,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dmutpsit(k)*indices(k,lista2(j))*Dparam (t(k),i)*psit(k);
                    end
                    %   Sub-block number 47 (Scale exponential
                    %   involved)
                    Hxx(2+nmu+ntend+nind+npsi+ntend2+j,1+i) = aux;
                end
            end
        end
        if nind3>0, %   New Sub-block OJO OJO
            for i = 1:nmu,
                for j = 1:nind3,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dmutepst(k)*indices(k,lista3(j))*Dparam (t(k),i)*epst(k);
                    end
                    %   New Sub-block number xx (Scale exponential involved) OJO OJO
                    %   involved)
                    Hxx(2+neps0+nmu+ntend+nind+npsi+ntend2+nind2+neps+ntend3+j,1+i) = aux;
                end
            end
        end
    end
    if npsi>0,
        for i = 1:npsi,
            aux = 0;
            for k = 1:length(t),
                aux = aux + (D2psit(k)*psit(k)+Dpsit(k))*Dparam (t(k),i)*psit(k);
            end
            %   Sub-block number 29 (Scale exponential involved)
            Hxx(2+nmu+ntend+nind+i,2+ntend+nind+nmu) = aux;
            for j = 1:i,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + (D2psit(k)*psit(k)+Dpsit(k))*Dparam (t(k),i)*Dparam (t(k),j)*psit(k);
                end
                %   Sub-block number 30 (Scale exponential involved)
                Hxx(2+nmu+ntend+nind+i,2+nmu+ntend+nind+j) = aux;
            end
        end
        if neps0 == 1,
            for i = 1:npsi,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dpsitepst(k)*Dparam (t(k),i)*psit(k);
                end
                %   Sub-block number 31 (Scale exponential involved)
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,2+nmu+ntend+nind+i) = aux;
            end
        end
        for i = 1:npsi,
            aux = 0;
            for k = 1:length(t),
                aux = aux + Dmutpsit(k)*Dparam (t(k),i)*psit(k);
            end
            %   Sub-block number 32 (Scale exponential involved)
            Hxx(2+nmu+ntend+nind+i,1) = aux;
        end
        if ntend>0,
            for i = 1:npsi,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dmutpsit(k)*t(k)*Dparam (t(k),i)*psit(k);
                end
                %   Sub-block number 33 (Scale exponential involved)
                Hxx(2+nmu+ntend+nind+i,2+nmu) = aux;
            end
        end
        if nind>0,
            for i = 1:npsi,
                for j = 1:nind,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dmutpsit(k)*indices(k,lista(j))*Dparam (t(k),i)*psit(k);
                    end
                    %   Sub-block number 34 (Scale exponential
                    %   involved)
                    Hxx(2+nmu+ntend+nind+i,1+nmu+ntend+j) = aux;
                end
            end
        end
    end
    if neps>0,
        for i = 1:neps,
            %   Primer elemento asociado al valor constante (primera columna)
            aux = 0;
            for k = 1:length(t),
                aux = aux + D2epst(k)*Dparam (t(k),i);
            end
            %   Sub-block number 35
            Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+neps0+nmu+npsi+ntend+nind+ntend2+nind2) = aux;
            for j = 1:i,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + D2epst(k)*Dparam (t(k),i)*Dparam (t(k),j);
                end
                %   Sub-block number 36
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+j) = aux;
            end
        end
        for i = 1:neps,
            aux = 0;
            for k = 1:length(t),
                aux = aux + Dpsitepst(k)*Dparam (t(k),i)*psit(k);
            end
            %   Sub-block number 37 (Scale exponential involved)
            Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+nmu+ntend+nind) = aux;
        end
        for i = 1:neps,
            aux = 0;
            for k = 1:length(t),
                aux = aux + Dmutepst(k)*Dparam (t(k),i);
            end
            %   Sub-block number 38
            Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,1) = aux;
        end
        if ntend>0,
            for i = 1:neps,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dmutepst(k)*t(k)*Dparam (t(k),i);
                end
                %   Sub-block number 39
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+nmu) = aux;
            end
        end
        if ntend2>0,
            for i = 1:neps,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dpsitepst(k)*t(k)*Dparam (t(k),i)*psit(k);
                end
                %   Sub-block number 44 (Scale exponential involved)
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+nmu+npsi+ntend+nind+ntend2) = aux;
            end
        end
        if ntend3>0, %   New Sub-block OJO OJO
            for i = 1:neps,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dpsitepst(k)*t(k)*Dparam (t(k),i)*epst(k);
                end
                %   Sub-block number xx (Scale exponential involved)
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+nmu+npsi+ntend+nind+ntend2+nind2+ntend3) = aux;
            end
        end
        if nind>0,
            for i = 1:neps,
                for j = 1:nind,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dmutepst(k)*indices(k,lista(j))*Dparam (t(k),i);
                    end
                    %   Sub-block number 40
                    Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,1+nmu+ntend+j) = aux;
                end
            end
        end
        if nind2>0,
            for i = 1:neps,
                for j = 1:nind2,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dpsitepst(k)*indices(k,lista2(j))*Dparam (t(k),i)*psit(k);
                    end
                    %   Sub-block number 45 (Scale exponential
                    %   involved)
                    Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+nmu+npsi+ntend+nind+ntend2+j) = aux;
                end
            end
        end
        if nind3>0, %   New Sub-block OJO OJO
            for i = 1:neps,
                for j = 1:nind3,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Destepst(k)*indices(k,lista3(j))*Dparam (t(k),i)*psit(k);
                    end
                    %   Sub-block number 45 (Scale exponential
                    %   involved)
                    Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+nmu+npsi+ntend+nind+ntend2+j) = aux;
                end
            end
        end
    end
    if ntend2>0,
        for i = 1:npsi,
            aux = 0;
            for k = 1:length(t),
                aux = aux + (D2psit(k)*psit(k)+Dpsit(k))*t(k)*Dparam (t(k),i)*psit(k);
            end
            %   Sub-block number 54 (Scale exponential involved)
            Hxx(2+nmu+ntend+nind+npsi+ntend2,2+nmu+ntend+nind+i) = aux;
        end
    end
    if nind2>0,
        for i = 1:npsi,
            for j = 1:nind2,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + (D2psit(k)*psit(k)+Dpsit(k))*indices(k,lista2(j))*Dparam (t(k),i)*psit(k);
                end
                %   Sub-block number 55 (Scale exponential involved)
                Hxx(2+nmu+ntend+nind+npsi+ntend2+j,2+nmu+ntend+nind+i) = aux;
            end
        end
    end
    if nmu>0 && npsi>0,
        for j = 1:nmu,
            for i = 1:npsi,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dmutpsit(k)*Dparam (t(k),i)*Dparam (t(k),j)*psit(k);
                end
                %   Sub-block number 41 (Scale exponential involved)
                Hxx(2+nmu+ntend+nind+i,1+j) = aux;
            end
        end
    end
    if nmu>0 && neps>0,
        for j = 1:nmu,
            for i = 1:neps,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dmutepst(k)*Dparam (t(k),i)*Dparam (t(k),j);
                end
                %   Sub-block number 42
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,1+j) = aux;
            end
        end
    end
    if npsi>0 && neps>0,
        for j = 1:npsi,
            for i = 1:neps,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dpsitepst(k)*Dparam (t(k),i)*Dparam (t(k),j)*psit(k);
                end
                %   Sub-block number 43 (Scale exponential involved)
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+nmu+ntend+nind+j) = aux;
            end
        end
    end
    %   Simmetric part of the Hessian
    Hxx = Hxx + tril(Hxx,-1)';
    %   Since the numerical problem is a minimization problem the
    %   Hessian sign must be changed
    Hxx = -Hxx;
elseif nargout>2 && (ntend3>0 || nind3>0),
	%%calculo el hessiano aproximado
    Hxx=amevaOPH.Hessiano(ps,OPHobj);
    Hxx = -Hxx;
else
    %   Initialize the hessian matrix
    Hxx = sparse(zeros(2+neps0+nmu+npsi+neps+ntend+nind+ntend2+nind2+ntend3+nind3));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function dp = Dparam (t,j)
        %
        %   Derivative of the location, scale and shape functions with respect
        %   to harmonic parameters. It correspond to the right hand side
        %   in equation A.11 of the paper
        %
        %   Input:
        %        t-> Time (Yearly scale)
        %        j-> Harmonic number
        %
        %   Output:
        %        dp-> Corresponding derivative
        %
        if mod(j,2) == 0,
            dp = sin(j/2*2*pi*t);
        else
            dp = cos((j+1)/2*2*pi*t);
        end
        
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end