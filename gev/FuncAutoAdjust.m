function  [OPHr,OPHobj,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,lista,lista2,loglikeobj,grad,Hessiano,popt,parametros_all,tpstring] = ...
    FuncAutoAdjust(Hsmax,Time,kt,indices,info,maxtype,confidenceLevel,criterio,Opciones)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Function FuncAutoAdjust automatically selects and calculates the parameters
%   which minimize the Akaike Information criterion related to the
%   Time-Dependent
%   GEV distribution using the maximum likelihood method within an
%   iterative
%   scheme, including one parameter at a time based on a perturbation
%   criteria.
%   The process is repeated until no further improvement in the objective
%   function is achieved.
%
%   Input:
%       Hsmax -> environmental maxima data
%       datemax -> time when maxima occurs (yearly scale)
%       kt -> Frequency parameter to measure the importance od the number
%       of points
%       indices -> covariatex matrix where ecah column corresponds to the
%       values of a covariate at the times when maxima occurs
%       maxtype -> tipe of time data: 'monthly' or 'annual'
%       cityname-> string with the name used to identify the example and
%       create the figure files, if is empty no graph is cretated
%       confidenceLevel -> quantile to be plotted, default value 0.95
%
%   Output:
%       beta0 -> Optimal constant parameter related to location
%       beta -> Optimal harmonic vector associated with location
%       alpha0 -> Optimal constant parameter related to scale
%       alpha -> Optimal harmonic vector associated with scale
%       gamma0 -> Optimal constant parameter related to shape
%       gamma -> Optimal harmonic vector associated with shape
%       betaT -> Optimal location trend parameter
%       auxvarphi -> Optimal location covariate vector
%       betaT -> Optimal scale trend parameter
%       auxvarphi -> Optimal scale covariate vector
%       grad -> Gradient of the log-likelihood function with the sign
%       changed at the optimal solution
%       Hessiano -> Hessian of the log-likelihood function with the sign
%       changed at the optimal solution
%       popt -> vector including the optimal values for the parameters in the
%       following order: beta0, beta, betaT, varphi, alpha0, alpha,
%       betaT2, varphi2, gamma0, gamma
%       stdpara -> vector including the standard deviation of the
%       optimal values for the parameters in the
%       following order: beta0, beta, betaT, varphi, alpha0, alpha,
%       betaT2, varphi2, gamma0, gamma
%
%   Authors: R. Minguez, F.J. Mendez, C. Izaguirre, M. Menendez,
%   and I.J. Losada
%   Environmental and Hydraulics Institute "IH Cantabria"
%   University of Cantabria
%   E.T.S. de Ingenieros de Caminos, Canales y Puertos
%   Avda de los Castros, s/n
%   39005 Santander, Spain
%   Tfno.: +34 942 20 18 52
%   Fax: +34 942 20 18 60
%   Corresponding author email: roberto.minguez@unican.es
%
%   Created: 09/09/2009
%
%   For more details see the paper:
%   "Pseudo-Optimal Parameter Selection of Non-Stationary
%   Generalized Extreme Value Models for Environmental Variables".
%
% Examples 1
%   load ameva;
%   OPHr=FuncAutoAdjust(exgev_Hsmax,exgev_datemax);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global neps0;

if nargin<=8, neps0 = 1; end%%OJO OJO tiene que definirse en el programa principal OJO OJO

if nargin<2, error('Faltan datos, se necesitan dos series para poder continuar'); end
if ~exist('kt','var') || isempty(kt),  kt = ones(size(Hsmax)); end
if ~exist('indices','var') || isempty(indices), indices = []; end
if ~exist('info','var') || isempty(info), info=[1 1 1 1 12 1 0 0 0 0]; end %En matlab solo usa las pos. 1,2,3,4,6,7,8,9 (profilike/akaike,arm,tend,cov,auto/man-arm,man-armL,man-armE,man-armF)
if ~exist('maxtype','var') || isempty(maxtype), maxtype = 'monthly'; end
if ~exist('confidenceLevel','var') || isempty(confidenceLevel), confidenceLevel = 0.95; end
if confidenceLevel<0.5, error(['confidenceLevel=',num2str(confidenceLevel)]); end
if ~exist('criterio','var') || isempty(criterio), criterio = 'ProfLike'; end
if sum(Opciones.svopt)>0, if ~exist([pwd filesep Opciones.carpeta],'dir'),  mkdir(Opciones.carpeta); end; end

if isempty(indices), info(4)=0; end %Si indices esta vacio info(4)=0

%Comprobamos que los datos temporales estan datenum de matlab o resc. year
awinc=amevaclass;%ameva windows commons methods
awinc.AmevaTimeCheck([],Time);
if awinc.timestate==true,
    datemax=yearlytimescale(Time,'N');
    disp('los datos temporales estan en formato datenum de matlab y se han rescalados a anhos!');
elseif awinc.timestate==false,
    datemax=Time;
    disp('los datos temporales estan rescalados a anhos!');
else
    error('los datos temporales deben estar en formato datenum de matlab o rescalados anhos!');
end%maximum annual time scale normalizado para usar GEV

%encuetro el numero total de anhos y la frecuencia temporal de los datos %OJO OJO nuevo
%solo se usa en el pos-proceso
Km=amevaOPH.FrecTemporal(datemax,Time,awinc.timestate);
Ny=ceil(datemax(end));%   Ny -> numero de anhos
disp(['Years Ny=' num2str(Ny)]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if length(Hsmax)~=length(datemax),
    error('Data Hsmax and datemax must be the same length');
end
if ~isempty(indices),
    if length(Hsmax)~=length(indices(:,1)),
        error('Data indices must be coherent with Hsmax and datemax');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   STARTING THE ITERATIVE PROCESS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   The code is related to the Algorithm 1 (Automatic Model Selection) of
%   the paper but it is not exactly the same
%   Maximum number of iterations, which coincides with the maximum plausible
%   number of harmonics to be introduced
itermax = 100;
%   Paramtetros para almacenar la evolucion del proceso
beta0IT = zeros(itermax,1);
betaIT = zeros(2*itermax,itermax);
alpha0IT = zeros(itermax,1);
alphaIT = zeros(2*itermax,itermax);
gamma0IT = zeros(itermax,1);
gammaIT = zeros(2*itermax,itermax);
betaTIT  = zeros(itermax,1);
varphiIT = zeros(2*itermax,itermax);
betaT2IT  = zeros(itermax,1);
varphi2IT = zeros(2*itermax,itermax);
npIT = zeros(itermax,1);
%   Storage the Akaike Information Criterion evolution
AIKIT = zeros(itermax,1);
loglikIT = zeros(itermax,1);
model=zeros(1,3);
%  Step 1:
%% MODELO ESTACIONARIO%<--yo
neps0 = 0; %    Fuerzo la eliminacion del parametro de forma constante Gumbel model
[beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano,pold] = OptiParamHessian_bak(0,0,0,Hsmax,datemax,kt,[],[],[],[],[],[]);
AIKIT (1)= -2*(-loglikeobj)+2*(2);
loglikIT (1) = -loglikeobj;
AIKITini = AIKIT (1);
loglikeobjITini = loglikIT (1);
beta0IT(1) = beta0;%--->yo
alpha0IT(1) = alpha0;
gamma0IT (1) = 0;
npIT (1) = 2;
nit=1;

neps0 = 1;
[beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano,popt] = OptiParamHessian_bak(0,0,0,Hsmax,datemax,kt,[],[],[],[],[],[]);
%   Check if the corresponding model is GUMBEL
if isempty(gamma0),
    neps0 = 0;
else
    if (abs(gamma0)<=1e-8),
        neps0 = 0;
    end
end
AIKIT (2) = -2*(-loglikeobj)+2*(2+neps0);
loglikIT (2) = -loglikeobj;
beta0IT(2) = beta0;
alpha0IT(2) = alpha0;
if neps0 == 1, gamma0IT (2) = gamma0; end
npIT (2) = 2+neps0;
%nit=2;
bettgumbel=true;%he anadido else para que funciones en el caso de dos iteracciones 3/8/13 OC
switch criterio,
    case 'ProfLike'
        if 2*(loglikIT (2)-loglikIT (1))-chi2inv(confidenceLevel,1) >0,
            neps0 = 1;nit=2;
            AIKITini = AIKIT (2);
            loglikeobjITini = loglikIT (2);
            %   La inclusion del parametro es significativa
            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano,popt] = OptiParamHessian_bak (0,0,0,Hsmax,datemax,kt,[],[],[],[],[],[]);
            bettgumbel=false;%he anadido else para que funciones en el caso de dos iteracciones 3/8/13 OC
        else
            neps0 = 0;nit=1;
            AIKITini = AIKIT (1);
            loglikeobjITini = loglikIT (1);
            %   La inclusion del parametro es significativa
            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano,popt] = OptiParamHessian_bak (0,0,0,Hsmax,datemax,kt,[],[],[],[],[],[]);
        end
    case 'Akaike'
        if AIKIT (2)<AIKIT (1),
            neps0 = 1;nit=2;
            AIKITini = AIKIT (2);
            loglikeobjITini = loglikIT (2);
            %   La inclusion del parametro es significativa
            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = OptiParamHessian_bak (0,0,0,Hsmax,datemax,kt,[],[],[],[],[],[]);
            bettgumbel=false;%he anadido else para que funciones en el caso de dos iteracciones 3/8/13 OC
        end
end
if neps0==0, npIT(1)=2; nit=1; end
if sum(info(2:4))==0 % SOLO MODELO ESTACIONARIO 3/8/13 OC
    if bettgumbel, %he anadido else para que funciones en el caso de dos iteracciones 3/8/13 OC
        neps0=0; npIT(1)=2;
        [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = OptiParamHessian_bak (0,0,0,Hsmax,datemax,kt,[],[],[],[],[],[]);
    end
end % END MODELO ESTACIONARIO

%%   HARMONIC Iterative process
%   Step 1:
%   Initial number of harmonics to be introduced, no harmonics, no covariates and no trends
nmu = 0;
npsi = 0;
neps = 0;
nind = 0;
ntend = 0;
nind2 = 0;
ntend2 = 0;
if info(2), %armonico
    if info(6), %manual o autoarmonicos
        for i = 1:itermax,
            %   Step 2: Obtaining the maximum likelihood estimators for the selected parameters
            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano,pold] = ...
                OptiParamHessian_bak(nmu,npsi,neps,Hsmax,datemax,kt,[],[],[],[],[],[]);
            %   Check if the corresponding model is GUMBEL
            neps0 = 1;
            if isempty(gamma0),
                neps0 = 0;
            else
                if (abs(gamma0)<=1e-8),
                    neps0 = 0;
                end
            end
            %   Calculates the optimal Akaike Information Criterion objective
            %   function
            AIKIT (i)= -2*(-loglikeobj)+2*(2+neps0+2*nmu+2*npsi+2*neps+ntend+nind);
            loglikIT (i) = -loglikeobj;
            beta0IT(i) = beta0;%--->yo
            betaIT (1:length(beta),i) = beta;
            alpha0IT(i) = alpha0;
            alphaIT (1:length(alpha),i) = alpha;
            if neps0 == 1, gamma0IT (i) = gamma0; end
            if neps>0, gammaIT (1:length(gamma),i) = gamma; end
            npIT (i) = 2+neps0+2*nmu+2*npsi+2*neps+ntend+nind+ntend2+nind2;%<--yo
            
            %   Step 4: Calculate the sensitivities of the optimal log-likelihood
            %   objective function with respect to possible additional harmonics
            %   for the location, scale and shape parameters. Note that the new
            %   parameter values are set to zero since derivatives do not depend on them.
            %     [auxf auxJx] = Jacobian (Hsmax,datemax,[],[],beta0,[beta;0;0],alpha0,[alpha;0;0],gamma0,[gamma;0;0],[],[],[],[]);
            [auxf auxJx auxHxx] = loglikelihood (Hsmax,datemax,kt,beta0,[beta;0;0],alpha0,[alpha;0;0],gamma0,[gamma;0;0],[],[],[],[],[],[]);
            %   Inverse of the Information Matrix
            invI0_ = inv(-auxHxx);
            %   Updating the best model so far
            if i>1,
                switch criterio,
                    case 'ProfLike'
                        if 2*(loglikIT (i)-loglikIT (i-1))>chi2inv(confidenceLevel,2),
                            modelant = [nmu npsi neps]';
                        end
                    case 'Akaike'
                        if AIKIT (i)<AIKIT (i-1),
                            modelant = [nmu npsi neps]';
                        end
                end
            else
                modelant = [nmu npsi neps]';
            end
            %   Step 5: Calculate maximum perturbation
            pos = 1;
            maximumval = abs(auxJx(1+2*nmu+1:1+2*nmu+2)'*invI0_(1+2*nmu+1:1+2*nmu+2,1+2*nmu+1:1+2*nmu+2)*auxJx(1+2*nmu+1:1+2*nmu+2));
            auxmax = abs(auxJx(2+2*nmu+ntend+nind+2*npsi+2+1:2+2*nmu+ntend+nind+2*npsi+2+2)'*invI0_(2+2*nmu+ntend+nind+2*npsi+2+1:2+2*nmu+ntend+nind+2*npsi+2+2,2+2*nmu+ntend+nind+2*npsi+2+1:2+2*nmu+ntend+nind+2*npsi+2+2)*auxJx(2+2*nmu+ntend+nind+2*npsi+2+1:2+2*nmu+ntend+nind+2*npsi+2+2));
            %modificar para tener lo mismo que el codigo en c++ OJO OJO%igual
            %maximumval = sum(abs(auxJx(1+2*nmu+1:1+2*nmu+2)));%que c++
            %auxmax = sum(abs(auxJx(2+2*nmu+ntend+nind+2*npsi+2+1:2+2*nmu+ntend+nind+2*npsi+2+2)));%que c++
            if auxmax>maximumval,
                maximumval=auxmax;
                pos = 2;
            end
            %     if neps0 ==1,
            auxmax = abs(auxJx(2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+1:2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+2)'*invI0_(2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+1:2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+2,2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+1:2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+2)*auxJx(2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+1:2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+2));
            %auxmax = sum(abs(auxJx(2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+1:2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+2)));
            if auxmax>maximumval,
                maximumval=auxmax;
                pos = 3;
            end
            %     end
            %   If the maximum perturbation corresponds to location, include a new
            %   harmonic
            if pos ==1,
                nmu = nmu+1;
            end
            %   If the maximum perturbation corresponds to scale, include a new
            %   harmonic
            if pos ==2,
                npsi = npsi+1;
            end
            %   If the maximum perturbation corresponds to shape, include a new
            %   harmonic
            if pos ==3,
                neps = neps+1;
            end
            %   Model update and convergence criterion
            if i==1 && pos==3,
                %    Variation of the shape parameter is not allow if any of the remaining
                %    parameters do not change also in time
                break;
            end
            if i>1,
                switch criterio,
                    case 'ProfLike'
                        if 2*(loglikIT (i)-loglikIT (i-1))<=chi2inv(confidenceLevel,2),
                            model = modelant;
                            AIKITini = AIKIT (i-1);
                            loglikeobjITini = loglikIT (i-1);
                            break;
                        else
                            model = [nmu npsi neps]';
                        end
                    case 'Akaike'
                        if AIKIT (i)>=AIKIT (i-1),
                            model = modelant;
                            AIKITini = AIKIT (i-1);
                            loglikeobjITini = loglikIT (i-1);
                            break;
                        else
                            model = [nmu npsi neps]';
                        end
                end
            end
        end
    else
        %YO OC
        nmu = info(7);
        npsi = info(8);
        neps = info(9);
        model = [nmu npsi neps]';
        [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano,popt] = ...
            OptiParamHessian_bak(nmu,npsi,neps,Hsmax,datemax,kt,[],[],[],[],[],[]);
        %   Check if the corresponding model is GUMBEL
        neps0 = 1;
        if isempty(gamma0),
            neps0 = 0;
        else
            if (abs(gamma0)<=1e-8),
                neps0 = 0;
            end
        end
        AIKIT (2)= -2*(-loglikeobj)+2*(2);
        loglikIT (2) = -loglikeobj;
        AIKITini = AIKIT (2);
        loglikeobjITini = loglikIT (2);
        beta0IT(2) = beta0;%--->yo
        alpha0IT(2) = alpha0;
        if neps0 == 1,	gamma0IT (2) = gamma0; end
        npIT (2) = 2+neps0+2*nmu+2*npsi+2*neps+ntend+nind+ntend2+nind2;
        i=2;
        %YO OC
    end
    %   Storage the iteration number
    if i>1, nit = i-1; end;%the last model is not valid <--yo -1
    %   Obtaining the maximum likelihood estimators for the best model
    nmu=model(1);npsi=model(2);neps=model(3);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   CHECKING of the SHAPE parameter
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    neps0 = 0; %    Fuerzo la eliminacion del parametro de forma constante
    [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano,popt] = ...
        OptiParamHessian_bak(nmu,npsi,neps,Hsmax,datemax,kt,[],[],[],[],[],[]);
    
    AIKIT (nit+1) = -2*(-loglikeobj)+2*(2+neps0+2*nmu+2*npsi+2*neps+ntend+nind+ntend2+nind2);
    loglikIT (nit+1) = -loglikeobj;
    beta0IT(nit+1) = beta0;%--->yo
    betaIT (1:length(beta),nit+1) = beta;
    alpha0IT(nit+1) = alpha0;
    alphaIT (1:length(alpha),nit+1) = alpha;
    if neps0 == 1,	gamma0IT (nit+1) = gamma0; end
    if neps>0, gammaIT (1:length(gamma),nit+1) = gamma; end
    npIT (nit+1) = 2+neps0+2*nmu+2*npsi+2*neps+ntend+nind+ntend2+nind2;%<--yo
    
    switch criterio,
        case 'ProfLike'
            if 2*(loglikeobjITini - loglikIT (nit+1))>chi2inv(confidenceLevel,1),
                neps0 = 1;
                %   La inclusion del parametro es significativa
                [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano,popt] = OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,[],[],[],[],[],[]);
            end
        case 'Akaike'
            if AIKITini > AIKIT (nit+1),
                neps0 = 1;
                %   La inclusion del parametro es significativa
                [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano,popt] = OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,[],[],[],[],[],[]);
            end
    end
    if neps0==0, npIT(1:nit)=npIT(1:nit)-1;end
    if info(6)==0, nit=2; end
    %actualizo el best model The last de xxIT --->yo
    beta0 = beta0IT(nit);%--->yo
    beta = betaIT(1:length(beta),nit);
    alpha0 = alpha0IT(nit);
    alpha = alphaIT(1:length(alpha),nit);
    if ~isempty(gamma0), gamma0 = gamma0IT (nit); end
    if isempty(gamma), gamma = gammaIT(1:length(gamma),nit); end%<---yo
end
    
%%   COVARIATES Iterative process
%   Size of covariate data, it must be same length as Hsmax, and datemax
if ~exist('indices','var'),
    indices = [];
end
[nd nind] = size(indices);

if ~isempty(indices) && info(4)==1,%info(4)-indice
    %   Data for covariates related to location and scale
    auxindices = [];
    lista = [];
    auxnind = nind;
    auxindices2 = [];
    lista2 = [];
    auxnind2 = nind;
    nind = 0;
    nind2 = 0;
    varphi = [];
    varphi2 = [];
    %   Starting COVARIATES iterative process
    for i = nit:nit-1+2*auxnind,
        auxvarphi = zeros(auxnind,1);
        if ~isempty(lista),
            auxvarphi(lista) = varphi;
        end
        auxvarphi2 = zeros(auxnind2,1);
        if ~isempty(lista2),
            auxvarphi2(lista2) = varphi2;
        end
        %   Step 7: Calculate the sensitivities of the optimal log-likelihood
        %   objective function with respect to possible additional covariates
        %   for the location and  scale parameters
        %         [auxf auxJx] = Jacobian (Hsmax,datemax,indices,indices,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,auxvarphi,betaT2,auxvarphi2);
        [auxf auxJx auxHxx] = loglikelihood (Hsmax,datemax,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,auxvarphi,betaT2,auxvarphi2,indices,indices);
        %   Step 10: Include in the parameter vector the corresponding covariate
        invI0_ = (-auxHxx);
        [maximo pos]= max(abs(auxJx(2+2*nmu+ntend:1+2*nmu+ntend+auxnind).^2./diag(invI0_(2+2*nmu+ntend:1+2*nmu+ntend+auxnind,2+2*nmu+ntend:1+2*nmu+ntend+auxnind))));
        [maximo2 pos2]= max(abs(auxJx(2+2*nmu+ntend+auxnind+2*npsi+1:2+2*nmu+ntend+auxnind+2*npsi+auxnind2).^2./diag(invI0_(2+2*nmu+ntend+auxnind+2*npsi+1:2+2*nmu+ntend+auxnind+2*npsi+auxnind2,2+2*nmu+ntend+auxnind+2*npsi+1:2+2*nmu+ntend+auxnind+2*npsi+auxnind2))));
        %modificar para tener lo mismo que el codigo en c++ OJO OJO
        %[maximo pos]=max(abs(auxJx(2+2*nmu+ntend:1+2*nmu+ntend+auxnind)));%igual que c++
        %[maximo2 pos2]=max(abs(auxJx(2+2*nmu+ntend+auxnind+2*npsi+1:2+2*nmu+ntend+auxnind+2*npsi+auxnind2)));%igual que c++
        
        varphini = [varphi];
        varphini2=varphi2;
        if maximo>maximo2,
            varphini = [varphi; 0];
            lista = [lista; pos];
            nind = nind + 1;
        else
            varphini2=[varphi2;0];
            lista2 = [lista2; pos2];
            nind2 = nind2 + 1;
        end
        auxindices = indices(:,lista);
        auxindices2 = indices(:,lista2);
        %   Step 9: Obtain the maximum-likelihood estimators for the selected
        %   parameters and calculate the Akaike Information criterion objective
        %   function AIC
        if length(lista)==23 || length(varphi)==24
            disp('aqui')
        end
        
        neps0 = 0;%%%AUMENTO ESTA LINEA  PARA FORZAR GAMMA0=0      
        if neps>0,
            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
                OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,auxindices,zeros(nind,1),betaT2,auxindices2,zeros(nind2,1),[popt(1:1+2*nmu); varphini; popt(2+2*nmu:2+2*nmu+2*npsi); varphini2; popt(2+1+2*nmu+2*npsi:end)]);
        else
            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
                OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,auxindices,zeros(nind,1),betaT2,auxindices2,zeros(nind2,1),[popt(1:1+2*nmu); varphini; popt(2+2*nmu:2+2*nmu+2*npsi); varphini2; ones(neps0,1)*0.01]);
        end
        %   Check if the model is Gumbel
        neps0 = 1;
        if isempty(gamma0),
            neps0 = 0;
        else
            if (abs(gamma0)<=1e-8),
                neps0 = 0;
            end
        end
        AIKIT (i) = -2*(-loglikeobj)+2*(2+neps0+2*nmu+2*npsi+2*neps+ntend+nind+ntend2+nind2);
        loglikIT (i) = -loglikeobj;
        beta0IT(i) = beta0;%--->yo
        betaIT (1:length(beta),i) = beta;
        alpha0IT(i) = alpha0;
        alphaIT (1:length(alpha),i) = alpha;
        if neps0 == 1, gamma0IT (i) = gamma0; end
        if neps>0, gammaIT (1:length(gamma),i) = gamma; end
        if ~isempty(varphiIT),  varphiIT(lista,i) = varphi; end
        if ~isempty(varphi2IT), varphi2IT(lista2,i) = varphi2; end
        if ~isempty(betaT),  betaTIT(i) = betaT; end
        if ~isempty(betaT2), betaT2IT(i) = betaT2; end
        npIT (i) = 2+neps0+2*nmu+2*npsi+2*neps+ntend+nind+ntend2+nind2;%<--yo
        
        switch criterio,
            case 'ProfLike'
                if 2*(loglikIT (i)-loglikeobjITini)>chi2inv(confidenceLevel,1),
                    loglikeobjITini = loglikIT (i);
                else
                    if maximo>maximo2,
                        lista = lista(1:end-1);
                        nind = nind-1;
                        varphi=varphi(1:end-1);
                        auxindices = indices(:,lista);
                        auxindices2 = indices(:,lista2);
                        if neps>0,
                            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
                                OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,auxindices,zeros(nind,1),betaT2,auxindices2,zeros(nind2,1),[popt(1:1+2*nmu); varphi; popt(2+2*nmu:2+2*nmu+2*npsi); varphi2; popt(2+1+2*nmu+2*npsi:end)]);
                        else
                            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
                                OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,auxindices,zeros(nind,1),betaT2,auxindices2,zeros(nind2,1),[popt(1:1+2*nmu); varphi; popt(2+2*nmu:2+2*nmu+2*npsi); varphi2; ones(neps0,1)*0.01]);
                        end
                    else
                        lista2 = lista2(1:end-1);
                        nind2 = nind2-1;
                        varphi=varphi(1:end-1);
                        auxindices = indices(:,lista);
                        auxindices2 = indices(:,lista2);
                        if neps>0,
                            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
                                OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,auxindices,zeros(nind,1),betaT2,auxindices2,zeros(nind2,1),[popt(1:1+2*nmu); varphi; popt(2+2*nmu:2+2*nmu+2*npsi); varphi2; popt(2+1+2*nmu+2*npsi:end)]);
                        else
                            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
                                OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,auxindices,zeros(nind,1),betaT2,auxindices2,zeros(nind2,1),[popt(1:1+2*nmu); varphi; popt(2+2*nmu:2+2*nmu+2*npsi); varphi2; ones(neps0,1)*0.01]);
                        end
                    end
                    nit = i-1;
                    break;
                end
            case 'Akaike'
                if AIKIT (i)<AIKITini,
                    AIKITini = AIKIT (i);
                else
                    if maximo>maximo2,
                        lista = lista(1:end-1);
                        nind = nind-1;
                        varphi=varphi(1:end-1);
                        auxindices = indices(:,lista);
                        auxindices2 = indices(:,lista2);
                        if neps>0,
                            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
                                OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,auxindices,zeros(nind,1),betaT2,auxindices2,zeros(nind2,1),[popt(1:1+2*nmu); varphi; popt(2+2*nmu:2+2*nmu+2*npsi); varphi2; popt(2+1+2*nmu+2*npsi:end)]);
                        else
                            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
                                OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,auxindices,zeros(nind,1),betaT2,auxindices2,zeros(nind2,1),[popt(1:1+2*nmu); varphi; popt(2+2*nmu:2+2*nmu+2*npsi); varphi2; ones(neps0,1)*0.01]);
                        end
                    else
                        lista2 = lista2(1:end-1);
                        nind2 = nind2-1;
                        varphi=varphi(1:end-1);
                        auxindices = indices(:,lista);
                        auxindices2 = indices(:,lista2);
                        if neps>0,
                            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
                                OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,auxindices,zeros(nind,1),betaT2,auxindices2,zeros(nind2,1),[popt(1:1+2*nmu); varphi; popt(2+2*nmu:2+2*nmu+2*npsi); varphi2; popt(2+1+2*nmu+2*npsi:end)]);
                        else
                            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
                                OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,auxindices,zeros(nind,1),betaT2,auxindices2,zeros(nind2,1),[popt(1:1+2*nmu); varphi; popt(2+2*nmu:2+2*nmu+2*npsi); varphi2; ones(neps0,1)*0.01]);
                        end
                    end
                    nit = i-1;
                    break;
                end
        end
    end
else
    nind = 0;
    nind2 = 0;
    lista = [];
    lista2 = [];
    varphini = [];
    varphini2 = [];
    auxnind = 0;
    auxnind2 = 0;
end
%%% OJOJOJOJO comprobar
%%%if info(4)==1 && nind==0 && nind2==0, nit=nit-1; end
if info(4)==1 && nind==0 && nind2==0 && nit>1, nit=nit-1; end
%   End of the index-covariate iterative process

if info(3), %if trends
    %   Step 12: Location trend
    ntend = 1;
    betaT = 0;
    %   Step 13: Obtain the maximum-likelihood estimators for the selected
    %   parameters and calculate the Akaike Information Criterion objective
    %   function
    nind=length(lista); nind2=length(lista2);
    [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
        OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,indices(:,lista),zeros(nind,1),betaT2,indices(:,lista2),zeros(nind2,1),[popt(1:1+2*nmu); zeros(ntend,1); varphini(1:nind); popt(2+2*nmu:2+2*nmu+2*npsi); varphini2(1:nind2); ones(neps0,1)*0.01; popt(2+1+neps0+2*nmu+2*npsi:end)]);
    AIKIT (nit+1) = -2*(-loglikeobj)+2*(2+neps0+2*nmu+2*npsi+2*neps+ntend+nind+ntend2+nind2);
    loglikIT (nit+1) = -loglikeobj;
    beta0IT(nit+1) = beta0;%--->yo
    betaIT (1:length(beta),nit+1) = beta;
    alpha0IT(nit+1) = alpha0;
    alphaIT (1:length(alpha),nit+1) = alpha;
    if neps0 == 1, gamma0IT (nit+1) = gamma0; end;
    if neps>0, gammaIT (1:length(gamma),nit+1) = gamma; end
    if ~isempty(varphiIT),  varphiIT(lista,nit+1) = varphi; end
    if ~isempty(varphi2IT), varphi2IT(lista2,nit+1) = varphi2; end
    if ~isempty(betaT),  betaTIT(nit+1) = betaT; end
    if ~isempty(betaT2), betaT2IT(nit+1) = betaT2; end
    npIT (nit+1) = 2+neps0+2*nmu+2*npsi+2*neps+ntend+nind+ntend2+nind2;%<--yo
    %   Step 14:
    switch criterio,
        case 'ProfLike'
            if 2*(loglikIT (nit+1)-loglikeobjITini)<=chi2inv(confidenceLevel,1),
                ntend = 0;
                betaT = [];
                [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
                    OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,indices(:,lista),zeros(nind,1),betaT2,indices(:,lista2),zeros(nind2,1),[popt(1:1+2*nmu); varphini(1:nind); popt(2+2*nmu:2+2*nmu+2*npsi); varphini2(1:nind2); ones(neps0,1)*0.01; popt(2+1+neps0+2*nmu+2*npsi:end)]);
            else
                AIKITini = AIKIT (nit+1);
                loglikeobjITini = loglikIT (nit+1);
                nit=nit+1;
            end
        case 'Akaike'
            if AIKIT (nit+1)>AIKITini,
                ntend = 0;
                betaT = [];
                [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
                    OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,indices(:,lista),zeros(nind,1),betaT2,indices(:,lista2),zeros(nind2,1),[popt(1:1+2*nmu); varphini(1:nind); popt(2+2*nmu:2+2*nmu+2*npsi); varphini2(1:nind2); ones(neps0,1)*0.01; popt(2+1+neps0+2*nmu+2*npsi:end)]);
            else
                AIKITini = AIKIT (nit+1);
                loglikeobjITini = loglikIT (nit+1);
                nit=nit+1;
            end
            
    end
    %   Step 15: Scale trend
    ntend2 = 1;
    betaT2 = 0;
    %   Step 16: Obtain the maximum-likelihood estimators for the selected
    %   parameters and calculate the Akaike Information Criterion objective
    %   function
    [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
        OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,indices(:,lista),zeros(nind,1),betaT2,indices(:,lista2),zeros(nind2,1),[popt(1:1+2*nmu); zeros(ntend,1); varphini(1:nind); popt(2+2*nmu:2+2*nmu+2*npsi); zeros(ntend2,1); varphini2(1:nind2); ones(neps0,1)*0.01; popt(2+1+neps0+2*nmu+2*npsi:end)]);
    
    AIKIT (nit+1) = -2*(-loglikeobj)+2*(2+neps0+2*nmu+2*npsi+2*neps+ntend+nind+ntend2+nind2);
    loglikIT (nit+1) = -loglikeobj;
    beta0IT(nit+1) = beta0;%--->yo
    betaIT (1:length(beta),nit+1) = beta;
    alpha0IT(nit+1) = alpha0;
    alphaIT (1:length(alpha),nit+1) = alpha;
    if neps0 == 1, gamma0IT (nit+1) = gamma0; end;
    if neps>0, gammaIT (1:length(gamma),nit+1) = gamma; end
    if ~isempty(varphiIT),  varphiIT(lista,nit+1) = varphi; end
    if ~isempty(varphi2IT), varphi2IT(lista2,nit+1) = varphi2; end
    if ~isempty(betaT),  betaTIT(nit+1) = betaT; end
    if ~isempty(betaT2), betaT2IT(nit+1) = betaT2; end
    npIT (nit+1) = 2+neps0+2*nmu+2*npsi+2*neps+ntend+nind+ntend2+nind2;%<--yo
    
    %   Step 17:
    switch criterio,
        case 'ProfLike'
            if 2*(loglikIT (nit+1)-loglikeobjITini)<=chi2inv(confidenceLevel,1),
                ntend2 = 0;
                betaT2 = [];
                [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
                    OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,indices(:,lista),zeros(nind,1),betaT2,indices(:,lista2),zeros(nind2,1),[popt(1:1+2*nmu); zeros(ntend,1); varphini(1:nind); popt(2+2*nmu:2+2*nmu+2*npsi); varphini2(1:nind2); ones(neps0,1)*0.01; popt(2+1+neps0+2*nmu+2*npsi:end)]);
            else
                AIKITini = AIKIT (nit+1);
                loglikeobjITini = loglikIT (nit+1);
                nit=nit+1;
            end
        case 'Akaike'
            if AIKIT (nit+1)>AIKITini,
                ntend2 = 0;
                betaT2 = [];
                [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,loglikeobj,grad,Hessiano] = ...
                    OptiParamHessian_bak (nmu,npsi,neps,Hsmax,datemax,kt,betaT,indices(:,lista),zeros(nind,1),betaT2,indices(:,lista2),zeros(nind2,1),[popt(1:1+2*nmu); zeros(ntend,1); varphini(1:nind); popt(2+2*nmu:2+2*nmu+2*npsi); varphini2(1:nind2); ones(neps0,1)*0.01; popt(2+1+neps0+2*nmu+2*npsi:end)]);
            else
                AIKITini = AIKIT (nit+1);
                loglikeobjITini = loglikIT (nit+1);
                nit=nit+1;
            end
    end
    %actualizo el best model The last de xxIT --->yo 09/2013
    beta0 = beta0IT(nit);%--->yo
    beta = betaIT(1:length(beta),nit);
    alpha0 = alpha0IT(nit);
    alpha = alphaIT(1:length(alpha),nit);
    if ~isempty(gamma0), gamma0 = gamma0IT (nit); end
    if isempty(gamma), gamma = gammaIT(1:length(gamma),nit); end%<---yo
    if ~isempty(varphiIT),  varphi = varphiIT(lista,nit); end
    if ~isempty(varphi2IT), varphi2 = varphi2IT(lista2,nit); end
    if ~isempty(betaT),  betaT = betaTIT(nit); end
    if ~isempty(betaT2), betaT2 = betaT2IT(nit); end%<---yo 09/2013
end % end of trends
%   Set the final values for the covariates in the appropriate order if
%   they are considered
% if nind>0,
auxvarphi = zeros(auxnind,1);
auxvarphi2 = zeros(auxnind2,1);
if ~isempty(lista),
    auxvarphi(lista) = varphi;
end
if ~isempty(lista2),
    auxvarphi2(lista2) = varphi2;
end
% end

%%%%%%%%%%%%solo este caso borrrarrrrrrr
% nuaxi=0;
% if ~isempty(varphi), lista=1; nuaxi=nuaxi+1; end
% if ~isempty(varphi2), lista2=1; nuaxi=nuaxi+1; end
% if ~isempty(varphi) || ~isempty(varphi2) , nit=nit+nuaxi; end


%%%%%%%%%%%%%%%%%%%%%%%%%% THE END Solution %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ps.beta0 = beta0;   ps.beta = beta;   ps.betaT = betaT;   ps.varphi = varphi;
ps.alpha0 = alpha0; ps.alpha = alpha; ps.betaT2 = betaT2; ps.varphi2 = varphi2;
ps.gamma0 = gamma0; ps.gamma = gamma; ps.betaT3 = [];     ps.varphi3=[];
pv=amevaOPH.pstovector(ps);
OPHr = amevaOPH.oph_r(pv,neps0,2*nmu,ntend,nind,2*npsi,ntend2,nind2,2*neps,0,0,lista,lista2,[],loglikeobj,grad,Hessiano,[]);

OPHobj.x=Hsmax; OPHobj.t=datemax; OPHobj.kt=kt; OPHobj.indices=indices; 

OPHobj.Km=Km; OPHobj.Ny = Ny; %OJO OJO nuevo

OPHr.invI0 = amevaOPH.invI0(OPHr.Hessiano,OPHr.np); % Calculo y asigno invI0 al resultado  OJO OJO
%OPHr.loglike = -OPHr.loglike; OPHr.gradiente = -OPHr.gradiente; OPHr.Hessiano = -OPHr.Hessiano; %Cambio de signo OJO OJO
%%%%%%%%%%%%%%%%%%%%%%%%%% THE END Solution %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
OPHobj=amevaOPH.OPHrToOPHobj(OPHobj,OPHr);% asigno la solucion a los datos

%   Paramtetros para almacenar la evolucion del proceso
IT.beta0IT = beta0IT;
IT.betaIT = betaIT;
IT.alpha0IT = alpha0IT;
IT.alpha0IT = alpha0IT;
IT.gamma0IT = gamma0IT;
IT.gammaIT = gammaIT;
IT.betaTIT  = betaTIT;
IT.varphiIT = varphiIT;
IT.betaT2IT  = betaT2IT;
IT.varphi2IT = varphi2IT;
IT.npIT = npIT;
IT.AIKIT = AIKIT;
IT.loglikIT = loglikIT;
IT.OPHr=OPHr; %storage last solution

%%%%%%%%%%%%%%%%%% POST PROSECOS PLOTS AND SOLUTION %%%%%%%%%%%%%%%%%%%%%%
[OPHobj,parametros_all,tpstring] = ...
    amevaFuncAutoAdjust.PosProsPlot(OPHr,OPHobj,nit,neps0,IT,info,criterio,confidenceLevel,Opciones);
                       %PosProsPlot(OPHr,OPHobj,nit,neps0,IT,Time,awinc,info,maxtype,criterio,confidenceLevel,Opciones);
end
