classdef amevaAgregateQuantilesAnuales < handle
    methods (Access = public, Static = true)
        function [Ts,quanaggr,stdDqX]=aAQA(Hsmax,datemax,ConfidenceLevelAlpha,kt,indices,OPHr,cityname,var_name,var_unit,gcax)
            % GEV Quantiles agregdos ANUALES... IMPORTANTE: los datos son ANUALES
            if isempty(kt), kt=ones(size(Hsmax)); end
            if ConfidenceLevelAlpha>0.5, error(['ConfidenceLevelAlpha=',num2str(ConfidenceLevelAlpha)]); end
            
            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,betaT3,varphi,varphi2,varphi3,lista,lista2,lista3]=amevaOPH.gevparamsol2array(OPHr);
            
            %   Aggregate quantiles for different return periods
            Ts = [1.05 1.5 2 5 10 20 25 50 75 100 200 300 400 500];
            nts = length(Ts);
            Km=1;
            quanaggr = zeros(1,nts);%  Quantiles Agregados
            for j = 1:nts,
                quanaggr(j)=QuantileAgregado(1-1/Ts(j),0,1,Km,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices(:,lista),indices(:,lista2),datemax);
            end
            %   Intervalos de confianza del cuantil anual
            stdup = zeros(nts,1);
            stdlo = zeros(nts,1);
            stdDqX = zeros(nts,1);
            for i=1:nts,
                stdQuan = ConfidInterQuanAggregate(1-1/Ts(i),0,1,Km,Hsmax,datemax,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices(:,lista),indices(:,lista2));
                stdDqX(i)=stdQuan*norminv(1-ConfidenceLevelAlpha/2,0,1);
                stdup(i)= quanaggr(i)+stdDqX(i);
                stdlo(i)= quanaggr(i)-stdDqX(i);
            end
            
            % plot si no hay salida
            if nargout==0
                if ~exist('cityname','var'), cityname=''; end
                if ~exist('var_name','var'), var_name=''; end
                if ~exist('var_unit','var'), var_unit=''; end
                if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
                if ~ishandle(gcax), disp('WARNING! amevaAgregateQuantilesAnuales with axes'); end
                set(gcf,'CurrentAxes',gcax)
                %   Datos anuales
                Hsmaxor = sort(Hsmax);
                ProHsmaxor = (((1:length(Hsmax)))/(length(Hsmax)+1))';
                Tapprox = 1./(1-ProHsmaxor);
                id = find(Tapprox>=1);
                semilogx(gca,Tapprox(id),Hsmaxor(id),'.','Color',[0 0 0]);hold on;
                semilogx(gca,Ts,quanaggr,'r')
                semilogx(gca,Ts,stdlo,'--r')
                semilogx(gca,Ts,stdup,'--r'); hold off;
                set(gca,'XTick',[2 10 50 100 500],'XTickLabel',{'2','10','50','100','500'},'Xlim',[2 500])
                grid(gca,'on'); title(['Aggregate Quantiles (' cityname ')']); xlabel(gca,'Return Period (years)');
                ylabel(gca,labelsameva(var_name,var_unit));
                legend('Annual data','Annual',[num2str(1-ConfidenceLevelAlpha) ' Bounds'],'Location','EastOutside')
                hold(gca,'off');
            end
            
        end
        
        function [quanaggr,stdup,stdlo]=AQA(Ts,Hsmax,datemax,quanval,kt,indices,OPHr)
            %    Los datos tienes que a una frecuencia anual
            %    Cuantil agregado anual para un periodo de retorno
            disp('Los datos tienes que a una frecuencia anual!');
            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,betaT3,varphi,varphi2,varphi3,lista,lista2]=amevaOPH.gevparamsol2array(OPHr);
            auxi=QuantileGEV(1-1/Ts,datemax,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,indices(:,lista),indices(:,lista2));
            quanaggr=auxi(1);
            %    Intervalos de confianza del cuantil anual
            stdQuan = ConfidInterQuanAggregateAnuales (1-1/Ts,0,1,Hsmax,datemax,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2);
            stdup= quanaggr+stdQuan*norminv(1-quanval/2,0,1);
            stdlo= quanaggr-stdQuan*norminv(1-quanval/2,0,1);
            
        end
    end
end

