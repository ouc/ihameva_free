% GEV
%
% Files
%   AgregateQuantiles               - GEV Quantiles agregdos MENSUALES... tambien se pueden mostrar los anuales. IMPORTANTE: los datos son mensuales
%   ajusteGEVAT                     - 
%   amevaAgregateQuantiles          - 
%   amevaAgregateQuantilesAnuales   - 
%   amevaConfidInterQuanAggregate   - OPTIMIZAR ESTA FUNCION OC UC
%   amevaFuncAutoAdjust             - 
%   amevaloglikelihood              - Loglikelihood function definition of the GEV distribution
%   amevaOPH                        - 
%   amevaQuantileAgregado           - OPTIMIZAR ESTA FUNCION OC UC
%   CDFAgregado                     - 
%   CDFGEVt                         - CDFGEVt function calculates the GEV distribution function corresponding
%   ConfidInterQuanAggregate        - 
%   ConfidInterQuanAggregateAnuales - 
%   ConfidInterQuanAggregateII      - 
%   Dmupsiepst                      - Function Dmupsiepst calculates the derivatives of the standardized maximum
%   DQuantile                       - Function DQuantile calculates the quantile q  derivative associated with a given
%   Dzweibull                       - Function Dmupsiepst calculates the derivatives of the standardized maximum
%   FuncAutoAdjust                  - Function FuncAutoAdjust automatically selects and calculates the parameters
%   gev                             - Generalized Extreme Value tool.
%   gev_param_t                     - Evaluation of the final parameters for plotting
%   gevexpplot                      - 
%   loglikelihood                   - Function Loglikelihood calculates the loglikelihood function, the Jacobian
%   OptiParamHessian_bak            - Function OptiParamHessian calculates the parameters of the
%   parametroEOF                    - parametroEOF function calculates the location, scale and shape parameters
%   PDFGEVt                         - PDFGEVt function calculates the GEV distribution function corresponding
%   QuantileAgregado                - 
%   QuantileAgregadoII              - Util para incluir las covariables, e integrar en varios anos pero
%   QuantileGEV                     - Function QuantileGEV calculates the quantile q associated with a given
%   Zstandardt                      - ZSTANDARD function calculates the standardized variable corresponding
