function [OPHr,OPHobj,quanGEV,stdDqG,quanPOT,stdDqP] = extremos_simple(data,tipo,intraAnual,autoArmonicos,Opciones,setGamma0)
%   extremos_simple Calcula el regimen extremal de datos horarios.
%   [OPHr,OPHobj,quanGEV,stdDqG,quanPOT,stdDqP] = extremos_simple(data,tipo,intraAnual,autoArmonicos,Opciones,setGamma0)
%
%   tipo = 'gev'/'pot'/'gevpot'
%   intraAnual = true -> gev muestra los meses/ pot muestra direcciones
%   autoArmonicos = true -> solo gev seleccion automatica de armonicos por defecto 3,3,1
%   Opciones -> help ameva_options para configurar otras opciones
%   setGamma0 = 0/1/2 %Forzar Gamma0. 0-Auto, 1-Forzar Gamma0=0, 2-Forzar Gamma0~=0
%
% Examples 1
%   load ameva;
%   extremos_simple(data_dow_sdr1,'gev'); % max anual ajuste
%   extremos_simple(data_dow_sdr1,'gevpot',true); % analisis intra-anual/direccional
%
% See also:
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   extremos_simple.
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows 7
%   2-06-2011 - The first distribution version

%% GEV Maximos MENSUALES -extremos-

amevamsg = ''; Time = []; X = []; Dir = [];
quanGEV = []; stdDqG = []; quanPOT = []; stdDqP = [];
if ~exist('tipo','var') || isempty(tipo), tipo = 'gev'; end
if ~ischar(tipo) || ~ismember(tipo,{'gev','pot','gevpot'}), 
    error(['tipo=',tipo,' y solo puede tomar valores gev/pot/gevpot gev (default)']); 
end
if ~exist('intraAnual','var') || isempty(intraAnual), intraAnual = false; end
if ~islogical(intraAnual), error('intraAnual solo puede tomar valores true/false false (default)'); end
if ~exist('autoArmonicos','var') || isempty(autoArmonicos), autoArmonicos = false; end
if ~islogical(autoArmonicos), error('autoArmonicos solo puede tomar valores true/false false (default)'); end
if isstruct(data) %%%%%%%%%%%%%% lectura de los datos %%%%%%%%%%%%%%%
    [Time,X,Dir] = validrvar(data);
elseif ismatrix(data) && ~isempty(data)
    if min(size(data)) == 14 %caso especial Mel
        Time = datenum(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),data(:,6));
        X = data(:,9);
        Dir = data(:,8);
        amevamsg = ['Se considera que la matriz tiene el formato:', ...
            '[year month day hour min sec Y Dir X] <-> [1:9,:] !'];
    elseif min(size(data)) == 8 % [year month day hour min sec X Dir]
        Time = datenum(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),data(:,6));
        X = data(:,7);
        Dir = data(:,8);
        amevamsg = ['Se considera que la matriz tiene el formato:', ...
            '[year month day hour min sec X Dir] <-> [1:8,:] !'];
    elseif min(size(data)) == 7 % [year month day hour min sec X]
        Time = datenum(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),data(:,6));
        X = data(:,7);
        amevamsg = ['Se considera que la matriz tiene el formato:', ...
            '[year month day hour min sec X] <-> [1:7,:] !'];
    elseif min(size(data)) == 3 % [Time X Dir]
        Time = data(:,1);
        X = data(:,2);
        Dir = data(:,3);
        amevamsg = ['Se considera que la matriz tiene el formato:', ...
            '[Time X Dir] <-> [1:3,:] !'];
    elseif min(size(data)) == 2 % [Time X]
        Time = data(:,1);
        X = data(:,2);
        amevamsg = ['Se considera que la matriz tiene el formato:', ...
            '[Time X] <-> [1:2,:] !'];
    end
else
    error('No existe la estructura: data.X  data.time o la matriz: [Y M D h m s X Dir] <-> [1:8,:] - variable de analisis!');
end
if isempty(X),
    error('No esta definido el campo: data.X  o data.hs - no se puede continuar!');
end
if isempty(Time),
    error('No esta definido el campo: data.time - no se puede continuar!');
end
if ~exist('Opciones','var') || ~isprop(Opciones,'svopt') %%%%%% lectura de los Opciones %%%%%%%%
    Opciones = ameva_options();
end
if ~exist('setGamma0','var') || isempty(setGamma0), setGamma0 = 0; end
N = length(X);

% Texto idiomas eng esp Cargo los titulos en los diferentes idiomas de todas las fichas
texto = amevaclass.amevatextoidioma(Opciones.idioma);
Lab = labelsameva(Opciones.var_name,Opciones.var_unit);%Labels

confidenceLevel = 0.95; confidenceLevelAlpha=1-confidenceLevel; % 0.95 <-> 0.05 %gev-pot cal. bandas

%% %%% gev %%%%
datemaxA= [];
HsmaxA  = [];
if strcmp(tipo,'gev') || strcmp(tipo,'gevpot')
    if intraAnual,
        Km = 12;
        [datemax,Hsmax] = maximostempfunction(Time,X,'monthly','maximum',0,1,3,'day');
        [datemaxA,HsmaxA] = maximostempfunction(Time,X,'annual','maximum',0,1,5,'day');
        datemaxA = yearlytimescale(datemaxA,'R'); %escala anual
    else
        Km=1;
        [datemax,Hsmax] = maximostempfunction(Time,X,'annual','maximum',0,1,5,'day');
    end
    datemax = yearlytimescale(datemax,'R'); %escala anual
    info(1) = 1;%aik Chi2 check (0 = aik, 1 = Chi2)
    info(2) = intraAnual;%intra_annual check
    info(3) = 0;%long term(Trend) check
    info(4) = 0;%inter-annual check
    info(5) = 16;%max. armonicos X3(se usa en c++). En Matlab itermax = 100
    info(6) = autoArmonicos;%seleccion auto de armonicos por defecto 3,3,1
    info(7) = 3;%arm. location
    info(8) = 3;%arm. scale
    info(9) = 1;%arm. shape
    info(10) = setGamma0;%Forzar Gamma0. 0-Auto, 1-Forzar Gamma0=0, 2-Forzar Gamma0~=0
    
    [OPHr,OPHobj] = amevaFuncAutoAdjust.AutoAjuste(Hsmax,datemax,[],[],info,confidenceLevel,Opciones);
    OPHobj.Km = Km;
    if ~isempty(datemaxA), OPHobj.datemaxA = datemaxA; OPHobj.HsmaxA = HsmaxA; end %datos para pintar el anual
    figure; [~,quanGEV,stdDqG] = amevaAgregateQuantiles.aqPlot(OPHobj,OPHr,confidenceLevelAlpha,Opciones,gca);
    
    if intraAnual,
        title(texto('rmfigTituloGEV'),'fontsize',11,'fontweight','b');
    else
        legend({'Annual','Annual data',[num2str(1-confidenceLevelAlpha) ' Bounds']},'Location','SouthEast');
        if isempty(OPHr.gamma0)
            title({texto('rmfigTituloGEV'),['(\mu=',num2str(OPHr.beta0,3), ...
                ' \psi=',num2str(exp(OPHr.alpha0),3),'). ']},'fontsize',11,'fontweight','b');
        else
            title({texto('rmfigTituloGEV'),['(\mu=',num2str(OPHr.beta0,3),' \xi=',num2str(OPHr.gamma0,3), ...
                ' \psi=',num2str(exp(OPHr.alpha0),3),'). ']},'fontsize',11,'fontweight','b');
        end
    end
    xlabel(texto('rmfigPeriodret'),'fontsize',11,'fontweight','b');
    ylabel(Lab{1},'fontsize',11,'fontweight','b');
    
    if isdeployed, amevasavefigure(gcf,[],[0 1 0]); end %Guardo la figura en png
    
end %%%%% gev %%%%

%% %%%%% pot %%%%
if strcmp(tipo,'pot') || strcmp(tipo,'gevpot')
    if isempty(Dir), % sino existe wave_dir busco dir
        amevamsg = ['No se puede hacer el analisis por direcciones:', ...
            'No existe la informacion direccional!'];
    end
    
    ns = 0;
    if intraAnual && ~isempty(Dir), ns = 16; end
    
    TimeVec=datevec(Time);
    sect=linspace(0,360,ns*2+1);
    upc_=99.70;% 99.90(Igara)porcentaje si no funciona y el chi es mayor que (upc_=99.86 0.255)at
    upc=99.50;% 99.5 porciento 99.95. Solo para el calculo del umbral
    xor=sort(X); %umbral = prctile(X,99.5);%calculo el umbral 99.5 %
    xn=N-round(upc*N/100);
    xna=N-round(upc*N/100);
    umbral=xor(end-xna);
    nts=22;%22 length de Ts = [2:1:9 10:10:90 100:100:500];
    if exist('Opciones','var') && isprop(Opciones,'Ts') && ~isempty(Opciones.Ts),
        nts=length(Opciones.Ts);
    end
    umbrali=nan(ns+1,1);
    potcoefhs=nan(ns+1,3);
    coefabc=nan(ns+1,4);
    quanPOT=nan(ns+1,nts);
    stdDqP=nan(ns+1,nts);%22 length de Ts
    idx=cell(ns+1,1);
    umbrali(ns+1)=umbral;
    
    
    
    % OJO OJO probar y quedarse con lo nuevo OJO OJO
    % arugumento de salida distintos [xumb,~,~,datemax,xmax]
    % [Xnew,Tnew] OJ OJO OJO
    Opciones.myPlots = 0; % para no mostrar gráficos
    [xumb,~,~,datemax,xmax]=SeleccionaMaximosIndepPOT([Time(:) TimeVec(:,1) TimeVec(:,2) TimeVec(:,3) TimeVec(:,4) X(:)],umbral,3);
    idx{ns+1}=find(ismember(Time,datemax));%find(ismember(Time,tumb(:,1)));
    ny=TimeVec(end,1)-TimeVec(1,1)+1;
    [quanPOT(ns+1,1:nts),Ts,TapproxA,XorA,potcoefhs(ns+1,1:3),stdDqP(ns+1,1:nts)]=potajuste(xumb,umbral,ny,xmax,Opciones);
    
    if false
        [tumb,xumb,Dmax] = maximostempfunction(Time,X,'','pot',[],1,3,'day',[],[],umbral);%maximos anuales COMPROBAR
        [tumb,ny] = yearlytimescale(tumb,'R');%transformo el tumb a anos y encuentro el nuemro de anos years
        [lambda0,sigma0,gamma0,loglikeobj,gradiente,Hessiano] = OptiParamHessianPOTsimple(xumb,umbral,ny);
        global neps0; neps0 = 0; if gamma0 ~= 0, neps0 = 1; end
        [quan95,stdDq,mut,psit,epst,damax,xamax,invI0] = potpreplot(tumb,xumb,lambda0,sigma0,gamma0,confidenceLevelAlpha,Hessiano,loglikeobj,gradiente,umbral);
        ny=length(xmax);
        figure
        amevaAgregateQuantilesPOT(xamax,umbral,0.05,lambda0,sigma0,gamma0,invI0,ny,Opciones,gca,upc);
    end
    
    for i=1:ns, myXor(i).Xor = []; myXor(i).Tapprox = []; end
    myDir = false(1,ns);
    clear umbral;
    
    figure;
    simbol_marker={'o','d','o','d','o','d','o','d','o','d','o','d','o','d','o','d'};
    [mpcolor,srtDir] = escaladocircular(2);
    for j=1:1:ns, %Direcciones
        if j==1,
            aux=find(Dir>=sect(end-1) | Dir<sect(2));% -N-
        else
            aux=find(Dir>=sect(2*(j-1)) & Dir<sect(2*(j-1)+2));
        end
        if length(aux)<=xn || length(aux)/N*100<=1, continue; end
        %upc=99.5;% 99 porciento
        %umbral = prctile(X(aux),upc);
        %xor=sort(X(aux));xn=length(xor)-round(upc*length(xor)/100);umbral=xor(end-xn);%Roberto
        xor=sort(X(aux));umbral=xor(end-xn);%
        umbrali(j)=umbral;
        [xumb,~,~,datemax,xmax]=SeleccionaMaximosIndepPOT([Time(aux) TimeVec(aux,1) TimeVec(aux,2) TimeVec(aux,3) TimeVec(aux,4) X(aux)],umbral,3);
        if length(xumb)/ny<=1, umbrali(j)=nan; continue; end; % tan solo se hace si hay al menos un termporal por ano
        ls='--';if mod(j,2)==1, ls='-'; end
        idx{j}=find(ismember(Time,datemax));%find(ismember(Time,tumb(:,1)));
        [quanPOT(j,1:nts),~,Tapprox,Xor,potcoefhs(j,1:3),stdDqP(j,1:nts)]=potajuste(xumb,umbral,ny,xmax,Opciones);
        maxchifactor=0.261;%0.261 JO OJO parametro maximo del facor chi OJO OJO
        auxchi=potcoefhs(j,3)*2;
        while potcoefhs(j,3)>=maxchifactor && (N-round(upc_*N/100))<length(xor)+50 && (N-round(upc_*N/100))>2 && auxchi>potcoefhs(j,3), %ojo con 100
            %OJO OJO condicion no se queda con el mejor sino con el siguiente un poco peor al mejor OJOO
            xn_=N-round(upc_*N/100);
            xor=sort(X(aux));umbral=xor(end-xn_);
            umbrali(j)=umbral;
            [xumb,~,~,datemax,xmax]=SeleccionaMaximosIndepPOT([Time(aux) TimeVec(aux,1) TimeVec(aux,2) TimeVec(aux,3) TimeVec(aux,4) X(aux)],umbral,3);
            ls='--';if mod(j,2)==1, ls='-'; end
            idx{j}=find(ismember(Time,datemax));%find(ismember(Time,tumb(:,1)));
            auxchi=potcoefhs(j,3);
            [quanPOT(j,1:nts),~,Tapprox,Xor,potcoefhs(j,1:3),stdDqP(j,1:nts)]=potajuste(xumb,umbral,ny,xmax,Opciones);
            upc_=upc_+0.033;
        end
        if potcoefhs(j,3)>=maxchifactor, disp(['Atencion: maxchifactor=' num2str(potcoefhs(j,3)) ', >= ' num2str(maxchifactor)]); end
        myXor(j).Xor = Xor;
        myXor(j).Tapprox = Tapprox;
        semilogx(Ts,quanPOT(j,1:nts),'Color',mpcolor(j,:),'LineStyle',ls, 'LineWidth',1.2); hold on;
        % ajuste  a+b*x+c*x^2+d*x^3
        cdate=quanPOT(j,:);pop=stdDqP(j,:);
        coefabc(j,1:4)=polyfit(cdate(:),pop(:),3);% coefabc(j,1:3) = nlinfit(cdate(:),pop(:),@potencia,[0 1 1]);
        myDir(j) = true;
        clear Tapprox Xor aux umbral quanX10 cdate pop indice;
    end
    srtDir = srtDir(myDir);
    %anual plot
    semilogx(TapproxA,XorA,'ok','MarkerSize',1.6);hold on;
    semilogx(Ts,quanPOT(ns+1,:),'-k','Linewidth',1.2);
    cdate=quanPOT(ns+1,:);pop=stdDqP(ns+1,:);
    coefabc(ns+1,1:4)=polyfit(cdate(:),pop(:),3);% coefabc(ns+1,1:3) = nlinfit(cdate(:),pop(:),@potencia,[0 1 1]);
    semilogx(Ts,cdate+polyval(coefabc(ns+1,:),cdate),'k--','Linewidth',1.1);
    if sum(myDir)>0,
        h2 = legend([srtDir,{'Annual data','Annual',[num2str(1-confidenceLevelAlpha) ' Bounds']}],'Location','EastOutside');
        title({texto('rmfigTituloPOT'),'Pareto-Poisson'},'fontsize',11,'fontweight','b');
    else
        legend({'Annual data','Annual',[num2str(1-confidenceLevelAlpha) ' Bounds']},'Location','SouthEast');
        title({texto('rmfigTituloPOT'),['Pareto-Poisson (\sigma=',num2str(potcoefhs(ns+1,2),3),' \xi=', ...
            num2str(potcoefhs(ns+1,3),3),' \lambda=',num2str(potcoefhs(ns+1,1),3),'). ',Opciones.var_name{1}, ...
            '_{',num2str(upc),'}=',num2str(umbrali(ns+1))]},'fontsize',11,'fontweight','b');
    end
    semilogx(Ts,cdate-polyval(coefabc(ns+1,:),cdate),'k--','Linewidth',1.1);
    clear TapproxA XorA aux umbral quanX10 cdate pop;
    xlabel(texto('rmfigPeriodret'),'fontsize',11,'fontweight','b');
    ylabel(Lab{1},'fontsize',11,'fontweight','b');
    xticks=[1 2 5 10 20 50 100 250 500];grid on;grid(gca,'minor')
    hlim=get(gca,'ylim');hlim=[0 hlim(2)];
    set(gca,'ylim',hlim,'xlim',[1 Ts(end)],'XTick',xticks,'XMinorTick','off','XMinorGrid','off');
    for j=1:1:ns, %Direcciones
        if j==1,
            aux=find(Dir>=sect(end-1) | Dir<sect(2));% -N-
        else
            aux=find(Dir>=sect(2*(j-1)) & Dir<sect(2*(j-1)+2));
        end
        if length(aux)<=xn || length(aux)/N*100<=1, continue; end
        semilogx(myXor(j).Tapprox,myXor(j).Xor,simbol_marker{j},'Color',mpcolor(j,:),'MarkerSize',1.9); hold on;
    end
    if isdeployed, amevasavefigure(gcf,[],[0 1 0]); end %Gurado la figura en png
end

if ~isempty(amevamsg), disp(amevamsg); end

end

function [Time,X,Dir] = validrvar(data)
% Valido los campo de la estructura
X = []; Dir = []; Time = [];
if isfield(data,'X') && ~isempty(data.X),
    X=data.X;
else
    if isfield(data,'hs') && ~isempty(data.hs),
        X=data.hs;
        disp('No existe el campo data.X, pero si data.hs. Usamos este campo');
    end
end
if isfield(data,'wave_dir') && ~isempty(data.wave_dir), %Dir
    Dir=data.wave_dir;
else
    if isfield(data,'dir') && ~isempty(data.dir), % sino existe wave_dir busco dir
        Dir=data.dir;
        disp('No existe el campo data.wave_dir, pero si data.dir. Usamos este campo');
    end
end
if isfield(data,'time') && ~isempty(data.time),
    Time=data.time;
end

end