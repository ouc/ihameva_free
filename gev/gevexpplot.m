function gevexpplot(auxstate,OPHr,marstate,ax1,hf)
global neps0
[beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,betaT3,varphi,varphi2,varphi3,lista,lista2,lista3,p,np]=amevaOPH.gevparamsol2array(OPHr);
% MODELO
[fmut,fpsit,fxit,fcdf,icdf]=modelfunction;%Modelo GEV
if exist('hf','var') && ~isempty(hf) && ishandle(hf),
    set(0,'CurrentFigure',hf); set(hf,'CurrentAxes',ax1); cla(ax1,'reset');
end
posaux_=get(ax1,'Position');
if posaux_(1)>0.13 %0.13 valor por defecto del axis nuevo OJO
    set(ax1,'Position',[0.35 0.07 0.63 0.91]);
end
if abs(0.13-posaux_(1))<0.01,
    figaux_=get(gcf,'Position');
    set(gcf,'Position',[figaux_(1) figaux_(2) 800 700]);
end
ypstx=[0.985,0.9275,0.875,0.8075,0.7475,0.63,0.550,0.480];
text(0.0,ypstx(1)+0.04,' GEV Model:','FontSize',11,'Parent',ax1,'HorizontalAlignment','left');
text(0.5,ypstx(1),fcdf,'Interpreter','latex','FontSize',11,'Parent',ax1,'HorizontalAlignment','Center');
text(0.5,ypstx(2),icdf,'Interpreter','latex','FontSize',11,'Parent',ax1,'HorizontalAlignment','Center');
text(0.5,ypstx(3),fmut,'Interpreter','latex','FontSize',11,'Parent',ax1,'HorizontalAlignment','Center');
text(0.5,ypstx(4),fpsit,'Interpreter','latex','FontSize',11,'Parent',ax1,'HorizontalAlignment','Center');
text(0.5,ypstx(5),fxit,'Interpreter','latex','FontSize',11,'Parent',ax1,'HorizontalAlignment','Center');
% AJUSTE
[fmut,fpsit,fxit]=modelfunction(auxstate);%Modelo a ajustar
text(0.0,ypstx(6)+0.04,' Model to Fit:','FontSize',11,'Parent',ax1,'HorizontalAlignment','left');
text(0.5,ypstx(6),fmut,'Interpreter','latex','FontSize',11,'Parent',ax1,'HorizontalAlignment','Center');
text(0.5,ypstx(7),fpsit,'Interpreter','latex','FontSize',11,'Parent',ax1,'HorizontalAlignment','Center');
text(0.5,ypstx(8),fxit,'Interpreter','latex','FontSize',11,'Parent',ax1,'HorizontalAlignment','Center');
% MODELO AJUSTADO
Fmuparam{1}=beta0;        Fmuparam{2}=beta;        Fmuparam{3}=betaT;        Fmuparam{4}=varphi;
Fpsiparam{1}=alpha0;        Fpsiparam{2}=alpha;        Fpsiparam{3}=betaT2;        Fpsiparam{4}=varphi2;
Fxiparam{1}=gamma0; if ~isempty(neps0) && ~neps0 && marstate, Fxiparam{1}=0; end
Fxiparam{2}=gamma;%Modelo ajustado
[fmut,fpsit,fxit]=modelfunction(Fmuparam,Fpsiparam,Fxiparam);
text(0,0.440,' Model Fitted:','FontSize',11,'Parent',ax1,'HorizontalAlignment','left');
text(0.5,0.390,fmut,'Interpreter','latex','FontSize',11,'Parent',ax1,'HorizontalAlignment','Center');
text(0.5,0.315,fpsit,'Interpreter','latex','FontSize',11,'Parent',ax1,'HorizontalAlignment','Center');
text(0.5,0.240,fxit,'Interpreter','latex','FontSize',11,'Parent',ax1,'HorizontalAlignment','Center');
%RESULTADOS/Results
[fmut,fpsit,fxit]=modelfunction(Fmuparam,Fpsiparam,Fxiparam,'result');
text(0,0.205,' Results:','FontSize',11,'Parent',ax1,'HorizontalAlignment','left');
text(0.5,0.150,fmut,'Interpreter','latex','FontSize',10,'Parent',ax1,'HorizontalAlignment','Center');
text(0.5,0.085,fpsit,'Interpreter','latex','FontSize',10,'Parent',ax1,'HorizontalAlignment','Center');
text(0.5,0.020,fxit,'Interpreter','latex','FontSize',10,'Parent',ax1,'HorizontalAlignment','Center');
axis off;

end
%% Latex text selected type fucntion adjust
function [fmut,fpsit,fxit,fcdf,icdf]=modelfunction(varargin)
%         fmut='$$\mu(t)=\beta_o+\sum_{i=1}\left[\beta_{2i-1}\cos(i\omega t)+\beta_{2i}\sin(i\omega t)\right]+\beta_{T1}t+\sum_k\phi_kn_{k,t}$$';
%         fpsit='$$\psi(t)=\exp(\alpha_o+\sum_{i=1}\left[\alpha_{2i-1}\cos(i\omega t)+\alpha_{2i}\sin(i\omega t)\right]+\beta_{T2}t+\sum_k\phi_kn_{k,t})$$';
%         fxit='$$\xi(t)=\gamma_o+\sum_{i=1}\left[\gamma_{2i-1}\cos(i\omega t)+\gamma_{2i}\sin(i\omega t)\right]$$';
fcdf='$$F(x;\mu,\psi,\xi)=\exp\left\{-\left[1+\xi\left(\frac{x;-\mu}{\psi}\right)\right]^{-1/\xi}\right\}$$';
icdf='$$H_s(T_r;\mu,\psi,\xi)=\mu - \frac{\psi}{\xi}(1 - (\frac{1}{T_r})\;^{-\xi} )$$';

Fmu='\mu(t)';
Fbeta{1}='\beta_o';
Fbeta{2}='\sum_{i=1}\left[\beta_{2i-1}\cos(i\omega t)+\beta_{2i}\sin(i\omega t)\right]';
Fbeta{3}='\beta_{T1}t';
Fbeta{4}='\sum_k\phi_kn_{k,t}';
Fpsi='\psi(t)';
Falpha{1}='\alpha_o';
Falpha{2}='\sum_{i=1}\left[\alpha_{2i-1}\cos(i\omega t)+\alpha_{2i}\sin(i\omega t)\right]';
Falpha{3}='\beta_{T2}t';
Falpha{4}='\sum_k\phi_kn_{k,t}';
Fxi='\xi(t)';
Fgamma{1}='\gamma_o';
Fgamma{2}='\sum_{i=1}\left[\gamma_{2i-1}\cos(i\omega t)+\gamma_{2i}\sin(i\omega t)\right]';
% OJO OJO esto he aumentado OJO OJO
Fgamma{3}='\beta_{T3}t';
Fgamma{4}='\sum_k\phi_kn_{k,t}';

if nargin==0,
    Fmuparam(1:4)={1};Fpsiparam(1:4)={1};Fxiparam(1:2)={1};
elseif nargin==1,
    Fmuparam(1:4)={[]};Fpsiparam(1:4)={[]};Fxiparam(1:2)={[]};
    Fmuparam{1}=1;  Fpsiparam{1}=1; Fxiparam{1}=1;
    aux=varargin{1};
    if  aux(1),
        Fmuparam{2}=1;
        Fpsiparam{2}=1;
        Fxiparam{2}=1;
    end
    if  aux(2),
        Fmuparam{3}=1;
        Fpsiparam{3}=1;
    end
    if  aux(3),
        Fmuparam{4}=1;
        Fpsiparam{4}=1;
    end
elseif nargin==3,
    Fmuparam=varargin{1};Fpsiparam=varargin{2};Fxiparam=varargin{3};
elseif nargin==4,
    Fmuparam=varargin{1};Fpsiparam=varargin{2};Fxiparam=varargin{3};
else
    fmut='';fpsit='';fxit='';fcdf=''; return;
end

fmut='';
for i=1:length(Fmuparam),
    if not(isempty(Fmuparam{i})),
        fmut=[fmut Fbeta{i} '+'];
    end
end
if not(isempty(fmut))
    if strcmp(fmut(end),'+'), fmut(end)=''; end
    fmut=['$$' Fmu '=' fmut '$$'];
end

fpsit='';
for i=1:length(Fpsiparam),
    if not(isempty(Fpsiparam{i})),
        fpsit=[fpsit Falpha{i} '+'];
    end
end
if not(isempty(fpsit))
    if strcmp(fpsit(end),'+'), fpsit(end)=''; end
    fpsit=['$$' Fpsi '=\exp\;(\;' fpsit '\;)$$'];
end

fxit='';
for i=1:length(Fxiparam),
    if not(isempty(Fxiparam{i})),
        fxit=[fxit Fgamma{i} '+'];
    end
end
if not(isempty(fxit))
    if strcmp(fxit(end),'+'), fxit(end)=''; end
    fxit=['$$' Fxi '=' fxit '$$'];
end
%solo si nargin == 4 resultados
if nargin==4
    forpres='%2.3f';
    fmut='';
    if not(isempty(Fmuparam{1})),
        fmut=[fmut ' \beta_o=' num2str(Fmuparam{1},forpres) ','];
    end
    if not(isempty(Fmuparam{2})),
        for i=1:length(Fmuparam{2}), fmut=[fmut ' \beta_' num2str(i) '=' num2str(Fmuparam{2}(i),forpres) ',']; end
    end
    if not(isempty(Fmuparam{3})),
        fmut=[fmut ' \beta_{T1}=' num2str(Fmuparam{3},forpres) ','];
    end
    if not(isempty(Fmuparam{4})),
        for i=1:length(Fmuparam{4}), fmut=[fmut ' \phi_' num2str(i) '=' num2str(Fmuparam{4}(i),forpres) ',']; end
    end
    if not(isempty(fmut)),
        fmut(end)='';
        fmut=['$$' fmut '$$'];
    end
    fpsit='';
    if not(isempty(Fpsiparam{1})),
        fpsit=[fpsit ' \alpha_o=' num2str(Fpsiparam{1},forpres) ','];
    end
    if not(isempty(Fpsiparam{2})),
        for i=1:length(Fpsiparam{2}), fpsit=[fpsit ' \alpha_' num2str(i) '=' num2str(Fpsiparam{2}(i),forpres) ',']; end
    end
    if not(isempty(Fpsiparam{3})),
        fpsit=[fpsit ' \beta_{T2}=' num2str(Fpsiparam{3},forpres) ','];
    end
    if not(isempty(Fpsiparam{4})),
        for i=1:length(Fpsiparam{4}), fpsit=[fpsit ' \phi_' num2str(i) '=' num2str(Fpsiparam{4}(i),forpres) ',']; end
    end
    if not(isempty(fpsit)),
        fpsit(end)='';
        fpsit=['$$' fpsit '$$'];
    end
    fxit='';
    if not(isempty(Fxiparam{1})),
        fxit=[fxit ' \gamma_o=' num2str(Fxiparam{1}) ','];
    end
    if not(isempty(Fxiparam{2})),
        for i=1:length(Fxiparam{2}), fxit=[fxit ' \gamma_' num2str(i) '=' num2str(Fxiparam{2}(i),forpres) ',']; end
    end
    if not(isempty(fxit)),
        fxit(end)='';
        fxit=['$$' fxit '$$'];
    end
end
end