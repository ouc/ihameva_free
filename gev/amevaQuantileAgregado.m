function quanaggr = amevaQuantileAgregado (q,t0,t1,OPHobj,pS)
% OPTIMIZAR ESTA FUNCION OC UC
tiempo = OPHobj.t;
kt = OPHobj.kt;
indices = OPHobj.indices;
Km = OPHobj.Km;
Ny = OPHobj.Ny;
lista = OPHobj.lista;
lista2 = OPHobj.lista2;
lista3 = OPHobj.lista3; 
a = []; b = []; if isfield(OPHobj,'a');   a = OPHobj.a;  b = OPHobj.b;  end
clear OPHobj;
q = q(:);
t0 = t0(:);
t1 = t1(:);
if min(size(q))>1,
    error('the quantile must be a double or a vector');
end
if min(size(t0))>1 || min(size(t1))>1,
    error('Initial/Final quantile aggregated integration time must be a double or a vector');
end
if max(size(t0)) ~= max(size(t1)),
    error('Initial/Final quantile aggregated integration time size must be equal size');
end
m = length(q);
n = length(t0);
%   For the required period the mean value of the corresponding covariates
%   is calculated and considered constant for the rest of the study
indicesint = FindMeanIndice(indices,tiempo,t0,t1,lista);
indices2int = FindMeanIndice(indices,tiempo,t0,t1,lista2);
indices3int = FindMeanIndice(indices,tiempo,t0,t1,lista3);
%   Requires quantile
quanaggr = zeros(n,m);%quad
media = zeros(1,n);
for jj=1:n,
    media(jj) = quad(@(x)amevaOPH.parametros(x,pS.mu,indices(:,lista),indicesint(:,jj),tiempo),0,1);%,[],10^-6 mu
end
for i1 = 1:m, %vector de quantiles
    for j1 = 1:n, %vector de tiempos t0 and t1
        %   para configurar la integral en bsimpson
        nInt = []; epsilon = [];
        if round(365*8*(t1(j1)-t0(j1))) >= 365*25, 
            nInt = max(365*25,min(Ny*Km,Ny*365)); epsilon=10^-4; 
        end
            %   Delta T min 1 anho
        DeltaT = max(1,(t1(j1)-t0(j1)))*1.0;%12/40 *(t1(i1,j1)-t0(i1,j1))
        zq = media(j1);
        err = 1;
        iter = 1;
%         profile on;
        while err>10^(-4) && iter<1000,
            zqold = zq;
            int=bsimp0(@(x)Fzeroquanint0(x,zqold,'F',pS,lista,lista2,lista3,indicesint,indices2int,indices3int,tiempo,indices,kt),t0(j1),t1(j1),nInt,epsilon);
            int=int+log(q(i1))/(Km*Thetaq(int,a,b))*DeltaT;%% OJO OJO comprobar periodo de retorno
            dint=bsimp0(@(x)Fzeroquanint0(x,zqold,'D',pS,lista,lista2,lista3,indicesint,indices2int,indices3int,tiempo,indices,kt),t0(j1),t1(j1),nInt,epsilon);
            zq = zq - int/dint;
            if abs(zq)>10^(-5),
                err = abs((zq - zqold)/zqold);
            else
                err = abs((zq - zqold));
            end
            iter = iter + 1;
            %disp(['[amevaQuantileAgregado] / -> [' num2str(q(i1)) '-' num2str(t1(j1)) ' - ' num2str(zq) '-' num2str(err) '-' num2str(iter) '] ' datestr(now,0)]);% COMENTAR
        end
%         profile viewer
        if iter==1000,
            zq = NaN;
            disp('WARNING: Maximum number of Newton iterations')
        end
        if int>10^(-2),
            disp('WARNING: False zero, check it')
            zq = NaN;
        end
        quanaggr(j1,i1)=zq;
    end
    %disp(['[amevaQuantileAgregado] / -> [' num2str(q(i1)) '] ' datestr(now,0)]);% COMENTAR
end
%%   Function Thetaq(q,a,b)
    function x=Thetaq(q,a,b)
        x=1;
        if ~isempty(a) || ~isempty(b)
            x=1+a*exp(-b*q);
        end
    end
%%   Function to find mean indice
    function indicesint = FindMeanIndice(indices,tiempo,t0,t1,lista)
        nn = length(t0);
        indicesint = zeros(length(lista),nn);
        if isempty(tiempo) || isempty(lista), return; end
        for j=1:nn,
            pos_ = (tiempo>=t0(j) & tiempo<=t1(j));
            indicesint(:,j)=mean(indices(pos_,lista),1);
        end
        %En caso de tener mayor resolucion temporal OJO OJO con lo que
        %hacer
    end
%%   Function to solve the quantile
    function zn = Fzeroquanint0(t,zq,ID,pS,lista,lista2,lista3,indicesint,indices2int,indices3int,tiempo,indices,ktold)
%         tic
        mut = amevaOPH.parametros(t,pS.mu,indices(:,lista),indicesint,tiempo);
        psit = exp(amevaOPH.parametros(t,pS.psi,indices(:,lista2),indices2int,tiempo));
        epst = amevaOPH.parametros(t,pS.chi,indices(:,lista3),indices3int,tiempo);
        posG = (abs(epst)<=1e-8);
        pos  = (abs(epst)>1e-8);
        epst(posG)=1;
        xn = (zq-mut)./psit;
        z = 1 + epst.*xn;
        z = max(1e-8,z);
        switch ID
            case 'F'
                zn =z.^(-1./epst);
                zn(posG) = exp(-xn(posG));              %    For the Gumbel case
            case 'D'
                zn =-z.^(-1-1./epst)./psit;
                zn(posG) = -exp(-xn(posG))./psit(posG); %    For the Gumbel case
            otherwise
                zn=[];
        end
%         toc
%         disp(['[Fzeroquanint0] / -> ' ID ' ' datestr(now,0)]);% COMENTAR
    end
%%   Function to integrate
    function [I,cnt]=bsimp0(funfcn,a,b,n,epsilon)
        if ~exist('n','var') || isempty(n), n = round(365*8*(b-a)); end
        if ~exist('epsilon','var') || isempty(epsilon), epsilon = 10^(-5); end
        if rem(n,2)~= 0,
            n = n+1;
            %            warning('The number of initial subintervals must be pair');
        end
%         tic
        h = (b-a)/n;
        x = linspace(a,b,n+1);
        y = feval(funfcn,x);
        Ainit=y(1)+y(n+1);
        
        AuxI1=sum(y(2:2:n));
        AuxI2=sum(y(1:2:n-1));
        cnt=n+1;
        I=(Ainit+4*AuxI1+2*AuxI2)*h/3;
        AuxTot=AuxI1+AuxI2;
        I1=I+epsilon*2;j=2;
        error = 1;
        while error>epsilon,
            cnt=cnt+j*n/2;
            I1=I;
            x=linspace(a+h/j,b-h/j,j*n/2); % OJO OJO aqui crece la longitud OJO OJO
            y = feval(funfcn,x);
            AuxI=sum(y(1:1:j*n/2));
            I=(Ainit+4*AuxI+2*AuxTot)*h/(3*j);
            j=j*2;
            AuxTot=AuxTot+AuxI;
            %    Error
            if abs(I1)>10^(-5),
                error = abs((I - I1)/I1);
            else
                error = abs((I - I1));
            end
        end
%         toc
%         disp(['[bsimp0] / -> ' num2str(n) ' ' datestr(now,0)]);% COMENTAR
    end
%%   Function to solve the quantile
    function zn = Fzeroquanint(t,zq,ID,pS,lista,lista2,lista3,indicesint,indices2int,indices3int,tiempo,indices,ktold)
        %   ID=0 ->"F"funcion;
        %   ID=1 ->"D"Derivada;
        %   Evaluate the location,scale and shape parameter at each time t as a function
        %   of the actual values of the parameters given by p
        %   Location
        mut1 = amevaOPH.parametros(t,pS.mu,indices(:,lista),indicesint,tiempo);
        %   Scale
        psit1 = exp(amevaOPH.parametros(t,pS.psi,indices(:,lista2),indices2int,tiempo));
        %   Shape
        epst = amevaOPH.parametros(t,pS.chi,indices(:,lista3),indices3int,tiempo);
        
        %   The values whose shape parameter is almost cero corresponds to
        %   the GUMBEL distribution, locate their positions if they exist
        posG = (abs(epst)<=1e-8);
        %   The remaining values correspond to WEIBULL or FRECHET
        pos  = (abs(epst)>1e-8);
        %   The corresponding GUMBEl values are set to 1 to avoid
        %   numerical problems, note that for those cases the GUMBEL
        %   expressions are used
        epst(posG)=1;
        
        %solo si kt es distinto de ones y existe tiempo
        if  sum(ktold)~=length(ktold) && ~isempty(tiempo),
            kt2 = spline(tiempo,ktold,t);
        else
            kt2 = ones(size(mut1));
        end
        %   Modifico los parametros para incluir el numero de datos
        mut = mut1;
        psit = psit1;
        mut(pos) = mut1(pos)+psit1(pos).*(kt2(pos).^epst(pos)-1)./epst(pos);
        psit(pos) = psit1(pos).*kt2(pos).^epst(pos);
        %   Modifico los parametros para incluir el numero de datos para GUMBEL
        mut(posG) = mut(posG)+psit(posG).*log(kt2(posG));
        
        %   Evaluate auxiliary variables
        xn = (zq-mut)./psit;
        z = 1 + epst.*xn;
        %   Since the z-values must be greater than zero in order to avoid
        %   numerical problems their values are set to be greater than 1e-4
        z = max(1e-8,z);
        switch ID
            case 'F'
                zn =z.^(-1./epst);
                zn(posG) = exp(-xn(posG));              %    For the Gumbel case
            case 'D'
                zn =-z.^(-1-1./epst)./psit;
                zn(posG) = -exp(-xn(posG))./psit(posG); %    For the Gumbel case
            otherwise
                zn=[];
        end
    end
%%   Function to integrate
    function [I,cnt]=bsimp(funfcn,a,b,n,epsilon,trace)
        %	BSIMP   Numerically evaluate integral, low order method.
        %   I = BSIMP('F',A,B) approximates the integral of F(X) from A to B
        %   within a relative error of 1e-3 using an iterative
        %   Simpson's rule.  'F' is a string containing the name of the
        %   function.  Function F must return a vector of output values if given
        %   a vector of input values.%
        %   I = BSIMP('F',A,B,EPSILON) integrates to a total error of EPSILON.  %
        %   I = BSIMP('F',A,B,N,EPSILON,TRACE,TRACETOL) integrates to a
        %   relative error of EPSILON,
        %   beginning with n subdivisions of the interval [A,B],for non-zero
        %   TRACE traces the function
        %   evaluations with a point plot.
        %   [I,cnt] = BSIMP(F,a,b,epsilon) also returns a function evaluation count.%
        %   Roberto Minguez Solana%   Copyright (c) 2001 by Universidad de Cantabria
%         if nargin < 4, n = 2; n=round(365*8*(b-a));  epsilon =10^(-8); trace = 0;end
%         if nargin < 5, epsilon =10^(-8); epsilon =10^(-6); trace = 0; end
%         if nargin < 6,trace = 0; end
%         if isempty(epsilon), epsilon =10^(-8); end
%         if isempty(trace), trace = 0; end
        if ~exist('n','var') || isempty(n), n = round(365*8*(b-a)); end
        if ~exist('epsilon','var') || isempty(epsilon), epsilon = 10^(-8); end
        if ~exist('trace','var') || isempty(trace), trace = 0; end
        
        if rem(n,2)~= 0,
            n = n+1;
            %            warning('The number of initial subintervals must be pair');
        end
        %Step 1
        h = (b-a)/n;
        % Step 3
        x = linspace(a,b,n+1);
        y = feval(funfcn,x);
        if trace
            figure(1)
            hold on
            lims1 = [min(x) max(x) min(y) max(y)];
            axis(lims1);
            plot(x,y,'.')
            drawnow
        end
        Ainit=y(1)+y(n+1);
        AuxI1=0;
        AuxI2=0;
        for i=1:1:n/2,
            AuxI1=AuxI1+y(2*i);
        end
        for i=1:1:n/2-1,
            AuxI2=AuxI2+y(2*i+1);
        end
        cnt=n+1;
        %Step 4
        I=(Ainit+4*AuxI1+2*AuxI2)*h/3;
        AuxTot=AuxI1+AuxI2;
        %Step 5
        I1=I+epsilon*2;j=2;
        %Step 6
        error = 1;
        while error>epsilon,
            cnt=cnt+j*n/2;
            %Step 7
            I1=I;
            %Step 8
            x=linspace(a+h/j,b-h/j,j*n/2);
            y = feval(funfcn,x);
            AuxI=sum(y(1:1:j*n/2));
            %Step 9
            I=(Ainit+4*AuxI+2*AuxTot)*h/(3*j);
            %Step 10
            j=j*2;
            %Step 11
            AuxTot=AuxTot+AuxI;
            %    Error
            if abs(I1)>10^(-5),
                error = abs((I - I1)/I1);
            else
                error = abs((I - I1));
            end
            %Plot
            if trace
                figure(1)
                hold on
                plot(x,y,'.')
                drawnow
            end
        end
    end
end
