
function [mut,psit,epst] = parametroEOF(indicesEOF,parametros_sol)


% parametroEOF function calculates the location, scale and shape parameters
%   for given parameters only with covariates. 


mut = parametros_sol.beta0 + sum(parametros_sol.varphi'.*indicesEOF(parametros_sol.lista));

psit = exp (parametros_sol.alpha0 + sum(parametros_sol.varphi2'.*indicesEOF(parametros_sol.lista2)));

epst = parametros_sol.gamma0 + sum(parametros_sol.varphi3'.*indicesEOF(parametros_sol.lista3));




end