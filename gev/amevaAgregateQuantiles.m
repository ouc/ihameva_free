classdef amevaAgregateQuantiles < handle
    methods (Access = public, Static = true)
        function [Ts,quanaggr,stdDqX,hLine]=aqPlot(OPHobj,OPHr,confidenceLevelAlpha,Opciones,gcax,anualplot,pathresult)
            % OPTIMIZAR ESTA FUNCION OC UC
            % GEV Quantiles agregdos MENSUALES/ANNUAL... tambien se pueden mostrar los anuales. IMPORTANTE: los datos son mensuales
            if confidenceLevelAlpha>0.5, error(['confidenceLevelAlpha=',num2str(confidenceLevelAlpha)]); end
            if ~exist('anualplot','var'), anualplot=true; end
            if ~exist('pathresult','var') || ~ischar(pathresult), pathresult=pwd; end
            if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
            if ~ishandle(gcax), disp('WARNING! amevaAgregateQuantiles with axes'); end
            set(gcf,'CurrentAxes',gcax)
            
            hLine = []; %handle off figure
            %Texto idioma
            texto=amevaclass.amevatextoidioma(Opciones.idioma); strMeses='';
            
            %  Aggregate quantiles for different return periods
            if ~isprop(Opciones,'Ts') || isempty(Opciones.Ts),
                %   ATENCION no se debe utilizar valores menores  que 2... las aprox. deja de ser validas. Maximos mensuales
                %Ts = [1.1 2:1:9 10:10:90 100:100:500];
                Ts = [2 5 10 20 25 50 75 100 200 300 400 500];
                %Ts=[1.1 2 10 20 50 500];
            else
                Ts=Opciones.Ts;
            end
            if min(Ts)<=1, error('El valor min de Ts tiene que ser mayor que 1'); end
            Ts0=Ts(1);
            
            %  Meses para calcular el periodo de retorno (datos mensuales)
            eval(['strMeses=' texto('meslong') ';']); %Meses idioma
            myMeses=1:12;
            simbol_marker={'o','d','o','d','o','d','o','d','o','d','o','d'};
            mapa_colores=escaladocircular(1);%colores meses
            if isprop(Opciones,'myMeses') && ~isempty(Opciones.myMeses),
                myMeses = Opciones.myMeses;
                simbol_marker = simbol_marker(myMeses);
                strMeses = strMeses(myMeses);
                mapa_colores = mapa_colores(myMeses,:);
            end
            
            nts = length(Ts);
            nMes = length(myMeses);
            quanaggr=nan(nMes,nts);
            stdDqX=nan(nMes,nts);
            quanaggrA = zeros(nts,1);
            stdup = zeros(nts,1);
            stdlo = zeros(nts,1);
            var_intra_annual=[length(OPHr.beta) length(OPHr.alpha) length(OPHr.gamma)];
            nmpc=[num2str(var_intra_annual(1)) num2str(var_intra_annual(2)) num2str(var_intra_annual(3))];
            
            if exist(fullfile(pathresult,['GEVQ_' nmpc '_' Opciones.cityname '.mat']),'file')==0 || Opciones.forzarAQ,
                if sum(var_intra_annual) > 0 && OPHobj.Km == 12 %solo datos mensuales
                    disp(['Calculando GEV Agregate Quantiles.... En funci',char(243),'n de su ordenador esto puede tardar unos min. Espere[' datestr(now), ...
                        ']. En un PC Intel Core2 Quad CPU aprox. ',num2str(sum(var_intra_annual)),' min.)']);
                    if nargout>=3,
                        [quanaggr,stdDqX]=amevaAgregateQuantiles.aqMonth(Ts,OPHobj,OPHr,confidenceLevelAlpha,myMeses);
                    else
                        quanaggr=amevaAgregateQuantiles.aqMonth(Ts,OPHobj,OPHr,confidenceLevelAlpha,myMeses);
                    end
                end
                %si se quieren mostrar anuales o son datos anuales. Por defecto si
                if anualplot ||  OPHobj.Km==1, %si se quieren se muestran los datos anuales o son datos anuales. Por defecto si
                    [quanaggrA,stdup,stdlo]=amevaAgregateQuantiles.aqAnual(Ts,OPHobj,OPHr,confidenceLevelAlpha);
                end
                save(fullfile(pathresult,['GEVQ_' nmpc '_' Opciones.cityname '.mat']),'quanaggr','stdDqX','quanaggrA','stdup','stdlo');
            else
                load(fullfile(pathresult,['GEVQ_' nmpc '_' Opciones.cityname '.mat']));
            end
            %   Cuantiles agregados en mas de 30 anos
            %     quanaggr222=QuantileAgregadoII (1-1/100,0,30,kt,beta0,beta,alpha0,alpha,gamma0,gamma,[],betaT,betaT2,varphi,varphi2,indices(:,lista),indices(:,lista2),datemax);
            %   Plot of the aggregate quantiles mensuales
            datemax_mod=mod(OPHobj.t,1);
            if sum(var_intra_annual)>0 && OPHobj.Km == 12 %solo datos mensuales
                for i = 1:nMes,
                    ls='--';if mod(i,2)==1, ls='-'; end
                    hLine(i) = semilogx(gcax,Ts,quanaggr(i,:),'color',mapa_colores(i,:),'linestyle',ls, 'linewidth',1.2);hold(gcax,'on');
                end
            end
            if (anualplot || OPHobj.Km==1) && ~isempty(quanaggrA), %si se quieren se muestran los datos anuales. Por defecto si
                hLine(end+1) = semilogx(gcax,Ts,quanaggrA,'Color',[0 0 0]); hold on;
                hmax_=zeros(1,OPHobj.Ny);
                for j=1:OPHobj.Ny,
                    if not(isempty(max(OPHobj.x(find(OPHobj.t>=j-1 & OPHobj.t<j)))))
                        hmax_(j)=max(OPHobj.x(find(OPHobj.t>=j-1 & OPHobj.t<j)));
                    end
                end
                hmaxor_ = sort(hmax_);
                ProHsmaxor = (((1:length(hmaxor_)))/(length(hmaxor_)+1))';
                Tapprox = 1./(1-ProHsmaxor);
                id = find(Tapprox>=Ts0);% OJO poner 1 si se quiere ver los valores chicos
                hLine(end+1) = semilogx(gcax,Tapprox(id),hmaxor_(id),'ok','MarkerSize',1.9);
                hLine(end+1) = semilogx(gcax,Ts,stdlo,'--k','Linewidth',1.1);
                hLine(end+1) = semilogx(gcax,Ts,stdup,'--k','Linewidth',1.1);
            end
            if (~isempty(Opciones.var_name{1}) || ~isempty(Opciones.var_unit{1}) ) && OPHobj.Km>1
                title(['Aggregate Quantiles (' Opciones.cityname ')'])
                xlabel(gcax,'Return Period (years)');
                ylabel(gcax,labelsameva(Opciones.var_name{1},Opciones.var_unit{1}));
                if sum(var_intra_annual)>0 && OPHobj.Km == 12
                    if anualplot,%si se quieren se muestran los datos anuales. Por defecto si
                        legend([strMeses,{'Annual','Annual data',[num2str(1-confidenceLevelAlpha) ' Bounds']}],'Location','EastOutside')
                    else
                        legend(strMeses,'Location','EastOutside')
                    end
                end
            end
            if sum(var_intra_annual)>0 && OPHobj.Km == 12 % solo datos mensuales
                for i = 1:nMes,
                    hmaxor_ = sort(OPHobj.x(find(datemax_mod>(myMeses(i)-1)/12 & datemax_mod<=myMeses(i)/12)));
                    ProHsmaxor = (((1:length(hmaxor_)))/(length(hmaxor_)+1))';
                    Tapprox = 1./(1-ProHsmaxor);
                    id = find(Tapprox>=Ts0);% OJO poner 1 si se quiere ver los valores chicos
                    semilogx(gcax,Tapprox(id),hmaxor_(id),simbol_marker{i},'Color',mapa_colores(i,:),'MarkerSize',1.9);
                end
            end
            xticks=[1 2 5 10 20 50 100 250 500];grid(gcax,'on');grid(gcax,'minor');
            % OJO OJO poner Ts0 o xticks(1) OJO OJO Ts0
            set(gcax,'xlim',[xticks(1) Ts(end)],'XTick',xticks,'ylim',[0 max(get(gca,'ytick'))],'XMinorTick','off','XMinorGrid','off','YMinorTick','on','YMinorGrid','on');
            hold off;
        end
        %% Cuantil agregado Annual para un periodo de retorno Ts
        function [quanaggr,stdup,stdlo,stdQuan]=aqAnual(Ts,OPHobj,OPHr,confidenceLevelAlpha)
            quanaggr = []; stdup = []; stdlo = [];
            Ny = 1; % one year
            if OPHobj.Km ~=1,
                if ~isfield(OPHobj,'HsmaxA') || isempty(OPHobj.HsmaxA), 
                    warning('Sus datos no son anuales o faltan OPHobj.HsmaxA, OPHobj.datemaxA');
                    return;
                end
                disp('[aqAnual] Calculando Ajuste max. anual');
                info = [1 0 0 0 12 1 0 0 0 0];
                Opciones = ameva_options();
                
%                 modelo = 'Auto'; %Modelo automatico al buscar Gamma0
%                 if info(10)==1
%                     modelo = 'Gumbel'; %Forzamos Gumbel Gamma0=0;
%                 elseif info(10)==2
%                     modelo = 'FrechetWeibull'; %Forzamos Frechet/Weibull Gamma0>0/Gamma0<0;
%                 end
%                 nit = 1;
%                 OPHobjS = amevaOPH.OPHstruct(OPHobj.HsmaxA,OPHobj.datemaxA,[],[],OPHobj.criterio,OPHobj.confidenceLevel); % Preparamos los datos
%                 [IT,OPHrS]=amevaOPH.AOPH_model([],OPHobjS,nit,modelo,OPHobjS.criterio,OPHobjS.confidenceLevel);
%                 OPHobjS=amevaOPH.asigntoresult(OPHobjS,OPHrS,nit);
                
                [OPHrS, OPHobjS] = amevaFuncAutoAdjust.AutoAjuste(OPHobj.HsmaxA,OPHobj.datemaxA,[],[],info,OPHobj.confidenceLevel,Opciones);
            else
                OPHobjS = OPHobj;
                OPHrS = OPHr;
            end
            disp(['Ny=' num2str(Ny) ' Total year, ' num2str(OPHobjS.Ny)]);
            quanaggr = amevaQuantileAgregado(1-1./Ts,0,Ny,OPHobjS,OPHrS.pS);
            if nargout>1,
                %%%%%%%%%%%Intervalos de confianza del cuantil anual%%%%%%%%%%%
                disp(['[aqAnual] Cuantil agregado Annual iter / -> ' datestr(now,0)]);%% COMENTAR
                tic %aqui se va el tiempo OJO OJO
                jacobiano = amevaConfidInterQuanAggregate(1-1./Ts,0,Ny,OPHobjS,OPHrS.pS);
                stdQuan = amevaAgregateQuantiles.stdQA(jacobiano,OPHrS.invI0);
                toc
                stdQuan = stdQuan*norminv(1 - confidenceLevelAlpha/2,0,1);
                stdup = quanaggr + stdQuan;
                stdlo = quanaggr - stdQuan;
                %save(fullfile(pwd,'CauntilAgregado.mat'),'quanaggr','stdup','stdlo');%OJO OJO comentar
            end
        end
        %% Evolution of the yearly aggregate quantiles during the fitting time
        function quanaggr=aqEvolution(Ts,OPHobj,pS)
            disp(['Total year, ' num2str(OPHobj.Ny)]);
            OPHobj.Km=OPHobj.Km; %/Ny frecuencia temporal paso_de_tiempo/N_total_años
            disp(['[aqEvolution] Evolution of the yearly aggregate quantiles / -> ' datestr(now,0)]);%% COMENTAR
            tic
            quanaggr = amevaQuantileAgregado((1-1./Ts),(1:OPHobj.Ny)-1,(1:OPHobj.Ny),OPHobj,pS);
            toc
            %%NO se calculan los intervalos de confianza de Evolution A.Q.%%%%%%%%%%%
        end
        %% Cuantil agregado Mensuales para periodo de retorno Ts -> January-December : Ts
        function [quanaggr,stdDqX]=aqMonth(Ts,OPHobj,OPHr,confidenceLevelAlpha,myMeses)
            % OPHobj.Km=12;%monthy
            %Mese para calcular el periodo de retorno (Todos) OJOJO Km=12 OJO OJO
            if ~exist('myMeses','var') || isempty(myMeses), myMeses=(1:12); end;
            tic
            quanaggr=amevaQuantileAgregado((1-1./Ts),(myMeses-1)/12,myMeses/12,OPHobj,OPHr.pS);
            toc
            disp(['[aqMonth] Cuantil agregado Mensuales / -> ' datestr(now,0)]);%% COMENTAR
            if nargout==2,
                %%%%%%%%%%%Intervalos de confianza del cuantil Mensual%%%%%%%%%%%
                tic %aqui se va el tiempo OJO OJO
                jacobiano = amevaConfidInterQuanAggregate(1-1./Ts,(myMeses-1)/12,myMeses/12,OPHobj,OPHr.pS);
                stdQuan = amevaAgregateQuantiles.stdQA(jacobiano,OPHr.invI0);
                stdDqX=stdQuan*norminv(1-confidenceLevelAlpha/2,0,1);
                toc
            end
        end
        %% std standart desviation
        function stdQA=stdQA(jacobiano,invI0)
            %   Standard deviation
            n=size(jacobiano,2);
            m=size(jacobiano,3);
            stdQA = zeros(n,m);%quad
            for i = 1:m,
                for j = 1:n
                    stdQA(j,i) = sqrt(jacobiano(:,j,i)'*invI0*jacobiano(:,j,i));
                end
            end
            %disp([num2str(i) ' iter / -> ' datestr(now,0)]);%% COMENTAR
        end
    end
end