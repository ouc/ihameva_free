function [Ts,quanaggr,stdDqX]=AgregateQuantiles(Hsmax,datemax,ConfidenceLevelAlpha,kt,indices,OPHr,cityname,var_name,var_unit,gcax,anualplot,pathresult,forzarAQ)
% GEV Quantiles agregdos MENSUALES... tambien se pueden mostrar los anuales. IMPORTANTE: los datos son mensuales

if ~exist('kt','var') || isempty(kt), kt = ones(size(Hsmax)); end
if ConfidenceLevelAlpha>0.5, error(['ConfidenceLevelAlpha=',num2str(ConfidenceLevelAlpha)]); end
if ~exist('forzarAQ','var'), forzarAQ=false; end %Forzar calcular el GEV Agregate Quantile true para forzar
if ~exist('cityname','var'), cityname=''; end
if ~exist('var_name','var'), var_name=''; end
if ~exist('var_unit','var'), var_unit=''; end
if ~exist('anualplot','var'), anualplot=true; end
if ~exist('pathresult','var') || ~ischar(pathresult), pathresult=pwd; end
if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
if ~ishandle(gcax), disp('WARNING! AgregateQuantiles with axes'); end
set(gcf,'CurrentAxes',gcax)
[beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,betaT3,varphi,varphi2,varphi3,lista,lista2,lista3,p,np]=amevaOPH.gevparamsol2array(OPHr);

% col2=[0   ,0   ,1;0   ,0.25,0.75;0   ,0.50,0.50;0   ,0.75,0.25;0.25,1   ,0;0.50,0.75,0;
%       0.75,0.50,0;1   ,0.75,0;1   ,1   ,0.25;1   ,0.75,0.25;1   ,0.50,0.50;0   ,1   ,1;1   ,0   ,0];
mapa_colores=escaladocircular(1);%colores meses
%   Aggregate quantiles for different return periods
%   ATENCION no se debe utilizar valores menores  que 2... las aprox. deja de ser validas. Maximos mensuales
Ts = [2:1:9 10:10:90 100:100:500];
%Ts = [2 5 10 20 25 50 75 100 200 300 400 500];

nts = length(Ts);
quanaggrA=nan(1,nts);
quanaggr=nan(12,nts);
stdDqX=nan(12,nts);
nmpc=[num2str(length(beta)) num2str(length(alpha)) num2str(length(gamma))];
if exist(fullfile(pathresult,['GEVQ_' nmpc '_' cityname '.mat']),'file')==0 || forzarAQ,
    if sum(str2num(nmpc(:)))>=2,
        disp(['Calculando GEV Agregate Quantiles.... En funci',char(243),'n de su ordenador esto puede tardar unos min. Espere[' datestr(now), ...
            ']. En un PC Intel Core2 Quad CPU aprox. ',num2str(sum(str2num(nmpc(:)))),' min.)']);
        %   Sarting calculations Mensuales
        Km=12;
        for i = 1:12,
            for j = 1:nts, %    January-December
                %quanaggr(i,j)=QuantileAgregado (1-1/Ts(j),0,1/12,Km,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2);
                quanaggr(i,j)=QuantileAgregado(1-1/Ts(j),(i-1)/12,i/12,Km,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices(:,lista),indices(:,lista2),datemax);
                if nargout==3,
                    stdQuan = ConfidInterQuanAggregate(1-1/Ts(j),(i-1)/12,i/12,Km,Hsmax,datemax,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices(:,lista),indices(:,lista2));
                    stdDqX(i,j)=stdQuan*norminv(1-ConfidenceLevelAlpha/2,0,1);
                end
            end
        end
    end
    stdup=[];stdlo=[];
    if anualplot, %si se quieren se muestran los datos anuales. Por defecto si
        Km=12; %% OJO OJO por que tiene que ir 12 OJO OJO
        % Annual
        for j = 1:nts, quanaggrA(j)=QuantileAgregado(1-1/Ts(j),0,1,Km,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices(:,lista),indices(:,lista2),datemax); end
        %   Intervalos de confianza del cuantil anual
        stdup = zeros(nts,1);
        stdlo = zeros(nts,1);
        for i=1:nts,
            %         stdQuan = ConfidInterQuanAggregate (1-1/Ts(i),0,1,Hsmax,datemax,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices(:,lista),indices(:,lista2));
            stdQuan = ConfidInterQuanAggregate(1-1/Ts(i),0,1,Km,Hsmax,datemax,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices(:,lista),indices(:,lista2));
            stdup(i)= quanaggrA(i)+stdQuan*norminv(1-ConfidenceLevelAlpha/2,0,1);
            stdlo(i)= quanaggrA(i)-stdQuan*norminv(1-ConfidenceLevelAlpha/2,0,1);
        end
    end
    save([pathresult 'GEVQ_' nmpc '_' cityname '.mat'],'quanaggr','stdDqX','quanaggrA','stdup','stdlo');
else
    load([pathresult 'GEVQ_' nmpc '_' cityname '.mat']);
end
%   Cuantiles agregados en mas de 30 anos
%     quanaggr222=QuantileAgregadoII (1-1/100,0,30,kt,beta0,beta,alpha0,alpha,gamma0,gamma,[],betaT,betaT2,varphi,varphi2,indices(:,lista),indices(:,lista2),datemax);
%   Plot of the aggregate quantiles mensuales
if sum(str2num(nmpc(:)))>=2,
    datemax_mod=mod(datemax,1);
    for i = 1:12,
        ls='--';if mod(i,2)==1, ls='-'; end
        semilogx(gca,Ts,quanaggr(i,:),'Color',mapa_colores(i,:),'LineStyle',ls, 'LineWidth',1.2);hold(gca,'on');
    end
end
if anualplot, %si se quieren se muestran los datos anuales. Por defecto si
    ny=ceil(datemax(end));
    hmax_=zeros(1,ny);
    for j=1:ny,
        if not(isempty(max(Hsmax(find(datemax>=j-1 & datemax<j)))))
            hmax_(j)=max(Hsmax(find(datemax>=j-1 & datemax<j)));
        end
    end
    hmaxor_ = sort(hmax_);
    ProHsmaxor = (((1:length(hmaxor_)))/(length(hmaxor_)+1))';
    Tapprox = 1./(1-ProHsmaxor);
    id = find(Tapprox>=2);
    semilogx(gca,Tapprox(id),hmaxor_(id),'ok','MarkerSize',1.6);
    semilogx(gca,Ts,quanaggrA,'Color',[0 0 0]); hold on;
    semilogx(gca,Ts,stdlo,'--k','Linewidth',1.1);
    semilogx(gca,Ts,stdup,'--k','Linewidth',1.1);
end
if ~isempty(var_name) || ~isempty(var_unit)
    title(['Aggregate Quantiles (' cityname ')'])
    xlabel(gca,'Return Period (years)');
    ylabel(gca,labelsameva(var_name,var_unit));
    if sum(str2num(nmpc(:)))>=2,
        if anualplot,%si se quieren se muestran los datos anuales. Por defecto si
            legend('January','February','March','April','May','June','July','August','September','October','November','December','Annual data','Annual',[num2str((1-ConfidenceLevelAlpha),2) ' Bounds'],'Location','EastOutside')
        else
            legend('January','February','March','April','May','June','July','August','September','October','November','December','Location','EastOutside')
        end
    end
end
if sum(str2num(nmpc(:)))>=2,
    for i = 1:12,
        hmaxor_ = sort(Hsmax(find(datemax_mod>(i-1)/12 & datemax_mod<=i/12)));
        ProHsmaxor = (((1:length(hmaxor_)))/(length(hmaxor_)+1))';
        Tapprox = 1./(1-ProHsmaxor);
        id = find(Tapprox>=2);% OJO poner 1 si se quiere ver los valores chicos 1 o 2
        semilogx(gca,Tapprox(id),hmaxor_(id),'o','Color',mapa_colores(i,:),'MarkerSize',1.6);
    end
end
xticks=[1 2 5 10 20 50 100 250 500];grid on;grid(gca,'minor')
set(gca,'xlim',[1 Ts(end)],'XTick',xticks,'ylim',[0 max(get(gca,'ytick'))],'XMinorTick','off','XMinorGrid','off','YMinorTick','on','YMinorGrid','on');
hold(gca,'off')
end
