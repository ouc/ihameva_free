classdef amevaOPH < handle
    properties(GetAccess = 'public', SetAccess = 'public')
    end
    properties (Constant)
        GumbelTol = 1e-8;% The remaining values correspond to WEIBULL or FRECHET
        ophBoundsShape = 1e-8;   % OtiParamHessian -Check if any of the bounds related to shape parameters become active,
        ophBoundsShapeInc = 0.05;% OtiParamHessian - increase 0.05
        ophIterMax = 10;         % OtiParamHessian - max iter 10
        loglikeSizeZval = 1e-8;% amevaloglikelihood Since the z-values must be greater than zero in order to avoid numerical problems their values are set to be greater than 1e-4
    end
    methods (Static = true)
        % Estructura para guardar los datos de entrada y el resultado final
        function OPHobj = OPHstruct(x,t,kt,indices,criterio,confidenceLevel,pv,beta0,beta,betaT,varphi,lista, ...
                                                        alpha0,alpha,betaT2,varphi2,lista2, ...
                                                        gamma0,gamma,betaT3,varphi3,lista3)
            OPHobj.x=x;     OPHobj.t=t;          ktsz=size(x);
            OPHobj.kt      =existevar('kt',ktsz);
            OPHobj.indices =existevar('indices');
%             OPHobj.indicesF=existevar('indicesF');
            OPHobj.criterio=existevar('criterio');
            OPHobj.confidenceLevel =existevar('confidenceLevel');
            OPHobj.pv      =existevar('pv');
            OPHobj.beta0   =existevar('beta0'); if ~isempty(OPHobj.beta0) && max(size(OPHobj.beta0))~=1, error('beta0 must be a double!');  end
            OPHobj.beta    =existevar('beta');
            OPHobj.betaT   =existevar('betaT');
            OPHobj.varphi  =existevar('varphi');
            OPHobj.lista   =existevar('lista');
            OPHobj.alpha0  =existevar('alpha0');if ~isempty(OPHobj.alpha0) && max(size(OPHobj.alpha0))~=1, error('alpha0 must be a double!');  end
            OPHobj.alpha   =existevar('alpha');
            OPHobj.betaT2  =existevar('betaT2');
            OPHobj.varphi2 =existevar('varphi2');
            OPHobj.lista2  =existevar('lista2');
            OPHobj.gamma0  =existevar('gamma0');if ~isempty(OPHobj.gamma0) && max(size(OPHobj.gamma0))~=1, error('gamma0 must be a double!');  end
            OPHobj.gamma   =existevar('gamma');
            OPHobj.betaT3  =existevar('betaT3');
            OPHobj.varphi3 =existevar('varphi3');
            OPHobj.lista3  =existevar('lista3');
            function obj = existevar(vn,ktsz)
                if exist(vn,'var') && not(isempty(vn)),
                    obj = eval(vn);
                else
                    if strcmp(vn,'kt')
                        obj = ones(ktsz);
                    elseif strcmp(vn,'nmu') || strcmp(vn,'npsi') || strcmp(vn,'neps'),
                        obj = 0;
                    else
                        obj = [];
                    end
                end
            end
        end
        % Estructura para guardar RESULTADO cada/iteraccion + loglike,gradiente,hess,invI0,pS,ps,pv,neps0,np
        function  [OPHr] = oph_r(pv,neps0,nmu,ntend,nind,npsi,ntend2,nind2,neps,ntend3,nind3,lista,lista2,lista3,loglike,gradiente,Hessiano,invI0)
            OPHr = struct('beta0',[],'beta',[],'betaT',[],'varphi',[],'alpha0',[],'alpha',[],'betaT2',[],'varphi2',[], ...
                        'gamma0',[],'gamma',[],'betaT3',[],'varphi3',[],'lista',[],'lista2',[],'lista3',[], ...
                        'loglike',[],'Hessiano',[],'invI0',[],'pv',[],'pS',[],'ps',[],'neps0',[],'np',0);
            if nargin==0, return; end %inizializo vacio
            [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,betaT3,varphi3] = ...
                p2parametros(pv,neps0,nmu,ntend,nind,npsi,ntend2,nind2,neps,ntend3,nind3);
            [pS,ps] = amevaOPH.objparam2pSps(beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,betaT3,varphi3);
            np = length(pv);
            OPHr.beta0 = beta0;
            OPHr.beta = beta;
            OPHr.betaT = betaT;
            OPHr.varphi = varphi;
            OPHr.alpha0 = alpha0;
            OPHr.alpha = alpha;
            OPHr.betaT2 = betaT2;
            OPHr.varphi2 = varphi2;
            OPHr.gamma0 = gamma0;
            OPHr.gamma = gamma;
            OPHr.betaT3 = betaT3;
            OPHr.varphi3 = varphi3;
            OPHr.lista  = lista;
            OPHr.lista2 = lista2;
            OPHr.lista3 = lista3;
            OPHr.loglike = loglike;
            OPHr.gradiente = gradiente;
            OPHr.Hessiano = Hessiano;
            OPHr.invI0 = invI0;
            OPHr.pS = pS;
            OPHr.ps = ps;
            OPHr.pv = pv;
            OPHr.neps0 = neps0;
            OPHr.np = np;
            %
            function [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,betaT3,varphi3] = ...
                    p2parametros(p,neps0,nmu,ntend,nind,npsi,ntend2,nind2,neps,ntend3,nind3)
                vi = 1;
                beta0  =  p(vi);
                beta = existevar(nmu);
                betaT = existevar(ntend);
                varphi = existevar(nind);
                alpha0  =  existevar(1);
                alpha = existevar(npsi);
                betaT2 = existevar(ntend2);
                varphi2 = existevar(nind2);
                if neps0, gamma0 = existevar(1); else gamma0 = []; end
                if gamma0 == 0, gamma0 = []; end %OJO OJO OJO oc 12/2015
                gamma = existevar(neps);
                if exist('ntend3','var'), betaT3 = existevar(ntend3); else betaT3 = []; end
                if exist('nind3','var'), varphi3 = existevar(nind3); else varphi3 = []; end
                function obj = existevar(nv)
                    if nv>0,
                        obj = p(vi+1:vi+nv);
                        vi = vi+nv;
                    else
                        obj = [];
                    end
                end
            end
            
        end
        %
        function  [pS,ps] = objparam2pSps(OPHobjOPHr,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,betaT3,varphi3)
            ps = struct('beta0',[],'beta',[],'alpha0',[],'alpha',[],'gamma0',[],'gamma',[], ...
                          'betaT',[],'varphi',[],'betaT2',[],'varphi2',[],'betaT3',[],'varphi3',[]);
            if nargin>1,
                pS.mu.alpha0 = OPHobjOPHr;ps.beta0 = OPHobjOPHr; % alpha0
                pS.mu.alpha = beta;       ps.beta = beta;
                pS.psi.alpha0 = alpha0;   ps.alpha0 = alpha0;
                pS.psi.alpha = alpha;     ps.alpha = alpha;
                pS.chi.alpha0 = gamma0;   ps.gamma0 = gamma0;
                pS.chi.alpha = gamma;     ps.gamma = gamma;
                pS.mu.betaT = betaT;      ps.betaT = betaT;
                pS.mu.varphi = varphi;    ps.varphi = varphi;
                pS.psi.betaT = betaT2;    ps.betaT2 = betaT2;
                pS.psi.varphi = varphi2;  ps.varphi2 = varphi2;
                pS.chi.betaT = betaT3;    ps.betaT3 = betaT3;
                pS.chi.varphi = varphi3;  ps.varphi3 = varphi3;
            else
                pS.mu.alpha0  =OPHobjOPHr.beta0;     ps.beta0=OPHobjOPHr.beta0;
                pS.mu.alpha   =OPHobjOPHr.beta;      ps.beta=OPHobjOPHr.beta;
                pS.psi.alpha0 =OPHobjOPHr.alpha0;    ps.alpha0=OPHobjOPHr.alpha0;
                pS.psi.alpha  =OPHobjOPHr.alpha;     ps.alpha=OPHobjOPHr.alpha;
                pS.chi.alpha0 =OPHobjOPHr.gamma0;    ps.gamma0=OPHobjOPHr.gamma0;
                pS.chi.alpha  =OPHobjOPHr.gamma;     ps.gamma=OPHobjOPHr.gamma;
                pS.mu.betaT   =OPHobjOPHr.betaT;     ps.betaT=OPHobjOPHr.betaT;
                pS.mu.varphi  =OPHobjOPHr.varphi;    ps.varphi=OPHobjOPHr.varphi;
                pS.psi.betaT  =OPHobjOPHr.betaT2;    ps.betaT2=OPHobjOPHr.betaT2;
                pS.psi.varphi =OPHobjOPHr.varphi2;   ps.varphi2=OPHobjOPHr.varphi2;
                if isfield(OPHobjOPHr,'betaT3'),  pS.chi.betaT  =OPHobjOPHr.betaT3; ps.betaT3=OPHobjOPHr.betaT3;  end
                if isfield(OPHobjOPHr,'varphi3'), pS.chi.varphi =OPHobjOPHr.varphi3; ps.varphi3=OPHobjOPHr.varphi3; end
            end
        end
        %
        function  [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,betaT3,varphi,varphi2,varphi3,lista,lista2,lista3,p,np] = ...
                gevparamsol2array(OPHr)
            beta0 = existevar('beta0');
            beta = existevar('beta');
            alpha0 = existevar('alpha0');
            alpha = existevar('alpha');
            gamma0 = existevar('gamma0');
            gamma = existevar('gamma');
            betaT = existevar('betaT');
            betaT2 = existevar('betaT2');
            betaT3 = existevar('betaT3');
            varphi = existevar('varphi');
            varphi2 = existevar('varphi2');
            varphi3 = existevar('varphi3');
            lista = existevar('lista');
            lista2 = existevar('lista2');
            lista3 = existevar('lista3');
            p = existevar('p');
            np = length(p);
            function obj = existevar(vn)
                if isfield(OPHr,vn),
                    obj = OPHr.(vn);
                else
                    obj = [];
                end
            end
        end
        %
        function OPHobj = OPHrToOPHobj(OPHobj,OPHr)
            % pasar solo los parametros optimos
            popt_nm = {'beta0','beta','betaT','varphi','lista','alpha0','alpha','betaT2','varphi2','lista2','gamma0','gamma','betaT3','varphi3','lista3','pv'};
            for i = 1:length(popt_nm)
                if isfield(OPHr,popt_nm{i})
                    OPHobj.(popt_nm{i}) = OPHr.(popt_nm{i});
                end
            end
        end
        % structura ps to vector pv
        function pv = pstovector(ps)
            pv = [];
            existevar(ps,'beta0');
            existevar(ps,'beta');
            existevar(ps,'betaT');
            existevar(ps,'varphi');
            existevar(ps,'alpha0');
            existevar(ps,'alpha');
            existevar(ps,'betaT2');
            existevar(ps,'varphi2');
            existevar(ps,'gamma0');
            existevar(ps,'gamma');
            existevar(ps,'betaT3');
            existevar(ps,'varphi3');
            function existevar(s,vn)
                if isfield(s,vn) && ~isempty(s.(vn)),
                    auxi = s.(vn);
                    pv = [pv;auxi(:)];
                end
            end
        end
        %% Frecuencia temporal de los datos
        function [Km,maxtype]=FrecTemporal(datemax,Time,timestate)
            %   Km -> to calcate return period  Km=1 -> annual, Km=12 -> monthly
            Km=[];
            if timestate==true,
                Km=round(365.25/mean(diff(Time))*100)/100;
            elseif timestate==false,
                Km=round(1/mean(diff(datemax)));
            end
            maxtype='Unknow';
            if abs(Km-365.25) < 2/10, 
                Km=365.25; maxtype='daily'; %Los datos son diarios
            elseif abs(Km-365.25/6) < 2/10,
                Km=365.25/6; maxtype='6 days'; %Los datos son 6 diarios
            elseif abs(Km-365.25/7) < 2/10,
                Km=365.25/7; maxtype='7 days'; %Los datos son 7 diarios
            elseif abs(Km-12) < 10/100,
                Km=12; maxtype='monthly'; %Los datos son mensuales
            elseif abs(Km-1) < 5/100, 
                Km=1; maxtype='annual'; %Los datos son anuales
            end
            if strcmp(maxtype,'Unknow')
                disp(['Los datos estan muestreado a: 1/' num2str(Km) ' - El tipo de frecuencia temporal es: [' maxtype '] - La cantidad de datos no es representativa']);
                disp('monthly max. 10/100 de huecos, annual max. 5/100 de huecos');
            else
                disp(['Los datos estan muestreado a: 1/' num2str(Km) ' per [' maxtype ']']);
            end
            % if strcmp(maxtype,'monthly')
            %     Km=12;
            % elseif strcmp(maxtype,'annual')
            %     Km=1;
            % elseif strcmp(maxtype,'daily')
            %     Km=365.25;
            % elseif strcmp(maxtype,'dailys')
            %     Km=365.25/6;
            % end
        end
        %%
        function [posG,pos] = posGumbel(epst)
            %   The values whose shape parameter is almost cero corresponds to
            %   the GUMBEL distribution, locate their positions if they exist
            %   The remaining values correspond to WEIBULL or FRECHET
            %   The corresponding GUMBEl values are set to 1 to avoid
            %   numerical problems, note that for those cases the GUMBEL
            %   expressions are used
            posG = (abs(epst)<=amevaOPH.GumbelTol);
            pos  = (abs(epst)> amevaOPH.GumbelTol);
        end
        %% OptiParamHessian
        function OPHr = OptiParamHessian (OPHobj)
            global neps0;% nesp0 tiene que estar definido aqui ya que se modifica en amevaloglikelihood
            nmu = length(OPHobj.beta);%   Related to the location
            npsi = length(OPHobj.alpha);%   Related to the scale
            neps = length(OPHobj.gamma);%   Related to the shape
            nind = length(OPHobj.lista);
            nind2 = length(OPHobj.lista2);
            nind3 = length(OPHobj.lista3);
            ntend = length(OPHobj.betaT);%   Related to the tendency for the location
            ntend2 = length(OPHobj.betaT2);%   Related to the tendency for the scale
            ntend3 = length(OPHobj.betaT3);%   Related to the tendency for the shape
            %   The number of covariates related to location and scale is storaged in
            %   nind and nind2, respectively
            optOPH.g_p_ini = 0.01;
            optOPH.g_lu_ini = 0.2;
            optOPH.g_nlu_ini = 0.15;
            optOPH.meanx = mean(OPHobj.x);
            optOPH.logstdx = log(std(OPHobj.x));
            [p,lb,up] = amevaOPH.oph_p_b(OPHobj.pv,optOPH,neps0,nmu,ntend,nind,npsi,ntend2,nind2,neps,ntend3,nind3);
            
            %   Set the options for the optimization routine, note that both gradients and Hessian are provided
            options = optimset('GradObj','on','Hessian','on','UseParallel','always','Algorithm','trust-region-reflective', ...
                'Display','final-detailed','FunValCheck','on','MaxIter',5000,'MaxFunEvals',5000, ...
                'TolFun',1e-15,'TolX',1e-15);%'Algorithm','trust-region-reflective',
            if sum(OPHobj.kt)~=length(OPHobj.kt) || nind3>0 || ntend3>0, % Si el vector de kt es distinto de ones Hessian off
                options.Hessian = 'Off';
            end
            
            %   Call the optimization routine
            %   Note that it is a minimization problem, instead a maximization problem,
            %   for this reason the log likelihood function sign is changed
            [p,loglike,exitflag,output,lambda,gradiente,Hessiano] = fmincon(@(p) amevaloglikelihood(p,OPHobj),p,[],[],[],[],lb,up,[],options);
            %   Check if any of the bounds related to shape parameters become active,
            %   if active increase or decrease the bound and call the optimization routine again
            auxlo = find(abs(lambda.lower)>amevaOPH.ophBoundsShape);
            lb(auxlo) = lb(auxlo)-amevaOPH.ophBoundsShapeInc;
            auxup = find(abs(lambda.upper)>amevaOPH.ophBoundsShape);
            up(auxup) = up(auxup)+amevaOPH.ophBoundsShapeInc;
            it = 1;
            while (~isempty(auxlo) || ~isempty(auxup)) && it<=amevaOPH.ophIterMax,
                it = it+1;
                [p,loglike,exitflag,output,lambda,gradiente,Hessiano] = fmincon(@(p) amevaloglikelihood(p,OPHobj),p,[],[],[],[],lb,up,[],options);
                auxlo = find(abs(lambda.lower)>amevaOPH.ophBoundsShape);
                lb(auxlo) = lb(auxlo)-amevaOPH.ophBoundsShapeInc;
                auxup = find(abs(lambda.upper)>amevaOPH.ophBoundsShape);
                up(auxup) = up(auxup)+amevaOPH.ophBoundsShapeInc;
            end
            
            %   Once the optimal solution is obtained redistribute the solution storaged in p in the following order:
            %   beta0, beta, betaT, varphi,alpha0, alpha, betaT2, varphi2,gamma0, gamma, betaT3, varphi3
            %   el valor de neps0 se asigan al resultado OPHr
            lista = OPHobj.lista; lista2 = OPHobj.lista2; lista3 = OPHobj.lista3; % OJO OJO loglike,gradiente,Hessiano no son de la solucion final
            [OPHr] = amevaOPH.oph_r(p,neps0,nmu,ntend,nind,npsi,ntend2,nind2,neps,ntend3,nind3,lista,lista2,lista3,loglike,gradiente,Hessiano,[]);
        end
        % Inverse matrix
        function invI0 = invI0(Hessiano,np)
            %   Standard deviation for the parameters
            %   LU decomposition of the Information matrix
            if ~exist('np','var') || isempty(np), np=length(Hessiano); end
            [LX,UX,PX] = lu(Hessiano);
            invI0 = (UX)\(LX\(PX*eye(np))); % To OPHr
        end
        function [p,lb,up] = oph_p_b(pv,opciones,neps0,nmu,ntend,nind,npsi,ntend2,nind2,neps,ntend3,nind3)
            %   g_p_ini = 0.01;g_lu_ini = 0.2;g_nlu_ini = 0.15;
            %   Initial bounds and possible initial values for the constant parameters related to location, scala and shape
            N = 2+neps0+nmu+npsi+neps+ntend+nind+ntend2+nind2+ntend3+nind3;
            p = zeros(N,1); lb = -Inf*ones(N,1); up = Inf*ones(N,1);%   Bounds on variables. Initially all parameters are unbounded
            p(1) = opciones.meanx;                  %   Initial beta0 parameter
            p(2+nmu+ntend+nind) = opciones.logstdx; %   Initial alpha0 parameter
            if neps0,                               %   Initial gamma0 parameter
                p(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2) = opciones.g_p_ini;
            end
            if neps>0,
                p(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+1:2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps) = opciones.g_p_ini;
            end
            if ntend3>0, %%%% new
                p(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps+1) = opciones.g_p_ini;
            end
            if nind3>0, %%%% OJO JO JOJO comprobar esto
                p(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps+ntend3:2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps+ntend3+nind3) = opciones.g_p_ini;
            end
            %   Initial bounds for the parameters related to the shape, gamma0 and gamma
            if neps0,
                lb(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2) = -opciones.g_lu_ini;
            end
            if neps>0,
                lb(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+1:2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps) = -opciones.g_nlu_ini;
            end
            if ntend3>0, %%%% new
                lb(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps+1) = -opciones.g_nlu_ini;
            end
            if nind3>0, %%%% OJO JO JOJO comprobar esto
                lb(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps+ntend3:2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps+ntend3+nind3) = -opciones.g_nlu_ini;
            end
            if neps0,
                up(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2) = opciones.g_lu_ini;
            end
            if neps>0,
                up(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+1:2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps) = opciones.g_nlu_ini;
            end
            if ntend3>0, %%%% new
                up(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps+1) = opciones.g_nlu_ini;
            end
            if nind3>0, %%%% OJO JO JOJO comprobar esto
                up(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps+ntend3:2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps+ntend3+nind3) = opciones.g_nlu_ini;
            end
            %   If an inital value for the parameters vector is provided, it is used
            if ~isempty(pv),
                if length(p)~=length(pv), % OJO OJO OJO he comentado esto OJO OJO OJO OJO
%                     error(['length p and pv are diferent! Lng(p)=',num2str(length(p)),' Lng(p)=',num2str(length(pv))]); 
                end
                for ii=1:length(p)
                    p(ii) = min(pv(ii),up(ii));
                    p(ii) = max(pv(ii),lb(ii));
                end
            end
        end
        function [solucion,rowLabels,columnLabels] = texsolutionpot(IT,OPHr,nit,neps0)
            %   ESCRITURA DEL MODELO DEFINITIVO
            nla = length(OPHr.beta)/2;
            nsig = length(OPHr.alpha)/2;
            neps = length(OPHr.gamma)/2;
            nind = length(OPHr.varphi);
            nind2 = length(OPHr.varphi2);
            ntend = length(OPHr.betaT);
            ntend2 = length(OPHr.betaT2);
            
            solucion = zeros(2+neps0+2*nla+ntend+nind+ntend2+nind2+2*nsig+2*neps+3,nit);
                        
            solucion(1,:) = IT.beta0IT(1:nit);
            rowLabels{1} = '$\beta_0$';
            if nla>0,
                solucion(2:1+2*nla,:) = IT.betaIT(1:2*nla,1:nit);
                for i = 1:nla,
                    rowLabels{1+2*i-1} = ['$\beta_{' num2str(2*i-1) '}$'];
                    rowLabels{1+2*i} = ['$\beta_{' num2str(2*i) '}$'];
                end
            end
            solucion(2+2*nla,:) = IT.alpha0IT(1:nit)';
            rowLabels{2+2*nla} = '$\alpha_0$';
            if nsig>0,
                solucion(3+2*nla:2+2*nla+2*nsig,:) = IT.alphaIT(1:2*nsig,1:nit);
                for i = 1:nsig,
                    rowLabels{2+2*nla+2*i-1} = ['$\alpha_{' num2str(2*i-1) '}$'];
                    rowLabels{2+2*nla+2*i} = ['$\alpha_{' num2str(2*i) '}$'];
                end
            end
            if neps0==1,
                solucion(2+neps0+2*nla+2*nsig,:) = IT.gamma0IT(1:nit)';
                rowLabels{2+neps0+2*nla+2*nsig} = '$\gamma_0$';
            end
            if neps>0,
                solucion(3+neps0+2*nla+2*nsig:2+neps0+2*nla+2*nsig+2*neps,:) = IT.gammaIT(1:2*neps,1:nit);
                for i = 1:neps,
                    rowLabels{2+neps0+2*nla+2*nsig+2*i-1} = ['$\gamma_{' num2str(2*i-1) '}$'];
                    rowLabels{2+neps0+2*nla+2*nsig+2*i} = ['$\gamma_{' num2str(2*i) '}$'];
                end
            end
            if nind>0,
                solucion(3+neps0+2*nla+2*nsig+2*neps:2+neps0+2*nla+2*nsig+2*neps+nind,:) = IT.varphiIT(1:nind,1:nit);
                for i = 1:nind,
                    rowLabels{2+neps0+2*nla+2*nsig+2*neps+i} = ['$\varphi_{' num2str(i) '}$'];
                end
            end
            if nind2>0,
                solucion(3+neps0+2*nla+2*nsig+2*neps+nind:2+neps0+2*nla+2*nsig+2*neps+nind+nind2,:) = IT.varphi2IT(1:nind2,1:nit);
                for i = 1:nind2,
                    rowLabels{2+neps0+2*nla+2*nsig+2*neps+nind+i} = ['$\varphi^2_{' num2str(i) '}$'];
                end
            end
            if ntend>0,
                solucion(3+neps0+2*nla+2*nsig+2*neps+nind+nind2,:) = IT.betaTIT(1:nit)';
                rowLabels{3+neps0+2*nla+2*nsig+2*neps+nind+nind2} = '$\beta_T$';
            end
            if ntend2>0,
                solucion(3+neps0+2*nla+2*nsig+2*neps+ntend+nind+nind2,:) = IT.betaT2IT(1:nit)';
                rowLabels{3+neps0+2*nla+2*nsig+2*neps+ntend+nind+nind2} = '$\beta_T^2$';
            end
            
            solucion(3+neps0+2*nla+2*nsig+2*neps+ntend+ntend2+nind+nind2,:) = IT.npIT(1:nit)';
            rowLabels{3+neps0+2*nla+2*nsig+2*neps+ntend+ntend2+nind+nind2} = '$n_p$';
            solucion(4+neps0+2*nla+2*nsig+2*neps+ntend+ntend2+nind+nind2,:) = IT.loglikIT(1:nit)';
            rowLabels{4+neps0+2*nla+2*nsig+2*neps+ntend+ntend2+nind+nind2} = 'log';
            solucion(5+neps0+2*nla+2*nsig+2*neps+ntend+ntend2+nind+nind2,:) = IT.AIKIT(1:nit)';
            rowLabels{5+neps0+2*nla+2*nsig+2*neps+ntend+ntend2+nind+nind2} = 'AIC';
            
            columnLabels = {};
            for j = 1:nit,
                columnLabels{j} = [' ' num2str(j) ' '];
            end
        end
        function [quan95,stdmut,stdpsit,stdepst,stdDq,mut,psit,epst,datemaxanual] = gevpreplot(OPHobj,OPHr,neps0,confidenceLevel)
            if confidenceLevel<0.5, error(['confidenceLevel=',num2str(confidenceLevel)]); end
            %   Confidence intervals
            [Dermut,Derpsit,Derepst] = amevaOPH.Dmupsiepst(OPHr.pS,OPHobj);
            
            nmu = length(OPHr.beta);
            npsi = length(OPHr.alpha);
            neps = length(OPHr.gamma);
            ntend = length(OPHr.betaT);
            ntend2 = length(OPHr.betaT2);
            nind  = length(OPHr.varphi);
            nind2  = length(OPHr.varphi2);
            
            stdmut = sqrt(sum((Dermut'*OPHr.invI0(1:(1+nmu+ntend+nind),1:(1+nmu+ntend+nind))).*Dermut',2));
            stdpsit = sqrt(sum((Derpsit'*OPHr.invI0((2+nmu+ntend+nind):(2+nmu+ntend+nind+npsi+ntend2+nind2),(2+nmu+ntend+nind):(2+nmu+ntend+nind+npsi+ntend2+nind2))).*Derpsit',2));
            
            if neps0 || not(isempty(OPHr.gamma)),
                stdepst = sqrt(sum((Derepst'*OPHr.invI0((3+nmu+ntend+nind+npsi+ntend2+nind2):end,(3+nmu+ntend+nind+npsi+ntend2+nind2):end)).*Derepst',2));
            else
                stdepst = [];
            end
            
            % t = linspace(0,1,100)';
            OPHobj.t = mod(OPHobj.t,1); % nuevo tiempo
            datemaxanual = OPHobj.t;
            
            [Dq] = amevaOPH.DQuantile (confidenceLevel,OPHobj,OPHr);
            stdDq = sqrt(sum((Dq'*OPHr.invI0).*Dq',2));
            
            [quan95] = amevaOPH.QuantileGEV(0.95,OPHr.pS,OPHobj);
            %   Location, Scale, Shape
            [mut,psit,epst] = amevaOPH.parameters_t(OPHr.pS,OPHobj);
        end
        function [Dq] = DQuantile (F,OPHobj,OPHr)
            %   Function DQuantile calculates the quantile q  derivative associated with a given
            %   parameterization with respect model parameters
            neps0 = 0;
            if OPHr.pS.chi.alpha0 ~= 0, neps0 = 1; end
            
            t = OPHobj.t;
            kt = OPHobj.kt;
            indices = OPHobj.indices;
%             indicesF = OPHobj.indicesF;
            nd = length(t);
            nmu = length(OPHr.beta);
            npsi = length(OPHr.alpha);
            neps = length(OPHr.gamma);
            lista = OPHr.lista;
            lista2 = OPHr.lista2;
            lista3 = OPHr.lista3;
            nind = length(lista);
            nind2 = length(lista2);
            nind3 = length(lista3);
            ntend = length(OPHr.betaT);
            ntend2 = length(OPHr.betaT2);
            ntend3 = length(OPHr.betaT3);
            if ~exist('kt','var') || isempty(kt),  kt = ones(size(t)); end
            if ~exist('indices','var'), indices = []; end
            %
            [mut,psit,epst,posG,pos,mut1,psit1] = amevaOPH.parameters_t(OPHr.pS,OPHobj);
            
            %   Derivatives of the quantile function with respect to location,
            %   scale and shape parameters
            Dqmut(pos) = ones(size(mut(pos)));
            Dqpsit(pos) = -(1-(-log(F)./kt(pos)).^(-epst(pos)))./epst(pos);
            Dqepst(pos) = psit(pos).*(1-(-log(F)./kt(pos)).^(-epst(pos)).*(1+epst(pos).*log(-log(F)./kt(pos))))./(epst(pos).*epst(pos));
            
            %   Gumbel derivatives
            Dqmut(posG) = ones(size(mut(posG)));
            Dqpsit(posG) = -log(-log(F)./kt(posG));
            Dqepst(posG) =zeros(size(mut(posG)));
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %   NUEVAS DERIVADAS
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            Dmutastmut = ones(size(kt));
            Dmutastpsit = (-1+kt.^epst)./epst;
            Dmutastepst = psit1.*(1+kt.^epst.*(epst.*log(kt)-1))./(epst.^2);
            
            Dpsitastpsit = kt.^epst;
            Dpsitastepst = log(kt).*psit1.*kt.^epst;
            
            Dmutastpsit(posG) = log(kt(posG));
            Dmutastepst(posG) = 0;
            
            Dpsitastpsit(posG) = 1;
            Dpsitastepst(posG) = 0;
            
            %   Derivadas de los parametros de localizacion, forma y escala con
            %   respecto a los parametros del modelo beta0, betaj, ...
            
            Dq = sparse(zeros(2+neps0+nmu+npsi+neps+ntend+nind+ntend2+nind2,nd));
            %   Jacobian elements related to the location parameters beta0
            %   and beta
            %     Dq(1,:) = Dqmut;
            Dq(1,:) = Dqmut.*Dmutastmut';
            %   If location harmonics are included
            if nmu>0,
                for i = 1:nmu,
                    for k = 1:length(t),
                        %   Funtion Dparam is explained below
                        %                 Dq(1+i,k) = Dqmut(k)*Dparam (t(k),i);
                        Dq(1+i,k) = Dqmut(k)*Dmutastmut(k)*amevaOPH.Dparam (t(k),i);
                    end
                    
                end
            end
            %   Jacobian elements related to the location parameters betaT,
            %   and varphi
            if ntend>0
                %         Dq(2+nmu,:) =  Dqmut.*(t');
                Dq(2+nmu,:) =  Dqmut.*(t.*Dmutastmut)';
            end
            if nind>0
                for i = 1:nind,
                    %             Dq(1+nmu+ntend+i,:) =  Dqmut.*(indices(:,lista(i))');
                    Dq(1+nmu+ntend+i,:) =  Dqmut.*(indices(:,lista(i)).*Dmutastmut)';
                end
            end
            %   Jacobian elements related to the scale parameters alpha0
            %   and alpha
            %     Dq(2+ntend+nind+nmu,:) = Dqpsit.*(psit');
            Dq(2+ntend+nind+nmu,:) = (psit1.*(Dqpsit'.*Dpsitastpsit+Dqmut'.*Dmutastpsit))';
            %   If scale harmonics are included
            if npsi>0,
                for i = 1:npsi,
                    for k = 1:length(t),
                        %                 Dq(2+nmu+ntend+nind+i,k) =Dqpsit(k)*Dparam
                        %                 (t(k),i)*psit(k);
                        Dq(2+nmu+ntend+nind+i,k) =amevaOPH.Dparam (t(k),i)*psit1(k)*(Dqpsit(k)*Dpsitastpsit(k)...
                            +Dqmut(k)*Dmutastpsit(k));
                    end
                end
            end
            %   Jacobian elements related to the scale parameters alphaT,
            %   and varphi, equation A.10
            if ntend2>0,
                %         Dq(2+neps0+nmu+ntend+nind+npsi,:) =  Dqpsit.*(t.*psit)';
                Dq(2+neps0+nmu+ntend+nind+npsi,:) =  ((Dqpsit'.*Dpsitastpsit+Dqmut'.*Dmutastpsit).*t.*psit1)';
            end
            if nind2>0,
                for i = 1:nind2,
                    %             Dq(2+nmu+ntend+nind+npsi+ntend2+i,:) =
                    %             Dqpsit.*(indices(:,lista2(i)).*psit)';
                    Dq(2+nmu+ntend+nind+npsi+ntend2+i,:) =  ((Dqpsit'.*Dpsitastpsit+Dqmut'.*Dmutastpsit).*indices(:,lista2(i)).*psit1)';
                end
            end
            %   Jacobian elements related to the shape parameters gamma0
            %   and gamma
            if neps0 == 1,
                %         Dq(2+neps0+ntend+nind+nmu+npsi+ntend2+nind2,:) = Dqepst;
                Dq(2+neps0+ntend+nind+nmu+npsi+ntend2+nind2,:) = (Dqepst'+Dqpsit'.*Dpsitastepst+Dqmut'.*Dmutastepst)';
            end
            %   If shape harmonics are included
            if neps>0,
                for i = 1:neps,
                    for k = 1:length(t),
                        %                 Dq(neps0+2+nmu+npsi+ntend+nind+ntend2+nind2+i,k) = Dqepst(k)*Dparam (t(k),i);
                        Dq(neps0+2+nmu+npsi+ntend+nind+ntend2+nind2+i,k) = (Dqepst(k)+Dqpsit(k)*Dpsitastepst(k)+Dqmut(k)*Dmutastepst(k))*amevaOPH.Dparam (t(k),i);
                    end
                end
            end
            %   Jacobian elements related to the shape parameters alphaT,
            %   and varphi, equation A.xx
            if ntend3>0, %OJO OJO nuevas derivadas
                Dq(neps0+2+nmu+npsi+ntend+nind+ntend2+nind2+neps+1,:) =  ((Dqepst'+Dqpsit'.*Dpsitastepst+Dqmut.*Dmutastepst).*t.*psit1)';
            end
            if nind3>0, %OJO OJO nuevas derivadas
                for i = 1:nind3,
                    Dq(neps0+2+nmu+npsi+ntend+nind+ntend2+nind2+neps+ntend3+i,:) =  ((Dqepst'+Dqpsit'.*Dpsitastepst+Dqmut'.*Dmutastepst).*indices(:,lista3(i)).*psit1)';%% OJO OJO cambiar indicesF x indices
                end
            end
        end
        function [Dermut,Derpsit,Derepst] = Dmupsiepst (pS,OPHobj)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %   Function Dmupsiepst calculates the derivatives of the standardized maximum
            %   with respect to parameters
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            neps0 = 0;
            if pS.chi.alpha0 ~= 0, neps0 = 1; end
            
            t = OPHobj.t;
            kt = OPHobj.kt;
            indices = OPHobj.indices;
%             indicesF = OPHobj.indicesF;
            lista  = OPHobj.lista;
            lista2 = OPHobj.lista2;
            lista3 = OPHobj.lista3;
            %   Related to armonic
            nmu = length(pS.mu.alpha);
            npsi = length(pS.psi.alpha);
            neps = length(pS.chi.alpha);
            %   Related to the covariate
            nind = length(OPHobj.lista);
            nind2 = length(OPHobj.lista2);
            nind3 = length(OPHobj.lista3);
            %   Related to the tendency
            ntend = length(pS.mu.betaT);
            ntend2 = length(pS.psi.betaT);
            ntend3 = length(pS.chi.betaT);
            
            %   Evaluate the location,scale and shape parameter at each time t as a function
            %   of the actual values of the parameters given by p
            [mut,psit,epst,posG,pos,mut1,psit1] = amevaOPH.parameters_t(pS,OPHobj);
            nd = length(t);
            Dmut = ones(nd,1);
            Dpsit = ones(nd,1);
            Depst = ones(nd,1);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %   NUEVAS DERIVADAS
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            Dmutastmut = ones(size(kt));
            Dmutastpsit = (-1+kt.^epst)./epst;
            Dmutastepst = psit1.*(1+kt.^epst.*(epst.*log(kt)-1))./(epst.^2);
            
            Dpsitastpsit = kt.^epst;
            Dpsitastepst = log(kt).*psit1.*kt.^epst;
            
            Dmutastpsit(posG) = log(kt(posG));
            Dmutastepst(posG) = 0;
            
            Dpsitastpsit(posG) = 1;
            Dpsitastepst(posG) = 0;
            
            %   Derivadas de los parametros de localizacion, forma y escala con
            %   respecto a los parametros del modelo beta0, betaj, ...
            
            Dermut = zeros(1+nmu+ntend+nind,nd);
            Derpsit = zeros(1+npsi+ntend2+nind2,nd);
            Derepst = zeros(neps0+neps,nd);
            %   Jacobian elements related to the location parameters beta0
            %   and beta
            Dermut(1,:) = Dmut'.*Dmutastmut';
            %   If location harmonics are included
            if nmu>0,
                for i = 1:nmu,
                    for k = 1:length(t),
                        %   Funtion Dparam is explained below
                        Dermut(1+i,k) = Dmut(k)*Dmutastmut(k)*amevaOPH.Dparam (t(k),i);
                    end
                end
            end
            %   Jacobian elements related to the location parameters betaT,
            %   and varphi
            if ntend>0
                Dermut(2+nmu,:) =  (Dmut.*t.*Dmutastmut)';
            end
            if nind>0
                for i = 1:nind,
                    Dermut(1+nmu+ntend+i,:) =  (Dmut.*indices(:,lista(i)).*Dmutastmut)';
                end
            end
            %   Jacobian elements related to the scale parameters alpha0
            %   and alpha
            Derpsit(1,:) = (psit1.*(Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit))';
            %   If scale harmonics are included
            if npsi>0,
                for i = 1:npsi,
                    for k = 1:length(t),
                        Derpsit(1+i,k) = amevaOPH.Dparam (t(k),i)*psit1(k)*(Dpsit(k)*Dpsitastpsit(k)...
                            +Dmut(k)*Dmutastpsit(k));
                    end
                end
            end
            %   Jacobian elements related to the scale parameters alphaT,
            %   and varphi, equation A.10
            if ntend2>0,
                %         Derpsit(1+ntend2+npsi,:) =  (Dpsit.*t.*psit)';
                Derpsit(1+ntend2+npsi,:) =  ((Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit).*t.*psit1)';
            end
            if nind2>0,
                for i = 1:nind2,
                    %             Derpsit(1+ntend2+npsi+i,:) =  (Dpsit.*indices2(:,i).*psit)';
                    Derpsit(1+ntend2+npsi+i,:) =  ((Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit).*indices(:,lista2(i)).*psit1)';
                end
            end
            %   Jacobian elements related to the shape parameters gamma0
            %   and gamma
            if neps0 == 1,
                %         Derepst(neps0,:) = (Depst)';
                Derepst(neps0,:) = (Depst+Dpsit.*Dpsitastepst+Dmut.*Dmutastepst)';
            end
            %   If shape harmonics are included
            if neps>0,
                for i = 1:neps,
                    for k = 1:length(t),
                        %                 Derepst(neps0+i,k) = Depst(k)*Dparam (t(k),i);
                        Derepst(neps0+i,k) = (Depst(k)+Dpsit(k)*Dpsitastepst(k)+Dmut(k)*Dmutastepst(k))*amevaOPH.Dparam (t(k),i);
                    end
                end
            end
            %   Jacobian elements related to the shape parameters alphaT,
            %   and varphi, equation A.xx
            if ntend3>0, %OJO OJO nuevas derivadas
                Derepst(1+neps0+neps,:) =  ((Depst+Dpsit.*Dpsitastepst+Dmut.*Dmutastepst).*t.*psit1)';
            end
            if nind3>0, %OJO OJO nuevas derivadas
                for i = 1:nind3,
                    Derepst(neps0+neps+ntend3+i,:) =  ((Depst+Dpsit.*Dpsitastepst+Dmut.*Dmutastepst).*indices(:,lista3(i)).*psit1)';%% OJO OJO cambiar indicesF x indices OJO OJO
                end
            end
        end
        % OJO OJO hacer una sola con la anterior
        % OJO OJO entra x o otro 
        function [Dq] = Dzweibull(pS,OPHobj)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %   Function Dmupsiepst calculates the derivatives of the standardized maximum
            %   with respect to parameters
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            neps0=0;
            if pS.chi.alpha0 ~= 0, neps0 = 1; end
            
            t = OPHobj.t;
            x = OPHobj.x;
            kt = OPHobj.kt;
            indices = OPHobj.indices;
%             indicesF = OPHobj.indicesF;
            lista = OPHobj.lista;
            lista2 = OPHobj.lista2;
            lista3 = OPHobj.lista3;
            %   Related to armonic
            nmu = length(pS.mu.alpha);
            npsi = length(pS.psi.alpha);
            neps = length(pS.chi.alpha);
            %   Related to the covariate
            nind = length(lista);
            nind2 = length(lista2);
            nind3 = length(lista3);
            %   Related to the tendency
            ntend = length(pS.mu.betaT);
            ntend2 = length(pS.psi.betaT);
            ntend3 = length(pS.chi.betaT);
            
            %   Evaluate the location,scale and shape parameter at each time t as a function
            %   of the actual values of the parameters given by p
            [mut,psit,epst,posG,pos,mut1,psit1] = amevaOPH.parameters_t(pS,OPHobj);
            nd = length(t);

            %   Evaluate auxiliary variables
            xn = (x-mut)./psit;
            z = 1 + epst.*xn;
            %   Since the z-values must be greater than zero in order to avoid
            %   numerical problems their values are set to be greater than 1e-4
            z = max(1e-8,z);
            zn =z.^(-1./epst);
            
            %   Derivatives of the quantile function with respect to location,
            %   scale and shape parameters
            Dmut(pos,1) = -1./(z(pos).*psit(pos));
            Dpsit(pos,1) =  xn(pos).*Dmut(pos);
            Depst(pos,1) = (1-1./z(pos)-log(z(pos)))./(epst(pos).*epst(pos));
            
            %   Gumbel derivatives
            Dmut(posG,1) = -1./psit(posG);
            Dpsit(posG,1) =-xn(posG)./psit(posG);
            Depst(posG,1) =0;
            %     Dmut(posG,1) = (1+epst-kt.*zn)./(psit.*z);%OC descomentar anterior
            %     Dpsit(posG,1) = -(1-xn.*(1-kt.*zn))./(psit.*z);
            %     Depst(posG,1) = zn.*(xn.*(kt-(1+epst)./zn)+...
            %         z.*(-kt+1./zn).*log(z)./epst)./(epst.*z);
            %
            %     %   GUMBEL derivatives given by equations (A.4)-(A.5) in the paper
            %     Dmut(posG,1) = (1-kt(posG).*exp(-xn(posG)))./(psit(posG));
            %     Dpsit(posG,1) = (xn(posG)-1-kt(posG).*xn(posG).*exp(-xn(posG)))./(psit(posG));
            %     Depst(posG,1)=0;%OC
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %   NUEVAS DERIVADAS
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            Dmutastmut = ones(size(kt));
            Dmutastpsit = (-1+kt.^epst)./epst;
            Dmutastepst = psit1.*(1+kt.^epst.*(epst.*log(kt)-1))./(epst.^2);
            
            Dpsitastpsit = kt.^epst;
            Dpsitastepst = log(kt).*psit1.*kt.^epst;
            
            Dmutastpsit(posG) = log(kt(posG));
            Dmutastepst(posG) = 0;
            
            Dpsitastpsit(posG) = 1;
            Dpsitastepst(posG) = 0;
            
            Dq = sparse(zeros(2+neps0+nmu+npsi+neps+ntend+nind+ntend2+nind2,nd));
            
            %   Derivadas de los parametros de localizacion, forma y escala con
            %   respecto a los parametros del modelo beta0, betaj, ...
            
            %   Jacobian elements related to the location parameters beta0
            %   and beta
            %     Dq(1,:) = Dmut';
            Dq(1,:) = (Dmut.*Dmutastmut)';
            %   If location harmonics are included
            if nmu>0,
                for i = 1:nmu,
                    for k = 1:length(t),
                        %   Funtion Dparam is explained below
                        %                 Dq(1+i,k) = Dmut(k)*Dparam (t(k),i);
                        Dq(1+i,k) = Dmut(k)*Dmutastmut(k)*amevaOPH.Dparam (t(k),i);
                    end
                end
            end
            %   Jacobian elements related to the location parameters betaT,
            %   and varphi
            if ntend>0
                %         Dq(2+nmu,:) =  (Dmut.*t)';
                Dq(2+nmu,:) =  (Dmut.*t.*Dmutastmut)';
            end
            if nind>0
                for i = 1:nind,
                    %             Dq(1+nmu+ntend+i,:) =  (Dmut.*indices(:,i))';
                    Dq(1+nmu+ntend+i,:) =  (Dmut.*indices(:,lista(i)).*Dmutastmut)';
                end
            end
            %   Jacobian elements related to the scale parameters alpha0
            %   and alpha
            %     Dq(2+ntend+nind+nmu,:) = (Dpsit.*psit)';
            Dq(2+ntend+nind+nmu,:) = (psit1.*(Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit))';
            %   If scale harmonics are included
            if npsi>0,
                for i = 1:npsi,
                    for k = 1:length(t),
                        %                 Dq(2+nmu+ntend+nind+i,k) = Dpsit(k)*Dparam
                        %                 (t(k),i)*psit(k);
                        Dq(2+nmu+ntend+nind+i,k) = amevaOPH.Dparam (t(k),i)*psit1(k)*(Dpsit(k)*Dpsitastpsit(k)...
                            +Dmut(k)*Dmutastpsit(k));
                    end
                end
            end
            %   Jacobian elements related to the scale parameters alphaT,
            %   and varphi, equation A.10
            if ntend2>0,
                %         Dq(2+neps0+nmu+ntend+nind+npsi,:) =  (Dpsit.*t.*psit)';
                Dq(2+neps0+nmu+ntend+nind+npsi,:) =  ((Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit).*t.*psit1)';
            end
            if nind2>0,
                for i = 1:nind2,
                    %             Dq(2+nmu+ntend+nind+npsi+ntend2+i,:) =  (Dpsit.*indices2(:,i).*psit)';
                    Dq(2+nmu+ntend+nind+npsi+ntend2+i,:) =  ((Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit).*indices(:,lista2(i)).*psit1)';
                end
            end
            %   Jacobian elements related to the shape parameters gamma0
            %   and gamma
            if neps0 == 1,
                %         Dq(2+neps0+ntend+nind+nmu+npsi+ntend2+nind2,:) = (Depst)';
                Dq(2+neps0+ntend+nind+nmu+npsi+ntend2+nind2,:) = (Depst+Dpsit.*Dpsitastepst+Dmut.*Dmutastepst)';
            end
            %   If shape harmonics are included
            if neps>0,
                for i = 1:neps,
                    for k = 1:length(t),
                        %                 Dq(neps0+2+nmu+npsi+ntend+nind+ntend2+nind2+i,k) = Depst(k)*Dparam (t(k),i);
                        Dq(neps0+2+nmu+npsi+ntend+nind+ntend2+nind2+i,k) = (Depst(k)+Dpsit(k)*Dpsitastepst(k)+Dmut(k)*Dmutastepst(k))*amevaOPH.Dparam (t(k),i);
                    end
                end
            end
            %   Jacobian elements related to the shape parameters alphaT,
            %   and varphi, equation A.xx
            if ntend3>0, %OJO OJO nuevas derivadas
                Dq(1+neps0+2+nmu+npsi+neps+ntend+nind+ntend2+nind2,:) =  ((Depst+Dpsit.*Dpsitastepst+Dmut.*Dmutastepst).*t.*psit1)';
            end
            if nind3>0, %OJO OJO nuevas derivadas
                for i = 1:nind3,
                    Dq(neps0+2+nmu+npsi+neps+ntend+nind+ntend2+nind2+ntend3+i,:) =  ((Depst+Dpsit.*Dpsitastepst+Dmut.*Dmutastepst).*indices(:,lista3(i)).*psit1)';% OJO OJO cambiar indicesF x indices
                end
            end
        end
        function dp = Dparam (t,j)
            %
            %   Derivative of the location, scale and shape functions with respect
            %   to harmonic parameters. It correspond to the right hand side
            %   in equation A.11 of the paper
            %
            %   Input:
            %        t-> Time (Yearly scale)
            %        j-> Harmonic number
            %
            %   Output:
            %        dp-> Corresponding derivative
            %
            if mod(j,2) == 0,
                dp = sin(j/2*2*pi*t);
            else
                dp = cos((j+1)/2*2*pi*t);
            end
        end
        function [Q] = QuantileGEV (F,pS,OPHobj)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %   Function QuantileGEV calculates the quantile q associated with a given
            %   parameterization
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            [mut,psit,epst,posG,pos] = amevaOPH.parameters_t(pS,OPHobj);
            %   Evaluate the quantile, not
            %   that the general and Gumbel expresions are used
            Q(pos) = mut(pos) - (1-(-log(F)./OPHobj.kt(pos)).^(-epst(pos))).*psit(pos)./epst(pos);
            Q(posG) = mut(posG) - psit(posG).*log(-log(F)./OPHobj.kt(posG));
            Q=Q(:);
        end
        function [Zt] = Zstandardt (pS,OPHobj)
            [mut,psit,epst,posG,pos]=amevaOPH.parameters_t(pS,OPHobj);
            %   Evaluate auxiliary variables
            xn = (OPHobj.x-mut)./psit;
            z = 1 + epst.*xn;
            %   Since the z-values must be greater than zero in order to avoid
            %   numerical problems their values are set to be greater than 1e-4
            z = max(amevaOPH.loglikeSizeZval,z);
            m=length(OPHobj.t);
            Zt = zeros (m,1);
            %   WEIBULL or FRECHET values
            Zt(pos) = (1./epst(pos)).*log(z(pos));
            %   GUMBEL values
            Zt(posG) = (OPHobj.x(posG)-mut(posG))./psit(posG);
            Zt = Zt(:);
        end
        function [mut,psit,epst,posG,pos,mut1,psit1] = parameters_t(p,OPHobj)
            %   Evaluate the location parameter at each time t as a function
            %   of the actual values of the parameters given by p
            kt = OPHobj.kt; if isempty(kt), kt = ones(size(OPHobj.t)); end
            mut1 = amevaOPH.parametros(OPHobj.t,p.mu,OPHobj.indices(:,OPHobj.lista));
            %   Evaluate the scale parameter at each time t as a function
            %   of the actual values of the parameters given by p
            psit1 = exp(amevaOPH.parametros(OPHobj.t,p.psi,OPHobj.indices(:,OPHobj.lista2)));
            %   Evaluate the shape parameter at each time t as a function
            epst = amevaOPH.parametros(OPHobj.t,p.chi,OPHobj.indices(:,OPHobj.lista3));%%% OJO OJO cambira indicesF x indices
            
            [posG,pos] = amevaOPH.posGumbel(epst);
            
            %   Modifico los parametros para incluir el numero de datos
            mut = mut1;
            psit = psit1;
            mut(pos) = mut1(pos)+psit1(pos).*(kt(pos).^epst(pos)-1)./epst(pos);
            psit(pos) = psit1(pos).*kt(pos).^epst(pos);
            %   Modifico los parametros para incluir el numero de datos para GUMBEL
            mut(posG) = mut(posG)+psit(posG).*log(kt(posG));
            epst(posG) = 1; % OJO OJO entender esto OJO OJO
        end
        %% parametros
        function y = parametros(t,p,indices,indicesint,t_old)
            t=t(:);
            m = length(t);
            if nargin<3, indices = []; end
            if nargin<4, indicesint = []; end
            if nargin<5, t_old = []; end
            np = length(p.alpha);
            if mod(np,2)~=0,
                error('Parameter number must be even');
            end
            y(m,1) = 0;
            if ~isempty(p.alpha0),
                y = repmat(p.alpha0,m,1);
            end
            % y = y + sum(repmat(p.alpha(1:2:np-1),m,1)'.*cos(repmat((1:np/2)*2*pi,m,1)'.*repmat(t,np/2,1)));
            % y = y + sum(repmat(p.alpha(2:2:np  ),m,1)'.*sin(repmat((1:np/2)*2*pi,m,1)'.*repmat(t,np/2,1)));
            for i = 1:np/2,
                y = y + p.alpha(2*i-1)*cos(i*2*pi*t) + p.alpha(2*i)*sin(i*2*pi*t);
            end
            if isfield(p,'betaT') && ~isempty(p.betaT),
                y = y + p.betaT*t;
            end
            if isfield(p,'varphi') && ~isempty(p.varphi),
                nind = length(p.varphi);
                if ~isempty(indicesint),
                    if isempty(t_old),
%                         y = y + sum(repmat(p.varphi',m,1)'.*repmat(indicesint',m,1)');
                        for i = 1:nind,
                            y = y + p.varphi(i)*indicesint(i);
                        end
                    else
                        indicesintaux = buscar(t,indices,t_old);
%                         y = y + sum(repmat(p.varphi,1,m).*indicesintaux);
                        for i = 1:length(nind),
                            y = y + p.varphi(i)*indicesintaux(:,i);
                        end
                    end
                else
%                     y = y + sum(repmat(p.varphi',m,1)'.*indices);
                    for i = 1:length(nind),
                        y = y + p.varphi(i)*indices(:,i);
                    end
                end
            end
            function indicesintaux = buscar(t,indices,t_old)
                nt_old = int32(length(t_old));
                nt = int32(length(t));
                posV(1,nt) = 0;
                posV=int32(posV);
                pos = int32(1);
                for j = 1:nt,
                    encontrado = true;
                    while encontrado && pos <= nt_old,
                        if t(j)<t_old(pos),
                            posV(j) = pos;
                            encontrado = false;
                        else
                            pos = pos+1;
                        end
                    end
                end
                pos=1;
                for j=1:nt
                    if posV(nt-j+1)==0
                        pos=nt-j+1;
                    else
                        break;
                    end
                end
                if pos>1
                    posV(pos:end)=posV(pos-1);
                end
                if posV(end)==0
                    pos
                end
                indicesintaux=indices(posV,:);%OJO OJO mas rapido indices(:,posV)
            end
        end
        function F = CDFGEVt(OPHobj,OPHr)
            [mut,psit,epst,posG,pos] = amevaOPH.parameters_t(OPHr.pS,OPHobj);
            %   Evaluate auxiliary variables
            xn = (OPHobj.x-mut)./psit;
            z = 1 + epst.*xn;
            %   Since the z-values must be greater than zero in order to avoid
            %   numerical problems their values are set to be greater than 1e-4
            z = max(amevaOPH.loglikeSizeZval,z);
            F = zeros (length(OPHobj.t),1);
            %   WEIBULL or FRECHET distribution function
            F(pos) = exp(-OPHobj.kt(pos).*(z(pos)).^(-1./epst(pos)));
            %   GUMBEL distribution function
            F(posG) = exp(-OPHobj.kt(posG).*exp(-(xn(posG))));
        end
        function f = PDFGEVt(OPHobj,OPHr)
            [mut,psit,epst,posG,pos] = amevaOPH.parameters_t(OPHr.pS,OPHobj);
            xn = (OPHobj.x-mut)./psit;
            aux = (1+epst(pos).*xn(pos));
            posS = (aux<=0);
            aux(posS) = 0;
            f = zeros (size(OPHobj.x));
            %   WEIBULL or FRECHET distribution function
            f(pos) = exp(-aux.^(-1./epst(pos))).*aux.^(-1-1./epst(pos))./psit(pos);
            %   GUMBEL distribution function
            f(posG) = exp(-exp(-xn(posG))-xn(posG))./psit(posG);
        end
        %% Hessiano aproximado
        function Hxx = Hessiano(ps,OPHobj)
            X = amevaOPH.pstovector(ps);
            NT = length(X);
            Hxx(NT,NT) = 0;
            h_const = 2e-5;%Precicion en el Hessiano y gradiente aproximado
            for j=1:NT
                for i=1:NT
                    if (i==j)
                        Xi=X;
                        XI=X;
                        Xi(i)=X(i) * (1.0 + h_const);
                        XI(i)=X(i) * (1.0 - h_const);
                        if (X(i)==0)
                            Xi(i)=h_const;
                            XI(i)=-h_const;
                        end
                        f= -amevaloglikelihood(X,OPHobj);
                        fi=-amevaloglikelihood(Xi,OPHobj);
                        fI=-amevaloglikelihood(XI,OPHobj);
                        dx=(X(i)*h_const)^2;
                        if (dx==0), dx=h_const; end
                        Hxx(i,j)=(fi-2*f+fI)/dx;
                    else
                        Xi=X;
                        Xj=X;
                        XI=X;
                        XJ=X;
                        Xi(i)=X(i) * (1.0 + h_const);
                        Xi(j)=X(j) * (1.0 + h_const);
                        Xj(i)=X(i) * (1.0 - h_const);
                        Xj(j)=X(j) * (1.0 - h_const);
                        XI(i)=X(i) * (1.0 + h_const);
                        XI(j)=X(j) * (1.0 - h_const);
                        XJ(i)=X(i) * (1.0 - h_const);
                        XJ(j)=X(j) * (1.0 + h_const);
                        if (X(i)==0)
                            Xi(i)= h_const;
                            Xj(i)= -h_const;
                            XI(i)= h_const;
                            XJ(i)= -h_const;
                        end
                        if (X(j)==0)
                            Xi(j)= h_const;
                            Xj(j)= -h_const;
                            XI(j)= -h_const;
                            XJ(j)= h_const;
                        end
                        fi=-amevaloglikelihood(Xi,OPHobj);
                        fI=-amevaloglikelihood(XI,OPHobj);
                        fj=-amevaloglikelihood(Xj,OPHobj);
                        fJ=-amevaloglikelihood(XJ,OPHobj);
                        dx=4.0*(X(i)*h_const)*(X(j)*h_const);%%OJO OJO no debe ir 4.0??
                        if (dx==0), dx=h_const; end
                        Hxx(i,j)=(fi-fJ-fI+fj)/dx;
                    end
                end
            end
        end
        %% PLOTS ameva gev
        function gevplots(OPHobj,OPHr,parametros_all,confidenceLevelAlpha,LocScaleShapeState,tpstring,Opciones,ax1)
            %GEV Extremos plot's
            % ejemplo
            % amevaOPH.gevplots(OPHobj,OPHr,parametros_all,0.05,[],{'AggregateQuantiles'},Opciones)
            [ni,mi]=size(tpstring);
            if mi<1, return; end;
            h=[];
            for i=1:mi,
%                  try
                    savename=tpstring{i};
                    if ~exist('ax1','var') || ~ishandle(ax1), h=figure('Visible','Off'); ax1 = axes('Parent',h); end
                    switch savename
                        case 'LocalScaleParmPlot'   %Location parameter plotting
                            amevaOPH.amevaLocation(OPHobj,parametros_all,confidenceLevelAlpha,Opciones,ax1);
                        case 'ScaleParmPlot'    %   Scale parameter plotting
                            amevaOPH.amevaScale(OPHobj,parametros_all,confidenceLevelAlpha,Opciones,ax1);
                        case 'FormParmPlot'    %   Shape parameter plotting
                            amevaOPH.amevaShape(OPHobj,parametros_all,confidenceLevelAlpha,Opciones,ax1);
                        case 'QQplot'   %   QQ plot
                            amevaOPH.amevaQplot(OPHobj,OPHr,confidenceLevelAlpha,ax1);
                        case 'PPplot'   %   PP plot
                            amevaOPH.amevaPplot(OPHobj,OPHr,confidenceLevelAlpha,ax1);
                        case 'GevParamSolution'   %   Expresion de los parametros
                            amevaOPH.GEV_expression(LocScaleShapeState,OPHr,ax1);
                        case 'AggregateQuantiles'   %   Agregate CuantilesPlot
                            if Opciones.myPlots>0,
                                % OJO OJO OJO comprobar que funciona OJO OJO
                                if isfield(OPHobj,'datemax_j') && ~isempty(OPHobj.datemax_j),
                                    [datemaxA,HsmaxA] = maximostempfunction(OPHobj.datemax_j,OPHobj.x,'annual','maximum',0,1,5,'day');
                                    datemaxA = yearlytimescale(datemaxA,'R'); %escala anual
                                    OPHobj.datemaxA = datemaxA; OPHobj.HsmaxA = HsmaxA;
                                end %datos para pintar el anual
                                amevaAgregateQuantiles.aqPlot(OPHobj,OPHr,confidenceLevelAlpha,Opciones,ax1);
                            end
                        case 'ParameterEvolution'   %   Agregate Cuantiles Evolution Plot
                            if Opciones.myPlots>1
                                disp('Agregate Cuantiles Evolution Plot... ploting wait!');
                                amevaOPH.amevaAgregateQuantilesEvo(OPHobj,OPHr,parametros_all.mut,parametros_all.psit,Opciones,ax1);
                            end
                        case 'Hsmax'
                            amevaOPH.HsmaxPlot(OPHobj,[],[],Opciones,ax1);
                        otherwise
                            disp(['Warnig!.Not exist this plot case ',savename]);
                    end
                    if ~isempty(h)
                        amevasavefigure(h,Opciones.carpeta,Opciones.svopt,savename)
                    end
%                  catch ME  %# Catch the exception
%                      continue;
%                  end
            end
        end
        %% PLOT Hsmax
        function HsmaxPlot(OPHobj,mkrformat,umbral,Opciones,gcax)
            if ~exist('mkrformat','var') || isempty(mkrformat), mkrformat='b'; end
            if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
            if ~ishandle(gcax), disp('WARNING! HsmaxPlot with axes'); end
            set(gcf,'CurrentAxes',gcax)
            axis(gcax,'xy');
            plot(gcax,OPHobj.t,OPHobj.x,mkrformat);
            if exist('umbral','var') && not(isempty(umbral)),
                hold on;
                line(OPHobj.t,umbral,'Color','r','LineWidth',4);
                hold off;
            end
            grid(gcax,'on');
            ylabel(gcax,labelsameva(Opciones.var_name{1},Opciones.var_unit{1}));
            xlabel(gcax,'Time (years)');
            legend(gcax,Opciones.var_name{1});
            title(gcax,Opciones.cityname);
        end
        %% PLOT Location
        function amevaLocation(OPHobj,parametros_all,confidenceLevelAlpha,Opciones,gcax)
            if confidenceLevelAlpha>0.5, error(['confidenceLevelAlpha=',num2str(confidenceLevelAlpha)]); end
            if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
            if ~ishandle(gcax), disp('WARNING! amevaLocation with axes'); end
            set(gcf,'CurrentAxes',gcax)
            
            auxup = parametros_all.mut+norminv(1-confidenceLevelAlpha/2,0,1)*parametros_all.stdmut;
            auxlo = parametros_all.mut-norminv(1-confidenceLevelAlpha/2,0,1)*parametros_all.stdmut;
            if isempty(OPHobj.betaT) && isempty(OPHobj.lista) && isempty(OPHobj.betaT2) && isempty(OPHobj.lista2) && isempty(OPHobj.betaT3) && isempty(OPHobj.lista3),
                [datemaxord,orden] = sort(parametros_all.datemaxanual);
                %[quan95] = amevaOPH.QuantileGEV(0.95,OPHr.pS,OPHobj);
                plot(gcax,datemaxord,parametros_all.quan95(orden),'k--','LineWidth',2)
                hold(gcax,'on')
                plot(gcax,datemaxord,OPHobj.x(orden),'+k')
                plot(gcax,datemaxord,parametros_all.mut(orden),'Color',[0.0 0.0 0.0],'LineWidth',2)
                
                fill([datemaxord; datemaxord(end); flipud(datemaxord)], [auxup(orden); auxup(orden(1)); flipud(auxlo(orden))],[.7 .7 .7])
                auxup = parametros_all.quan95+norminv(1-confidenceLevelAlpha/2,0,1)*parametros_all.stdDq;
                auxlo = parametros_all.quan95-norminv(1-confidenceLevelAlpha/2,0,1)*parametros_all.stdDq;
                fill([datemaxord; datemaxord(end); flipud(datemaxord)], [auxup(orden); auxup(orden(1)); flipud(auxlo(orden))],[.7 .7 .7])
                
                plot(gcax,datemaxord,parametros_all.quan95(orden),'k--','LineWidth',2)
                plot(gcax,datemaxord,OPHobj.x(orden),'+k')
                plot(gcax,datemaxord,parametros_all.mut(orden),'Color',[0.0 0.0 0.0],'LineWidth',2)
            else
                plot(gcax,OPHobj.t,parametros_all.quan95,'k--','LineWidth',2)
                hold(gcax,'on')
                plot(gcax,OPHobj.t,OPHobj.x,'+k')
                plot (OPHobj.t,parametros_all.mut,'Color',[0.0 0.0 0.0],'LineWidth',2)
                fill([OPHobj.t; OPHobj.t(end); flipud(OPHobj.t)], [auxup; auxup(1); flipud(auxlo)],[.7 .7 .7])
                auxup = parametros_all.quan95+norminv(1-confidenceLevelAlpha/2,0,1)*parametros_all.stdDq;
                auxlo = parametros_all.quan95-norminv(1-confidenceLevelAlpha/2,0,1)*parametros_all.stdDq;
                fill([OPHobj.t; OPHobj.t(end); flipud(OPHobj.t)], [auxup; auxup(1); flipud(auxlo)],[.7 .7 .7])
            end
            legend(gcax,['x_{' num2str(1-confidenceLevelAlpha) ',t}'],[Opciones.var_name{1},'^{max}'],'\mu_t','Location','Best')
            
            grid(gcax,'on')
            title(gcax,['Location parameter (' Opciones.cityname ')'])
            ylabel(gcax,['\mu_t (',Opciones.var_unit{1},')'])
            xlabel(gcax,'Time (yearly scale)')
            hold(gcax,'off')
        end
        %% PLOT Scale
        function amevaScale(OPHobj,parametros_all,confidenceLevelAlpha,Opciones,gcax)
            if confidenceLevelAlpha>0.5, error(['confidenceLevelAlpha=',num2str(confidenceLevelAlpha)]); end
            if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
            if ~ishandle(gcax), disp('WARNING! amevaScale with axes'); end
            set(gcf,'CurrentAxes',gcax)
            datemaxanual=parametros_all.datemaxanual;
            if abs(OPHobj.Km-12) < 2/100,
                datemaxanual=OPHobj.t;
            end
            auxup = parametros_all.psit+norminv(1-confidenceLevelAlpha/2,0,1)*parametros_all.stdpsit;
            auxlo = parametros_all.psit-norminv(1-confidenceLevelAlpha/2,0,1)*parametros_all.stdpsit;
            [datemaxord,orden] = sort(datemaxanual);
            if isempty(OPHobj.betaT) && isempty(OPHobj.lista) && isempty(OPHobj.betaT2) && isempty(OPHobj.lista2) && isempty(OPHobj.betaT3) && isempty(OPHobj.lista3),
                fill([datemaxord; datemaxord(end); flipud(datemaxord)], [auxup(orden); auxup(orden(1)); flipud(auxlo(orden))],[.7 .7 .7]);hold(gcax,'on');
                plot(gcax,datemaxord,parametros_all.psit(orden),'k','LineWidth',2)
            else
                plot(gcax,OPHobj.t,parametros_all.psit,'k','LineWidth',2);hold(gcax,'on')
                fill([OPHobj.t; OPHobj.t(end); flipud(OPHobj.t)], [auxup; auxup(1); flipud(auxlo)],[.7 .7 .7])
            end
            grid(gcax,'on')
            title(gcax,['Scale parameter (' Opciones.cityname ')'])
            ylabel(gcax,['\psi_t (',Opciones.var_unit{1},')'])
            hold(gcax,'off')
            if abs(OPHobj.Km-12) < 2/100,
                xlabel(gcax,'Time')
            else
                xlabel(gcax,'Time (yearly scale)')
            end
        end
        %% PLOT Shape
        function amevaShape(OPHobj,parametros_all,confidenceLevelAlpha,Opciones,gcax)
            if confidenceLevelAlpha>0.5, error(['confidenceLevelAlpha=',num2str(confidenceLevelAlpha)]); end
            if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
            if ~ishandle(gcax), disp('WARNING! amevaScale with axes'); end
            set(gcf,'CurrentAxes',gcax)
            datemaxanual=parametros_all.datemaxanual;
            if abs(OPHobj.Km-12) < 2/100,
                datemaxanual=OPHobj.t;
            end
            auxup = parametros_all.epst+norminv(1-confidenceLevelAlpha/2,0,1)*parametros_all.stdepst;
            auxlo = parametros_all.epst+norminv(confidenceLevelAlpha/2,0,1)*parametros_all.stdepst;
            [datemaxord,orden] = sort(datemaxanual);
            fill([datemaxord; datemaxord(end); flipud(datemaxord)], [auxup(orden); auxup(orden(1)); flipud(auxlo(orden))],[.7 .7 .7]);
            hold(gcax,'on')
            plot(gcax,datemaxord,parametros_all.epst(orden),'k','LineWidth',2)
            grid(gcax,'on')
            title(gcax,['Shape parameter (' Opciones.cityname ')']);
            if abs(OPHobj.Km-12) < 2/100,
                xlabel(gcax,'Time')
            else
                xlabel(gcax,'Time (yearly scale)')
            end
            ylabel(gcax,'\epsilon_t');
            hold(gcax,'off');
        end
        %% PLOT AgregateQuantilesEvo Evolution of the yearly aggregate quantiles during the fitting time
        function amevaAgregateQuantilesEvo(OPHobj,OPHr,mut,psit,Opciones,gcax)
            if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
            if ~ishandle(gcax), disp('WARNING! amevaAgregateQuantilesEvo with axes'); end
            set(gcf,'CurrentAxes',gcax)
            axis(gcax,'xy');
            %   Aggregate quantiles for different return periods
            Ts = [10 20 50 100]; %[10 20 50 100]
            nts = length(Ts);
            pS=OPHr.pS; %Solution in struct var
            %   Sarting calculations   %    Annual
            quanaggr=amevaAgregateQuantiles.aqEvolution(Ts,OPHobj,pS);
            
            col = summer(nts);
            Ts_str = '';
            for i = 1:nts,
                plot(gcax,((1:OPHobj.Ny)-0.5)',quanaggr(:,nts-i+1),'Color',col(nts-i+1,:))
                hold(gcax,'on')
                Ts_str{i} = num2str(Ts(nts-i+1)); 
            end
            %   Plot of data
            plot(gcax,OPHobj.t,OPHobj.x,'+r')
            hold(gcax,'on')
            %   Plot of quantiles
            [quan95] = amevaOPH.QuantileGEV(0.95,pS,OPHobj);
            plot(gcax,OPHobj.t,quan95,'g')
            [AX,H1,H2] = plotyy(gcax,OPHobj.t,mut,OPHobj.t,psit,'plot');
            set(get(AX(1),'Ylabel'),'String',['\mu_t (',Opciones.var_unit{1},'),',Opciones.var_name{1},' (',Opciones.var_unit{1},')'])
            set(get(AX(2),'Ylabel'),'String',['\psi_t (',Opciones.var_unit{1},')'])
            grid(gcax,'on')
            set(H1,'LineWidth',1.6)
            set(H2,'LineWidth',1.6)
            xlabel(gcax,'Time (years)')
            title(gcax,['Evolution of adjustment (' Opciones.cityname ')'])
            legend(gcax,[Ts_str,'Data','Quantile 95%','LocationP','Scale'],'Location','NorthWest');%SouthEastOutside
            hold(gcax,'off')
        end
        %% QQ Plot
        function [Ze,Zmsort,Zup,Zlb] = amevaQplot(OPHobj,OPHr,confidenceLevelAlpha,gcax)
            if confidenceLevelAlpha>0.5, error(['confidenceLevelAlpha=',num2str(confidenceLevelAlpha)]); end
            lista=OPHr.lista; lista2=OPHr.lista2; lista3=OPHr.lista3; %Related to the covariate
            Ze = -log(-log((1:length(OPHobj.x))'/(length(OPHobj.x)+1))); %Standarized empirical variable
            Zm = amevaOPH.Zstandardt (OPHr.pS,OPHobj); %Standarized adjustment variable
            [Dwei] = amevaOPH.Dzweibull (OPHr.pS,OPHobj);
            stdDwei = sqrt(sum((Dwei'*OPHr.invI0).*Dwei',2));
            [Zmsort,ordenZm]= sort(Zm);%Sorted standarized adjustment variable
            %   Quantile-quantile PLOT
            Zup=[]; Zlb=[]; %Up and Lb %OJO revisar esta condicion con Fer o Roberto
            if isempty(lista) && isempty(lista2) && isempty(lista3), % && isempty(OPHr.betaT) && isempty(OPHr.betaT2) && isempty(OPHr.betaT3)
               Zup=Zmsort+norminv(1-confidenceLevelAlpha/2,0,1)*stdDwei(ordenZm);
               Zlb=Zmsort-norminv(1-confidenceLevelAlpha/2,0,1)*stdDwei(ordenZm);
            end
            if nargout==0
                if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
                if ~ishandle(gcax), disp('WARNING! amevaQplot with axes'); end
                set(gcf,'CurrentAxes',gcax)
                plot(gcax,linspace(min(Zmsort),max(Zmsort)),linspace(min(Zmsort),max(Zmsort)),'k');
                hold(gcax,'on')
                plot(gcax,Ze,Zmsort,'o','MarkerEdgeColor','k','MarkerFaceColor',[0.0 0.0 0.0],'MarkerSize',3)
                if not(isempty(Zup)) && not(isempty(Zlb)),
                    plot(gcax,Ze,Zup,'--k');
                    plot(gcax,Ze,Zlb,'--k');
                end
                grid(gcax,'on')
                title(gcax,'Best model QQ plot')
                xlabel(gcax,'Empirical');
                ylabel(gcax,'Fitted');
                axis(gcax,'square');
                axis(gcax,[min(min(Ze),min(Zmsort)) max(max(Ze),max(Zmsort)) min(min(Ze),min(Zmsort)) max(max(Ze),max(Zmsort))])
                hold(gcax,'off')
            end
        end
        %% PP plot
        function [Fe,Fmsort] = amevaPplot(OPHobj,OPHr,confidenceLevelAlpha,gcax)
            if confidenceLevelAlpha>0.5, error(['confidenceLevelAlpha=',num2str(confidenceLevelAlpha)]); end
            lista=OPHr.lista; lista2=OPHr.lista2; lista3=OPHr.lista3; %Related to the covariate
            Fe = (1:length(OPHobj.x))'/(length(OPHobj.x)+1);
            Zm = amevaOPH.Zstandardt (OPHr.pS,OPHobj); %Standarized adjustment variable
            [Dwei] = amevaOPH.Dzweibull (OPHr.pS,OPHobj);
            stdDwei = sqrt(sum((Dwei'*OPHr.invI0).*Dwei',2));
            Fm = amevaOPH.CDFGEVt(OPHobj,OPHr);
            %   Sort the distribution function values
            [Fmsort,ordenFm]= sort(Fm);
            %   Dibujo del grafico probabilidad-probabilidad
            if nargout==0
                if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
                if ~ishandle(gcax), disp('WARNING! amevaPplot with axes'); end
                set(gcf,'CurrentAxes',gcax)
                plot(gcax,linspace(0,1),linspace(0,1),'k');
                hold(gcax,'on')
                plot(gcax,Fe,Fmsort,'o','MarkerEdgeColor','k','MarkerFaceColor',[0 0 0],'MarkerSize',3)
                if isempty(lista) && isempty(lista2) && isempty(lista3), % && isempty(OPHr.betaT) && isempty(OPHr.betaT2) && isempty(OPHr.betaT3)
                    plot(gcax,Fe,exp(-exp(-Zm(ordenFm)+norminv(1-confidenceLevelAlpha/2,0,1)*stdDwei(ordenFm))),'--k')
                    plot(gcax,Fe,exp(-exp(-Zm(ordenFm)-norminv(1-confidenceLevelAlpha/2,0,1)*stdDwei(ordenFm))),'--k')
%                     plot(gcax,Fe,exp(-exp(-Zm(ordenFm)+norminv(1-confidenceLevelAlpha,0,1)*stdDwei(ordenFm))),'--k')
%                     plot(gcax,Fe,exp(-exp(-Zm(ordenFm)+norminv(confidenceLevelAlpha,0,1)*stdDwei(ordenFm))),'--k')
                end
                grid(gcax,'on')
                title(gcax,'Best model PP plot')
                xlabel(gcax,'Empirical');
                axis(gcax,'square');
                ylabel(gcax,'Fitted');
                hold(gcax,'off')
            end
        end
        %% Parameter expression PLOT
        function GEV_expression(LocScaleShapeState,OPHr,gcax)
            % MODELO
            [fmut,fpsit,fxit,fcdf,icdf]=amevaOPH.modelfunction([1 2 2]);%Modelo GEV default model
            
            posaux_=get(gcax,'Position'); %0.13 valor por defecto del axis nuevo
            if posaux_(1)>0.13, % si no es nuevo el eje cambio de pos ax1
                %set(gcax,'Position',[0.35 0.07 0.63 0.9025]);
            end
            if abs(0.13-posaux_(1))<0.01, %en figura nueva aumento la figura
                figaux_=get(gcf,'Position');
                set(gcf,'Position',[figaux_(1) figaux_(2) 800 700]);
            end
            interprete='latex';
            ypstx=[0.985,0.9275,0.875,0.8075,0.7475,0.63,0.550,0.480];
            text(0.0,ypstx(1)+0.04,' GEV Model:','FontSize',11,'Parent',gcax,'HorizontalAlignment','left');
            text(0.5,ypstx(1),fcdf,'Interpreter',interprete,'FontSize',11,'Parent',gcax,'HorizontalAlignment','Center');
            text(0.5,ypstx(2),icdf,'Interpreter',interprete,'FontSize',11,'Parent',gcax,'HorizontalAlignment','Center');

            text(0.5,ypstx(4),fpsit,'Interpreter',interprete,'FontSize',11,'Parent',gcax,'HorizontalAlignment','Center');
            text(0.5,ypstx(5),fxit,'Interpreter',interprete,'FontSize',11,'Parent',gcax,'HorizontalAlignment','Center');
            % AJUSTE
            [fmut,fpsit,fxit]=amevaOPH.modelfunction(LocScaleShapeState);%Modelo a ajustar
            text(0.0,ypstx(6)+0.04,' Model to Fit:','FontSize',11,'Parent',gcax,'HorizontalAlignment','left');
            text(0.5,ypstx(6),fmut,'Interpreter',interprete,'FontSize',11,'Parent',gcax,'HorizontalAlignment','Center');
            text(0.5,ypstx(7),fpsit,'Interpreter',interprete,'FontSize',11,'Parent',gcax,'HorizontalAlignment','Center');
            text(0.5,ypstx(8),fxit,'Interpreter',interprete,'FontSize',11,'Parent',gcax,'HorizontalAlignment','Center');
            % MODELO AJUSTADO
            Fmuparam{1}=OPHr.beta0;        Fmuparam{2}=OPHr.beta;        Fmuparam{3}=OPHr.betaT;        Fmuparam{4}=OPHr.varphi;
            Fpsiparam{1}=OPHr.alpha0;      Fpsiparam{2}=OPHr.alpha;      Fpsiparam{3}=OPHr.betaT2;      Fpsiparam{4}=OPHr.varphi2;
            Fxiparam{1}=OPHr.gamma0;       Fxiparam{2}=OPHr.gamma;       Fxiparam{3}=OPHr.betaT3;       Fxiparam{4}=OPHr.varphi3;
            [fmut,fpsit,fxit]=amevaOPH.modelfunction(LocScaleShapeState,Fmuparam,Fpsiparam,Fxiparam);
            text(0,0.440,' Model Fitted:','FontSize',11,'Parent',gcax,'HorizontalAlignment','left');
            text(0.5,0.390,fmut,'Interpreter',interprete,'FontSize',11,'Parent',gcax,'HorizontalAlignment','Center');
            text(0.5,0.315,fpsit,'Interpreter',interprete,'FontSize',11,'Parent',gcax,'HorizontalAlignment','Center');
            text(0.5,0.240,fxit,'Interpreter',interprete,'FontSize',11,'Parent',gcax,'HorizontalAlignment','Center');
            %RESULTADOS/Results
            interprete='tex';
            [fmut,fpsit,fxit]=amevaOPH.modelfunction(LocScaleShapeState,Fmuparam,Fpsiparam,Fxiparam,'result');
            text(0,0.205,' Results:','FontSize',11,'Parent',gcax,'HorizontalAlignment','left');
            text(0.5,0.150,fmut,'Interpreter',interprete,'FontSize',10,'Parent',gcax,'HorizontalAlignment','Center');
            text(0.5,0.085,fpsit,'Interpreter',interprete,'FontSize',10,'Parent',gcax,'HorizontalAlignment','Center');
            text(0.5,0.020,fxit,'Interpreter',interprete,'FontSize',10,'Parent',gcax,'HorizontalAlignment','Center');
            axis(gcax,'off');
            
        end
        %% Latex text selected type fucntion adjust
        function [fmut,fpsit,fxit,fcdf,icdf] = modelfunction(varargin)
            fmut='';fpsit='';fxit='';fcdf='';
            %Fmuparam(1:4)={1};Fpsiparam(1:4)={1};Fxiparam(1:2)={1};
            if nargin==1,
                LocScaleShapeState=varargin{1};
                Fmuparam(1:4)={[]};Fpsiparam(1:4)={[]};Fxiparam(1:4)={[]};
                Fmuparam{1}=1;  Fpsiparam{1}=1; Fxiparam{1}=1;
                if  LocScaleShapeState(1),
                    Fmuparam{2}=1;
                    Fpsiparam{2}=1;
                    Fxiparam{2}=1;
                end
                if  LocScaleShapeState(2)>0,
                    Fmuparam{3}=1;
                    Fpsiparam{3}=1;
                    if LocScaleShapeState(2)>1,
                        Fxiparam{3}=1;
                    end
                end
                if  LocScaleShapeState(3)>0,
                    Fmuparam{4}=1;
                    Fpsiparam{4}=1;
                    if LocScaleShapeState(3)>1,
                        Fxiparam{4}=1;
                    end
                end
            elseif nargin==4,
                LocScaleShapeState=varargin{1};
                Fmuparam=varargin{2};Fpsiparam=varargin{3};Fxiparam=varargin{4};
            elseif nargin==5,
                LocScaleShapeState=varargin{1};
                Fmuparam=varargin{2};Fpsiparam=varargin{3};Fxiparam=varargin{4};
            else
                return;
            end
            %         fmut='$$\mu(t)=\beta_o+\sum_{i=1}\left[\beta_{2i-1}\cos(i\omega t)+\beta_{2i}\sin(i\omega t)\right]+\beta_{T1}t+\sum_k\phi_kn_{k,t}$$';
            %         fpsit='$$\psi(t)=\exp(\alpha_o+\sum_{i=1}\left[\alpha_{2i-1}\cos(i\omega t)+\alpha_{2i}\sin(i\omega t)\right]+\beta_{T2}t+\sum_k\phi_kn_{k,t})$$';
            %         fxit='$$\xi(t)=\gamma_o+\sum_{i=1}\left[\gamma_{2i-1}\cos(i\omega t)+\gamma_{2i}\sin(i\omega t)\right]$$';
            fcdf='$$F(x;\mu,\psi,\xi)=\exp\left\{\!^{\_}\left[1+\xi\left(\frac{x\!^{\_}\mu}{\psi}\right)\right]^{\!^{\_}1/\xi}\right\};\xi\not=0,\exp\left\{\!^{\_}\exp\left[\!^{\_}\left(\frac{x\!^{\_}\mu}{\psi}\right)\right]\right\};\xi=0$$';
            icdf='$$H_s(T_r;\mu,\psi,\xi)=\mu \!^{\_} \frac{\psi}{\xi}(1 \!^{\_} (\frac{1}{T_r})\;^{\!^{\_}\xi} )$$';
            
            Fmu='\mu(t)';
            Fbeta{1}='\beta_o';
            Fbeta{2}='\sum_{i=1}\left[\beta_{2i\!^{\_}1}\cos(i\omega t)+\beta_{2i}\sin(i\omega t)\right]';
            Fbeta{3}='\beta_{T1}t';
            Fbeta{4}='\sum_k\phi_kn_{k,t}';
            Fpsi='\psi(t)';
            Falpha{1}='\alpha_o';
            Falpha{2}='\sum_{i=1}\left[\alpha_{2i\!^{\_}1}\cos(i\omega t)+\alpha_{2i}\sin(i\omega t)\right]';
            Falpha{3}='\beta_{T2}t';
            Falpha{4}='\sum_k\phi_kn_{k,t}';
            Fxi='\xi(t)';
            Fgamma{1}='\gamma_o';
            Fgamma{2}='\sum_{i=1}\left[\gamma_{2i\!^{\_}1}\cos(i\omega t)+\gamma_{2i}\sin(i\omega t)\right]';
            if LocScaleShapeState(2)>1, Fgamma{3}='\beta_{T3}t'; end
            if LocScaleShapeState(3)>1, Fgamma{4}='\sum_k\phi_kn_{k,t}'; end
            
            fmut='';
            for i=1:length(Fmuparam),
                if not(isempty(Fmuparam{i})),
                    fmut=[fmut Fbeta{i} '+'];
                end
            end
            if not(isempty(fmut))
                if strcmp(fmut(end),'+'), fmut(end)=''; end
                fmut=['$$' Fmu '=' fmut '$$'];
            end
            
            fpsit='';
            for i=1:length(Fpsiparam),
                if not(isempty(Fpsiparam{i})),
                    fpsit=[fpsit Falpha{i} '+'];
                end
            end
            if not(isempty(fpsit))
                if strcmp(fpsit(end),'+'), fpsit(end)=''; end
                fpsit=['$$' Fpsi '=\exp\;(\;' fpsit '\;)$$'];
            end
            
            fxit='';
            for i=1:length(Fxiparam),
                if not(isempty(Fxiparam{i})),
                    fxit=[fxit Fgamma{i} '+'];
                end
            end
            if not(isempty(fxit))
                if strcmp(fxit(end),'+'), fxit(end)=''; end
                fxit=['$$' Fxi '=' fxit '$$'];
            end
            %solo si nargin == 5 input
            if nargin==5
                forpres='%2.3f';
                fmut='';
                if not(isempty(Fmuparam{1})),
                    fmut=[fmut ' \beta_o=' num2str(Fmuparam{1},forpres) ','];
                end
                if not(isempty(Fmuparam{2})),
                    for i=1:length(Fmuparam{2}), fmut=[fmut ' \beta_' num2str(i) '=' num2str(Fmuparam{2}(i),forpres) ',']; end
                end
                if not(isempty(Fmuparam{3})),
                    fmut=[fmut ' \beta_{T1}=' num2str(Fmuparam{3},forpres) ','];
                end
                if not(isempty(Fmuparam{4})),
                    for i=1:length(Fmuparam{4}), fmut=[fmut ' \phi_' num2str(i) '=' num2str(Fmuparam{4}(i),forpres) ',']; end
                end
                if not(isempty(fmut)),
                    fmut(end)='';
                    %fmut=['$$' fmut '$$'];
                end
                fpsit='';
                if not(isempty(Fpsiparam{1})),
                    fpsit=[fpsit ' \alpha_o=' num2str(Fpsiparam{1},forpres) ','];
                end
                if not(isempty(Fpsiparam{2})),
                    for i=1:length(Fpsiparam{2}), fpsit=[fpsit ' \alpha_' num2str(i) '=' num2str(Fpsiparam{2}(i),forpres) ',']; end
                end
                if not(isempty(Fpsiparam{3})),
                    fpsit=[fpsit ' \beta_{T2}=' num2str(Fpsiparam{3},forpres) ','];
                end
                if not(isempty(Fpsiparam{4})),
                    for i=1:length(Fpsiparam{4}), fpsit=[fpsit ' \phi_' num2str(i) '=' num2str(Fpsiparam{4}(i),forpres) ',']; end
                end
                if not(isempty(fpsit)),
                    fpsit(end)='';
                    %fpsit=['$$' fpsit '$$'];
                end
                fxit='';
                if not(isempty(Fxiparam{1})),
                    fxit=[fxit ' \gamma_o=' num2str(Fxiparam{1}) ','];
                end
                if not(isempty(Fxiparam{2})),
                    for i=1:length(Fxiparam{2}), fxit=[fxit ' \gamma_' num2str(i) '=' num2str(Fxiparam{2}(i),forpres) ',']; end
                end
                if not(isempty(Fxiparam{3})),
                    fxit=[fxit ' \beta_{T2}=' num2str(Fxiparam{3},forpres) ','];
                end
                if not(isempty(Fxiparam{4})),
                    for i=1:length(Fxiparam{4}), fxit=[fxit ' \phi_' num2str(i) '=' num2str(Fxiparam{4}(i),forpres) ',']; end
                end
                if not(isempty(fxit)),
                    fxit(end)='';
                    %fxit=['$$' fxit '$$'];
                end
            end
        end
        %% armonicos   funciones auxiliare
        function [pos,nmu,npsi,neps] = selectarmon(OPHobj,OPHr)
            %   Step 4: Calculate the sensitivities of the optimal log-likelihood
            %   objective function with respect to possible additional harmonics
            %   for the location, scale and shape parameters. Note that the new
            %   parameter values are set to zero since derivatives do not depend on them.
            %     [auxf auxJx] = Jacobian (OPHobj.x,OPHobj.t,[],[],beta0,[beta;0;0],alpha0,[alpha;0;0],gamma0,[gamma;0;0],[],[],[],[]);
            neps0=0;
            if OPHr.pS.chi.alpha0~=0, neps0 = 1; end
            
            OPHobj.beta = [OPHobj.beta;0;0];
            OPHobj.alpha = [OPHobj.alpha;0;0];
            OPHobj.gamma = [OPHobj.gamma;0;0];
            
            [auxf,auxJx,auxHxx] = amevaloglikelihood ([],OPHobj);
            auxJx = -auxJx;
            
            nmu = length(OPHr.beta)/2; npsi = length(OPHr.alpha)/2; neps = length(OPHr.gamma)/2;
            ntend = 0; ntend2 = 0; nind = 0; nind2 = 0;
            
            %             [auxf auxJx auxHxx] = loglikelihood (OPHobj.x,OPHobj.t,kt,beta0,[beta;0;0],alpha0,[alpha;0;0],gamma0,[gamma;0;0],[],[],[],[],[],[]);
            %   Inverse of the Information Matrix
            %invI0_ = inv(auxHxx);
            invI0_= amevaOPH.invI0(auxHxx);
            %   Step 5: Calculate maximum perturbation
            pos = 1;
            %estas dos lineas funcionan peor que las de c++
            %maximumval = abs(auxJx(1+2*nmu+1:1+2*nmu+2)'*invI0_(1+2*nmu+1:1+2*nmu+2,1+2*nmu+1:1+2*nmu+2)*auxJx(1+2*nmu+1:1+2*nmu+2));
            %auxmax = abs(auxJx(2+2*nmu+ntend+nind+2*npsi+2+1:2+2*nmu+ntend+nind+2*npsi+2+2)'*invI0_(2+2*nmu+ntend+nind+2*npsi+2+1:2+2*nmu+ntend+nind+2*npsi+2+2,2+2*nmu+ntend+nind+2*npsi+2+1:2+2*nmu+ntend+nind+2*npsi+2+2)*auxJx(2+2*nmu+ntend+nind+2*npsi+2+1:2+2*nmu+ntend+nind+2*npsi+2+2));
            %modificar para tener lo mismo que el codigo en c++ OJO OJO%igual
            maximumval = sum(abs(auxJx(1+2*nmu+1:1+2*nmu+2)));%que c++
            auxmax = sum(abs(auxJx(2+2*nmu+ntend+nind+2*npsi+2+1:2+2*nmu+ntend+nind+2*npsi+2+2)));%que c++
            if auxmax>maximumval,
                maximumval=auxmax;
                pos = 2;
            end
            auxmax = abs(auxJx(2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+1:2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+2)'*invI0_(2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+1:2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+2,2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+1:2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+2)*auxJx(2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+1:2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+2));
            %auxmax = sum(abs(auxJx(2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+1:2+neps0+2*nmu+ntend+nind+ntend2+nind2+2*npsi+2*neps+4+2)));
            if auxmax>maximumval,
                maximumval=auxmax;
                pos = 3;
            end
            %     end
            %   If the maximum perturbation corresponds to location, include a new
            %   harmonic
            if pos ==1,
                nmu = nmu+1;
            end
            %   If the maximum perturbation corresponds to scale, include a new
            %   harmonic
            if pos ==2,
                npsi = npsi+1;
            end
            %   If the maximum perturbation corresponds to shape, include a new
            %   harmonic
            if pos ==3,
                neps = neps+1;
            end
        end
        %% covariables funciones auxiliaes Busca y seleccona el varphi que mas perturba
        function [varphi,lista,varphi2,lista2,varphi3,lista3] = selectvarphi(OPHobj,OPHr,auxnind)
            % buscar varphi
            OPHobj.varphi=amevaOPH.setinivarphi(OPHr.varphi,OPHr.lista,auxnind);
            OPHobj.lista=1:auxnind;
            OPHobj.varphi2=amevaOPH.setinivarphi(OPHr.varphi2,OPHr.lista2,auxnind);
            OPHobj.lista2=1:auxnind;
            if nargout>4,
                OPHobj.varphi3=amevaOPH.setinivarphi(OPHr.varphi3,OPHr.lista3,auxnind);
                OPHobj.lista3=1:auxnind;
            end
            %   Step 10: Include in the parameter vector the corresponding covariate
            neps0=0; if OPHr.gamma0~=0, neps0=1; end            
            nmu=length(OPHr.beta)/2;  npsi=length(OPHr.alpha)/2;  neps=length(OPHr.gamma)/2;
            ntend=length(OPHr.betaT); ntend2=length(OPHr.betaT2); ntend3=length(OPHr.betaT3);
            [auxf,auxJx,auxHxx] = amevaloglikelihood([],OPHobj);
            invI0_ = auxHxx;
            [maximo,pos]= max(abs(auxJx(2+2*nmu+ntend:1+2*nmu+ntend+auxnind).^2./diag(invI0_(2+2*nmu+ntend:1+2*nmu+ntend+auxnind,2+2*nmu+ntend:1+2*nmu+ntend+auxnind))));
            [maximo2,pos2]= max(abs(auxJx(2+2*nmu+ntend+auxnind+2*npsi+1:2+2*nmu+ntend+auxnind+2*npsi+auxnind).^2./diag(invI0_(2+2*nmu+ntend+auxnind+2*npsi+1:2+2*nmu+ntend+auxnind+2*npsi+auxnind,2+2*nmu+ntend+auxnind+2*npsi+1:2+2*nmu+ntend+auxnind+2*npsi+auxnind))));
            if nargout>4
                [maximo3,pos3]= max(abs(auxJx(2+neps0+2*nmu+ntend+auxnind+2*npsi+auxnind+1:2+neps0+2*nmu+ntend+auxnind+2*npsi+auxnind+auxnind).^2./diag(invI0_(2+neps0+2*nmu+ntend+auxnind+2*npsi+auxnind+1:2+neps0+2*nmu+ntend+auxnind+2*npsi+auxnind+auxnind,2+neps0+2*nmu+ntend+auxnind+2*npsi+auxnind+1:2+neps0+2*nmu+ntend+auxnind+2*npsi+auxnind+auxnind))));
%                 auxJx=-auxJx;
%                 [maximo  pos]=  max(abs(auxJx(2+2*nmu+ntend:1+2*nmu+ntend+auxnind)));
%                 [maximo2 pos2]= max(abs(auxJx(2+2*nmu+ntend+auxnind+2*npsi+1:2+2*nmu+ntend+auxnind+2*npsi+auxnind)));
%                 [maximo3 pos3]= max(abs(auxJx(2+neps0+2*nmu+ntend+auxnind+2*npsi+auxnind+1:2+neps0+2*nmu+ntend+auxnind+2*npsi+auxnind+auxnind)));
            end
            
            % seleccionar varphi
            varphi =OPHr.varphi;
            varphi2=OPHr.varphi2;
            varphi3=OPHr.varphi3;
            lista=OPHr.lista;
            lista2=OPHr.lista2;
            lista3=OPHr.lista3;
            if nargout>4,
                if maximo>max(maximo2,maximo3),
                    varphi = [OPHr.varphi; 0];
                    lista = [OPHr.lista; pos];
                elseif maximo2>max(maximo,maximo3)
                    varphi2=[OPHr.varphi2;0];
                    lista2 = [OPHr.lista2; pos2];
                elseif maximo3>max(maximo,maximo2)
                    varphi3=[OPHr.varphi3;0];
                    lista3 = [OPHr.lista3; pos3];
                else
                    error('no se puede seleccionar un maximo. amevaOPH.selectvarphi linea1771');
                end
            else
                if maximo>maximo2,
                    varphi = [OPHr.varphi; 0];
                    lista = [OPHr.lista; pos];
                else
                    varphi2=[OPHr.varphi2;0];
                    lista2 = [OPHr.lista2; pos2];
                end
            end
        end
        function auxvarphi=setinivarphi(varphi,lista,auxnind)
            auxvarphi = zeros(auxnind,1);
            if ~isempty(lista),
                auxvarphi(lista) = varphi;
            end
        end
    end
    methods (Access = public, Static = true)
        function SetModelo_neps0(modelo)
            if isempty(modelo), return; end
            global neps0;
            if strcmp(modelo,'Gumbel')
                neps0 = 0; %Forzamos Gumbel nesp0=0;
            elseif strcmp(modelo,'FrechetWeibull')
                neps0 = 1; %Forzamos Frechet/Weibull nesp0=1;
            end
        end
        function [obj,OPHr,status] = AOPH_model(obj,Objs,nit,modelo,criterio,confidenceLevel)
            amevaOPH.SetModelo_neps0(modelo);%Set the value of neps0
            if strcmp(modelo,'Auto') && nit==1, %modelo estacionario auto, busca el mejor modelo nesp0~=0;
                [OPHr,status] = amevaOPH.autoOPH(Objs,confidenceLevel,criterio,[modelo '0']);
                obj=amevaOPH.asigntoresult(obj,OPHr,nit);
            elseif not(isempty(modelo)) && nit>1, %modelo auto nit>1
                [OPHr,status] = amevaOPH.autoOPH(Objs,confidenceLevel,criterio,modelo,obj.OPHr);
                obj=amevaOPH.asigntoresult(obj,OPHr,nit);
            else
                % Modelo fijo no se necesita ni criterio ni confidenceLevel, modelo=[]
                OPHr =amevaOPH.OptiParamHessian(Objs); status=[]; disp(modelo);
                if not(isempty(obj)),
                    obj=amevaOPH.asigntoresult(obj,OPHr,nit);
                else
                    obj=OPHr;
                end
                return;
            end
        end
        function [obj,status] = autoOPH(Obj,confidenceLevel,criterio,modelo,Objs_old)
            global neps0;
            switch modelo
                case 'Auto0' %escogemos el modelo de forma auto segun criterio
                    neps0 = 0;
                    OPH_obj_rold =amevaOPH.OptiParamHessian (Obj);
                    neps0 = 1;
                    OPH_obj_rnew =amevaOPH.OptiParamHessian (Obj);
                otherwise %case 'Auto' %escogemos el modelo de forma auto segun criterio
                    OPH_obj_rold =Objs_old;
                    OPH_obj_rnew =amevaOPH.OptiParamHessian (Obj);
            end
            %si hay criterio calculo el mejor
            [tf,OPH_obj_rnew.gamma0]=amevaOPH.check_gumbel_model(OPH_obj_rnew.gamma0); %   Check if the corresponding model is NO GUMBEL (1-true) set global neps0
            ctrconf = set_ctrconf(criterio,OPH_obj_rold.loglike,OPH_obj_rnew.loglike,confidenceLevel,OPH_obj_rold.np,OPH_obj_rnew.np);
            status=check_criterio(ctrconf);
            if strcmp(modelo,'Auto0') && status
                obj=OPH_obj_rnew; %si cumple el criterio me quedo con el resultado ultimo
            elseif  strcmp(modelo,'Auto0') && tf
                neps0 = 0; % me quedo con el resultado anterior sin neps0=0;
                obj=OPH_obj_rold; %no cumple el criterio me quedo con el resultado anterior
            elseif status==true
                obj=OPH_obj_rnew; %si cumple el criterio me quedo con el resultado ultimo
            elseif status==false
                obj=OPH_obj_rold; %no cumple el criterio me quedo con el resultado anterior
            end
        end
        
    end
    methods (Static = true)
        function ParSol_IT = asigntoresult(ParSol_IT,OPHr,nit)
            global neps0;
            ParSol_IT.beta0IT(nit) = OPHr.beta0;
            ParSol_IT.alpha0IT(nit) = OPHr.alpha0;
            ParSol_IT.gamma0IT (nit) = 0;
            if neps0 == 1 && ~isempty(OPHr.gamma0),
                ParSol_IT.gamma0IT (nit) = OPHr.gamma0;
            end
            asigntovector('beta',true);
            asigntovector('alpha',true);
            asigntovector('gamma',true);
            asigntovector('betaT',false);
            asigntovector('varphi',true);
            asigntovector('betaT2',false);
            asigntovector('varphi2',true);
            asigntovector('betaT3',false);
            asigntovector('varphi3',true);
            
            ParSol_IT.npIT(nit) = OPHr.np;
            ParSol_IT.AIKIT (nit)= -2*(-OPHr.loglike)+2*(OPHr.np);
            ParSol_IT.loglikIT (nit) = -OPHr.loglike;
            
            ParSol_IT.OPHr=OPHr;
            function asigntovector(vn,tipo)
                if ~isempty(OPHr.(vn)) && tipo==true
                    ParSol_IT.([vn 'IT'])(1:length(OPHr.(vn)),nit)=OPHr.(vn);
                elseif ~isempty(OPHr.(vn)) && tipo==false
                    ParSol_IT.([vn 'IT'])(nit)=OPHr.(vn);
                end
            end
        end
        % Check if the corresponding model is GUMBEL
        function [tf_model,gamma0] = check_gumbel_model(gamma0)
            global neps0;
            if isempty(gamma0),
                neps0 = 0;
                tf_model = false;
                gamma0 = [];
            else
                if abs(gamma0) <= amevaOPH.GumbelTol,
                    neps0 = 0;
                    tf_model = false;
                    gamma0 = [];
                else
                    neps0 = 1;
                    tf_model = true;
                end
            end
        end
    end
end
function obj = obj_proflike(ctrconf) %PROF-LOGLIKELIHOOD
obj = 2*(-ctrconf.loglikeobj_new+ctrconf.loglikeobj_old) - chi2inv(ctrconf.confidenceLevel,max(1,abs(ctrconf.np_new-ctrconf.np_old)));
end
function obj = obj_akaike(ctrconf) %AKAIKE-2*(-loglikeobj)+2*(2+neps0+2*nmu+2*npsi+2*neps+ntend+nind+ntend2+nind2)
obj = ( -2*(-ctrconf.loglikeobj_old)+2*(ctrconf.np_old) ) - ( -2*(-ctrconf.loglikeobj_new)+2*(ctrconf.np_new) );
end
function obj = check_criterio(ctrconf) %Criterios
obj = false;
if strcmpi(ctrconf.criterio,'ProfLike') && obj_proflike(ctrconf)>0,
    obj = true;
elseif strcmpi(ctrconf.criterio,'Akaike') && obj_akaike(ctrconf)>0,
    obj = true;
end
end
function obj = set_ctrconf(criterio,loglike_obj_old,loglike_obj_new,confidenceLevel,np_old,np_new) %Set Criterio conf
if nargin==0, obj.criterio = []; obj.loglikeobj_old = []; obj.confidenceLevel = []; obj.np_old = []; obj.np_new = []; return; end
obj.criterio = criterio;
obj.loglikeobj_old = loglike_obj_old;
obj.loglikeobj_new = loglike_obj_new;
obj.confidenceLevel = confidenceLevel;
obj.np_old = np_old;
obj.np_new = np_new;
end