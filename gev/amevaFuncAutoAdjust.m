classdef amevaFuncAutoAdjust < handle
    methods (Access = public, Static = true)
        
        function [kt,info,confidenceLevel,datemax,criterio,modelo,Km,Ny,datemax_j] = Initialize(Hsmax,Time,kt,indices,info,confidenceLevel,opciones)
            
            error(nargchk(2,9,nargin));
            
            %Comprobamos que los datos temporales estan datenum de matlab o resc. year
            datemax_j = [];
            awinc=amevaclass;%ameva windows commons methods
            awinc.AmevaTimeCheck([],Time);
            if awinc.timestate==true,
                datemax=yearlytimescale(Time,'N');
                datemax_j = Time;
                disp('los datos temporales estan en formato datenum de matlab y se han rescalados a anhos!');
            elseif awinc.timestate==false,
                datemax=Time;
                disp('los datos temporales estan rescalados a anhos!');
            else
                error('los datos temporales deben estar en formato datenum de matlab o rescalados anhos!');
            end%maximum annual time scale normalizado para usar GEV
            
            %encuetro el numero total de anhos y la frecuencia temporal de los datos
            %solo se usa en el pos-proceso
            Km=amevaOPH.FrecTemporal(datemax,Time,awinc.timestate);
            Ny=ceil(datemax(end));%   Ny -> numero de anhos
            disp(['Years Ny=' num2str(Ny)]);

            
            if ~exist('kt','var') || isempty(kt),  kt = ones(size(Hsmax)); end
            if ~exist('indices','var') || isempty(indices), indices = []; end
            if ~exist('info','var') || isempty(info), info=[1 1 1 1 12 1 0 0 0 0]; end %(profilike/akaike,arm,tend,cov,auto/man-arm,man-armL,man-armE,man-armF,0/1/2-Auto/0/not0 g0-)
            if ~exist('confidenceLevel','var') || isempty(confidenceLevel), confidenceLevel = 0.95; end
            if confidenceLevel<0.5, error(['confidenceLevel=',num2str(confidenceLevel)]); end
            if sum(opciones.svopt)>0, if ~exist([pwd filesep opciones.carpeta],'dir'),  mkdir(opciones.carpeta); end; end
            %Comprobar que queremos covariables en Shape info(4)==2 -> info(10)==2, Forzamos Frechet/Weibull.
            if info(4)>1, info(10) = 2; end
            % Seleccion del modelo
            modelo = 'Auto'; %Modelo automatico al buscar Gamma0
            if info(10)==1
                modelo = 'Gumbel'; %Forzamos Gumbel Gamma0=0;
            elseif info(10)==2
                modelo = 'FrechetWeibull'; %Forzamos Frechet/Weibull Gamma0>0/Gamma0<0;
            end
            % Seleccion del criterio ProfLike/Akaike
            criterio = 'ProfLike';
            if info(1)==0, criterio = 'Akaike'; end
            
            if abs(Km-1)<2/100,     info(2)=0; end %Si los datos son anuales (Km=1) no se calculan armonicos (intra-anual)
            if isempty(indices),    info(4)=0; end %Si indices esta vacio info(4)=0
            
            if length(Hsmax)~=length(datemax),
                error('Data Hsmax and datemax must be the same length');
            end
            if ~isempty(indices),
                if length(Hsmax)~=length(indices(:,1)),
                    error('Data indices must be coherent with Hsmax and datemax');
                end
            end
        end
        
        function [OPHr,OPHobj,parametros_all,IT,tpstring] = AutoAjuste(Hsmax,Time,kt,indices,info,confidenceLevel,opciones)
            global neps0;
            %   Comprobamos las variables de inicio
            [kt,info,confidenceLevel,datemax,criterio,modelo,Km,Ny,datemax_j] = ...
                amevaFuncAutoAdjust.Initialize(Hsmax,Time,kt,indices,info,confidenceLevel,opciones);
            
            %   Paramtetros para almacenar la evolucion del proceso
            IT.beta0IT = [];
            IT.betaIT = [];
            IT.alpha0IT = [];
            IT.alphaIT = [];
            IT.gamma0IT = [];
            IT.gammaIT = [];
            IT.betaTIT  = [];
            IT.varphiIT = [];
            IT.betaT2IT  = [];
            IT.varphi2IT = [];
            IT.betaT3IT  = [];
            IT.varphi3IT = [];
            IT.npIT = [];
            IT.AIKIT = [];
            IT.loglikIT = [];
            IT.OPHr=[]; %storage last solution
            %% parallel ini
            if opciones.poolparalelo && matlabpool('size')==0, matlabpool open; end
            %% MODELO ESTACIONARIO
            OPHobj = amevaOPH.OPHstruct(Hsmax,datemax,kt,[],criterio,confidenceLevel); % Preparamos los datos
            nit=1;
            [IT,OPHr]=amevaOPH.AOPH_model(IT,OPHobj,nit,modelo,criterio,confidenceLevel);
            OPHobj=amevaOPH.OPHrToOPHobj(OPHobj,OPHr);% asigno la solucion a los datos
            
            %%   HARMONIC Iterative process
            %   Step 1:
            %   Initial number of harmonics to be introduced, no harmonics, no covariates and no trends
            itermax = 100;%   Maximum number of iterations
            if info(2), %armonico
                if info(6), %0=false -> manual o 1=true -> autoarmonicos
                    for i = 1:itermax,
                        %   Step 2: Obtaining the maximum likelihood estimators for the selected parameters
                        pos=amevaOPH.selectarmon(OPHobj,OPHr);
                        if pos==1, OPHr.ps.beta=[OPHr.beta;0;0];  OPHobj.beta=OPHr.ps.beta; end
                        if pos==2, OPHr.ps.alpha=[OPHr.alpha;0;0];OPHobj.alpha=OPHr.ps.alpha; end
                        if pos==3, OPHr.ps.gamma=[OPHr.gamma;0;0];OPHobj.gamma=OPHr.ps.gamma; end
                        OPHobj.pv=amevaOPH.pstovector(OPHr.ps);
                        
                        nit=nit+1;
                        [IT,OPHr,status]=amevaOPH.AOPH_model(IT,OPHobj,nit,modelo,criterio,confidenceLevel);
                        if status==false,
                            nit=nit-1;
                            OPHobj=amevaOPH.OPHrToOPHobj(OPHobj,OPHr);% asigno la solucion a los datos
                            break;
                        end
                        %   Model update and convergence criterion
                        if i==1 && pos==3,
                            %    Variation of the shape parameter is not allow if any of the remaining
                            %    parameters do not change also in time
                            disp('1era iter de ajuste de armonicos pos gamma');
                            ST = dbstack('-completenames');
                            disp([{ST.file}',{ST.name}.',{ST.line}.']);
                            break;
                        end
                    end
                else
                    OPHobj.beta=zeros(info(7)*2,1);
                    OPHobj.alpha=zeros(info(8)*2,1);
                    OPHobj.gamma=zeros(info(9)*2,1);
                    OPHobj.pv=[]; % OPHobj.pv=[] -> pini=[]
                    nit=nit+1;
                    [IT,OPHr]=amevaOPH.AOPH_model(IT,OPHobj,nit,[]);
                    OPHobj=amevaOPH.OPHrToOPHobj(OPHobj,OPHr);% asigno la solucion a los datos
                end
            end
            %%   info(4)-COVARIATES Iterative process
            if info(4)>0,
                OPHobj.indices=indices;
                %     OPHobj.indicesF=indicesF;
                [nd auxnind] = size(indices);
                %   Starting COVARIATES iterative process (Location, Scale and Shape)
                OPHr.varphi=[]; OPHr.varphi2=[]; OPHr.varphi3=[];
                OPHr.lista=[]; OPHr.lista2=[]; OPHr.lista3=[];
                for i = 1:auxnind,
                    if info(4)>1
                        [varphi,lista,varphi2,lista2,varphi3,lista3]=amevaOPH.selectvarphi(OPHobj,OPHr,auxnind);
                        %   Step 9: Obtain the maximum-likelihood estimators for the selected
                        OPHr.ps.varphi=varphi;
                        OPHr.ps.varphi2=varphi2;
                        OPHr.ps.varphi3=varphi3;
                        OPHobj.pv=amevaOPH.pstovector(OPHr.ps);
                        OPHobj.lista=lista;
                        OPHobj.lista2=lista2;
                        OPHobj.lista3=lista3;
                        OPHobj.varphi=varphi;
                        OPHobj.varphi2=varphi2;
                        OPHobj.varphi3=varphi3;
                    else
                        [varphi,lista,varphi2,lista2]=amevaOPH.selectvarphi(OPHobj,OPHr,auxnind);
                        %   Step 9: Obtain the maximum-likelihood estimators for the selected
                        OPHr.ps.varphi=varphi;
                        OPHr.ps.varphi2=varphi2;
                        OPHr.ps.varphi3=[];
                        OPHobj.pv=amevaOPH.pstovector(OPHr.ps);
                        OPHobj.lista=lista;
                        OPHobj.lista2=lista2;
                        OPHobj.lista3=[];
                        OPHobj.varphi=varphi;
                        OPHobj.varphi2=varphi2;
                        OPHobj.varphi3=[];
                    end
                    nit=nit+1;
                    [IT,OPHr,status]=amevaOPH.AOPH_model(IT,OPHobj,nit,modelo,criterio,confidenceLevel);
                    if status==false, %toca quedarse con la solucion anterior y seguir y/o salir
                        nit=nit-1;
                        OPHobj=amevaOPH.OPHrToOPHobj(OPHobj,OPHr);% asigno la solucion a los datos
                    end
                end
            end
            %%   info(3)-TENDENCIAS trends
            if info(3)>0,
                %%%%%%%%%%%         Step 12: Location trend
                OPHr.ps.betaT=0;
                OPHobj.pv=amevaOPH.pstovector(OPHr.ps);
                OPHobj.betaT=0;
                nit=nit+1;
                [IT,OPHr,status]=amevaOPH.AOPH_model(IT,OPHobj,nit,modelo,criterio,confidenceLevel);
                if status==false, %toca quedarse con la solucion anterior y seguir y/o salir
                    nit=nit-1;
                    OPHobj=amevaOPH.OPHrToOPHobj(OPHobj,OPHr);% asigno la solucion a los datos
                end
                %%%%%%%%%%%         Step 15: Scale trend
                OPHr.ps.betaT2=0;
                OPHobj.pv=amevaOPH.pstovector(OPHr.ps);
                OPHobj.betaT2=0;
                nit=nit+1;
                [IT,OPHr,status]=amevaOPH.AOPH_model(IT,OPHobj,nit,modelo,criterio,confidenceLevel);
                if status==false, %toca quedarse con la solucion anterior y seguir y/o salir
                    nit=nit-1;
                    OPHobj=amevaOPH.OPHrToOPHobj(OPHobj,OPHr);% asigno la solucion a los datos
                end
                %%%%%%%%%%         Step 15 OC: Shape trend
                if info(3)>1
                    OPHr.ps.betaT3=0;
                    OPHobj.pv=amevaOPH.pstovector(OPHr.ps);
                    OPHobj.betaT3=0;
                    nit=nit+1;
                    [IT,OPHr,status]=amevaOPH.AOPH_model(IT,OPHobj,nit,modelo,criterio,confidenceLevel);
                    if status==false, %toca quedarse con la solucion anterior y seguir y/o salir
                        nit=nit-1;
                        OPHobj=amevaOPH.OPHrToOPHobj(OPHobj,OPHr);% asigno la solucion a los datos
                    end
                end
            end
            if opciones.poolparalelo && matlabpool('size')>0; matlabpool close; end %% parallel fin
            
            [OPHr.loglike,OPHr.gradiente,OPHr.Hessiano] = amevaloglikelihood ([],OPHobj); % log, grad, hess de la solucion 
            OPHr.invI0 = amevaOPH.invI0(OPHr.Hessiano); %OJO OJO ,OPHr.np Calculo invI0 al resultado
            %%%%%%%%%%%%%%%%%%%%%%%%%% THE END Solution %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            OPHobj=amevaOPH.OPHrToOPHobj(OPHobj,OPHr);% asigno la solucion a los datos
            OPHobj.Ny=Ny; OPHobj.Km=Km; %n-anhos y frec temporal
            OPHobj.datemax_j = datemax_j;
            %%%%%%%%%%%%%%%%%% POST PROSECOS PLOTS AND SOLUTION %%%%%%%%%%%%%%%%%%%%%%
            [OPHobj,parametros_all,tpstring] = ...
                amevaFuncAutoAdjust.PosProsPlot(OPHr,OPHobj,nit,neps0,IT,info,criterio,confidenceLevel,opciones);
            
        end
        
        %% POST PROSECOS PLOTS AND SOLUTION
        function [OPHobj,parametros_all,tpstring] = PosProsPlot(OPHr,OPHobj,nit,neps0,IT,info,criterio,confidenceLevel,opciones)            
            % Solucion %  Best parameter vector is in --> OPHr.pv;
            [solucion,rowLabels,columnLabels]=amevaOPH.texsolutionpot(IT,OPHr,nit,neps0);

            % Ts vector para calcular los periodos de retorno
            if ~isempty(opciones.Ts), OPHobj.Ts=opciones.Ts; end
            %   Plots String name for the plots
            tpstring=cell(1,2);
            tpstring{1,1}='LocalScaleParmPlot';
            tpstring{1,2}='ScaleParmPlot';
            if not(isempty(OPHr.gamma0)),  tpstring{1,end+1}='FormParmPlot'; end
            tpstring{1,end+1}='QQplot';
            tpstring{1,end+1}='PPplot';
            tpstring{1,end+1}='GevParamSolution';
            if opciones.myPlots > 0, tpstring{1,end+1}='AggregateQuantiles'; end
            if opciones.myPlots > 1, tpstring{1,end+1}='ParameterEvolution'; end
            tpstring{1,end+1}='Hsmax';
            %  gev preplots
            [quan95,stdmut,stdpsit,stdepst,stdDq,mut,psit,epst,datemaxanual]=amevaOPH.gevpreplot(OPHobj,OPHr,neps0,confidenceLevel);
            
            %% all parametros
            parametros_all.stdpara=sqrt(diag(OPHr.invI0)); % Standard deviation
            parametros_all.solucion=solucion;
            parametros_all.rowLabels=rowLabels;
            parametros_all.columnLabels=columnLabels;
            parametros_all.quan95=quan95;
            parametros_all.stdmut=stdmut;
            parametros_all.stdpsit=stdpsit;
            parametros_all.stdepst=stdepst;
            parametros_all.stdDq=stdDq;
            parametros_all.mut=mut;
            parametros_all.psit=psit;
            parametros_all.epst=epst;
            parametros_all.datemaxanual=datemaxanual;
            parametros_all.stdDq=stdDq;
            
            %% Salida guardamos datos y plots
            if sum(opciones.svopt)>0,
                %Save file
                save(fullfile(pwd,opciones.carpeta,['data_gev',opciones.cityname,'_ML.mat']), ...
                    'OPHobj','OPHr','confidenceLevel','criterio','IT','parametros_all');
                %Plots
                LocScaleShapeState=info(2:4);
                confidenceLevelAlpha=1-confidenceLevel;
                amevaOPH.gevplots(OPHobj,OPHr,parametros_all,confidenceLevelAlpha,LocScaleShapeState,tpstring,opciones);
                %Save
                outpres=4;%precision de salida de los resultado 3
                caption = {'Automatic parameter selection.' opciones.cityname};
                matrix2latex(solucion(1:length(rowLabels),1:length(columnLabels)), ...
                    fullfile(pwd,opciones.carpeta,['LaTexSolutionGEV_',opciones.cityname,'.tex']), ...
                    'rowLabels', rowLabels,'columnLabels', columnLabels, ...
                    'alignment', 'c', 'format', '%-8.3f', 'size', 'scriptsize','tablelong', 'normal','caption',caption);
                SolTextLatex=[char([rowLabels,{'Code:MatLab','Stop criteria:'}]) char([num2str(solucion,outpres) {'',criterio}]) char([rowLabels,{'',''}]) ];
                save(fullfile(pwd,opciones.carpeta,['data_gev',opciones.cityname,'_ML.mat']),'SolTextLatex','-append');
            end
        end
        
        %% Funcion de auto ajuste usando c++
        function  [OPHr,OPHobj,parametros_all,IT,tpstring] = c_AutoAjuste(Hsmax,Time,kt,indices,info,confidenceLevel,opciones)
            
            %   Comprobamos las variables de inicio
            [kt,info,confidenceLevel,datemax,criterio,Km,Ny] = ...
                amevaFuncAutoAdjust.Initialize(Hsmax,Time,kt,indices,info,confidenceLevel,opciones);
            
            if isempty(opciones.luint), opciones.luint = 0.02; end %Solo C++ por ahora
            
            %    c++ optimization
            [exito,P,LNG,ALLP,loglikeobj,gradiente,XH,mut,psit,epst,lista,lista2]=c_ophgev(Hsmax,datemax,info,kt,indices,indices,opciones.luint,confidenceLevel);
            
            nmu=LNG(2)/2;
            ntend=LNG(3);
            nind=LNG(4);
            npsi=LNG(6)/2;
            ntend2=LNG(7);
            nind2=LNG(8);
            neps0=LNG(9);
            neps=LNG(10)/2;
            
            NT=sum(LNG);
            Hessiano=reshape(XH,NT,NT);
            
            nit=ALLP(1);
            npIT=ALLP(2*nit+2:3*nit+1);
            PList=ALLP(3*nit+2:3*nit+1+sum(npIT));
            LList=reshape(ALLP(3*nit+2+sum(npIT):3*nit+1+sum(npIT)+12*nit),12,nit);
            
            IT.beta0IT=zeros(nit,1);
            IT.betaIT=zeros(2*nmu,nit);
            IT.betaTIT=zeros(nit,1);
            IT.varphiIT=zeros(length(lista),nit);
            IT.alpha0IT=zeros(nit,1);
            IT.alphaIT=zeros(2*npsi,nit);
            IT.betaT2IT=zeros(nit,1);
            IT.varphi2IT=zeros(length(lista2),nit);
            IT.gamma0IT=zeros(nit,1);
            IT.gammaIT=zeros(2*neps,nit);
            
            IT.npIT = ALLP(2*nit+2:3*nit+1);
            IT.AIKIT = ALLP(nit+2:2*nit+1);
            IT.loglikIT=ALLP(2:nit+1);
            
            IT.OPHr=[]; %? %storage last solution
            
            %llist(1)=0;    for i=1:nit-1, llist(i+1)=sum(sum(LList(:,1:i))); end
            auxi=1;
            for i=1:nit,
                if LList(1,i)>0, IT.beta0IT(i)=PList(auxi); auxi=auxi+1; end;
                if LList(2,i)>0, for j=1:LList(2,i), IT.betaIT(j,i)=PList(auxi); auxi=auxi+1; end; end;
                if LList(3,i)>0, IT.betaTIT(i)=PList(auxi); auxi=auxi+1; end;
                if LList(4,i)>0, for j=1:LList(4,i), IT.varphiIT(j,i)=PList(auxi); auxi=auxi+1; end; end;
                if LList(5,i)>0, IT.alpha0IT(i)=PList(auxi); auxi=auxi+1; end;
                if LList(6,i)>0, for j=1:LList(6,i), IT.alphaIT(j,i)=PList(auxi); auxi=auxi+1; end; end;
                if LList(7,i)>0, IT.betaT2IT(i)=PList(auxi); auxi=auxi+1; end;
                if LList(8,i)>0, for j=1:LList(8,i), IT.varphi2IT(j,i)=PList(auxi); auxi=auxi+1; end; end;
                if LList(9,i)>0, IT.gamma0IT(i)=PList(auxi); auxi=auxi+1; end;
                if LList(10,i)>0, for j=1:LList(10,i), IT.gammaIT(j,i)=PList(auxi); auxi=auxi+1; end; end;
            end
            [nd,auxnind] = size(indices);
            IT.VarphiIT = zeros(auxnind,nit);
            IT.Varphi2IT = zeros(auxnind,nit);
            if ~isempty(lista),
                for i=1:nit
                    IT.VarphiIT(lista,i) = IT.varphiIT(:,i);
                end
            end
            if ~isempty(lista2),
                for i=1:nit
                    IT.Varphi2IT(lista2,i) = IT.varphi2IT(:,i);
                end
            end

            %%%%%%%%%%%%%%%%%%%%%%%%%% THE END Solution %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            OPHr = amevaOPH.oph_r(P,neps0,2*nmu,ntend,nind,2*npsi,ntend2,nind2,2*neps,0,0,lista,lista2,[],-loglikeobj,-gradiente,-Hessiano,[]);
            OPHr.invI0 = amevaOPH.invI0(OPHr.Hessiano,OPHr.np);
            
            %   Check if the corresponding model is NO GUMBEL (1-true) set global neps0
            amevaOPH.check_gumbel_model(OPHr.gamma0);
            
            OPHobj.x=Hsmax; OPHobj.t=datemax; OPHobj.kt=kt; OPHobj.indices=indices;
            %%%%%%%%%%%%%%%%%%%%%%%%%% THE END Solution %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            OPHobj=amevaOPH.OPHrToOPHobj(OPHobj,OPHr);% asigno la solucion a los datos
            OPHobj.Ny=Ny; OPHobj.Km=Km; %n-anhos y frec temporal

            %%%%%%%%%%%%%%%%%% POST PROSECOS PLOTS AND SOLUTION %%%%%%%%%%%%%%%%%%%%%%
            [OPHobj,parametros_all,tpstring] = ...
                amevaFuncAutoAdjust.PosProsPlot(OPHr,OPHobj,nit,neps0,IT,info,criterio,confidenceLevel,opciones);
            
            %%%%%%%%%%%%%%%%%%% other c++ funciones auxiliares inicio
            % OJO OJO comprobar y borrar
            function [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,...
                    nmu,npsi,neps,neps0,ntend,nind,ntend2,nind2]=plngTobag(P,LNG)
                
                if LNG(1)>0, beta0=P(1); else beta0=[]; end
                if LNG(2)>0, beta(1:LNG(2))=P(1+LNG(1):sum(LNG(1:2))); else beta=[]; end
                if LNG(3)>0, betaT=P(sum(LNG(1:3))); else betaT=[]; end
                if LNG(4)>0, varphi(1:LNG(4))=P(1+sum(LNG(1:3)):sum(LNG(1:4))); else varphi=[]; end
                
                if LNG(5)>0, alpha0=P(sum(LNG(1:5))); else alpha0=[]; end
                if LNG(6)>0, alpha(1:LNG(6))=P(1+sum(LNG(1:5)):sum(LNG(1:6))); else alpha=[]; end
                if LNG(7)>0, betaT2=P(sum(LNG(1:7))); else betaT2=[]; end
                if LNG(8)>0, varphi2(1:LNG(8))=P(1+sum(LNG(1:7)):sum(LNG(1:8))); else varphi2=[]; end
                
                if LNG(9)>0, gamma0=P(sum(LNG(1:9))); else gamma0=[]; end
                if LNG(10)>0, gamma(1:LNG(10))=P(1+sum(LNG(1:9)):sum(LNG(1:10))); else gamma=[]; end
                
                nmu=LNG(2)/2;
                ntend=LNG(3);
                nind=LNG(4);
                npsi=LNG(6)/2;
                ntend2=LNG(7);
                nind2=LNG(8);
                neps0=LNG(9);
                neps=LNG(10)/2;
            end %%%%%%%%%%%%%%%%%%% other c++ funciones auxiliares end
        end
        
    end
end