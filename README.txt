AMEVA v1.4.3 Toolbox README
			     July 15, 2015

================================================================================
The AMEVA v1.4.3 Toolbox packages ("Code") provided here are distributed by
AMEVA Toolbox Team, Marine Climate and Climate Change Group
Environmental Hydraulics Institute, UNIVERSIDAD DE CANTABRIA as aconvenience
to our customers.

================================================================================

                       Information on the Web

A public mailing list and bug reporting system is in place for the AMEVA Toolbox:
	 http://ihameva.ihacantabria.com
To send to the mailing list, send mail to ihameva@ihacantabria.com.

New software and new versions of existing software will become available on a
regular basis.  The new files can be accessed via the AMEVA web site at
http://ihameva.ihacantabria.com.

================================================================================

                               CONTENTS

Included in this README are:
  Toolbox Installation Instructions
  Installing Additional Software
  How to report problems
  Frequently Asked Questions

================================================================================

                    Toolbox Installation Instructions

NOTICE: These instructions pertain to AMEVA v1.4.3 Toolbox and later.

   Approximate installed AMEVA v1.4.3 Toolbox packages requirements on a system:
     aeva v1.2.9
     disfitanalysis v1.5.3
     gev v2.2.8
     pot v1.0.3
     calibration v1.3.5
     heteroscedasticmodel v0.5.5
     amevadataselection v0.5.6

   You should have at least 10MB free for install the toolbox.

1.   Create the installation directory:
     As user:
	Create a directory on your system to access the Toolbox software. 

================================================================================

                  Installing Additional Software

   SOM Toolbox 2.0.

================================================================================

                       How to report problems

This program is offered on an "as-is" basis, but you may report problems to the
public mailing list at: ihameva@ihacantabria.com.  We may respond as well.

================================================================================

                Frequently Asked Questions
             
A list of frequently asked questions is available on the web site:
http://ihameva.ihacantabria.com.

================================================================================
